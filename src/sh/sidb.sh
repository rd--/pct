#!/bin/sh
# sidb.sh - (c) rohan drape, 1996

# make directory
if test ! -d sidb
then
    echo "making directory sidb"
    mkdir sidb
else
    echo "removing all files below sidb"
    rm -f -R sidb/*
fi

# make database
for sc in $(echo $@ | tr " " "\n" | pct fn)
do
	echo "generating sidb/$sc"
	pct pg $sc | pct pcom pcseg iseg | pct pcom iseg iset | sort -u > sidb/$sc
done
