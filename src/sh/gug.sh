#!/bin/sh
# gug.sh - (c) rohan drape, 1997

length=2

# header
echo graph G {

# graph
while read a b
do
    echo "  \"$a\" -- \"$b\" [ len = $length ] ;"
done

# end of graph
echo '}'
