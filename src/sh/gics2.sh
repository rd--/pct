#!/bin/sh
# gisc2.sh - (c) rohan drape, 1998

# check arguments
if test $# != 3
then
    echo usage: $0 icset-cset multi-iset-cset sc-cset
    exit 2
fi

# put header
echo "digraph G {"
echo "rankdir=LR;"
echo "ranksep=4;"

# make graph
for i in $(pct sp 1 2 3 4 5 6 | pct cf  $1)
do
    for j in $(pct ici -c $i)
    do
	for k in $(pct se -c$2 $j | pct pup | pct pcom iseg sc | pct fn | egrep [$3]'-' | sort -u)
	do 
	    echo "    \"$i\" -> \"$k\";"
	done
    done
done

# end of graph
echo "}"
