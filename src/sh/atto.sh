#!/bin/sh
# atto.sh - (c) rohan drape, 1997

# check arguments
if test $# != 1
then
    echo "usage: "$0" File" 
    exit 1
fi

# echo "writing Tn transformations"
for trs in 0 1 2 3 4 5 6 7 8 9 t e
do
   pct asro T$trs < $1
done

# echo "writing TnI transformations"
for trs in 0 1 2 3 4 5 6 7 8 9 t e
do
    pct asro "T"$trs"I" < $1
done

# echo "writing TnM transformations"
for trs in 0 1 2 3 4 5 6 7 8 9 t e
do
    pct asro "T"$trs"M" < $1
done

# echo "writing TnMI transformations"
for trs in 0 1 2 3 4 5 6 7 8 9 t e
do
    pct asro "T"$trs"MI" < $1
done
