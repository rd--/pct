# gen-pct - (c) rohan drape, 2000-2001

printf "#!/bin/sh\n" > pct
printf "# src/sh/pct - (c) rohan drape, 2000-2001\n" >> pct

printf "if test \$# = 0 ;\n" >> pct
printf "then\n" >> pct
printf "echo \"usage: pct cmd  [ arg... ]\";\n" >> pct
printf "ls %s;\n" $1 >> pct
printf "exit 2;\n" >> pct
printf "fi;\n" >> pct

printf "cmd=\$1\n" >> pct
printf "shift\n" >> pct

printf "exec %s/\$cmd \"\$@\"\n" $1 >> pct

