#!/bin/sh
# gorl.sh - (c) rohan drape, 1997

# check arguments
if test $# != 0
then
    echo "usage: $0"
    exit 1
fi

# put header
echo "digraph G {"

# make graph
IFS=:
while read p r q
do
    if test $r = "in"
    then 
	echo "	"$p" -> "$q";"
    elif test $r = "has"
    then 
	echo "	"$q" -> "$p";"
    elif test $r = "is"
    then 
	echo "	"$p" -> "$q"[dir=none];"
    else 
	echo "	"$p" -> "$q"[label=\""$r"\"];"
    fi
done

# end of graph
echo '}'
