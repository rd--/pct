#!/bin/sh
# gdg.sh - (c) rohan drape, 1998


length=2
width=12
height=12


# header
echo digraph G {
echo
echo "    centre = true ;"
echo "    ratio = auto ;"
echo "    label = \"\" ;"
echo


# put attributes
echo "    edge [ len = $length ] ;"
echo


# graph
while read a b
do
echo "    \"$a\" -> \"$b\" ;"
done


# end of graph
echo '}'
