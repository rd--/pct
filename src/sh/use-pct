
# say "evaluates to [ the ] FUNC_NAME ARG1"

function basic-interval-pattern-of-segment 
{
    pcom pcseg iseg $1 | pcom iseg icseg | nrm -r ;
}

function combinations-drawn-from-set 
{
    cg $1 ;
}

function complement-of-set 
{
    cmpl $1 ;
}

function complement-of-set-FILTER 
{
    cmpl ;
}

function cyclic-form-of-segment 
{
    cyc $1 ;
}

function cyclic-form-of-segment-FILTER 
{
    cyc ;
}

function cyclic-interval-succession-of-segment 
{
    cyc $1 | pcom pcseg iseg ;
}

function diatonic-implications-of-set 
{
    dim $1 ;
}

function five-part-partitions-of-set 
{
    prt $1 5 0123456789te ;
}

function four-part-partitions-of-set 
{
    prt $1 4 0123456789te ;
}

function fragmentation-of-cycles-of-set 
{
    frg $1 ;
}

function half-matrix-of-segment 
{
    tmatrix -h $1 ;
}

function information-about-row 
{
    ri $1 ;
}

function information-about-segment 
{
    sgi $1 ;
}

function information-about-set 
{
    si $1 ;
}

function interval-class-succession-of-segment 
{
    pcom pcseg iseg $1 | pcom iseg icseg ;
}

function interval-class-vector-of-set-class 
{
    pcom pcset icv $1 ;
}

function interval-set-defined-by-diatonic-interval-set 
{
    dis $1 ;
}

function interval-sets-defined-by-interval-class-set 
{
    ici $1 ;
}

function interval-sets-defined-by-set-SIMPLE 
{
    pg $1 | sort -u | pcom pcseg iseg | pcom iseg iset | sort -u ;
}

function interval-succession-of-segment 
{
    pcom pcseg iseg $1 ;
}

function name-of-set 
{
    fn $1 ;
}

function name-of-set-EXTENDED-NOTATION 
{
    fn -v $1 ;
}

function name-of-set-FILTER 
{
    fn ;
}

function name-of-set-INCLUDING-TRANSFORMATION 
{
    fn -t $1 ;
}

function normalization-of-multiset 
{
    nrm $1 ;
}

function normalization-of-multiset-RETAIN-DUPLICATES 
{
    nrm -r $1 ;
}

function one-element-imbrications-of-segment 
{
    imb -c2 $1 ;
}

function permutations-of-segment 
{
    pg $1 ;
}

function permutations-of-segment-UNIQUE 
{
    pg $1 | sort -u ;
}

function properties-of-segment 
{
    sgdb $1 ;
}

function properties-of-segment-OR-SUBSEGMENT 
{
    sgdb -s $1 ;
}

function properties-of-set-class 
{
    scdb $1 ;
}

function rotational-array-defined-by-segment 
{
    echo $1 | sra ;
}

function set-class-of-interval-succession 
{
    pcom iseg sc $1 | fn ;
}

function set-classes-defined-by-interval-set-COMPLEX 
{
    issl $1 ;
}

function set-classes-defined-by-interval-set-COMPLEX-RESTRICT-TO-SET-CLASS 
{
    issl -s$2 $1 ;
}

function set-classes-defined-by-interval-set-SIMPLE 
{
    pg $1 | sort -u | pcom iseg sc | sort -u | fn ;
}

function set-classes-defined-by-interval-set-SIMPLE-WITH-EXPANSION 
{
    se -c$2 $1 | pg | sort -u | pcom iseg sc | sort -u | fn ;
}

function set-classes-of-size 
{
    fl $1 ;
}

function set-classes-with-interval-class-vector 
{
    icv -r $1 ;
}

function t-matrix-of-segment 
{
    tmatrix $1 ;
}

function three-element-imbrications-of-segment 
{
    imb -c3 $1 ;
}

function three-part-partitions-of-set 
{
    prt $1 3 0123456789te ;
}

function tics-vector-of-set-class 
{
    tics $1 ;
}

function two-element-imbrications-of-segment 
{
    imb -c2 $1 ;
}

function two-part-partitions-of-set 
{
    prt $1 2 0123456789te ;
}


# say "evaluates to [ the ] FUNC_NAME ARG1 and ARG2 [ and ... ]"

function largest-set-classes-included-in 
{
    echo "sorry, un-implemented function" ;
}

function relation-between-segments 
{
    rsg $1 $2 ;
}

function relation-between-sets 
{
    rs $1 $2 ;
}

function set-classes-included-in 
{
    sb $@ | fn ;
}

function set-classes-that-include 
{
    spsc -a $@ ;
}

function smallest-set-classes-that-include 
{
    spsc $@ ;
}

function union-of-sets 
{
    mg $@ ;
}


# predicates

function is-interval-succession-cyclic 
{
    echo $1 | icf ;
}

function is-interval-succession-cyclic-FILTER 
{
    icf ;
}

function is-object-of-size 
{
    echo $1 | cf $2 ;
}

function is-object-of-size-FILTER 
{
    cf $1 ;
}


# long names

function cardinality-frequency-analysis 
{
    cfa $@ ;
}

function chain-segments 
{
    chn $@ | sort -u ;
}

function combine-set-of-isets 
{
    mbs $@ ;
}

function degree-of-intersection 
{
    doi $@ ;
}

function evaluate-pitch-materials 
{
    epmq $@ ;
}

function file-comparison 
{
    fc $@ ;
}

function imbrications-of-segment 
{
    imb -c234 $@ | pfmt ;
}

function note-names-of-pitch-classes 
{
    echo $@ | mw C C# D D# E F F# G G# A A# B ;
}

function pitch-class-distribution-analysis 
{
    po $@ ;
}

function proximate-voice-leading-between-set-classes 
{
    pvl $@ | pfmt 4 ;
}

function relations-between-objects-in-file 
{
    orl $1 ;
}

function relations-between-objects-in-file-SUPRESS-MULTIPLICATION 
{
    orl -m $1 ;
}

function search-transformation-list-for-embedded-segment 
{
    echo $2 | ess $1 ;
}

function search-transformation-list-for-segment 
{
    echo $2 | trs $1 ;
}

function search-transformation-list-for-segment-EXCLUDE-MULTIPLICATION 
{
    echo $2 | trs -m $1 ;
}

function search-transformation-list-for-set 
{
    sis $1 $2 ;
}

function segment-invariance 
{
    pi $1 $2 ;
}

function subsets-and-intersections-of-set-class 
{
    lsi $@ ;
}

function union-and-intersection-of-sets 
{
    uis $@ ;
}

function generate-undirected-graph 
{
    cat $1 | gug ;
}

function generate-undirected-graph-FILTER 
{
    gug ;
}


# operation -> object functions ????

function translatation-of-object-TYPE-TYPE-OBJ 
{
    pcom $1 $2 $3 ;
}

function partition-set-SET-N-CSET 
{
    prt $1 $2 $3 ;
}

function combinations-drawn-from-set-N-SET 
{
    cg -r$1 $2 ;
}

function apply-transformation-to-segment-SRO-OBJ 
{
    echo $2 | sro $1 ;
}

function apply-transformation-to-segment-FILTER-SRO 
{
    sro $1 ;
}

function reordering-of-segment 
{
    echo $2 | sgr $1 ;
}

function arbitrary-pitch-class-map-FILTER 
{
    mpc $1 ;
}


# file filters ????

function view-textual-concordance-for 
{
    ccdg $@ | sort -f | less ;
}

function view-duplicate-lines 
{
    pdl $@ ;
}

function remove-duplicate-lines 
{
    rdl $@ ;
}

function sort-lines-internally 
{
    lsrt ;
}

function sort-by-forte-name 
{
    fsrt $@ ;
}

function format-object-stream 
{
    pfmt $@ ;
}


# browsers: FUNC_NAME ARG

function view-set-classes-of-size 
{
    fl -v $1 | less ;
}

function view-pct-manual-for-program 
{
    m $1 ;
}

function view-information-about-row 
{
    ri $1 | less ;
}


# 

function sets-that-complete-set-class-A-given-set-B 
{
    scc $1 $2 ;
}

function sets-in-set-class-A-that-have-degree-of-intersection-B-with-set-C 
{
    echo $3 | doi $2 $1 | sort -u ;
}

function transformations-of-segment-A-invariant-at-locations-B 
{
    pi $1 $2 ;
}
