#!/bin/sh
# gisdb.sh - (c) rohan drape, 1997

# check argument
if test $# != 1 && test ! -d $1
then
    echo 'usage: sh '$0' DIRECTORY' ; exit 
fi

# move to directory
cd $1 


# make all edges (from isets to scs)
for i in $1/*
do
	grep --with-filename "" $(basename $i) | sed "s/:/ /g"
done
