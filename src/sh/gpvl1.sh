#!/bin/sh
# gpvl1.sh - (c) rohan drape, 1999

# neato doesn't handle self edges, so dont make them now and watch for fixes from att

if test $# != 3
then
    echo "usage: gpvl constraint-iset in-sc-cset out-sc-cset"
    exit 1
fi

constraint_iset=$1
in_sc_cset=$2
out_sc_cset=$3

echo "graph G {"
echo "edge[dir=none,len=2]"

for a in $(pct fl $in_sc_cset)
do
    for b in $(pct fl $in_sc_cset)
    do
	# to suppress self edges, otherwise it should be after the command sequence
	if test $a = $b
	then 
	    break
	fi
 	pct pvl $b $a -c$constraint_iset -s$out_sc_cset | pct pfmt 4 | \
 	awk '{print $1} {print $2} {print $3}' | pct fn | pct pfmt 3 | sort -u | \
 	awk '{print "\"" $1 "\"  -- \"" $2 "\" [label=\"" $3 "\"] ;"}'
    done
done

echo "}"
