#!/bin/sh
# gpvl2.sh - (c) rohan drape, 1999

# neato doesn't handle self edges, so use dot, and watch for fixes from att

if test $# != 2
then
    echo "usage: gpvl constraint-iset sc-set"
    exit 1
fi

echo "digraph G {"
echo "edge[dir=none]"
echo

for a in $2
do
    for b in $2
    do
 	pct pvl $b $a -c$1 | pct pfmt 4 | \
 	awk '{print $1 "\n" $2 "\n" $3}' | pct fn | pct pfmt 3 | sort -u | \
 	awk '{print "\"" $1 "\"  -> \"" $2 "\" [label=\"" $3 "\"] ;"}'
	if test $a = $b
	then 
	    break
	fi
    done
done

echo "}"
