#!/bin/sh
# tto.sh - (c) rohan drape, 1997

sort=1

# make sure we have exactly one argument
if test $# != 1
then
    echo usage: $0 pcset
    exit 
fi


# make Tn transformations
for trs in 0 1 2 3 4 5 6 7 8 9 t e
do
    if test $sort = 0
    then echo $1 | pct sro T$trs
    else echo $1 | pct sro T$trs | pct pcom pcseg pcset
    fi
done

	
# make TnI transformations
for trs in 0 1 2 3 4 5 6 7 8 9 t e
do
    if test $sort = 0
    then echo $1 | pct sro T$trs"I"
    else echo $1 | pct sro T$trs"I" | pct pcom pcseg pcset
    fi
done
	
