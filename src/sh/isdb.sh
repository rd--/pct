#!/bin/sh
# isdb.sh - (c) rohan drape, 1997

case $# in
1)
    echo "usage: sh "$0" n e" ;;
2)
    echo $0: making the directory \"isdb/$1/$2\"
    mkdir -p isdb/$1/$2
	
    for iset in $(pct sp 1 2 3 4 5 6 7 8 9 A B | pct cf $1)
    do
	echo $(basename $0): generating the file \"isdb/$1/$2/$iset\"
	pct se -c$[$2-1] $iset | pct pup | pct isegsc -l | pct fn | sort -u > isdb/$1/$2/$iset
    done

esac



