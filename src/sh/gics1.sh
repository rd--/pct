#!/bin/sh
# gisc1.sh - (c) rohan drape, 1998

# check arguments
if test $# != 3
then
    echo "usage: $0 icset iset-cset sc-cset"
    exit 2
fi

# put header
echo "digraph G {"
echo "rankdir=LR;"
echo "ranksep=4;"

# make graph
for i in $(pct ici -c $1)
do
    for j in $(pct se -c$2 $i | pct pup | pct pcom iseg sc | pct fn | egrep [$3]'-' | sort -u)
    do 
	echo "	\"$i\" -> \"$j\";"
    done
done

# end of graph file
echo "}"
