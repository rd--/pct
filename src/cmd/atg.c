/* atg.c - rohan drape, 5/96 */

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>

#include "libpct.h"

void transpositions(Ppca_t *p);
void inversions_transpositions(Ppca_t *p);
void retrogrades_inversions_transpositions(Ppca_t *p);
void retrogrades_transpositions(Ppca_t *p);

static void usage(char *pname)
{
	fprintf(stderr, "usage: %s [ -oN ] [ File ]\n", pname);
	exit(EXIT_FAILURE);
}

int main(int argc, char **argv)
{
	int i, transformation_type = 0;
	Ppca_t *e;

	for (i = 1; i < argc; i++) {
		if (argv[i][0] == '-') {
			switch (argv[i][1]) {
			case 'o':
				transformation_type = c2i(argv[i][2]);
				break;
			default:
				usage(argv[0]);
				break;
			}
		} else {
			if (!freopen(argv[i], "r", stdin)) {
				usage(argv[0]);
			}
		}
	}

	e = paopen("atg");
	assert(e);
	while (paread(e, stdin)) {
		switch (transformation_type) {
		case 0:
			transpositions(e);
			break;
		case 1:
			inversions_transpositions(e);
			break;
		case 2:
			retrogrades_inversions_transpositions(e);
			break;
		case 3:
			retrogrades_transpositions(e);
			break;
		default:
			die("bad -o value");
			break;
		}
	}
	paclose(e);

	exit(EXIT_SUCCESS);
	return EXIT_SUCCESS;
}

void transpositions(Ppca_t *p)
{
	int *tl = calloc(12, sizeof(int));
	int i;

	assert(p->r > 1);
	while (tl[0] == 0) {
		pawrite(p, stdout);
		tl[p->r - 1]++;
		trspcs(1, p->data[p->r - 1], p->size[p->r - 1]);
		for (i = p->r - 1; i > 0; i--) {
			if (tl[i] == 12) {
				tl[i] = 0;
				tl[i - 1]++;
				trspcs(1, p->data[i - 1], p->size[i - 1]);
			}
		}
	}
	trspcs(-1, p->data[0], p->size[0]);
}

void inversions_transpositions(Ppca_t *p)
{
	int *tl = calloc(12, sizeof(int));
	int i;

	while (tl[0] < 2) {
		transpositions(p);
		tl[p->r - 1]++;
		invpcs(0, p->data[p->r - 1], p->size[p->r - 1]);
		for (i = p->r - 1; i > 0; i--) {
			if (tl[i] == 2) {
				tl[i] = 0;
				tl[i - 1]++;
				invpcs(0, p->data[i - 1], p->size[i - 1]);
			}
		}
	}
}

void retrogrades_inversions_transpositions(Ppca_t *p)
{
	int *tl = calloc(12, sizeof(int));
	int i;

	while (tl[0] < 2) {
		inversions_transpositions(p);
		tl[p->r - 1]++;
		rtrpcs(p->data[p->r - 1], p->size[p->r - 1]);
		for (i = p->r - 1; i > 0; i--) {
			if (tl[i] == 2) {
				tl[i] = 0;
				tl[i - 1]++;
				rtrpcs(p->data[i - 1], p->size[i - 1]);
			}
		}
	}
}

void retrogrades_transpositions(Ppca_t *p)
{
	int *tl = calloc(12, sizeof(int));
	int i;

	while (tl[0] < 2) {
		transpositions(p);
		tl[p->r - 1]++;
		rtrpcs(p->data[p->r - 1], p->size[p->r - 1]);
		for (i = p->r - 1; i > 0; i--) {
			if (tl[i] == 2) {
				tl[i] = 0;
				tl[i - 1]++;
				rtrpcs(p->data[i - 1], p->size[i - 1]);
			}
		}
	}
}
