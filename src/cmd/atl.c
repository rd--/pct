/* atl.c - rohan drape,  9/97 */

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "libpct.h"

int write_latex_table(Ppca_t *e, FILE *fp);
int write_latex_table(Ppca_t *e, FILE *fp)
{
	char str[MAX_STR];
	int maxprt;
	int index;
	int i, j, k;

	fprintf(fp, "\n\\begin{tabular}{|");
	for (i = 0; i < e->c; i++)
		fprintf(fp, "l|");
	fprintf(fp, "} \\hline\n");
	e->column_sep[0] = '&';

	for (i = 0; i < e->r; i++) { /* i is the current row */
		for (j = 0; j < e->c; j++) { /* j is the current partition */
			/* get position */
			for (index = 0, k = 0; k < j; k++) {
				index += e->partition[i][k];
			}
			pcs2str(&(e->data[i][index]), e->partition[i][j], str, FALSE);
			/* do neat printout */
			for (maxprt = 0, k = 0; k < e->r; k++) {
				if (e->partition[k][j] > maxprt) {
					maxprt = e->partition[k][j];
				}
			}
			if (maxprt == 0)
				maxprt++;
			fprintf(fp, "%s", str);
			nextcol(maxprt - strlen(str));
			j + 1 < e->c ? fprintf(fp, " %s", e->column_sep) : fprintf(fp, " \\\\ \\hline\n");
		}
	}

	e->column_sep[0] = ':';
	fprintf(fp, "\\end{tabular}\n\n");

	return TRUE;
}

int main(int argc, char **argv)
{
	Ppca_t *e;

	if (argc >= 2 && !freopen(argv[1], "r", stdin)) {
		fprintf(stderr, "%s [ File ]\n", argv[0]);
		exit(EXIT_FAILURE);
	}

	e = paopen("atl");
	assert(e);
	while (paread(e, stdin)) {
		write_latex_table(e, stdout);
	}
	paclose(e);

	exit(EXIT_SUCCESS);
	return EXIT_SUCCESS;
}
