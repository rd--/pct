/* lsrt.c - rohan drape, 1996 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "libpct.h"

static void usage(char *pname)
{
	fprintf(stderr, "usage: %s\n", pname);
	exit(EXIT_FAILURE);
}

static int compare(const void *n1, const void *n2)
{
	return strcmp(n1, n2);
}

#define MAX_LINE 8192 /* line size limit */
#define MAX_NTOK 1024 /* number of tokens limit */
#define MAX_STOK 32 /* token size limit */
int main(int argc, char **argv)
{
	char line[MAX_LINE], tok[MAX_NTOK][MAX_STOK], *lp;
	int i, ntok, nc;

	if (argc != 1)
		usage(argv[0]);

	while (fgets(line, MAX_LINE, stdin) != NULL) {
		lp = line;
		ntok = 0;
		while (sscanf(lp, "%s%n", tok[ntok], &nc) == 1) {
			if (nc >= MAX_STOK)
				die("lsrt: token exceeds token size limit");
			while (nc--)
				lp++;
			ntok++;
			if (ntok >= MAX_NTOK)
				die("lsrt: too many tokens in line");
		}
		qsort(tok, ntok, MAX_STOK * sizeof(char), compare);
		for (i = 0; i < ntok; i++)
			printf("%s ", tok[i]);
		printf("\n");
	}
	return EXIT_SUCCESS;
}
