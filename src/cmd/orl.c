/* orl.c - rohan drape, 9/97 */

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "libpct.h"

#define ORL_OBJ_MAX 24

static void usage(char *pname)
{
	fprintf(stderr, "%s [ -v ] [ -cN ] [ File ]\n", pname);
	exit(EXIT_FAILURE);
}

static void print_orl(Pco_t *p, char *s, Pco_t *q, char c, int v)
{
	char str[3][32];

	if (v) {
		pco_write(p, str[0]);
		pco_write(q, str[1]);
		printf("\"%s\"%c%s%c\"%s\"\n", str[0], c, s, c, str[1]);
	} else {
		pco_write_pretty(p, str[0]);
		pco_write_pretty(q, str[1]);
		printf("\"%s\"%c%s%c\"%s\"\n", str[0], c, s, c, str[1]);
	}
}

int main(int argc, char **argv)
{
	char str[MAX_STR];
	int onum = 0, i, j, verbose = FALSE, field_sep = ' ';
	Pco_t *o[ORL_OBJ_MAX];
	Sro_t s;

	for (i = 1; i < argc; i++) {
		if (argv[i][0] == '-') {
			switch (argv[i][1]) {
			case 'm':
				M_in_tto = FALSE;
				M_in_sro = FALSE;
				break;
			case 'v':
				verbose = TRUE;
				break;
			case 'c':
				field_sep = argv[i][2];
				break;
			default:
				usage(argv[0]);
				break;
			}
		} else {
			if (!freopen(argv[i], "r", stdin)) {
				usage(argv[0]);
			}
		}
	}

	while (fgets(str, MAX_STR, stdin)) {
		o[onum] = pco_create("epmty");
		assert(o[onum]);
		i = pco_read(o[onum], str);
		onum++;
		assert(onum < ORL_OBJ_MAX);
	}

	for (i = 0; i < onum; i++) {
		for (j = 0; j < onum; j++) {
			if (i > j && pco_typeof(o[i]) == pco_typeof(o[j]) && (pco_typeof(o[i]) == pcset || pco_typeof(o[i]) == pcseg)) {
				/* pcseg -> sro -> pcseg */
				/* pcset -> tto -> pcset */
				if (o[i]->size == o[j]->size) {
					if (pco_sro_derive(o[i], o[j], &s)) {
						sro_write(&s, str);
						print_orl(o[i], str, o[j], field_sep, verbose);
					}
				}
				/* pcseg -> is|in -> pcseg */
				/* pcset -> is|in -> pcset */
				else {
					if (pco_cmp(o[i], o[j]) == PCO_EQUAL)
						print_orl(o[i], "is", o[j], field_sep, verbose);
					else if (pco_cmp(o[i], o[j]) == PCO_SUBSET)
						print_orl(o[i], "in", o[j], field_sep, verbose);
					else if (pco_cmp(o[i], o[j]) == PCO_SUPERSET)
						print_orl(o[j], "in", o[i], field_sep, verbose);
				}
			} else if (i != j && (pco_typeof(o[j]) == pcset || pco_typeof(o[j]) == pcseg || pco_typeof(o[j]) == sc)
				&& pco_typeof(o[i]) == sc) {
				/* sc -> is|has -> pcset|pcseg|sc */
				if (pco_cmp(o[i], o[j]) == PCO_EQUAL)
					print_orl(o[j], "is", o[i], field_sep, verbose);
				else if (pco_cmp(o[i], o[j]) == PCO_SUPERSET)
					print_orl(o[j], "in", o[i], field_sep, verbose);
			}
		}
	}

	for (i = 0; i < onum; i++) {
		pco_free(o[i]);
	}

	return EXIT_SUCCESS;
}
