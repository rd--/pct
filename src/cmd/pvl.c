/* pvl.c - rohan drape, 1998 */

#include <stdio.h>
#include <stdlib.h>

#include "libpct.h"

static void usage(void)
{
	fprintf(stderr, "usage: pvl [ -e ] [ -cicset ] [ -scset ] sc sc \n");
	fprintf(stderr, "\t -e run exists search \n");
	fprintf(stderr, "\t -c set constraint set \n");
	fprintf(stderr, "\t -s set union size set \n");
	exit(1);
}

/* put the interval-class voice-leading from a to b into c */
static void pvl(Pco_t *a, Pco_t *b, Pco_t *c)
{
	int i;
	for (i = 0; i < a->size; i++) {
		c->data[i] = i2ic(b->data[i] - a->data[i]);
	}
	c->size = a->size;
}

/* predicate: true if all elements of a are in b */
static int constraint(Pco_t *a, Pco_t *b)
{
	int i;
	for (i = 0; i < a->size; i++) {
		if (!pco_has_element(b, a->data[i]))
			return 0;
	}
	return 1;
}

int main(int argc, char **argv)
{
	Pco_t *p = 0, *q = 0, *r, *s, *t, *u;
	Pg_t *pg_q;
	tr_t *tr_p, *tr_q;
	int i, exists_search_only = 0;

	r = pco_create("iseg");
	s = pco_create("icset");
	t = pco_create("sc");
	u = pco_create("cset");
	pco_read(s, "0123456");
	pco_read(u, "0123456789te");

	for (i = 1; i < argc; i++) {
		if (argv[i][0] == '-') {
			switch (argv[i][1]) {
			case 'c':
				pco_read(s, &argv[i][2]);
				break;
			case 'e':
				exists_search_only = 1;
				break;
			case 's':
				pco_read(u, &argv[i][2]);
				break;
			default:
				usage();
				break;
			}
		} else {
			if (!p) {
				p = pco_create("pcset");
				pco_read(p, argv[i]);
			} else if (!q) {
				q = pco_create("pcset");
				pco_read(q, argv[i]);
			} else
				usage();
		}
	}

	if (!q)
		usage();
	if (p->size != q->size)
		die("pvl : sets must be the same length");

	tr_p = tr_init(p->data, p->size, 0, 0, 0);
	tr_q = tr_init(q->data, q->size, 0, 0, 0);

	while (tr_next(tr_p, p->data, &(p->size))) {
		while (tr_next(tr_q, q->data, &(q->size))) {
			pg_q = pgopen(q->data, q->size);
			while (pgnext(pg_q, q->data)) {
				pvl(p, q, r);
				if (constraint(r, s)) {
					pco_union(t, p, q);
					pco_convert(t, sc);
					if (pco_has_element(u, pco_sizeof(t))) {
						if (exists_search_only)
							return 0;
						pco_put_pretty(p, stdout);
						pco_put_pretty(q, stdout);
						pco_put_pretty(t, stdout);
						pco_put_pretty(r, stdout);
					}
				}
			}
			pgclose(pg_q);
		}
	}

	tr_free(tr_p);
	tr_free(tr_q);
	pco_free(p);
	pco_free(q);
	pco_free(r);
	pco_free(s);
	pco_free(t);

	if (exists_search_only)
		return 1;
	else
		return 0;
}
