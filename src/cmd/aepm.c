/* aepm.c - rohan drape, 8/97 */

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "libpct.h"

#define MAX_DIMENSIONS 64

static void usage(char *pname)
{
	fprintf(stderr, "usage: %s [ -a ] [ -fFile ] [ Condition ] ... \n",
		pname);
	exit(EXIT_FAILURE);
}

static int aepm_parse(char *str, Epm_t **erows, Epm_t **ecolumns)
{
	char c;
	int i, err, n, index;

	if (str[0] != '#') {
		err = sscanf(str, " %c%d%n", &c, &index, &n);
		assert(err == 2);
		assert(index < MAX_DIMENSIONS);
		if (c == 'r') {
			if (index < 0) {
				for (i = 0; i < MAX_DIMENSIONS; i++) {
					err = epmparse(erows[i], &str[n]);
					assert(err);
				}
			} else {
				err = epmparse(erows[index], &str[n]);
				assert(err);
			}
		} else if (c == 'c') {
			if (index < 0) {
				for (i = 0; i < MAX_DIMENSIONS; i++) {
					err = epmparse(ecolumns[i], &str[n]);
					assert(err);
				}
			} else {
				err = epmparse(ecolumns[index], &str[n]);
				assert(err);
			}
		} else {
			die("aepmparse failed: invalid character (not 'r' or 'c')");
		}
	}

	return TRUE;
}

static int aepm_assert(Ppca_t *e, Epm_t **erows, Epm_t **ecolumns)
{
	int i;
	Pco_t *q = pco_create("pcseg");
	assert(e->r < MAX_DIMENSIONS);
	assert(e->c < MAX_DIMENSIONS);
	for (i = 0; i < e->r; i++) {
		q->size = e->size[i];
		memcpy(q->data, e->data[i], q->size * sizeof(int));
		if (epmeval(erows[i], q) != -1) {
			return FALSE;
		}
	}
	for (i = 0; i < e->c; i++) {
		pacolumn(e, i, q, PPCA_READ);
		if (epmeval(ecolumns[i], q) != -1) {
			return FALSE;
		}
	}
	pco_free(q);
	return TRUE;
}

int main(int argc, char **argv)
{
	char str[MAX_STR];
	int i, err;
	FILE *fp;
	Epm_t *erows[MAX_DIMENSIONS], *ecolumns[MAX_DIMENSIONS];
	Ppca_t *e;

	e = paopen("aepm");
	assert(e);
	for (i = 0; i < MAX_DIMENSIONS; i++) {
		erows[i] = epmopen("aepm", "not-proper");
		assert(erows[i]);
		ecolumns[i] = epmopen("aepm", "not-proper");
		assert(ecolumns[i]);
	}

	for (i = 1; i < argc; i++) {
		if (argv[i][0] == '-') {
			switch (argv[i][1]) {
			case 'a':
				pco_allow_ambigous_mappings = 1;
				break;
			case 'f':
				fp = fopen(&argv[i][2], "r");
				assert(fp != NULL);
				while (fgets(str, MAX_STR, fp)) {
					err = aepm_parse(str, erows, ecolumns);
					assert(err);
				}
				fclose(fp);
				break;
			default:
				usage(argv[0]);
				break;
			}
		} else {
			if (!aepm_parse(argv[i], erows, ecolumns)) {
				usage(argv[0]);
			}
		}
	}

	while (paread(e, stdin)) {
		if (aepm_assert(e, erows, ecolumns)) {
			pawrite(e, stdout);
		}
	}

	paclose(e);
	for (i = 0; i < MAX_DIMENSIONS; i++) {
		epmclose(erows[i]);
		epmclose(ecolumns[i]);
	}
	exit(EXIT_SUCCESS);
	return EXIT_SUCCESS;
}
