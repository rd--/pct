/* doi.c -  (c) rohan drape, 1997 */

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>

#include "libpct.h"

static void usage(char *pname)
{
	fprintf(stderr, "usage: %s cset [ sc ]\n", pname);
	exit(EXIT_FAILURE);
}

int main(int argc, char **argv)
{
	Pco_t p, q, d;
	tr_t *t;
	int s = 0, D, n;

	pco_set_typeof(&d, cset);
	pco_set_typeof(&q, pcset);
	pco_set_typeof(&p, pcset);

	if (argc < 2)
		usage(argv[0]);
	else
		pco_read(&d, argv[1]);

	if (argc == 3) {
		pco_read(&q, argv[2]);
		s = 1;
	}

	while (pco_get(&p, stdin)) {
		if (!s)
			pco_copy(&q, &p);
		t = tr_init(pco_data(&q), pco_sizeof(&q), 0, 0, 0);
		while (tr_next(t, pco_data(&q), &n)) {
			pco_set_sizeof(&q, n);
			D = pco_degree_of_intersection(&p, &q);
			if (pco_has_element(&d, D))
				pco_put(&q, stdout);
		}
		tr_free(t);
	}

	return EXIT_SUCCESS;
}
