/* partition.c - rohan drape, 11/96 */

#include <stdio.h>
#include <stdlib.h>

#include "libpct.h"

static int print_line(int *p, int plen, int *q, int qlen, char *str)
{
	char pcsstr[MAX_STR];

	srtpcs(p, plen);
	srtpcs(q, qlen);
	pcs2str(p, plen, pcsstr, FALSE);
	printf("%s %s ", str, pcsstr);
	pcs2str(q, qlen, pcsstr, FALSE);
	printf("%s\n", pcsstr);
	return TRUE;
}

/*
 * writes the literal n-part partitions of the pcset p.
 * prepends the string pstr to each line of output.
 * calls itself recursively.
 */
static int
literal_partitions(int *p, int plen, int n, int *c, int clen, char *pstr)
{
	char line[256], str[256], str1[256];
	int q[12], qlen, comp[12];
	FILE *univ_tbl = fopen(univ_lookup, "r");

	if (univ_tbl == NULL) {
		die("literal_partitions() : unable to open univ table");
		return FALSE;
	}

	while (fgets(line, 256, univ_tbl)) {
		if (!str2pcs(line, q, &qlen))
			die("error reading univ file");
		if (qlen >= plen)
			break;
		if (!isel(qlen, c, clen))
			continue;
		if (islitsub(q, qlen, p, plen)) {
			if (n == 2) {
				pcs2cmpn(q, qlen, p, plen, comp);
				print_line(q, qlen, comp, plen - qlen, pstr);
			} else {
				srtpcs(q, qlen);
				pcs2str(q, qlen, str1, FALSE);
				sprintf(str, "%s %.*s", pstr, qlen, str1);
				pcs2cmpn(q, qlen, p, plen, comp);
				literal_partitions(comp, plen - qlen, n - 1, c, clen, str);
			}
		}
	}
	fclose(univ_tbl);
	return TRUE;
}

int main(int argc, char **argv)
{
	int p[MAX_PCS], plen;
	int c[MAX_PCS], clen;
	int n;

	if (argc != 4 || (n = atoi(argv[2])) <= 1 || !str2pcs(argv[3], c, &clen) || !str2pcs(argv[1], p, &plen)) {
		fprintf(stderr, "usage: %s pcset n cset\n", argv[0]);
		exit(EXIT_FAILURE);
	}

	remdup(p, &plen);
	literal_partitions(p, plen, n, c, clen, "");

	return EXIT_SUCCESS;
}
