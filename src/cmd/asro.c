/* asro.c - rohan drape,  9/97 */

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>

#include "libpct.h"

void usage(char *pname);
void usage(char *pname)
{
	fprintf(stderr, "%s sro \n", pname);
	exit(EXIT_FAILURE);
}

int main(int argc, char **argv)
{
	int r;
	Sro_t o;
	Ppca_t *e;

	if (argc != 2 || !sro_read(&o, argv[1])) {
		usage(argv[0]);
	}

	e = paopen("asro");
	assert(e);
	while (paread(e, stdin)) {
		r = pasro(e, &o);
		assert(r);
		r = pawrite(e, stdout);
		assert(r);
	}
	paclose(e);

	exit(EXIT_SUCCESS);
	return EXIT_SUCCESS;
}
