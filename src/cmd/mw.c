/* mw.c - rohan drape, 3/97 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "libpct.h"

#define MAX_WORDS 12

int main(int argc, char **argv)
{
	char word[MAX_WORDS][256];
	int i, c;
	int numwords;

	numwords = argc - 1;
	if (numwords > MAX_WORDS || numwords < 1) {
		fprintf(stderr, "usage: %s Word1 [Word2 ...]\n", argv[0]);
		exit(EXIT_FAILURE);
	}
	for (i = 0; i < numwords; i++) {
		strcpy(word[i], argv[i + 1]);
	}
	while ((c = fgetc(stdin)) != EOF) {
		if (c2i(c) != -1 && c2i(c) < numwords) {
			printf("%s ", word[c2i(c)]);
		} else {
			fputc(c, stdout);
		}
	}
	exit(EXIT_SUCCESS);
}
