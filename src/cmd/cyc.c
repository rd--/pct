/* cyc.c - rohan drape, 11/97 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "libpct.h"

static void usage(char *pname)
{
	fprintf(stderr, "usage: %s\n", pname);
	exit(EXIT_FAILURE);
}

int main(int argc, char **argv)
{
	char str[MAX_STR];
	int i, p[MAX_PCS], plen;

	if (argc == 2 && strcmp(argv[1], "-h"))
		usage(argv[0]);

	if (argc > 1) {
		for (i = 1; i < argc; i++) {
			if (str2pcs(argv[i], p, &plen)) {
				p[plen] = p[0];
				plen++;
				pcs2str(p, plen, str, FALSE);
				puts(str);
			}
		}
	} else {
		while (fgets(str, MAX_STR, stdin)) {
			if (str2pcs(str, p, &plen)) {
				p[plen] = p[0];
				plen++;
				pcs2str(p, plen, str, FALSE);
				puts(str);
			}
		}
	}

	exit(EXIT_SUCCESS);
	return EXIT_SUCCESS;
}
