/*
 * fmerge.c
 * merges tokens from files on cl to stdout in collums
 *
 * rohan drape, 12/96
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void usage(char *pname);
void usage(char *pname)
{
	fprintf(stderr, "usage: %s File ...\n", pname);
	exit(EXIT_FAILURE);
}

int main(int argc, char **argv)
{
	char str[256];
	int i, num_files = argc - 1, eof_on_all = 0;
	FILE *fp[FOPEN_MAX];

	if (argc < 2 || argc > FOPEN_MAX) {
		usage(argv[0]);
	}

	for (i = 0; i < num_files; i++) {
		if ((fp[i] = fopen(argv[i + 1], "r")) == NULL) {
			usage(argv[0]);
		}
	}

	while (!eof_on_all) {
		for (i = 0; i < num_files; i++) {
			str[0] = '\0';
			fscanf(fp[i], "%s", str);
			printf("%s\t", str);
		}
		printf("\n");
		eof_on_all = 1;
		for (i = 0; i < num_files; i++) {
			if (!feof(fp[i])) {
				eof_on_all = 0;
			}
		}
	}

	for (i = 0; i < num_files; i++) {
		fclose(fp[i]);
	}

	exit(EXIT_SUCCESS);
	return EXIT_SUCCESS;
}
