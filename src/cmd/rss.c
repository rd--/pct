/*
 * rss.c
 * rotational sub string
 *
 * rohan drape, 4/96
 */

#include <stdio.h>
#include <stdlib.h>

#include "libpct.h"

void usage(char *pname);

int main(int argc, char **argv)
{

	char str[256];
	int p[24], plen;
	int c[12] = { 0 }, clen = 1;
	int n = 0;
	int i, j;

	if (argc < 3 || argc > 4) {
		usage(argv[0]);
	}

	str2pcs(argv[1], p, &plen);
	str2pcs(argv[2], c, &clen);
	if (argc == 4) {
		n = c2i(*argv[3]) - 1;
	}
	if (plen == 0 || clen == 0 || n == -1) {
		usage(argv[0]);
	}

	for (j = 0; j < clen; j++) {
		for (i = 0; i < plen; i++) {
			pcs2str(&p[n], c[j], str, FALSE);
			printf("<%s>\n", str);
			rotpcs(1, p, plen);
		}
	}

	exit(EXIT_SUCCESS);
	return EXIT_SUCCESS;
}

void usage(char *pname)
{
	fprintf(stderr, "usage: %s PCSEG CSet [ Number ]\n", pname);
	exit(EXIT_FAILURE);
}
