/*
 * fsrt.c
 * forte name sort
 *
 * rohan drape, 2/96
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "libpct.h"

#define MAX_LINES 3000
#define MAX_CHARS 256

int fsrt(FILE *fp, int n);
int gline(char *line, long num, long maxc, FILE *fp);
void usage(char *pname);

int main(int argc, char **argv)
{
	int n = 1, i;
	FILE *temp = tmpfile();

	if (argc > 3) {
		usage(argv[0]);
	}
	for (i = 1; i < argc; i++) {
		if (strlen(argv[i]) < 3) {
			if (sscanf(argv[i], "%d", &n) != 1) {
				die("position indicator (%s) invalid", argv[i]);
			}
		} else {
			if (freopen(argv[i], "r", stdin) == NULL) {
				die("input file %s unavailable", argv[i]);
			}
		}
	}
	copyfile(temp, stdin);
	fsrt(temp, n);
	exit(EXIT_SUCCESS);
	return EXIT_SUCCESS;
}

/*
 * long comprison routine for qsort
 */
int lcompare(const void *n1, const void *n2);
int lcompare(const void *n1, const void *n2)
{
	return (int)(*((long *)n1) - *((long *)n2));
}

struct sortdata {
	long value;
	long line;
};

/*
 * fsrt()
 *
 * sort the file at fp by the nth forte
 * name of each line (n>=1)
 * if no forte name can be found for a
 * line it is ignored
 */
int fsrt(FILE *fp, int n)
{
	char line[MAX_STR];
	char forteName[MAX_STR];

	int j;
	int card = 0;
	int fnum = 0;
	int offset;
	int gf;
	int numlines;

	long cline;
	long i;

	struct sortdata sortarray[MAX_LINES];

	/*
	 * sort process:
	 * translate forte name into long (ie 6-42 becomes 642)
	 */
	cline = 0;
	numlines = 0;
	fseek(fp, 0, SEEK_SET);
	while (fgets(line, MAX_CHARS, fp) != NULL) {
		offset = 0;
		gf = TRUE;
		for (i = 1; i <= n; i++) {
			j = str2forte(&line[offset], forteName);
			if (!j) {
				gf = FALSE;
				break;
			}
			offset += j;
		}
		if (gf) {
			if (strstr(forteName, "Z") != NULL) {
				if (sscanf(forteName, "%d-Z%d", &card, &fnum) != 2) {
					return FALSE;
				}
			} else {
				if (sscanf(forteName, "%d-%d", &card, &fnum) != 2) {
					return FALSE;
				}
			}
			sortarray[cline].value = (card * 100) + fnum;
			sortarray[cline].line = numlines;
			cline++;
		}
		if (cline >= MAX_LINES) {
			die("input file too large");
		}
		numlines++;
	}
	qsort(&sortarray[0], cline, sizeof(struct sortdata), lcompare);
	for (i = 0; i < cline; i++) {
		gline(line, sortarray[i].line, MAX_CHARS, fp);
		fputs(line, stdout);
	}
	return TRUE;
}

/*
 * gline()
 *
 * put line 'num' of the file
 * at fp into 'line', where zero
 * is the first line.
 */
int gline(char *line, long num, long maxc, FILE *fp)
{
	fseek(fp, 0, SEEK_SET);
	fgets(line, (int)maxc, fp);
	while (num--) {
		if (fgets(line, (int)maxc, fp) == NULL) {
			return FALSE;
		}
	}
	return TRUE;
}

void usage(char *pname)
{
	fprintf(stderr, "usage: %s [ Number ] [ File ]\n", pname);
	exit(EXIT_FAILURE);
}
