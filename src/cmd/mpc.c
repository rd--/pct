/* mpc.c - rohan drape, 5/96 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "libpct.h"

int main(int argc, char **argv)
{
	int i, c;
	int map[12];

	if (argc != 2 || strlen(argv[1]) != 12) {
		fprintf(stderr, "usage: %s pcv\n", argv[0]);
		exit(EXIT_FAILURE);
	}

	for (i = 0; i < 12; i++) {
		map[i] = c2i(argv[1][i]);
	}

	while ((c = getc(stdin)) != EOF) {
		if (c2i(c) != -1) {
			putc(i2c(map[c2i(c)]), stdout);
		} else {
			putc(c, stdout);
		}
	}

	exit(EXIT_SUCCESS);
	return EXIT_SUCCESS;
}
