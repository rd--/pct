/* pcom.c -  rohan drape,  10/97 */

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>

#include "libpct.h"

static void usage(char *pname)
{
	fprintf(stderr, "%s src_type dst_type [ pco ]\n", pname);
	exit(EXIT_FAILURE);
}

int main(int argc, char **argv)
{
	char str[MAX_STR];
	Pco_t *p, *q;

	if (argc < 3) {
		usage(argv[0]);
	}

	p = pco_create(argv[1]);
	q = pco_create(argv[2]);

	if (argc == 4) {
		str2pcs(argv[3], p->data, &(p->size));
		if (pco_map(q, pco_typeof(q), p)) {
			pco_write(q, str);
			puts(str);
		}
	} else {
		while (fgets(str, MAX_STR, stdin)) {
			str2pcs(str, p->data, &(p->size));
			if (pco_map(q, pco_typeof(q), p)) {
				pco_write(q, str);
				puts(str);
			}
		}
	}

	pco_free(p);
	pco_free(q);
	exit(EXIT_SUCCESS);
	return EXIT_SUCCESS;
}
