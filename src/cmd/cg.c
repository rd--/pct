/* cg.c - (c) rohan drape, 1996 */

#include <stdio.h>
#include <stdlib.h>

#include "libpct.h"

/* If r > 0 print only nCr, else print everything. */
static void print_object_combinations(Pco_t *obj, int r)
{
	int i, n;
	Pco_t *s;
	Cg_t *c;

	s = pco_create("pcset");
	for (i = 1; i <= pco_sizeof(obj); i++) {
		if (r)
			i = r;
		c = cgopen(pco_sizeof(obj), i, pco_data(obj));
		while (1) {
			n = cgnext(c, pco_data(s));
			if (!n)
				break;
			pco_set_sizeof(s, n);
			pco_put(s, stdout);
		}
		cgclose(c);
		if (r)
			break;
	}
	pco_free(s);
}

int main(int argc, char **argv)
{
	Pco_t *p;
	int i, r = 0;

	p = pco_create("pcset");
	if (argc > 1) {
		for (i = 1; i < argc; i++) {
			if (argv[i][0] == '-' && argv[i][1] == 'r') {
				r = atoi(&argv[i][2]);
			} else if (pco_read(p, argv[i])) {
				print_object_combinations(p, r);
			} else {
				fprintf(stderr, "usage: %s [ -rN ] pcset ... \n", argv[0]);
			}
		}
	}
	pco_free(p);
	return EXIT_SUCCESS;
}
