/* airr.c - rohan drape, 5/98 */

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "libpct.h"

static void usage(char *pname)
{
	fprintf(stderr, "usage: %s { q [ -rN ] } | { a [ -cN ] -fFile }\n",
		pname);
	fprintf(stderr, "\t q \t query mode\n");
	fprintf(stderr,
		"\t-r \t set row pair to be examined (def. all combinations)\n");
	fprintf(stderr, "\t a \t assert mode\n");
	fprintf(stderr,
		"\t-c \t set columns to be tested (def. all columns)\n");
	fprintf(stderr, "\t-f \t set assertion file\n");
	exit(EXIT_FAILURE);
}

static int airr_write(Ppca_t *e, int r0, int r1)
{
	int i, j, k;
	Pco_t *p[2];

	p[0] = pco_create("pcseg");
	p[1] = pco_create("pcseg");
	for (i = 0; i < e->c; i++) {
		paposition(e, r0, i, p[0], PPCA_READ);
		paposition(e, r1, i, p[1], PPCA_READ);
		for (j = 0; j < pco_sizeof(p[0]); j++) {
			for (k = 0; k < pco_sizeof(p[1]); k++) {
				printf("%c",
					i2c(mod12(pco_element(p[1], k) - pco_element(p[0], j))));
			}
		}
		printf(" ");
	}
	printf("\n");

	pco_free(p[0]);
	pco_free(p[1]);
	return TRUE;
}

static int
airr_assert(Ppca_t *e, int r0, int r1, int *s, int slen, int *c, int clen)
{
	int i, j, k;
	Pco_t *p[2];

	p[0] = pco_create("pcseg");
	p[1] = pco_create("pcseg");
	for (i = 0; i < e->c; i++) {
		if (isel(i, c, clen)) {
			paposition(e, r0, i, p[0], PPCA_READ);
			paposition(e, r1, i, p[1], PPCA_READ);
			for (j = 0; j < pco_sizeof(p[0]); j++) {
				for (k = 0; k < pco_sizeof(p[1]); k++) {
					if (!isel(mod12(pco_element(p[1], k) - pco_element(p[0], j)), s,
							slen)) {
						return FALSE;
					}
				}
			}
		}
	}
	pco_free(p[0]);
	pco_free(p[1]);
	return TRUE;
}

#define MAX_CONDITIONS 32
static void airr_filter(char *file_name, int *c, int clen)
{
	char str[MAX_STR];
	int i, good_array, num_conditions, r[MAX_CONDITIONS][2];
	int s[MAX_CONDITIONS][MAX_PCS], slen[MAX_CONDITIONS];
	FILE *fp;
	Ppca_t *e;

	fp = fopen(file_name, "r");
	assert(fp);

	num_conditions = 0;
	while (fgets(str, MAX_STR, fp)) {
		if (str[0] != '#') {
			assert(str[0] == 'r');
			r[num_conditions][0] = c2i(str[1]);
			r[num_conditions][1] = c2i(str[2]);
			str2pcs(&str[4], s[num_conditions], &slen[num_conditions]);
			assert(r[num_conditions][0] >= 0
				&& r[num_conditions][0] <= 12);
			assert(r[num_conditions][1] >= 0
				&& r[num_conditions][1] <= 12);
			assert(slen[num_conditions] > 0 && slen[num_conditions] <= 12);
			num_conditions++;
			assert(num_conditions < MAX_CONDITIONS);
		}
	}

	e = paopen("airr-a");
	assert(e);
	while (paread(e, stdin)) {
		good_array = TRUE;
		for (i = 0; i < num_conditions; i++) {
			if (!airr_assert(e, r[i][0], r[i][1], s[i], slen[i], c, clen)) {
				good_array = FALSE;
				break;
			}
		}
		if (good_array) {
			pawrite(e, stdout);
		}
	}
	paclose(e);
	exit(EXIT_SUCCESS);
}

#define AIRR_ASSERTION_MODE 1
#define AIRR_QUERY_MODE 2

int main(int argc, char **argv)
{
	char assertion_file[FILENAME_MAX] = "";
	int i, mode = -1, q[2] = { -1, -1 }, s[MAX_PCS], slen;
	Ppca_t *e;
	Cg_t *c;

	for (i = 0; i < MAX_PCS; i++)
		s[i] = i;
	slen = MAX_PCS;

	if (argc == 1)
		usage(argv[0]);
	switch (argv[1][0]) {
	case 'a':
		mode = AIRR_ASSERTION_MODE;
		break;
	case 'q':
		mode = AIRR_QUERY_MODE;
		break;
	default:
		usage(argv[0]);
		break;
	}

	for (i = 2; i < argc; i++) {
		if (argv[i][0] == '-') {
			switch (argv[i][1]) {
				/*****  set columns to be tested *****/
			case 'c':
				str2pcs(&argv[i][2], s, &slen);
				assert(slen > 0 && slen < MAX_PCS);
				break;
				/***** set filter file *****/
			case 'f':
				strcpy(assertion_file, &argv[i][2]);
				break;
				/***** set query values *****/
			case 'r':
				q[0] = c2i(argv[i][2]);
				q[1] = c2i(argv[i][3]);
				break;
				/***** help *****/
			default:
				usage(argv[0]);
				break;
			}
		} else {
			usage(argv[0]);
		}
	}

	if (mode == AIRR_ASSERTION_MODE) {
		airr_filter(assertion_file, s, slen);
	} else if (mode == AIRR_QUERY_MODE) {
		/* all combinations */
		if (q[0] == -1) {
			e = paopen("airr-q");
			assert(e);
			while (paread(e, stdin)) {
				c = cgopen(e->r, 2, NULL);
				assert(c);
				while (cgnext(c, q) == 2) {
					printf("(r%d,r%d): ", q[0], q[1]);
					airr_write(e, q[0], q[1]);
				}
				cgclose(c);
			}
			paclose(e);
		} else {
			/* specified combination */
			e = paopen("airr-q-r");
			assert(e);
			while (paread(e, stdin)) {
				airr_write(e, q[0], q[1]);
			}
			paclose(e);
		}
	}

	return EXIT_SUCCESS;
}
