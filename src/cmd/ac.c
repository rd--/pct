/* ac.c - rohan drape, 7/97 */

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>

#include "libpct.h"

int main(int argc, char **argv)
{
	int count = 0;
	Ppca_t *e;

	if (argc != 1) {
		fprintf(stderr, "usage: %s\n", argv[0]);
		return EXIT_FAILURE;
	}

	e = paopen("ac");
	assert(e);
	while (paread(e, stdin)) {
		count += 1;
	}
	printf("%d\n", count);
	paclose(e);

	return EXIT_SUCCESS;
}
