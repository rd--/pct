/* spsc.c - rohan drape, 4/97 */

#include <limits.h>
#include <stdio.h>
#include <stdlib.h>

#include "libpct.h"

#define MAX_SUBSETS 256

static void usage(char *pname)
{
	fprintf(stderr, "usage: %s [ -a ] sc ...\n", pname);
	exit(EXIT_FAILURE);
}

int main(int argc, char **argv)
{
	char str[256];
	struct {
		int p[MAX_PCS], plen;
	} subset_tbl[MAX_SUBSETS];
	int tbl_sz = 0, smallest_subset = 0;
	int p[12], plen;
	int superset_of_all, lowest_cardinality = 12, write_all = 0;
	int i;
	FILE *sc_tbl = fopen(sc_lookup, "r");

	if (sc_tbl == NULL)
		die("sc_lookup file not available???");

	for (i = 1; i < argc; i++) {
		if (argv[i][0] == '-') {
			switch (argv[i][1]) {
			case 'a':
				write_all = 1;
				break;
			default:
				usage(argv[0]);
				break;
			}
		} else {
			if (!str2pcs(argv[i], subset_tbl[tbl_sz].p,
					&(subset_tbl[tbl_sz].plen))) {
				usage(argv[0]);
			}
			if (subset_tbl[tbl_sz].plen < smallest_subset) {
				smallest_subset = subset_tbl[tbl_sz].plen;
			}
			remdup(subset_tbl[tbl_sz].p, &(subset_tbl[tbl_sz].plen));
			tbl_sz += 1;
			if (tbl_sz > MAX_SUBSETS)
				die("subsets exceed compiled in limit");
		}
	}

	while (getnextpcs(sc_tbl, p, &plen, '[', FALSE) != EOF) {
		if (plen < smallest_subset)
			continue;
		if (!write_all && plen > lowest_cardinality)
			break;
		superset_of_all = TRUE;
		for (i = 0; i < tbl_sz; i++) {
			if (!isabssub(subset_tbl[i].p, subset_tbl[i].plen, p, plen)) {
				superset_of_all = FALSE;
				break;
			}
		}
		if (superset_of_all) {
			pcs2scstr(p, plen, str, 0);
			printf("%s\n", str);
			lowest_cardinality = plen;
		}
	}

	fclose(sc_tbl);
	return EXIT_SUCCESS;
}
