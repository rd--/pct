/*fc.c - rohan drape, 2/96 */

#include <stdio.h>
#include <stdlib.h>

#include "libpct.h"

#define MAX_FILES 12
#define MAX_LINE 512

void usage(char *pname);
void usage(char *pname)
{
	fprintf(stderr, "usage: %s [ -v ] File1 File2 ...\n", pname);
	exit(EXIT_FAILURE);
}

int main(int argc, char **argv)
{
	FILE *fp[MAX_FILES];
	FILE *tmp[MAX_FILES];
	FILE *all = tmpfile();
	FILE *log = tmpfile();
	char str[MAX_LINE];
	char *fnames[MAX_LINE];
	int i, j;
	int nfiles = 0;
	int fnd[MAX_FILES];
	int fndall;
	int vv = FALSE;

	if (all == NULL || log == NULL) {
		die("tempfile() failed");
	}

	if (argc < 3) {
		usage(argv[0]);
	}

	for (i = 1; i < argc; i++) {
		/* OPTIONS */
		if (argv[i][0] == '-') {
			if (nfiles != 0) {
				die("options must be specified before file list");
			}
			switch (argv[i][1]) {
			case 'v':
				vv = TRUE;
				break;
			default:
				usage(argv[0]);
				break;
			}
			continue;
		}
		/* INPUT FILES */
		if ((fp[nfiles] = fopen(argv[i], "r")) == NULL) {
			die("fopen() failed for file '%s'", argv[i]);
		}
		if (vv) {
			if ((tmp[nfiles] = tmpfile()) == NULL) {
				die("tmpfile() creation failed");
			}
		}
		fnames[nfiles] = argv[i];
		nfiles++;
	}

	for (j = 0; j < nfiles; j++) {
		while (fgets(str, MAX_LINE, fp[j])) {
			if (!lineinfile(log, str)) {
				fprintf(log, "%s", str);
				fndall = TRUE;
				for (i = 0; i < nfiles; i++) {
					if (lineinfile(fp[i], str)) {
						fnd[i] = TRUE;
					} else {
						fnd[i] = FALSE;
						fndall = FALSE;
					}
				}
				if (fndall) {
					fprintf(all, "%s", str);
				} else {
					if (vv) {
						for (i = 0; i < nfiles; i++) {
							if (fnd[i]) {
								fprintf(tmp[i], "%s", str);
							}
						}
					}
				}
			}
		}
	}

	if (vv) {
		printf("all files:\n");
		copyfile(stdout, all);
		for (i = 0; i < nfiles; i++) {
			printf("\nfile '%s':\n", fnames[i]);
			copyfile(stdout, tmp[i]);
		}
	} else {
		copyfile(stdout, all);
	}

	for (i = 0; i < nfiles; i++) {
		fclose(fp[i]);
		if (vv) {
			fclose(tmp[i]);
		}
	}
	fclose(log);
	fclose(all);
	exit(EXIT_SUCCESS);
	return EXIT_SUCCESS;
}
