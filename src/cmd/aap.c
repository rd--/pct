/* aap.c - rohan drape, 1/98 */

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>

#include "libpct.h"

int main(int argc, char **argv)
{
	int N;
	Ppca_t *e;

	if (argc != 2) {
		fprintf(stderr, "usage: %s N\n", argv[0]);
		return EXIT_FAILURE;
	} else {
		N = atoi(argv[1]);
	}

	e = paopen("aap");
	assert(e);
	while (paread(e, stdin)) {
		paprtnrm(e, N);
		pawrite(e, stdout);
	}
	paclose(e);

	return EXIT_SUCCESS;
}
