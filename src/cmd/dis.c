/*
 * dis.c
 * diatonic-interval sets
 *
 * rohan drape, 6/96
 */

#include <stdio.h>
#include <stdlib.h>

#include "libpct.h"

void usage(char *pname);
void usage(char *pname)
{
	fprintf(stderr, "usage: %s [ d-iset ]\n", pname);
	exit(EXIT_FAILURE);
}

int main(int argc, char **argv)
{
	char str[256];
	int p[12], plen;

	if (argc > 2) {
		usage(argv[0]);
	}
	if (argc == 2) {
		if (str2pcs(argv[1], p, &plen) && diset2iset(p, &plen)) {
			pcs2str(p, plen, str, 0);
			printf("%s\n", str);
		}
	} else {
		while (fgets(str, 256, stdin) != NULL) {
			if (str2pcs(str, p, &plen) && diset2iset(p, &plen)) {
				pcs2str(p, plen, str, 0);
				printf("%s\n", str);
			}
		}
	}
	exit(EXIT_SUCCESS);
	return EXIT_SUCCESS;
}
