/* scdb.c - rohan drape,  3/96 */

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "libpct.h"

FILE *database_fp;
int scdb(int *p, int plen);
int scdb(int *p, int plen)
{
	char str[MAX_STR];
	int q[MAX_PCS], qlen;

	prime(p, &plen, NULL);
	rewind(database_fp);
	while (fgets(str, MAX_STR, database_fp) != NULL) {
		if (str2pcs(str, q, &qlen) && isequiv(p, plen, q, qlen)) {
			fputs(str, stdout);
		}
	}

	return TRUE;
}

int main(int argc, char **argv)
{
	char str[MAX_STR], database_name[FILENAME_MAX];
	int p[MAX_PCS], plen;
	int i;

	strcpy(database_name, pct_data_dir);
	strcat(database_name, "/scdb");
	database_fp = fopen(database_name, "r");
	assert(database_fp);

	if (argc == 1) {
		while (fgets(str, MAX_STR, stdin) != NULL) {
			if (str2pcs(str, p, &plen)) {
				scdb(p, plen);
			}
		}
	}

	for (i = 1; i < argc; i++) {
		if (str2pcs(argv[i], p, &plen)) {
			scdb(p, plen);
		} else {
			fprintf(stderr, "usage: %s [ sc ... ]\n", argv[0]);
			exit(EXIT_FAILURE);
		}
	}

	fclose(database_fp);
	exit(EXIT_SUCCESS);
	return EXIT_SUCCESS;
}
