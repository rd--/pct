/* sra.c - rohan drape, 8/97 */

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "libpct.h"

int main(int argc, char **argv)
{
	int i, j, rv;
	Ppca_t *e;

	if (argc > 1) {
		fprintf(stderr, "usage: %s\n", argv[0]);
		exit(EXIT_FAILURE);
	}

	e = paopen("sra");
	assert(e);

	while (paread(e, stdin)) {
		assert(e->r == 1);
		e->r = e->size[0];
		for (i = 1; i < e->r; i++) {
			memcpy(e->data[i], e->data[i - 1], e->r * sizeof(int));
			e->size[i] = e->r;
			for (j = 0; j < e->r; j++) {
				e->partition[i][j] = e->partition[0][j];
			}
			rotpcs(1, e->data[i], e->size[i]);
			trspcs(-e->data[i][0], e->data[i], e->size[i]);
		}
		rv = pawrite(e, stdout);
		assert(rv);
	}

	paclose(e);
	exit(EXIT_SUCCESS);
	return EXIT_SUCCESS;
}
