/* po.c - rohan drape, 5/96 */

#include <stdio.h>
#include <stdlib.h>

#include "libpct.h"

int main(int argc, char **argv)
{
	int c;
	int i;
	int total = 0;
	int count[12] = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
	double pcnt = 0;

	while ((c = getc(stdin)) != EOF) {
		if ((i = c2i((char)c)) != -1) {
			count[i]++;
			total++;
		}
	}
	for (i = 0; i < 12; i++) {
		if (count[i] != 0) {
			pcnt = ((double)count[i] / total) * 100;
			printf("%c  %4d  %f\n", i2c(i), count[i], pcnt);
		}
	}
	/*printf("t=%d\n",total); */
	exit(EXIT_SUCCESS);
	return EXIT_SUCCESS;
}
