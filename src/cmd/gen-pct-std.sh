# src/cmd/gen-use-std.sh - (c) rohan drape, 2000-2001

function format_function 
{
    printf "function %s \n{\n\tpct %s \$@ ;\n}\n\n" $1 $1
}

printf "# -*-sh-*-\n# src/cmd/pct-std - (c) rohan drape, 2000-2001\n\n" > pct-std

for cmd in $@ ; do format_function $cmd >> pct-std ; done

