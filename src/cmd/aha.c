/* aha.c - rohan drape, 1997 */

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "libpct.h"

/*******/

static int count_harmonies(Ppca_t *e)
{
	char str[16], names[MAX_PCS][16];
	int i, j, cnt = 0;
	Pco_t *q = pco_create("sc");

	for (i = 0; i < e->c; i++) {
		pacolumn(e, i, q, PPCA_READ);
		pcs_to_name(str, q->data, q->size);
		for (j = 0; j < cnt; j++) {
			if (strcmp(str, names[j]) == 0)
				break;
		}
		if (j == cnt) {
			strncpy(names[cnt], str, 16);
			cnt++;
		}
	}
	pco_free(q);
	return cnt;
}

typedef struct {
	int c[12], clen;
} ahd_t;

static void *array_harmonic_diversity_init(char *value)
{
	ahd_t *d;

	d = emalloc(sizeof(ahd_t));
	str2pcs(value, d->c, &(d->clen));
	return d;
}

static int array_harmonic_diversity_eval(Ppca_t *e, void *d)
{
	return isel(count_harmonies(e), ((ahd_t *)d)->c, ((ahd_t *)d)->clen);
}

/*******/

typedef struct {
	int t, n_exceptions;
} apt_t;

static void *array_pc_transference_init(char *value)
{
	apt_t *d;

	d = emalloc(sizeof(apt_t));
	assert(sscanf(value, "%d, %d", &(d->t), &(d->n_exceptions)) == 2);
	return d;
}

static int array_pc_transference_eval(Ppca_t *p, void *d)
{
	Pco_t *v[2];
	int i, j = 1, k = 0;

	v[0] = pco_create("pcset");
	v[1] = pco_create("pcseg");
	pacolumn(p, 0, v[0], PPCA_READ);
	remdup(v[0]->data, &(v[0]->size));
	for (i = 1; i < p->c; i++) {
		pacolumn(p, i, v[j], PPCA_READ);
		remdup(v[j]->data, &(v[j]->size));
		if (intersection(v[0]->data, v[0]->size, v[1]->data, v[1]->size) != ((apt_t *)d)->t) {
			if (k < ((apt_t *)d)->n_exceptions)
				k++;
			else
				return FALSE;
		}
		j = (j ? 0 : 1);
	}
	pco_free(v[0]);
	pco_free(v[1]);
	return TRUE;
}

/*******/

typedef struct {
	Pco_t *c;
} ahs_t;

static void *array_harmonic_size_init(char *value)
{
	ahs_t *d;

	d = emalloc(sizeof(ahs_t));
	d->c = pco_create("cset");
	pco_read(d->c, value);
	return d;
}

static int array_harmonic_size_eval(Ppca_t *e, void *d)
{
	Pco_t *q;
	int i;

	q = pco_create("pcseg");
	for (i = 0; i < e->c; i++) {
		pacolumn(e, i, q, PPCA_READ);
		remdup(q->data, &(q->size));
		if (!isel(q->size, ((ahs_t *)d)->c->data, ((ahs_t *)d)->c->size))
			return FALSE;
	}
	pco_free(q);
	return TRUE;
}

/*******/

static int array_harmonies_include(Ppca_t *p, Ppca_t *q, char *relation)
{
	Pco_t *s = pco_create("pcseg");
	int i, j, meets_criteria;

	for (i = 0; i < q->r; i++) { /* rows in q */
		meets_criteria = 0;
		for (j = 0; j < p->c; j++) { /* collumns in p till we find one */
			pacolumn(p, j, s, PPCA_READ);
			if (strcmp(relation, "in-sc") == 0) {
				if (isabssub(s->data, s->size, q->data[i], q->size[i]) || isequiv(s->data, s->size, q->data[i], q->size[i])) {
					meets_criteria = 1;
				}
			} else if (strcmp(relation, "is-sc") == 0) {
				if (isequiv(s->data, s->size, q->data[i], q->size[i])) {
					meets_criteria = 1;
				}
			} else if (strcmp(relation, "is-pcset") == 0) {
				if (isequal(s->data, s->size, q->data[i], q->size[i])) {
					meets_criteria = 1;
				}
			} else if (strcmp(relation, "is-pcseg") == 0) {
				if (q->size[i] != s->size
					&& isident(s->data, q->data[i], q->size[i])) {
					meets_criteria = 1;
				}
			} else {
				fprintf(stderr,
					"array_harmonies_include () : invalid relation\n");
				return 0;
			}
			if (meets_criteria)
				break;
		}
		if (!meets_criteria)
			return 0;
	}
	return 1;
}

typedef struct {
	Ppca_t *q;
	char str[512];
	char *relation;
} ahc_t;

static void *array_harmonic_content_init(char *value)
{
	ahc_t *d;
	char *token;

	d = emalloc(sizeof(ahc_t));
	d->q = paopen("ahc.q");
	strcpy(d->str, value);
	d->relation = strtok(d->str, " \t");
	while ((token = strtok(NULL, " \t"))) {
		str2pcs(token, d->q->data[d->q->r], &(d->q->size[d->q->r]));
		d->q->partition[d->q->r][0] = d->q->size[d->q->r];
		(d->q->r)++;
	}
	return d;
}

static int array_harmonic_content_eval(Ppca_t *e, void *d)
{
	return array_harmonies_include(e, ((ahc_t *)d)->q,
		((ahc_t *)d)->relation);
}

/*******/

static int str_to_vector(int *d, char *str)
{
	int i;

	for (i = 0; i < 12; i++) {
		if (str[i] == '.')
			d[i] = -1;
		else {
			d[i] = c2i(str[i]);
			if (d[i] < 0)
				return 0;
		}
	}
	return 1;
}

typedef struct {
	int v[12];
} apd_t;

void *array_pc_distribution_init(char *value)
{
	apd_t *d;

	d = emalloc(sizeof(apd_t));
	str_to_vector(d->v, value);
	return d;
}

static int array_pc_distribution_eval(Ppca_t *e, void *d)
{
	Pco_t *distr_vector = pco_create("pcv");
	int i;

	padistr(e, distr_vector);
	for (i = 0; i < 12; i++) {
		if (((apd_t *)d)->v[i] != distr_vector->data[i]
			&& ((apd_t *)d)->v[i] != -1)
			return 0;
	}
	pco_free(distr_vector);
	return 1;
}

/*******/

typedef void *(*aha_init_f)(char *);
typedef int (*aha_eval_f)(Ppca_t *, void *);

struct {
	char name[32];
	aha_init_f initf;
	aha_eval_f evalf;
} sym_tbl[] = {
	{ "pc-transference", array_pc_transference_init,
		array_pc_transference_eval },
	{ "harmonic-diversity", array_harmonic_diversity_init,
		array_harmonic_diversity_eval },
	{ "harmonic-size", array_harmonic_size_init, array_harmonic_size_eval }, { "harmonic-content", array_harmonic_content_init, array_harmonic_content_eval }, { "pc-distribution", array_pc_distribution_init, array_pc_distribution_eval }
};
int n_sym = 5;

typedef struct {
	void *o; /* object returned by init_f */
	aha_eval_f f; /* eval_f */
} aha_attr_t;

int main(int argc, char **argv)
{
	int i, j, meets_criteria, n_attr = 0;
	char name[32], value[512];
	Ppca_t *e;
	aha_attr_t attr[20];

	assert(argc <= 20);
	for (i = 1; i < argc; i++) {
		sscanf(argv[i], "%s = %n", name, &j);
		strcpy(value, &argv[i][j]);
		for (j = 0; j < n_sym; j++) {
			if (strcmp(sym_tbl[j].name, name) == 0) {
				attr[n_attr].o = sym_tbl[j].initf(value);
				attr[n_attr].f = sym_tbl[j].evalf;
				n_attr++;
				break;
			}
		}
		if (j == n_sym) {
			fprintf(stderr, "unsupported attribute: %s\n", name);
			abort();
		}
	}

	e = paopen("aha");
	while (paread(e, stdin)) {
		meets_criteria = 1;
		for (i = 0; i < n_attr; i++) {
			if (attr[i].f(e, attr[i].o) == 0) {
				meets_criteria = 0;
				break;
			}
		}
		if (meets_criteria) {
			pawrite(e, stdout);
		}
	}
	paclose(e);

	exit(EXIT_SUCCESS);
	return EXIT_SUCCESS;
}
