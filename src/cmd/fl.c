/* fl.c - rohan drape, 6/96 */

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>

#include "libpct.h"

#define COLUMN_OFFSET 18

static void usage(char *pname)
{
	fprintf(stderr, "usage: %s [ -c ] [ -l ] [ -v ] [ cset ]\n", pname);
	exit(EXIT_FAILURE);
}

static int
forte_listing(int *c, int clen, int verbose, int concise, int literal)
{
	char str[256];
	int p[12], plen = 0, q[12], nc;
	FILE *sc_tbl;

	sc_tbl = fopen(sc_lookup, "r");
	assert(sc_tbl);

	if (verbose) {
		printf("sc Name%n", &nc);
		nextcol(COLUMN_OFFSET - nc);
		printf("Interval Vector%n", &nc);
		nextcol(COLUMN_OFFSET - nc);
		printf("TICS Vector\n");
		printf("--------------------------------------------------\n");
	}

	while (getnextpcs(sc_tbl, p, &plen, '[', FALSE) != EOF) {
		if (isel(plen, c, clen)) {
			if (verbose) {
				pcs2scstr(p, plen, str, concise);
				printf("%s%n", str, &nc);
				nextcol(COLUMN_OFFSET - nc);
				pcs2icv(p, plen, q);
				pcs2str(q, 7, str, 0);
				printf("<%s>%n", str, &nc);
				nextcol(COLUMN_OFFSET - nc);
				pcs2tics(p, plen, q);
				pcs2str(q, 12, str, 0);
				printf("<%s>\n", str);
			} else if (literal) {
				pcs2str(p, plen, str, 0);
				printf("%s\n", str);
			} else {
				pcs2scstr(p, plen, str, concise);
				printf("%s\n", str);
			}
		}
	}

	fclose(sc_tbl);
	return TRUE;
}

int main(int argc, char **argv)
{
	int i, c[12] = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11 }, clen = 11;
	int verbose = FALSE, concise = 0, literal = 0;

	for (i = 1; i < argc; i++) {
		if (argv[i][0] == '-') {
			switch (argv[i][1]) {
			case 'v':
				verbose = TRUE;
				break;
			case 'c':
				concise = 3;
				break;
			case 'l':
				literal = 3;
				break;
			default:
				usage(argv[0]);
				break;
			}
		} else if (!str2pcs(argv[i], c, &clen)) {
			usage(argv[0]);
		}
	}

	forte_listing(c, clen, verbose, concise, literal);
	return EXIT_SUCCESS;
}
