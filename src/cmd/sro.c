/***** sro.c - (c) rohan drape, 1996 *****/

#include <stdio.h>
#include <stdlib.h>

#include "libpct.h"

int main(int argc, char **argv)
{
	Pco_t *p;
	Sro_t o;

	if (argc != 2 || !sro_read(&o, argv[1])) {
		fprintf(stderr, "usage: %s sro\n", argv[0]);
		exit(EXIT_FAILURE);
	}

	p = pco_create("pcseg");
	while (pco_get(p, stdin)) {
		pco_sro_apply(p, &o);
		pco_put(p, stdout);
	}
	pco_free(p);

	return EXIT_SUCCESS;
}
