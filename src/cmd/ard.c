/* ard.c - (c) rohan drape, 1996 */

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "libpct.h"

#define ARD_MAX_ARRAYS 8192

static int array_in_list(Ppca_t *e, Ppca_t **el, int el_sz)
{
	int i;
	for (i = 0; i < el_sz; i++) {
		if (paequal(e, el[i])) {
			return TRUE;
		}
	}
	return FALSE;
}

int main(int argc, char **argv)
{
	Ppca_t *e;
	Ppca_t **el;
	int el_sz = 0;
	int i;
	int write_duplicates_only = 0;

	if (argc > 1) {
		if (strcmp(argv[1], "-d") == 0) {
			write_duplicates_only = 1;
		} else {
			fprintf(stderr, "usage: ard [-d]\n");
			exit(EXIT_FAILURE);
		}
	}

	e = paopen("input");
	assert(e);
	el = malloc(ARD_MAX_ARRAYS * sizeof(Ppca_t *));
	assert(el);

	while (paread(e, stdin)) {
		if (!array_in_list(e, el, el_sz)) {
			el[el_sz] = paopen("unique");
			pacopy(el[el_sz], e);
			el_sz += 1;
			if (el_sz >= ARD_MAX_ARRAYS) {
				die("ard: too many arrays");
			}
		} else {
			if (write_duplicates_only) {
				pawrite(e, stdout);
			}
		}
	}

	if (!write_duplicates_only) {
		for (i = 0; i < el_sz; i++) {
			pawrite(el[i], stdout);
		}
	}

	return EXIT_SUCCESS;
}
