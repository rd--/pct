/*
 * mcs.c
 * implementation stuff for mcs.tex (see there for details)
 *
 * rohan drape, 12/96
 */

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>

#include "libpct.h"

void usage(char *pname);

/* the source copies for these commands are kept in ~/rdmath/combinatrics/
   and should be modified there only */
int print_nCr(int n, int r);
int print_nPr(int n, int r);
int print_permutations(int *s, int slen);

int main(int argc, char **argv)
{
	char sysstr[512];
	int n, r;

	if (argc < 3 || argc > 4) {
		usage(argv[0]);
	}

	n = atoi(argv[1]);
	r = atoi(argv[2]);
	assert(r < n);

	if (argc == 3 || argv[3][0] == 'c') {
		assert(print_nCr(n, r));
	}
	if (argc == 3 || argv[3][0] == 's') {
		assert(print_nPr(n, r));
	}
	if (argc == 3 || argv[3][0] == 'f') {
		sprintf(sysstr, "rm -f \"<\"*.spl ; pct mcs %d %d s | pct fsplit %d ; "
						"pct fmerge \"<\"*.spl | more ; rm -f \"<\"*.spl",
			n, r, r);
		/*puts(sysstr); */
		assert(system(sysstr) != 127);
	}

	exit(EXIT_SUCCESS);
	return EXIT_SUCCESS;
}

/*
 * print the combinations nCr to stdout.
 * the set is the integers {0-(n-1)}.
 */
int print_nCr(int n, int r)
{
	int *i = calloc(n, sizeof(int)), j;

	if (r > n || i == NULL) {
		return FALSE;
	}

	for (j = 0; j < r; j++) { /* init i[] counters for all r */
		i[j] = j;
	}

	while (i[0] < (n - (r - 1))) {
		/* print combination */
		printf("{");
		for (j = 0; j < r; j++) {
			printf("%d,", i[j]);
		}
		printf("\b} ");
		/* increment indexes as required from right to left */
		i[r - 1]++;
		for (j = r - 1; j > 0; j--) {
			if (i[j] > n - (r - j)) {
				i[j - 1]++;
				i[j] = 0;
			}
		}
		/* reset indexes as required from left to right */
		for (j = 1; j < r; j++) {
			if (i[j] <= i[j - 1]) {
				i[j] = i[j - 1] + 1;
				printf("\n"); /* make nice formatting on output */
			}
		}
	}
	return TRUE;
}

/*
 * print the permutations nPr to stdout.
 * the set is the integers {0-(n-1)}.
 */
int print_nPr(int n, int r)
{
	int *i = calloc(n, sizeof(int)), j;

	if (r > n || i == NULL) {
		return FALSE;
	}

	for (j = 0; j < r; j++) { /* init i[] counters for all r */
		i[j] = j;
	}

	while (i[0] < (n - (r - 1))) {
		/* print permutations of current combination */
		print_permutations(i, r);
		/* increment indexes as required from right to left */
		i[r - 1]++;
		for (j = r - 1; j > 0; j--) {
			if (i[j] > n - (r - j)) {
				i[j - 1]++;
				i[j] = 0;
			}
		}
		/* reset indexes as required from left to right */
		for (j = 1; j < r; j++) {
			if (i[j] <= i[j - 1]) {
				i[j] = i[j - 1] + 1;
				printf("\n"); /* make nice formatting on output */
			}
		}
	}
	return TRUE;
}

/*
 * print the permutations of the set s to stdout
 * called from nPr for each combination of nCr
 */
int print_permutations(int *s, int slen)
{
	int *o = calloc(slen, sizeof(int));
	int i;

	if (o == NULL) {
		return FALSE;
	}
	for (i = 0; i < slen; i++) {
		o[i] = i;
	}
	while (o[0] < slen) {
		if (!isdup(o, slen)) {
			printf("<");
			for (i = 0; i < slen; i++) {
				printf("%d", s[o[i]]);
			}
			printf("> ");
		}
		o[slen - 1]++;
		for (i = slen - 1; i > 0; i--) {
			if (o[i] > slen - 1) {
				o[i] = 0;
				o[i - 1]++;
			}
		}
	}
	return TRUE;
}

void usage(char *pname)
{
	fprintf(stderr, "usage: %s # n [ c | s | f ]\n", pname);
	exit(EXIT_FAILURE);
}
