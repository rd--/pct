/* src/cmd/ist.c - (c) rohan drape, 2001 */

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>

#include "libpct.h"

static void interval_segment_transpoition(Pco_t *i, Pco_t *p, Pco_t *q)
{
	int j;
	assert(pco_sizeof(i) == pco_sizeof(p));
	for (j = 0; j < pco_sizeof(p); j++) {
		pco_set_element(q, j,
			mod12(pco_element(p, j) + pco_element(i, j)));
	}
	pco_validate(q);
	pco_put(q, stdout);
}

int main(int argc, char **argv)
{
	Pco_t *p, *q, *i;
	if (argc != 2) {
		fprintf(stderr, "usage: %s pcseg\n", argv[0]);
		exit(2);
	}
	p = pco_create("pcseg");
	pco_read(p, argv[1]);
	q = pco_dup(p);
	i = pco_create("iseg");
	while (pco_get(i, stdin)) {
		interval_segment_transpoition(i, p, q);
	}
	return EXIT_SUCCESS;
}
