/* arcs.c - rohan drape, 8/97 */

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "libpct.h"

void usage(char *pname);
void usage(char *pname)
{
	fprintf(stderr, "usage: %s [ -r] [ File ]\n", pname);
	exit(EXIT_FAILURE);
}

int main(int argc, char **argv)
{
	int i, j, k, reverse_op = FALSE;
	Ppca_t *p, *q;
	Pco_t *clm = 0;

	for (i = 1; i < argc; i++) {
		if (argv[i][0] == '-') {
			switch (argv[i][1]) {
			case 'r':
				reverse_op = TRUE;
				break;
			default:
				usage(argv[0]);
				break;
			}
		} else {
			freopen(argv[1], "r", stdin);
		}
	}

	p = paopen("arcs.p");
	q = paopen("arcs.q");
	assert(p && q);
	clm = pco_create("pcseg");

	while (paread(p, stdin)) {
		pacopy(q, p);
		q->r = p->c;
		q->c = p->r;
		for (i = 0; i < p->c; i++) {
			if (reverse_op) {
				pacolumn(p, i, clm, PPCA_READ);
				q->size[q->r - i - 1] = clm->size;
				memcpy(q->data[q->r - i - 1], clm->data,
					clm->size * sizeof(int));
				rtrpcs(q->data[q->r - i - 1], q->size[q->r - i - 1]);
				arrayvert(p->partition, p->r, i,
					q->partition[q->r - i - 1]);
				rtrpcs(q->partition[q->r - i - 1], q->c);
				for (j = 0, k = 0; j < q->c; j++) {
					rtrpcs(&(q->data[q->r - i - 1][k]),
						q->partition[q->r - i - 1][j]);
					k += q->partition[q->r - i - 1][j];
				}
			} else {
				pacolumn(p, i, clm, PPCA_READ);
				q->size[i] = clm->size;
				memcpy(q->data[i], clm->data, clm->size * sizeof(int));
				arrayvert(p->partition, p->r, i, q->partition[i]);
			}
		}
		pawrite(q, stdout);
	}
	paclose(p);
	paclose(q);
	pco_free(clm);

	exit(EXIT_SUCCESS);
	return EXIT_SUCCESS;
}
