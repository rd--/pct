/*
 * sgi.c
 *
 * rohan drape, 6/96
 */

#include <stdio.h>
#include <stdlib.h>

#include "libpct.h"

#define COLLUM_OFFSET 15

int sgi(int *p, int plen);
int printmatrix(int *p, int plen);
int printisubs(int *p, int plen, int vv);
int printbip(FILE *fp, int clm, int n, int *p, int plen);
void usage(char *pname);
void usage(char *pname)
{
	fprintf(stderr, "usage: %s [ pcseg ... ]\n", pname);
	exit(EXIT_FAILURE);
}

int main(int argc, char **argv)
{
	char str[MAX_STR];
	int p[256], plen = 0;
	int i;

	if (argc == 1) {
		while (fgets(str, MAX_STR, stdin) != NULL) {
			if (str2pcs(str, p, &plen)) {
				sgi(p, plen);
			}
		}
	}

	for (i = 1; i < argc; i++) {
		if (!str2pcs(argv[1], p, &plen)) {
			usage(argv[0]);
		}
		sgi(p, plen);
	}

	exit(EXIT_SUCCESS);
	return EXIT_SUCCESS;
}

int sgi(int *p, int plen)
{
	char str[MAX_STR];
	int intv[MAX_PCS];

	pcs2scstr(p, plen, str, 0);
	printf("%s\t", str);

	printpcs(stdout, "P=<", p, plen, ">\n", FALSE);

	pcs2int(p, plen, intv);
	printpcs(stdout, "INT(P)=<", intv, plen - 1, ">\t", FALSE);

	multpcs(5, p, plen);
	printpcs(stdout, "T0MP=<", p, plen, ">\n", FALSE);

	multpcs(5, p, plen);
	printmatrix(p, plen);

	printf("\n");
	printisubs(p, plen, 0);

	return TRUE;
}

/*
 * printmatrix()
 *
 * prints a matrix of p to stdout.
 * maintains p transposition.
 */
int printmatrix(int *p, int plen)
{
	int i;
	int trs;
	int hold;

	hold = p[0];
	trspcs(0 - p[0], p, plen);
	printpcs(stdout, "\n\t", p, plen, "\n", 0);
	for (i = 0; i < plen - 1; i++) {
		trs = p[i] - p[i + 1];
		MOD(trs, 12);
		trspcs(trs, p, plen);
		printpcs(stdout, "\t", p, plen, "\n", 0);
	}
	trspcs(hold - p[0], p, plen);
	return TRUE;
}

/*
 * printisubs()
 *
 * prints the imbricated subcollections
 * of a p, ie the 7 hexachords, the
 * 9 tetrachords and the 10 trichords.
 */
int printisubs(int *p, int plen, int vv)
{
	char str[256];
	int i, j;

	if (vv) {
		for (i = 2; i < plen; i++) {
			printf("Subsets(#%-2d):\n", i);
			printbip(stdout, COLLUM_OFFSET, i, p, plen);
			printf("\n");
		}
	} else {
		for (i = 2; i < plen; i++) {
			printf("#%-2d: ", i);
			for (j = 0; j <= plen - i; j++) {
				pcs2scstr(&p[j], i, str, 3);
				printf("%s ", str);
			}
			printf("\n");
		}
	}

	return TRUE;
}

/*
 * printbip()
 *
 * print the BIPn of p. See Morris "Class
 * Notes" pg. 45-46.
 */
int printbip(FILE *fp, int clm, int n, int *p, int plen)
{
	char str[MAX_STR];
	int i, j;
	int nc;

	if (n > plen - 1) {
		return FALSE;
	}
	for (i = 0; i < (plen - n + 1); i++) {
		pcs2str(p, n, str, FALSE);
		fprintf(fp, "\t<%s>%n", str, &nc);
		for (j = nc; j < clm; j++) {
			fprintf(fp, " ");
		}
		pcs2scstr(p, n, str, 1);
		fprintf(fp, "%s\n", str);
		rotpcs(1, p, plen);
	}
	rotpcs(n - 1, p, plen);
	return TRUE;
}
