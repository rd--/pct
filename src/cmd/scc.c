/* scc.c - (c) rohan drape, 1996 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "libpct.h"

static void usage(char *pname)
{
	fprintf(stderr, "usage: %s sc pcset\n", pname);
	exit(EXIT_FAILURE);
}

int set_class_completion(const int *sc, int sclen, const int *p, int plen)
{
	char str[256];
	int q[12], qlen = 0;
	int s[12], slen = 0;
	int i;

	cpypcs(s, &slen, sc, sclen);
	if (!isabssub(p, plen, s, slen))
		return FALSE;

	for (i = 0; i < 12; i++) {
		if (islitsub(p, plen, s, slen)) {
			qlen = pcs2cmpn(p, plen, s, slen, q);
			srtpcs(q, qlen);
			pcs2str(q, qlen, str, 0);
			printf("%s\n", str);
		}
		trspcs(1, s, slen);
	}
	if (!issym(s, slen)) {
		invpcs(0, s, slen);
		for (i = 0; i < 12; i++) {
			if (islitsub(p, plen, s, slen)) {
				qlen = pcs2cmpn(p, plen, s, slen, q);
				pcs2str(q, qlen, str, 0);
				printf("%s\n", str);
			}
			trspcs(1, s, slen);
		}
	}
	return TRUE;
}

int main(int argc, char **argv)
{
	int s[12], slen; /* reference SC */
	int p[12], plen; /* sub PCS */

	if (argc != 3 || !str2pcs(argv[1], s, &slen) || !str2pcs(argv[2], p, &plen)) {
		usage(argv[0]);
	} else {
		prime(s, &slen, (tni_t *)NULL);
		set_class_completion(s, slen, p, plen);
		return EXIT_SUCCESS;
	}
}
