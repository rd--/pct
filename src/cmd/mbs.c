/*
 * mbs.c
 *
 * rohan drape, 2/96
 */

#include <stdio.h>
#include <stdlib.h>

#include "libpct.h"

void usage(char *pname);
int is2sl(int *iset, int n, int *s, int slen);
int is2slc(int *iset, int n);

int main(int argc, char **argv)
{

	int t[12] = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
	int iset[12][12], ilen[12] = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 }, n = 0;
	int i, j;
	int p[12];
	int s[12], slen = 0;
	int vv = TRUE;

	if (argc < 2) {
		usage(argv[0]);
	}
	for (i = 1; i < argc; i++) {
		if (argv[i][0] != '-') {
			if (!str2pcs(argv[i], iset[n], &ilen[n])) {
				usage(argv[0]);
			}
			remdup(iset[n], &ilen[n]);
			n++;
		}
		j = 1;
		while (argv[i][0] == '-') {
			switch (argv[i][j]) {
			case 's':
				if (!str2pcs(&argv[i][2], s, &slen)) {
					usage(argv[0]);
				}
				break;
			case 'c':
				vv = FALSE;
				break;
			default:
				argv[i][0] = 'x';
				break;
			}
			j++;
		}
	}

	while (t[0] < ilen[0]) {
		printf("\niset={");
		for (i = 0; i < n; i++) {
			printf("%c", i2c(iset[i][t[i]]));
			p[i] = iset[i][t[i]];
		}
		printf("}\n");
		if (vv) {
			is2sl(p, n, s, slen);
		} else {
			is2slc(p, n);
		}
		t[n - 1]++;
		for (i = n - 1; i > 0; i--) {
			if (t[i] == ilen[i]) {
				t[i] = 0;
				t[i - 1]++;
			}
		}
	}
	exit(EXIT_SUCCESS);
	return EXIT_SUCCESS;
}

/*
 * is2sl()
 *
 * iset to set list
 * if slen is not 0, only instances of s are printed
 * Note: referential version of this routine in is2sl.c
 */
int is2sl(int *iset, int n, int *s, int slen)
{
	int tl[12] = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
	int i, j;
	int p[24];
	int plen;
	int nc;
	int print;

	char str[256] = { '\0' };

	remdup(iset, &n);
	while (tl[0] == 0) {
		print = TRUE;
		for (i = 0, j = 0; i < n; i++, j += 2) {
			p[j] = tl[i];
			p[j + 1] = iset[i] + tl[i];
			MOD(p[j + 1], 12);
		}
		plen = n * 2;
		remdup(p, &plen);
		if (slen != 0) {
			if (!isequiv(p, plen, s, slen)) {
				print = FALSE;
			}
		}
		if (print) {
			printf("-");
			for (i = 0; i < n; i++) {
				printf("%c", i2c(tl[i]));
			}
			printf("- ");
			srtpcs(p, plen);
			pcs2str(p, plen, str, FALSE);
			printf("{%s} %n", str, &nc);
			nextcol(20 - nc);
			pcs2scstr(p, plen, str, 1);
			printf("%s\n", str);
		}
		tl[n - 1]++;
		for (i = n - 1; i > 0; i--) {
			if (tl[i] == 12) {
				tl[i] = 0;
				tl[i - 1]++;
			}
		}
	}
	return TRUE;
}

/*
 * is2slc()
 *
 * iset to set list (concise)
 */
int is2slc(int *iset, int n)
{
	char str[256] = { '\0' };
	int tl[12] = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
	int i, j;
	int p[24], plen;
	FILE *temp = tmpfile();

	remdup(iset, &n);
	while (tl[0] == 0) {
		for (i = 0, j = 0; i < n; i++, j += 2) {
			p[j] = tl[i];
			p[j + 1] = iset[i] + tl[i];
			MOD(p[j + 1], 12);
		}
		plen = n * 2;
		remdup(p, &plen);
		if (!prime(p, &plen, (tni_t *)NULL)) {
			die("prime() failed");
		}
		pcs2scstr(p, plen, str, 0);
		if (!strinfile(temp, str)) {
			printf("%s\n", str);
			fprintf(temp, "%s\n", str);
		}
		tl[n - 1]++;
		for (i = n - 1; i > 0; i--) {
			if (tl[i] == 12) {
				tl[i] = 0;
				tl[i - 1]++;
			}
		}
	}
	fclose(temp);
	return TRUE;
}

void usage(char *pname)
{
	fprintf(stderr, "usage: %s [ -sSC ] [ -v ] iset ...\n", pname);
	exit(EXIT_FAILURE);
}
