/* rs.c - rohan drape, 1997 */

#include <stdio.h>
#include <stdlib.h>

#include "libpct.h"

int main(int argc, char **argv)
{
	char pco_type[PCO_TYPMAX] = "pcset";
	Sro_t sro;
	Pco_t *from, *to;

	from = pco_create(pco_type);
	to = pco_create(pco_type);

	if (argc == 3 && pco_read(from, argv[1]) && pco_read(to, argv[2])) {
		if (pco_sro_derive(from, to, &sro)) {
			sro_put(&sro, stdout);
		}
	} else {
		fprintf(stderr, "usage: %s pco pco \n", argv[0]);
		exit(EXIT_FAILURE);
	}

	pco_free(from);
	pco_free(to);

	exit(EXIT_SUCCESS);
	return EXIT_SUCCESS;
}
