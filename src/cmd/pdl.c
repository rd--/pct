/* pdl.c - (c) rohan drape, 1996 */

#include <stdio.h>
#include <stdlib.h>

#include "libpct.h"

int main(int argc, char **argv)
{
	char ln[8192];
	FILE *in = tmpfile(), *tmp1 = tmpfile(), *tmp2 = tmpfile();

	if (argc == 2 && freopen(argv[1], "r", stdin) == NULL)
		die("cannot open input file '%s'", argv[1]);

	if (in == NULL || tmp1 == NULL || tmp2 == NULL)
		die("tempfile unavailable");

	/* problems on klang unless this is done? */
	copyfile(in, stdin);
	rewind(in);

	while (fgets(ln, 8192, in) != NULL) {
		if (ln[0] != '\n' && strinfile(tmp1, ln)) {
			if (!strinfile(tmp2, ln)) {
				fputs(ln, stdout);
				fputs(ln, tmp2);
			}
		} else {
			fputs(ln, tmp1);
		}
	}

	return EXIT_SUCCESS;
}
