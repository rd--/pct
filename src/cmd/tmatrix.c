/* tmatrix.c - rohan drape, 6/96 */

#include <stdio.h>
#include <stdlib.h>

#include "libpct.h"

/*
 * prints the T-matrix of p and q to stdout.
 */
static int tmatrix(int *p, int plen, int h)
{
	char str[MAX_STR];
	int i;
	int trs;

	pcs2str(p, plen, str, FALSE);
	printf("\n%s\n", str);
	for (i = 0; i < plen - 1; i++) {
		trs = p[i] - p[i + 1];
		MOD(trs, 12);
		trspcs(trs, p, plen);
		if (!h) {
			pcs2str(p, plen, str, FALSE);
			printf("%s\n", str);
		} else {
			pcs2str(&p[i + 1], plen - i - 1, str, FALSE);
			nextcol(i + 1);
			printf("%s\n", str);
		}
	}
	return TRUE;
}

static void usage(char *pname)
{
	fprintf(stderr, "usage: %s [ -h ] [ PCSEG ... ]\n", pname);
	exit(EXIT_FAILURE);
}

int main(int argc, char **argv)
{
	char str[MAX_STR];
	int p[256], plen = 0;
	int i;
	int hh = FALSE, rr = TRUE;

	for (i = 1; i < argc; i++) {
		if (argv[i][0] == '-') {
			switch (argv[i][1]) {
			case 'h':
				hh = TRUE;
				break;
			default:
				usage(argv[0]);
				break;
			}
		} else {
			if (!str2pcs(argv[i], p, &plen)) {
				usage(argv[0]);
			}
			tmatrix(p, plen, hh);
			rr = FALSE;
		}
	}

	if (rr) {
		while (fgets(str, MAX_STR, stdin) != NULL) {
			if (str2pcs(str, p, &plen)) {
				tmatrix(p, plen, hh);
			}
		}
	}

	exit(EXIT_SUCCESS);
	return EXIT_SUCCESS;
}
