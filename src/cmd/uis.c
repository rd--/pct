/* uis.c - rohan drape, 2/96 */

#include <stdio.h>
#include <stdlib.h>

#include "libpct.h"

static int print_line(int *p, int plen, int *q, int qlen, int *m, int mlen)
{
	char str[256];
	int nc;
	int s[12], slen;

	pcs2scstr(p, plen, str, 2);
	printf("%s%n", str, &nc);
	nextcol(15 - nc);
	pcs2scstr(q, qlen, str, 2);
	printf("%s%n", str, &nc);
	nextcol(15 - nc);
	pcs2scstr(m, mlen, str, 1);
	printf("%s%n", str, &nc);
	nextcol(25 - nc);
	if (getintsctn(p, plen, q, qlen, s, &slen)) {
		pcs2scstr(s, slen, str, 1);
		printf("%s\n", str);
	} else {
		printf("\n");
	}
	return TRUE;
}

static int union_and_intersection(int *p, int plen, int *q, int qlen)
{
	int m[MAX_PCS], mlen;
	int i;

	remdup(p, &plen);
	prime(p, &plen, NULL);
	remdup(q, &qlen);
	prime(q, &qlen, NULL);

	for (i = 0; i < 12; i++) {
		getmerge(m, &mlen, p, plen, q, qlen);
		print_line(p, plen, q, qlen, m, mlen);
		trspcs(1, q, qlen);
	}
	if (!issym(q, qlen)) {
		invpcs(0, q, qlen);
		for (i = 0; i < 12; i++) {
			getmerge(m, &mlen, p, plen, q, qlen);
			print_line(p, plen, q, qlen, m, mlen);
			trspcs(1, q, qlen);
		}
		invpcs(0, q, qlen);
	}
	if (!isequiv(p, plen, q, qlen)) {
		if (!issym(p, plen)) {
			invpcs(0, p, plen);
			for (i = 0; i < 12; i++) {
				getmerge(m, &mlen, p, plen, q, qlen);
				print_line(p, plen, q, qlen, m, mlen);
				trspcs(1, p, plen);
			}
			invpcs(0, p, plen);
		}
		if (!issym(p, plen) && !issym(q, qlen)) {
			invpcs(0, p, plen);
			invpcs(0, q, qlen);
			for (i = 0; i < 12; i++) {
				getmerge(m, &mlen, p, plen, q, qlen);
				print_line(p, plen, q, qlen, m, mlen);
				trspcs(1, p, plen);
			}
			invpcs(0, p, plen);
			invpcs(0, q, qlen);
		}
	}
	return TRUE;
}

int main(int argc, char **argv)
{
	Pco_t *p = pco_create("sc");
	Pco_t *q = pco_create("sc");

	switch (argc) {
	case 2:
		pco_read(p, argv[1]);
		pco_copy(q, p);
		break;
	case 3:
		pco_read(p, argv[1]);
		pco_read(q, argv[2]);
		break;
	default:
		fprintf(stderr, "usage: %s sc [ sc ]\n", argv[0]);
		exit(EXIT_FAILURE);
		break;
	}
	union_and_intersection(p->data, p->size, q->data, q->size);

	pco_free(p);
	pco_free(q);
	exit(EXIT_SUCCESS);
	return EXIT_SUCCESS;
}
