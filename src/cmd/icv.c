/* icv.c - rohan drape, 1997 */

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "libpct.h"

static void usage(char *pname)
{
	fprintf(stderr, "usage: %s [ -r ] [ pcset | icv ] ... \n", pname);
	exit(EXIT_FAILURE);
}

static int scicv(char *str)
{
	int p[MAX_PCS], plen, v[7];
	if (str2pcs(str, p, &plen)) {
		pcs2icv(p, plen, v);
		pcs2str(v, 7, str, FALSE);
		puts(str);
	}
	return TRUE;
}

static int icvsc(char *pattern_str)
{
	Pco_t a, b;
	FILE *sc_tbl = fopen(sc_lookup, "r");

	pco_set_typeof(&a, sc);
	pco_set_typeof(&b, icv);

	RMNL(pattern_str);
	while (pco_get(&a, sc_tbl)) {
		pco_map(&b, icv, &a);
		if (pco_match(&b, pattern_str)) {
			pco_put_pretty(&a, stdout);
		}
	}

	fclose(sc_tbl);
	return TRUE;
}

int main(int argc, char **argv)
{
	char str[MAX_STR];
	int i, reverse_operation = 0;

	for (i = 1; i < argc; i++) {
		if (argv[i][0] == '-') {
			switch (argv[i][1]) {
			case 'r':
				reverse_operation = TRUE;
				break;
			default:
				usage(argv[0]);
				break;
			}
		} else {
			if (reverse_operation) {
				icvsc(argv[i]);
			} else {
				scicv(argv[i]);
			}
		}
	}

	if (argc == 1 || (argc == 2 && reverse_operation)) {
		while (fgets(str, MAX_STR, stdin)) {
			if (reverse_operation) {
				icvsc(str);
			} else {
				scicv(str);
			}
		}
	}

	exit(EXIT_SUCCESS);
	return EXIT_SUCCESS;
}
