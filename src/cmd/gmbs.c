/*
 * gmbs.c
 *
 * rohan drape, 1997
 */

#include <stdio.h>
#include <stdlib.h>

#include "libpct.h"

void usage(char *pname);
int is2slc(int *iset, int n);

int main(int argc, char **argv)
{

	char str[256];
	int t[12] = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
	int iset[12][12], ilen[12] = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 }, n = 0;
	int i, j;
	int p[12];

	/* parse arguments */
	if (argc < 2) {
		usage(argv[0]);
	}
	for (i = 1; i < argc; i++) {
		if (argv[i][0] != '-') {
			if (!str2pcs(argv[i], iset[n], &ilen[n])) {
				usage(argv[0]);
			}
			remdup(iset[n], &ilen[n]);
			n++;
		}
		j = 1;
		while (argv[i][0] == '-') {
			switch (argv[i][j]) {
			default:
				argv[i][0] = 'x';
				break;
			}
			j++;
		}
	}

	/* put header */
	printf("digraph mbs {\n\n");
	printf("\trankdir = LR;\n");
	printf("\tranksep = 4;\n\n");
	printf("\tsize = \"4,4\";\n");
	printf("\tratio = auto;\n");
	printf("\tcenter = true;\n\n");
	printf("\tnode [shape = ellipse];\n\n");
	printf("\tlabel = \"");
	for (i = 0; i < argc; i++) {
		printf("%s ", argv[i]);
	}
	printf("\";\n\n");

	/* put central cluster of isets */
	printf("\tsubgraph cluster0 {\n");
	for (i = 0; i < n; i++) {
		pcs2str(iset[i], ilen[i], str, FALSE);
		printf("\t\ti%d [label = \"%s\"];\n", i, str);
	}
	printf("\t\tlabel = \"initial isets\";\n");
	printf("\t}\n\n");

	/* put iset->iset edges */
	printf("\n\t/* edge set */\n");
	while (t[0] < ilen[0]) {
		for (i = 0; i < n; i++) {
			p[i] = iset[i][t[i]];
		}
		for (i = 0; i < n; i++) {
			pcs2str(p, n, str, FALSE);
			printf("\ti%d->\"%s\";\n", i, str);
		}
		t[n - 1]++;
		for (i = n - 1; i > 0; i--) {
			if (t[i] == ilen[i]) {
				t[i] = 0;
				t[i - 1]++;
			}
		}
	}
	for (i = 0; i < 12; i++) {
		t[i] = 0;
	}

	/* put iset->sc edges */
	while (t[0] < ilen[0]) {
		for (i = 0; i < n; i++) {
			p[i] = iset[i][t[i]];
		}
		is2slc(p, n);
		t[n - 1]++;
		for (i = n - 1; i > 0; i--) {
			if (t[i] == ilen[i]) {
				t[i] = 0;
				t[i - 1]++;
			}
		}
	}

	/* close it out */
	printf("}\n\n");

	exit(EXIT_SUCCESS);
	return EXIT_SUCCESS;
}

/*
 * is2slc()
 *
 * iset to set list (concise)
 */
int is2slc(int *iset, int n)
{
	char str[256], istr[256];
	int tl[12] = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
	int i, j;
	int p[24], plen;
	FILE *temp = tmpfile();

	remdup(iset, &n);
	pcs2str(iset, n, istr, FALSE);
	while (tl[0] == 0) {
		for (i = 0, j = 0; i < n; i++, j += 2) {
			p[j] = tl[i];
			p[j + 1] = iset[i] + tl[i];
			MOD(p[j + 1], 12);
		}
		plen = n * 2;
		remdup(p, &plen);
		if (!prime(p, &plen, (tni_t *)NULL)) {
			die("prime() failed");
		}
		pcs2scstr(p, plen, str, 0);
		if (!strinfile(temp, str)) {
			printf("\t\"%s\" -> \"%s\";\n", istr, str);
			fprintf(temp, "%s\n", str);
		}
		tl[n - 1]++;
		for (i = n - 1; i > 0; i--) {
			if (tl[i] == 12) {
				tl[i] = 0;
				tl[i - 1]++;
			}
		}
	}
	fclose(temp);
	return TRUE;
}

void usage(char *pname)
{
	fprintf(stderr, "usage: %s [ -sSC ] [ -v ] ISET ...\n", pname);
	exit(EXIT_FAILURE);
}
