/* lsi.c - rohan drape, 2/96 */

#include <stdio.h>
#include <stdlib.h>

#include "libpct.h"

int _print_line(int *p, int plen, int *q, int qlen, int *m, int mlen);
int _print_line(int *p, int plen, int *q, int qlen, int *m, int mlen)
{
	char str[256];
	int o[12], olen;
	int nc;

	pcs2scstr(p, plen, str, 1);
	printf("%s%n", str, &nc);
	nextcol(18 - nc);
	pcs2scstr(q, qlen, str, 1);
	printf("%s%n", str, &nc);
	nextcol(18 - nc);
	pcs2scstr(m, mlen, str, 2);
	printf("%s%n", str, &nc);
	nextcol(12 - nc);
	getintsctn(p, plen, q, qlen, o, &olen);
	pcs2scstr(o, olen, str, 1);
	printf("%s\n", str);
	return TRUE;
}

int main(int argc, char **argv)
{
	FILE *f1 = fopen(sc_lookup, "r");
	FILE *f2 = fopen(sc_lookup, "r");
	int s[12], slen; /* super set */
	int c[12], clen; /* subset cardinality set */
	int p[12], plen;
	int q[12], qlen;
	int m[12], mlen;
	int i;
	long count = 0;

	if (argc != 3 || f1 == NULL || f2 == NULL) {
		fprintf(stderr, "usage: %s SC Cset\n", argv[0]);
		exit(EXIT_FAILURE);
	}
	if (!str2pcs(argv[1], s, &slen)) {
		die("superset invalid");
	}
	if (!str2pcs(argv[2], c, &clen)) {
		die("cardinality set invalid");
	}
	while (getnextpcs(f1, p, &plen, '[', FALSE) != EOF) {
		if (isel(plen, c, clen)) {
			rewind(f2);
			while (getnextpcs(f2, q, &qlen, '[', FALSE) != EOF) {
				if (isel(qlen, c, clen)) {
					for (i = 0; i < 12; i++) {
						getmerge(m, &mlen, p, plen, q, qlen);
						if (isequiv(m, mlen, s, slen)) {
							_print_line(p, plen, q, qlen, m, mlen);
						}
						trspcs(1, q, qlen);
						count++;
					}
					if (!issym(q, qlen)) {
						invpcs(0, q, qlen);
						for (i = 0; i < 12; i++) {
							getmerge(m, &mlen, p, plen, q, qlen);
							if (isequiv(m, mlen, s, slen)) {
								_print_line(p, plen, q, qlen, m, mlen);
							}
							trspcs(1, q, qlen);
							count++;
						}
					}
				}
			}
			if (!issym(p, plen)) {
				invpcs(0, p, plen);
				rewind(f2);
				while (getnextpcs(f2, q, &qlen, '[', FALSE) != EOF) {
					if (isel(qlen, c, clen)) {
						for (i = 0; i < 12; i++) {
							getmerge(m, &mlen, p, plen, q, qlen);
							if (isequiv(m, mlen, s, slen)) {
								_print_line(p, plen, q, qlen, m, mlen);
							}
							trspcs(1, q, qlen);
							count++;
						}
						if (!issym(q, qlen)) {
							invpcs(0, q, qlen);
							for (i = 0; i < 12; i++) {
								getmerge(m, &mlen, p, plen, q, qlen);
								if (isequiv(m, mlen, s, slen)) {
									_print_line(p, plen, q, qlen, m, mlen);
								}
								trspcs(1, q, qlen);
								count++;
							}
						}
					}
				}
			}
		}
	}
	/*fprintf(stderr,"count = %ld\n",count); */
	fclose(f1);
	fclose(f2);
	exit(EXIT_SUCCESS);
	return EXIT_SUCCESS;
}
