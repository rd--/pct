/* sp.c - (c) rohan drape, 1996 */

#include <limits.h>
#include <stdio.h>
#include <stdlib.h>

#include "libpct.h"

static void usage(char *pname)
{
	fprintf(stderr, "usage: %s [ -rN ] [ -m ] pcseg ...\n", pname);
	exit(EXIT_FAILURE);
}

/*+ If r > 0 print only nCr, else print everything. +*/
/*+ If m is 0 print each object seperated by a space, else merge objects into set. +*/
static void
print_objectset_combinations(Pco_t *p[], int pnum, int r, int m)
{
	int i, j, k;
	char str[512];
	Pco_t *X, *Y;
	Cg_t *c;

	Y = pco_create("pcset");
	for (i = 1; i <= pnum; i++) {
		if (r)
			i = r;
		/* init X */
		X = pco_create("pcseg");
		for (j = 0; j < pnum; j++) {
			pco_set_element(X, j, j);
		}
		pco_set_sizeof(X, pnum);
		/* make combinations */
		c = cgopen(pnum, i, pco_data(X));
		while ((k = cgnext(c, pco_data(X)))) {
			pco_set_sizeof(X, k);
			pco_set_sizeof(Y, 0);
			for (j = 0; j < i; j++) {
				if (m)
					pco_union(Y, Y, p[pco_element(X, j)]);
				else {
					pco_write(p[pco_element(X, j)], str);
					printf("%s ", str);
				}
			}
			if (m)
				pco_put(Y, stdout);
			else
				printf("\n");
		}
		cgclose(c);
		pco_free(X);
		if (r)
			break;
	}
	pco_free(Y);
}

#define MAX_OBJ 256
int main(int argc, char **argv)
{
	Pco_t *p[MAX_OBJ];
	int pnum = 0, i, r = 0, m = 1;

	if (argc >= MAX_OBJ)
		die("cg: too many objects");

	for (i = 1; i < argc; i++) {
		if (argv[i][0] == '-') {
			switch (argv[i][1]) {
			case 'r':
				r = atoi(&argv[i][2]);
				break;
			case 'm':
				m = 0;
				break;
			default:
				usage(argv[0]);
				break;
			}
		} else {
			p[pnum] = pco_create("pcseg");
			if (!pco_read(p[pnum], argv[i]))
				usage(argv[0]);
			pnum++;
		}
	}

	print_objectset_combinations(p, pnum, r, m);

	return EXIT_SUCCESS;
}
