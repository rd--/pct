/* sgr.c - rohan drape, 8/97 */

#include <stdio.h>
#include <stdlib.h>

#include "libpct.h"

void usage(char *pname);
void usage(char *pname)
{
	fprintf(stderr, "usage: %s n\n", pname);
	exit(EXIT_FAILURE);
}

int main(int argc, char **argv)
{
	char str[MAX_STR];
	int p[MAX_PCS], plen;
	int q[MAX_PCS], qlen;
	int i, j, n;

	if (argc != 2) {
		usage(argv[0]);
	}
	n = atoi(argv[1]);

	while (fgets(str, MAX_STR, stdin) != NULL) {
		if (str2pcs(str, p, &plen)) {
			qlen = plen;
			for (i = 0, j = 0; i < plen; i++) {
				q[i] = p[j];
				j += n;
				if (plen % 2) {
					MOD(j, plen);
				} else {
					if (j == plen) {
						j += n;
					}
					MOD(j, plen + 1);
				}
			}
			pcs2str(q, qlen, str, FALSE);
			puts(str);
		}
	}

	exit(EXIT_SUCCESS);
	return EXIT_SUCCESS;
}
