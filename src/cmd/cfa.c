/* cfa.c - rohan drape,  9/96 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "libpct.h"

static void usage(char *pname)
{
	fprintf(stderr, "usage: %s [ File ... ]\n", pname);
	exit(EXIT_FAILURE);
}

static int cfa(FILE *fp)
{
	char str[MAX_STR];
	int c[12] = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
	int p[MAX_PCS], plen;
	int nc;

	while (fgets(str, MAX_STR, fp)) {
		if (str2pcs(str, p, &plen)) {
			c[plen - 1]++;
		}
	}
	pcs2str(c, 12, str, 0);
	printf("%s%n", str, &nc);
	nextcol(36 - nc);
	printf("(%d)\n", sumpcs(c, 12));
	return TRUE;
}

int main(int argc, char **argv)
{
	int i;
	int nc;
	FILE *fp;

	if (argc == 2 && strcmp(argv[1], "-h"))
		usage(argv[0]);

	if (argc == 1) {
		cfa(stdin);
	}
	for (i = 1; i < argc; i++) {
		if ((fp = fopen(argv[i], "r")) == NULL) {
			die("%s: cannot open '%s'", argv[0], argv[i]);
		}
		printf("%s:%n", argv[i], &nc);
		nextcol(16 - nc);
		cfa(fp);
		fclose(fp);
	}

	exit(EXIT_SUCCESS);
	return EXIT_SUCCESS;
}
