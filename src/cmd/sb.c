/* sb.c - rohan drape, 2/96 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "libpct.h"

static void usage(char *pname)
{
	fprintf(stderr, "usage: %s { sc ... } | { -l pcset ...} \n", pname);
	fprintf(stderr, "\t -l \t write literal shared subsets \n");
	exit(EXIT_FAILURE);
}

int main(int argc, char **argv)
{
	char obj_type[16] = "sc";
	Pco_t *obj[256], *lkp_obj = 0;
	int n_obj = 0, i;
	FILE *lkp_tbl;

	for (i = 1; i < argc; i++) {
		if (argv[i][0] == '-') {
			switch (argv[i][1]) {
			case 'l':
				strcpy(obj_type, "pcset");
				break;
			default:
				usage(argv[0]);
				break;
			}
		} else {
			obj[n_obj] = pco_create(obj_type);
			pco_read(obj[n_obj], argv[i]);
			n_obj++;
		}
	}

	lkp_obj = pco_create(obj_type);
	if (strcmp(obj_type, "pcset") == 0)
		lkp_tbl = fopen(univ_lookup, "r");
	else
		lkp_tbl = fopen(sc_lookup, "r");

	while (pco_get(lkp_obj, lkp_tbl)) {
		for (i = 0; i < n_obj; i++)
			if (pco_cmp(lkp_obj, obj[i]) != PCO_SUBSET)
				break;
		if (i == n_obj)
			pco_put(lkp_obj, stdout);
	}

	for (i = 0; i < n_obj; i++)
		pco_free(obj[i]);
	pco_free(lkp_obj);
	fclose(lkp_tbl);

	return EXIT_SUCCESS;
}
