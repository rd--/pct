/* sis.c - rohan drape,  6/96 */

#include <stdio.h>
#include <stdlib.h>

#include "libpct.h"

/*
 * prepend the string s1 to each line of output
 */
static int write_bracketed_set(const int *p, int plen, const int *s,
	int slen, char *s1)
{
	char str[256];
	int i;

	for (i = 0; i <= plen - slen; i++) {
		if (isequal(&p[i], slen, s, slen)) {
			pcs2str(p, plen, str, 0);
			insrtchar(str, ']', i + slen);
			insrtchar(str, '[', i);
			printf("%s%s\n", s1, str);
		}
	}
	return TRUE;
}

static void usage(char *pname)
{
	fprintf(stderr, "usage: %s PCSEG SC\n", pname);
	exit(EXIT_FAILURE);
}

int main(int argc, char **argv)
{
	char s1[12] = "";
	int p[256], plen = 0;
	int s[12], slen = 0;
	tr_t *t;

	if (argc != 3 || !str2pcs(argv[1], p, &plen)
		|| !str2pcs(argv[2], s, &slen)) {
		usage(argv[0]);
	}

	t = tr_init(p, plen, 1, 1, 0);
	while (tr_next(t, p, &plen)) {
		write_bracketed_set(p, plen, s, slen, s1);
	}
	tr_free(t);
	return EXIT_SUCCESS;
}
