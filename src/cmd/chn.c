/***** chn.c - (c) rohan drape, 1997 *****/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "libpct.h"

static void usage(char *pname)
{
	fprintf(stderr, "usage: %s sro n\n", pname);
	exit(EXIT_FAILURE);
}

static void chn(Pco_t p, rrtnmi_t o, int d, int verbose)
{
	char str[MAX_STR];
	Pco_t q, s;
	tr_t *t;
	int i, n;

	pco_set_typeof(&q, pcseg);
	pco_set_typeof(&s, pcseg);

	/* make the target overlap segment [NOTE: fix pco.c to include pco_subsegment()] */
	cpypcs(s.data, &s.size, &p.data[p.size - d], d);
	pco_sro_apply(&s, &o);

	/* search all transformations for overlap identity */
	t = tr_init(pco_data(&p), pco_sizeof(&p), 1, 1, 0);
	while (tr_next(t, pco_data(&q), &n)) {
		pco_set_sizeof(&q, n);
		for (i = 0; i < d; i++)
			if (pco_element(&q, i) != pco_element(&s, i))
				break;
		if (i == d) {
			pco_write(&q, str);
			printf("%s", str);
			if (verbose > 0) {
				pco_sro_derive(&p, &q, &o);
				sro_write(&o, str);
				printf(" (%s)", str);
			}
			printf("\n");
		}
	}
	tr_free(t);
	return;
}

int main(int argc, char **argv)
{
	Pco_t p;
	int d, verbose = 1;
	rrtnmi_t o;

	if (argc != 3 || !sro_read(&o, argv[1]))
		usage(argv[0]);
	d = atoi(argv[2]);

	pco_set_typeof(&p, pcseg);
	while (pco_get(&p, stdin))
		chn(p, o, d, verbose);

	return EXIT_SUCCESS;
}
