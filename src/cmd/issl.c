/* issl.c - rohan drape, 2/96 */

#include <stdio.h>
#include <stdlib.h>

#include "libpct.h"

static void usage(char *pname)
{
	fprintf(stderr, "usage: %s [ -ssc ] [ iset ... ] \n", pname);
	exit(EXIT_FAILURE);
}

static int iset_to_set_list(int *iset, int n, int *s, int slen)
{
	int tl[12] = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
	int i, j;
	int p[24];
	int plen;
	int nc;
	int print;

	char str[256] = { '\0' };

	remdup(iset, &n);
	while (tl[0] == 0) {
		print = TRUE;
		for (i = 0, j = 0; i < n; i++, j += 2) {
			p[j] = tl[i];
			p[j + 1] = iset[i] + tl[i];
			MOD(p[j + 1], 12);
		}
		plen = n * 2;
		remdup(p, &plen);
		if (slen != 0) {
			if (!isequiv(p, plen, s, slen)) {
				print = FALSE;
			}
		}
		if (print) {
			printf("-");
			for (i = 0; i < n; i++) {
				printf("%c", i2c(tl[i]));
			}
			printf("- ");
			srtpcs(p, plen);
			pcs2str(p, plen, str, FALSE);
			printf("{%s} %n", str, &nc);
			nextcol(20 - nc);
			pcs2scstr(p, plen, str, 1);
			printf("%s\n", str);
		}
		tl[n - 1]++;
		for (i = n - 1; i > 0; i--) {
			if (tl[i] == 12) {
				tl[i] = 0;
				tl[i - 1]++;
			}
		}
	}
	return TRUE;
}

int main(int argc, char **argv)
{
	char str[MAX_STR];
	int i;
	int tt = TRUE;
	int iset[12], n = 0;
	int s[12], slen = 0;

	for (i = 1; i < argc; i++) {
		if (argv[i][0] == '-') {
			switch (argv[i][1]) {
			case 's':
				if (!str2pcs(&argv[i][2], s, &slen)) {
					usage(argv[0]);
				}
				remdup(s, &slen);
				break;
			default:
				usage(argv[0]);
				break;
			}
		} else {
			if (!str2pcs(argv[i], iset, &n) || n > 12) {
				usage(argv[0]);
			}
			iset_to_set_list(iset, n, s, slen);
			tt = FALSE;
		}
	}

	if (tt) {
		while (fgets(str, MAX_STR, stdin) != NULL) {
			if (str2pcs(str, iset, &n)) {
				iset_to_set_list(iset, n, s, slen);
			}
		}
	}

	exit(EXIT_SUCCESS);
	return EXIT_SUCCESS;
}
