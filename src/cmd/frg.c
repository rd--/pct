/* frg.c - rohan drape, 3/97 */

#include <stdio.h>
#include <stdlib.h>

#include "libpct.h"

static int fragmentation(int *p, int plen, int c /* cycle */)
{
	int i, j;

	c = i2ic(c); /* verify and-or fix input cycle */
	printf("Fragmentation of %d-cycle(s):  ", c);
	if (c == 5) { /* exception handling for c=5 */
		printf("[");
		for (i = 0; i < 12; i++) {
			j = mod12(i * c);
			if (isel(j, p, plen)) {
				printf("%c", i2c(j));
			} else {
				printf("-");
			}
		}
		printf("]\n");
		return TRUE;
	}
	for (j = 0; j < c; j++) {
		printf("[");
		for (i = j; i < 12; i += c) {
			i = mod12(i);
			if (isel(i, p, plen)) {
				printf("%c", i2c(i));
			} else {
				printf("-");
			}
		}
		printf("] ");
	}
	printf("\n");
	return TRUE;
}

/*
 * implementation of the fragmentation of cycles stuff
 * that Buchlers Setmaker produces as a "SET PROPERTY"
 */
static int fragmentation_of_cycles(int *p, int plen)
{
	int i;
	for (i = 1; i <= 6; i++) {
		fragmentation(p, plen, i);
	}
	return TRUE;
}

/*
 * implementation of the IC cycle vector stuff
 * that Buchlers Setmaker produces as a "SET PROPERTY"
 */
static int ic_cycle_vector(int *p, int plen)
{
	int i, j, c, sum, first, prev;

	printf("IC cycle vector: ");
	for (c = 1; c <= 6; c++) {
		printf("<");
		if (c == 5) { /* exception handling for c=5 */
			sum = 0;
			prev = FALSE;
			first = FALSE;
			for (i = 0; i < 12; i++) {
				j = mod12(i * c);
				if (isel(j, p, plen)) {
					if (i == 0) {
						first = TRUE;
					}
					if (prev) {
						sum++;
					}
					if (i == 11 && first) {
						sum++;
					}
					prev = TRUE;
				} else {
					prev = FALSE;
				}
			}
			printf("%d", sum);
		} else {
			for (j = 0; j < c; j++) {
				sum = 0;
				prev = FALSE;
				first = FALSE;
				for (i = j; i < 12; i += c) {
					i = mod12(i);
					if (isel(i, p, plen)) {
						if (i == j) {
							first = TRUE;
						}
						if (prev) {
							sum++;
						}
						if ((i + c) > 11 && first) {
							sum++;
						}
						prev = TRUE;
					} else {
						prev = FALSE;
					}
				}
				printf("%d", sum);
			}
		}
		printf("> ");
	}
	printf("\n");
	return TRUE;
}

static void usage(char *pname)
{
	fprintf(stderr, "usage: %s [ PCS ... ]\n", pname);
	exit(EXIT_FAILURE);
}

int main(int argc, char **argv)
{
	char str[MAX_STR];
	int p[MAX_PCS], plen;
	int i;

	if (argc == 1) {
		while (fgets(str, MAX_STR, stdin) != NULL) {
			if (str2pcs(str, p, &plen)) {
				fragmentation_of_cycles(p, plen);
				ic_cycle_vector(p, plen);
			}
		}
	}

	for (i = 1; i < argc; i++) {
		if (!str2pcs(argv[i], p, &plen) || !fragmentation_of_cycles(p, plen) || !ic_cycle_vector(p, plen)) {
			usage(argv[0]);
		}
	}

	exit(EXIT_SUCCESS);
	return EXIT_SUCCESS;
}
