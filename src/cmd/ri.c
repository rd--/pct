/*
 * ri.c
 *
 * rohan drape, 1/96
 */

#include <stdio.h>
#include <stdlib.h>
/*#include <string.h>*/

#include "libpct.h"

#define COLLUM_OFFSET 20

int b_ri(int argc, char **argv);
int ri(int *row);
int evenRowPrint(int *row);
int printMatrix(int *row);
int printDiscreteSubsets(int *row);
int printImbricatedSubsets(int *row);
int printallpcsinfo(int *pcs, int len);
int printbip(FILE *fp, int clm, int n, int *p, int len);
void usage(char *pname);

int main(int argc, char **argv)
{
	return b_ri(argc, argv);
}

int b_ri(int argc, char **argv)
{
	int p[MAX_PCS], plen;

	if (argc != 2 || !str2pcs(argv[1], p, &plen) || plen != 12 || isdup(p, plen)) {
		usage(argv[0]);
	}

	ri(p);

	exit(EXIT_SUCCESS);
	return EXIT_SUCCESS;
}

int ri(int *row)
{
	int intv[11];

	printpcs(stdout, "P=", row, 12, "", FALSE);

	/* print succesive interval list */
	pcs2int(row, 12, intv);
	printpcs(stdout, "\nINT(P)=", intv, 11, "", FALSE);

	/* print matrix */
	printf("\n\nMatrix:\n");
	printMatrix(row);

	/* print conjunct/discrete pc-sets */
	printf("\n\nDiscrete Subsets:\n");
	printDiscreteSubsets(row);

	/* print imbricated subsets */
	printf("\n\nImbricated Subsets:\n");
	printImbricatedSubsets(row);

	return TRUE;
}

/*
 * prints a matrix of row to stdout.
 * maintains row transposition.
 */
int printMatrix(int *row)
{
	int i;
	int trs;
	int hold;

	hold = row[0];
	trspcs(0 - row[0], row, 12);
	evenRowPrint(row);
	for (i = 0; i < 11; i++) {
		trs = row[i] - row[i + 1];
		MOD(trs, 12);
		trspcs(trs, row, 12);
		evenRowPrint(row);
	}
	trspcs(hold - row[0], row, 12);
	return TRUE;
}

/*
 * prints a row evenly, without demarcation.
 * used by matrix print.
 */
int evenRowPrint(int *row)
{
	int i;

	for (i = 0; i < 12; i++) {
		printf("%2X ", row[i]);
	}
	printf("\n");
	return TRUE;
}

/*
 * prints the discrete subcollections
 * of a row, ie the two hexachords, the
 * three tetrachords and the four trichords.
 */
int printDiscreteSubsets(int *row)
{
	int i;

	/* hexachords */
	printf(" Hexachords:\n");
	for (i = 0; i < 2; i++) {
		printallpcsinfo(row, 6);
		rotpcs(6, row, 12);
	}

	/* tetrachords */
	printf("\n Tetrachords:\n");
	for (i = 0; i < 3; i++) {
		printallpcsinfo(row, 4);
		rotpcs(4, row, 12);
	}

	/* trichords */
	printf("\n Trichords:\n");
	for (i = 0; i < 4; i++) {
		printallpcsinfo(row, 3);
		rotpcs(3, row, 12);
	}
	return TRUE;
}

/*
 * prints the Imbricated subcollections
 * of a row, ie the 7 hexachords, the
 * 9 tetrachords and the 10 trichords.
 */
int printImbricatedSubsets(int *row)
{
	/* hexachords */
	printf(" Hexachords:\n");
	printbip(stdout, COLLUM_OFFSET, 6, row, 12);

	/* tetrachords */
	printf("\n Tetrachords:\n");
	printbip(stdout, COLLUM_OFFSET, 4, row, 12);

	/* trichords */
	printf("\n Trichords:\n");
	printbip(stdout, COLLUM_OFFSET, 3, row, 12);
	return TRUE;
}

/*
 * prints all information about a pcs.
 */
int printallpcsinfo(int *pcs, int len)
{
	int nc;
	char str[256];

	pcs2str(pcs, len, str, FALSE);
	printf("\t<%s>%n", str, &nc);
	nextcol(COLLUM_OFFSET - nc);
	pcs2scstr(pcs, len, str, 1);
	printf("%s\n", str);
	return TRUE;
}

/*
 * print the BIPn of p. See Morris "Class
 * Notes" pg. 45-46.
 */
int printbip(FILE *fp, int clm, int n, int *p, int plen)
{
	int i;
	int j;
	int nc;
	char str[256];

	if (n > 6) {
		return FALSE;
	}
	for (i = 0; i < (plen - n + 1); i++) {
		pcs2str(p, n, str, FALSE);
		fprintf(fp, "\t<%s>%n", str, &nc);
		for (j = nc; j < clm; j++) {
			fprintf(fp, " ");
		}
		pcs2scstr(p, n, str, 1);
		fprintf(fp, "%s\n", str);
		rotpcs(1, p, plen);
	}
	rotpcs(n - 1, p, plen);
	return TRUE;
}

/*
 * usage.
 */
void usage(char *pname)
{
	fprintf(stderr, "usage: %s TTR\n", pname);
	exit(EXIT_FAILURE);
}
