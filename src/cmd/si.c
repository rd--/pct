/* si.c - rohan drape, 1/96 */

#include <stdio.h>
#include <stdlib.h>

#include "libpct.h"

void usage(char *pname);
void usage(char *pname)
{
	fprintf(stderr, "usage: %s [ pcset ... ]\n", pname);
	exit(EXIT_FAILURE);
}

int si(int *p, int plen);
int si(int *p, int plen)
{
	char str[MAX_STR];
	int res[MAX_PCS];

	if (plen < 1) {
		return FALSE;
	}
	srtpcs(p, plen);
	pcs2str(p, plen, str, FALSE);
	printf("pitch-class-set: {%s}\n", str);
	remdup(p, &plen);
	pcs2scstr(p, plen, str, 1);
	printf("set-class: %s\n", str);
	pcs2icv(p, plen, res);
	pcs2str(res, 7, str, FALSE);
	printf("interval-class-vector: [%s]\n", str);
	if (plen < 12) {
		pcs2tics(p, plen, res);
		pcs2str(res, 12, str, FALSE);
		printf("tics: [%s]\n", str);
		pcs2cmpl(p, plen, res);
		pcs2str(res, 12 - plen, str, FALSE);
		printf("complement: {%s} ", str);
		pcs2scstr(res, 12 - plen, str, 2);
		printf("(%s)\n", str);
		multpcs(5, p, plen);
		pcs2str(p, plen, str, FALSE);
		printf("multiplication-by-five-transform: {%s} ", str);
		pcs2scstr(p, plen, str, 2);
		printf("(%s)\n", str);
	}
	return TRUE;
}

int main(int argc, char **argv)
{
	Pco_t *p;
	int i;

	p = pco_create("pcset");

	if (argc == 1) {
		while (pco_get(p, stdin)) {
			si(p->data, p->size);
		}
	}

	for (i = 1; i < argc; i++) {
		if (!pco_read(p, argv[i]) || !si(p->data, p->size)) {
			usage(argv[0]);
		}
	}

	pco_free(p);

	exit(EXIT_SUCCESS);
	return EXIT_SUCCESS;
}
