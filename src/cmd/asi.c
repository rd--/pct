/* asi - rohan drape, 3/98 */

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>

#include "libpct.h"

static void usage(char *pname)
{
	fprintf(stderr,
		"usage: %s [ -a ] [ -b ] [ -c ] [ -p ] [ -r ] [ -v ] [ File ]\n",
		pname);
	exit(EXIT_FAILURE);
}

int main(int argc, char **argv)
{
	char str[256];
	Pco_t *q = pco_create("pcseg");
	int i, j, name_type = 3;
	int clm = 0, pos = 0, row = 0, browse = 0;
	Ppca_t *e;

	for (i = 1; i < argc; i++) {
		if (argv[i][0] == '-') {
			switch (argv[i][1]) {
			case 'a': /* all */
				clm = row = pos = TRUE;
			case 'b': /* browse */
				browse = TRUE;
			case 'c': /* columns */
				clm = TRUE;
				break;
			case 'r': /* rows */
				row = TRUE;
				break;
			case 'p': /* positions */
				pos = TRUE;
				break;
			case 'v': /* verbose */
				name_type = 1;
				break;
			default:
				usage(argv[0]);
				break;
			}
		} else {
			freopen(argv[i], "r", stdin);
		}
	}

	e = paopen("aps");
	assert(e);
	while (paread(e, stdin)) {
		if (browse) {
			pawrite(e, stdout);
			printf("\n");
		}
		if (clm) {
			for (i = 0; i < e->c; i++) {
				pacolumn(e, i, q, PPCA_READ);
				pcs2scstr(q->data, q->size, str, name_type);
				printf("%s:", str);
			}
			printf("\b\n\n");
		}
		if (row) {
			for (i = 0; i < e->r; i++) {
				parow(e, i, q, PPCA_READ);
				pcs2scstr(q->data, q->size, str, name_type);
				printf("%s\n", str);
			}
			printf("\n");
		}
		if (pos) {
			for (i = 0; i < e->r; i++) {
				for (j = 0; j < e->c; j++) {
					paposition(e, i, j, q, PPCA_READ);
					pcs2scstr(q->data, q->size, str, name_type);
					printf("%s:", str);
				}
				printf("\b\n");
			}
			printf("\n");
		}
	}
	paclose(e);
	pco_free(q);
	exit(EXIT_SUCCESS);
	return EXIT_SUCCESS;
}
