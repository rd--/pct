/* epmq.c - rohan drape, 8/97 */

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "libpct.h"

static void usage(char *pname)
{
	fprintf(stderr, "usage: %s [ -fFile ] [ Condition ]\n", pname);
	exit(EXIT_FAILURE);
}

static int epmq(Epm_t *e, FILE *fp)
{
	char str[MAX_STR];
	Pco_t *obj = pco_create("pcseg");
	int error, verbose = FALSE;
	FILE *redirected_fp;

	while (fgets(str, MAX_STR, fp) != NULL) {
		switch (str[0]) {
		case '+':
			epmparse(e, &str[1]);
			break;
		case '-':
			epmrmv(e, &str[1]);
			break;
		case '<':
			RMNL(str); /*fprintf(stderr,"target: '%s'\n",&str[1]); */
			redirected_fp = fopen(&str[1], "r");
			assert(redirected_fp);
			epmq(e, redirected_fp);
			fclose(redirected_fp);
			fprintf(stderr, "'%s' complete\n", &str[1]);
			break;
		case '@':
			epmwrite(e, stderr);
			break;
		case '^':
			epmwrite(e, stdout); /* THIS NEEDS FIXING */
			break;
		case '&':
			if (verbose)
				verbose = FALSE;
			else
				verbose = TRUE;
			break;
		default:
			if (pco_read(obj, str)) {
				error = epmeval(e, obj);
				if (error == -1) {
					fputs(str, stdout);
				} else if (verbose) {
					epmstr(e, error, str);
					printf("failed at: %s\n", str);
				}
			}
			break;
		}
	}
	pco_free(obj);
	return TRUE;
}

int main(int argc, char **argv)
{
	int i;
	FILE *fp;
	Epm_t *e;

	e = epmopen("epmq", "not-proper");
	assert(e);

	for (i = 1; i < argc; i++) {
		if (argv[i][0] == '-') {
			switch (argv[i][1]) {
			case 'f':
				fp = fopen(&argv[i][2], "r");
				assert(fp != NULL);
				epmread(e, fp);
				fclose(fp);
				break;
			case 'p':
				epmsetincltype(e, "proper");
				break;
			default:
				usage(argv[0]);
				break;
			}
		} else {
			if (!epmparse(e, argv[i])) {
				usage(argv[0]);
			}
		}
	}

	epmq(e, stdin);

	epmclose(e);
	exit(EXIT_SUCCESS);
	return EXIT_SUCCESS;
}
