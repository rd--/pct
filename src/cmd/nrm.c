/*
 * nrm.c
 * normalise multiset
 *
 * rohan drape, 8/97
 */

#include <stdio.h>
#include <stdlib.h>

#include "libpct.h"

void usage(char *pname);

int main(int argc, char **argv)
{
	char str[MAX_STR];
	int p[MAX_PCS], plen;
	int i, rr = TRUE;

	if (argc > 1) {
		for (i = 1; i < argc; i++) {
			if (argv[i][0] == '-') {
				argc--;
				switch (argv[i][1]) {
				case 'r':
					rr = FALSE;
					break;
				default:
					usage(argv[0]);
				}
			} else if (str2pcs(argv[i], p, &plen)) {
				if (rr) {
					remdup(p, &plen);
				}
				srtpcs(p, plen);
				pcs2str(p, plen, str, FALSE);
				puts(str);
			} else {
				usage(argv[0]);
			}
		}
	}

	if (argc == 1) {
		while (fgets(str, MAX_STR, stdin)) {
			if (str2pcs(str, p, &plen)) {
				if (rr) {
					remdup(p, &plen);
				}
				srtpcs(p, plen);
				pcs2str(p, plen, str, FALSE);
				puts(str);
			}
		}
	}

	exit(EXIT_SUCCESS);
	return EXIT_SUCCESS;
}

void usage(char *pname)
{
	fprintf(stderr, "usage: %s [ pcset ... ]\n", pname);
	exit(EXIT_FAILURE);
}
