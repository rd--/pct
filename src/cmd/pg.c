/* src/cmd/pg.c */

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>

#include "libpct.h"

static void print_object_permutations(Pco_t *obj)
{
	Pg_t *e;

	e = pgopen(obj->data, obj->size);
	assert(e);
	while ((obj->size = pgnext(e, obj->data))) {
		pco_put(obj, stdout);
	}
	pgclose(e);
}

int main(int argc, char **argv)
{
	Pco_t *p;
	int i;

	p = pco_create("pcseg");
	if (argc > 1) {
		for (i = 1; i < argc; i++) {
			if (pco_read(p, argv[i])) {
				print_object_permutations(p);
			} else {
				fprintf(stderr, "usage: %s [ pcseg ... ]\n", argv[0]);
			}
		}
	} else {
		while (pco_get(p, stdin)) {
			print_object_permutations(p);
		}
	}
	pco_free(p);
	exit(EXIT_SUCCESS);
	return EXIT_SUCCESS;
}
