/***** trs.c - (c) rohan drape, 1996 *****/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "libpct.h"

static void usage(void)
{
	fprintf(stderr, "usage: trs [ -m ] pcseg\n");
	fprintf(stderr, "\t -m \t exclude M5 operation\n");
	exit(EXIT_FAILURE);
}

/*+
  Prints the transformations of p which include the segment s.
  m determines if M5 is a valid operation.
  +*/
static void abstract_segmental_inclusion(Pco_t *p, Pco_t *s, int m)
{
	Pco_t *q;
	tr_t *t;
	int c, n;

	q = pco_create("pcseg");
	t = tr_init(pco_data(p), pco_sizeof(p), 1, m, 0);

	while (tr_next(t, pco_data(q), &n)) {
		pco_set_sizeof(q, n);
		c = pco_cmp(s, q);
		if (c == PCO_SUBSET || c == PCO_EQUAL)
			pco_put(q, stdout);
	}

	tr_free(t);
	pco_free(q);
}

int main(int argc, char **argv)
{
	Pco_t p, s;
	int m = 1; /* M5 inclusion flag */
	int i;

	pco_set_typeof(&p, pcseg);
	pco_set_typeof(&s, pcseg);

	for (i = 1; i < argc; i++) {
		if (strcmp(argv[i], "-h") == 0)
			usage();
		else if (strcmp(argv[i], "-m") == 0)
			m = 0;
		else
			pco_read(&p, argv[i]);
	}

	while (pco_get(&s, stdin)) {
		abstract_segmental_inclusion(&p, &s, m);
	}

	return EXIT_SUCCESS;
}
