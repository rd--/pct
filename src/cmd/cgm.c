/* src/cmd/cgm.c - (c) rohan drape, 1996 */

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>

#include "libpct.h"

#define SET_LIMIT 512

static void print_combination_unions(int R, Pco_t **p, int n)
{
	int i;
	int x[SET_LIMIT], y[SET_LIMIT], y_n;
	char x_str[SET_LIMIT][MAX_STR], s_str[MAX_STR];
	Pco_t *s;
	Cg_t *c;

	for (i = 0; i <= n; i++) {
		x[i] = i;
	}

	s = pco_create("pcset");
	c = cgopen(n, R, x);
	while ((y_n = cgnext(c, y))) {
		assert(y_n == R);
		pco_set_sizeof(s, 0);
		for (i = 0; i < R; i++) {
			pco_cat(s, p[y[i]]);
			pco_write(p[y[i]], x_str[i]);
		}
		pco_write(s, s_str);
		printf("%s\t; ", s_str);
		for (i = 0; i < R; i++) {
			printf("%s ", x_str[i]);
		}
		printf("\n");
	}
	cgclose(c);
	pco_free(s);
}

int main(int argc, char **argv)
{
	Pco_t *p[SET_LIMIT];
	int i, R, n = 0;

	if (argc < 4 || argc > SET_LIMIT) {
		fprintf(stderr, "usage: %s R pcset pcset... \n", argv[0]);
		exit(2);
	}

	R = atoi(argv[1]);
	for (i = 2; i < argc; i++) {
		p[n] = pco_create("pcset");
		if (!pco_read(p[n], argv[i])) {
			fprintf(stderr, "invalid argument:%s\n", argv[i]);
			exit(2);
		}
		n++;
	}

	print_combination_unions(R, p, n);

	return EXIT_SUCCESS;
}
