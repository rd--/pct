/* sgdb.c - rohan drape,  1996 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "libpct.h"

static void usage(char *pname)
{
	fprintf(stderr, "usage: %s [ -a ] [ -s ] [ pcseg ... ]\n", pname);
	exit(EXIT_FAILURE);
}

static int sgdb(int *p, int plen, int ss, int aa)
{
	char str[MAX_STR], db_name[FILENAME_MAX];
	int q[MAX_PCS], qlen;
	FILE *db_fp;
	rrtnmi_t o;

	strcpy(db_name, pct_data_dir);
	strcat(db_name, "/sgdb");
	db_fp = fopen(db_name, "r");
	if (!db_fp)
		return 0;

	while (fgets(str, MAX_STR, db_fp) != NULL) {
		if (str2pcs(str, q, &qlen)) {
			if (!ss && plen == qlen && get_sro(p, plen, q, qlen, &o)) {
				if (aa || (o.rt == 0 && !o.m)) {
					fputs(str, stdout);
				}
			}
			if (ss && isass(p, plen, q, qlen)) {
				fputs(str, stdout);
			}
		}
	}

	fclose(db_fp);
	return TRUE;
}

int main(int argc, char **argv)
{
	char str[MAX_STR];
	int p[MAX_PCS], plen;
	int i;
	int sub_segment = FALSE, all_operators = FALSE, read_stdin = TRUE;

	for (i = 1; i < argc; i++) {
		if (argv[i][0] == '-') {
			switch (argv[i][1]) {
			case 'a':
				all_operators = TRUE;
				break;
			case 's':
				sub_segment = TRUE;
				break;
			default:
				usage(argv[0]);
				break;
			}
		} else {
			read_stdin = FALSE;
			if (str2pcs(argv[i], p, &plen)) {
				sgdb(p, plen, sub_segment, all_operators);
			} else {
				usage(argv[0]);
			}
		}
	}

	if (read_stdin) {
		while (fgets(str, MAX_STR, stdin) != NULL) {
			if (str2pcs(str, p, &plen)) {
				sgdb(p, plen, sub_segment, all_operators);
			}
		}
	}

	exit(EXIT_SUCCESS);
	return EXIT_SUCCESS;
}
