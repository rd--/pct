/* scfa.c - rohan drape, 2/96 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "libpct.h"

#define COLLUMN_WIDTH 20
#define MAX_LINES 1024
#define MAX_CHARS 2056

typedef struct {
	long value;
	long line;
} sortData;

int sortilist(char *fname);
int getln(char *line, long num, long maxc, FILE *fp);
int getninc(FILE *fp, int *inc, int len, int *ntst, char open, int sep);
int getnocc(FILE *fp, int *inc, int len, int *ntst, char open, int sep);
void usage(char *pname);

/*
 * comparison routine for qsort
 */
int listcomp(const void *n1, const void *n2);
int listcomp(const void *n1, const void *n2)
{
	return (int)(*((long *)n2) - *((long *)n1));
}

int main(int argc, char **argv)
{
	int p[12];
	int plen;
	int i;
	int nfnd; /* num found */
	int ntst; /* num tested */
	int nc;
	int vv = FALSE;
	int sort = FALSE;
	int sep = FALSE;
	int inc = FALSE;

	char pcsstr[256];
	char fname[256];
	char sortname[256];
	char open_marker = '{';

	FILE *sc_tbl = fopen(sc_lookup, "r");
	FILE *temp = tmpfile();

	double pct;

	for (i = 1; i < argc; i++) {
		if (argv[i][0] == '-') {
			switch (argv[i][1]) {
			case 'f':
				i++;
				if ((sscanf(argv[i], "%s", fname)) != 1) {
					usage(argv[0]);
				}
				if (freopen(fname, "r", sc_tbl) == NULL) {
					die("-f option sc table redirection failed");
				}
				break;
			case 'i':
				inc = TRUE;
				break;
			case 'o':
				open_marker = argv[i][2];
				if (!isopen(open_marker)) {
					die("open marker redefinition invalid");
				}
				break;
			case 'r':
				sort = TRUE;
				i++;
				if ((sscanf(argv[i], "%s", sortname)) != 1) {
					usage(argv[0]);
				}
				if (freopen(sortname, "w", stdout) == NULL) {
					die("-r option stdout redirection failed");
				}
				break;
			case 's':
				sep = TRUE;
				break;
			case 'v':
				vv = TRUE;
				break;
			case 'h':
			default:
				usage(argv[0]);
				break;
			}
		} else {
			if (freopen(argv[i], "r", stdin) == NULL) {
				die("couldnt open input file '%s'", argv[i]);
			}
		}
	}
	if (sc_tbl == NULL || temp == NULL) {
		die("sc_tbl lookup file unavailable");
	}
	copyfile(temp, stdin);
	if (inc) {
		printf("Test run for abstract inclusion within scs\n");
	}
	while (getnextpcs(sc_tbl, p, &plen, '[', FALSE) != EOF) {
		if (inc) {
			nfnd = getninc(temp, p, plen, &ntst, open_marker, sep);
		} else {
			nfnd = getnocc(temp, p, plen, &ntst, open_marker, sep);
		}
		if (nfnd > 0 || vv) {
			pcs2scstr(p, plen, pcsstr, 0);
			printf("%s%n", pcsstr, &nc);
			nextcol(COLLUMN_WIDTH - nc);
			printf("%3d%n", nfnd, &nc);
			nextcol(COLLUMN_WIDTH - nc);
			pct = (double)nfnd / (double)ntst;
			pct *= 100.0;
			printf("%6.2f%%\n", pct);
		}
	}
	if (sort) {
		fclose(stdout);
		if (!sortilist(sortname)) {
			die("sort failed");
		}
	}
	fclose(sc_tbl);
	fclose(temp);
	exit(EXIT_SUCCESS);
	return EXIT_SUCCESS;
}

/*
 * getninc()
 *
 * get number of inclusions
 */
int getninc(FILE *fp, int *inc, int len, int *ntst, char open, int sep)
{
	int num_found = 0;
	int p[12];
	int plen;
	long hold;

	*ntst = 0;
	hold = ftell(fp);
	fseek(fp, 0, SEEK_SET);
	while (getnextpcs(fp, p, &plen, open, sep) != -1) {
		*ntst += 1;
		if (isabssub(inc, len, p, plen)) {
			num_found += 1;
		}
	}
	fseek(fp, hold, SEEK_SET);
	return num_found;
}

/*
 * getnocc()
 *
 * get number of occurences
 */
int getnocc(FILE *fp, int *occ, int len, int *ntst, char open, int sep)
{
	int num_found = 0;
	int p[12];
	int plen;
	long hold;

	*ntst = 0;
	hold = ftell(fp);
	fseek(fp, 0, SEEK_SET);
	while (getnextpcs(fp, p, &plen, open, sep) != -1) {
		*ntst += 1;
		if (isequiv(occ, len, p, plen)) {
			num_found += 1;
		}
	}
	fseek(fp, hold, SEEK_SET);
	return num_found;
}

/*
 * sortilist()
 *
 * sort inclusions list
 */
int sortilist(char *fname)
{
	FILE *ifp = fopen(fname, "r");
	FILE *ofp = fopen(strcat(fname, ".srt"), "w");
	char line[MAX_CHARS];
	long cline;
	long i;
	sortData sortArray[MAX_LINES];

	if (ifp == NULL || ofp == NULL) {
		die("sort file open failure");
	}

	/*
	 * load sortArray values
	 */
	cline = 0;
	fseek(ifp, 0, SEEK_SET);
	while (fgets(line, MAX_CHARS, ifp) != NULL) {
		sscanf(line, "%*s %ld", &(sortArray[cline].value));
		sortArray[cline].line = cline;
		cline += 1;
		if (cline >= MAX_LINES) {
			die("file to large to sort");
		}
	}

	qsort(&sortArray[0], cline, sizeof(sortData), listcomp);

	for (i = 0; i < cline; i++) {
		getln(line, sortArray[i].line, MAX_CHARS, ifp);
		fputs(line, ofp);
	}

	fclose(ifp);
	fclose(ofp);
	return TRUE;
}

/*
 * getln()
 *
 * put line 'num' of the file
 * at fp into 'line', where zero
 * is the first line.
 */
int getln(char *line, long num, long maxc, FILE *fp)
{
	fseek(fp, 0, SEEK_SET);
	fgets(line, (int)maxc, fp);
	while (num--) {
		if (fgets(line, (int)maxc, fp) == NULL) {
			return FALSE;
		}
	}
	return TRUE;
}

void usage(char *pname)
{
	fprintf(stderr,
		"usage: %s [ -i ] [ -oCharacter ] [ -rFile ] [ -s ] [ -v ] File\n\n",
		pname);
	fprintf(stderr, "Options:  -i   test for abstract inclusion\n");
	fprintf(stderr,
		"         -o[] redefine the open marker (default is '{')\n");
	fprintf(stderr,
		"         -r[] flag a sort of the output by frequency;\n");
	fprintf(stderr,
		"         -s   PCSs are internally seperated by commas (ie {0,1,2})\n");
	fprintf(stderr,
		"         -v   verbose: all tests will be printed, not\n");
	fprintf(stderr, "              only those succesful.\n");
	exit(EXIT_FAILURE);
}
