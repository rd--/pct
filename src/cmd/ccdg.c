/*
 * cg.c
 * concordance generator
 *
 * rohan drape, 5/96
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void cg(FILE *fp, char *fname);

int main(int argc, char **argv)
{
	int i;

	if (argc == 1) {
		cg(stdin, NULL);
	}
	for (i = 1; i < argc; i++) {
		if ((freopen(argv[i], "r", stdin)) == NULL) {
			fprintf(stderr, "%s: freopen() failed on the file '%s'\n",
				argv[0], argv[i]);
			fprintf(stderr, "usage: %s [ File ... ]\n", argv[0]);
			exit(EXIT_FAILURE);
		} else {
			cg(stdin, NULL);
		}
	}
	exit(EXIT_SUCCESS);
	return EXIT_SUCCESS;
}

/*
 * cg()
 *
 * generate a simple concordance
 * for the file at fp showing the
 * line and place number for
 * each word found. if fname
 * is not equal to NULL, it is
 * prepended to each line of output.
 */
void cg(FILE *fp, char *fname)
{
	char *c;
	char ln[512];
	int i = 0, j;

	while (fgets(ln, 512, fp) != NULL) {
		if (ln[0] == '\n') {
			continue;
		}
		i++;
		j = 1;
		if ((c = strtok(ln, ".,:; ()[]{}<>\"'\t\n/")) == NULL) {
			continue;
		}
		if (fname == NULL) {
			printf("%s %d,%d\n", c, i, j);
		} else {
			printf("%s \"%s\",%d,%d\n", c, fname, i, j);
		}
		while ((c = strtok(NULL, ".,:; ()[]{}<>\"'\t\n/")) != NULL) {
			j++;
			if (fname == NULL) {
				printf("%s %d,%d\n", c, i, j);
			} else {
				printf("%s\"%s\",%d,%d\n", c, fname, i, j);
			}
		}
	}
}
