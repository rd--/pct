/*
 * mg.c
 *
 * rohan drape, 2/96
 */

#include <stdio.h>
#include <stdlib.h>

#include "libpct.h"

void usage(char *pname);

int main(int argc, char **argv)
{
	char str[256];
	int sub[12][12], sublen[12];
	int numsub;
	int merge[12], mergelen;
	int i;

	if (argc < 2 || argc > 13) {
		usage(argv[0]);
	}

	numsub = 0;
	for (i = 1; i < argc; i++) {
		if (!str2pcs(argv[i], sub[i - 1], &sublen[i - 1])) {
			usage(argv[0]);
		}
		remdup(sub[i - 1], &sublen[i - 1]);
		numsub += 1;
	}

	mergelen = 0;
	for (i = 0; i < numsub; i++) {
		mergein(merge, &mergelen, sub[i], sublen[i]);
	}
	srtpcs(merge, mergelen);
	pcs2str(merge, mergelen, str, FALSE);
	printf("%s\n", str);
	exit(EXIT_SUCCESS);
	return EXIT_SUCCESS;
}

void usage(char *pname)
{
	fprintf(stderr, "usage: %s PCS ...\n", pname);
	exit(EXIT_FAILURE);
}
