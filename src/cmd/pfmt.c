/* pfmt.c - (c) rohan drape, 1997 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "libpct.h"

int main(int argc, char **argv)
{
	char str[MAX_STR];
	char sep[MAX_STR] = " ";
	int p[MAX_PCS], plen = 0, last = -1, n = 0, cnt = 0, empty_stream = 1;
	if (argc == 2) {
		n = atoi(argv[1]);
	}
	while (fgets(str, MAX_STR, stdin)) {
		empty_stream = 1;
		RMNL(str);
		cnt++;
		if (n) {
			printf("%s", str);
			if (cnt == n) {
				printf("\n");
				cnt = 0;
			} else
				printf("%s", sep);
		} else {
			if (str2pcs(str, p, &plen)) {
				if (plen != last && last != -1) {
					printf("\n%s%s", str, sep);
				} else
					printf("%s%s", str, sep);
				last = plen;
			}
		}
	}
	if ((n && !empty_stream && n != cnt) || (!n && last == plen)) {
		printf("\n");
	}
	return EXIT_SUCCESS;
}
