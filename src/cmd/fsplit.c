/*
 * fsplit.c
 * splits tokens at stdin into files based upon str equality to n places
 *
 * rohan drape, 12/96
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void usage(char *pname);
void usage(char *pname)
{
	fprintf(stderr, "usage: %s n\n", pname);
	exit(EXIT_FAILURE);
}

int main(int argc, char **argv)
{
	char str[256], token[256];
	int n;
	FILE *fp;

	if (argc != 2) {
		usage(argv[0]);
	}
	n = atoi(argv[1]);

	while (fscanf(stdin, "%s", token) == 1) {
		strncpy(str, token, n);
		str[n] = '\0';
		strcat(str, ".spl\0");
		/*printf("token is: %s\nfilename is: %s\n",token,str); */
		fp = fopen(str, "a");
		fprintf(fp, "%s\n", token);
		fclose(fp);
	}

	exit(EXIT_SUCCESS);
	return EXIT_SUCCESS;
}
