/* isegsc.c - rohan drape, 8/97 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "libpct.h"

static void usage(char *pname)
{
	fprintf(stderr, "usage: %s [ -l ]\n", pname);
	exit(EXIT_FAILURE);
}

int main(int argc, char **argv)
{
	char str[MAX_STR];
	int s[MAX_PCS], slen = 0;
	int p[MAX_PCS], plen = 0;
	int i, literal = 0;

	for (i = 1; i < argc; i++) {
		if (strcmp(argv[i], "-l") == 0) {
			literal = 1;
		} else {
			usage(argv[0]);
		}
	}

	while (fgets(str, 256, stdin)) {
		if (str2pcs(str, s, &slen)) {
			plen = slen + 1;
			p[0] = 0;
			for (i = 0; i < slen; i++) {
				p[i + 1] = mod12(p[i] + s[i]);
			}
			literal ? pcs2str(p, plen, str, FALSE) : pcs2scstr(p, plen, str, 3);
			puts(str);
		}
	}

	exit(EXIT_SUCCESS);
	return EXIT_SUCCESS;
}
