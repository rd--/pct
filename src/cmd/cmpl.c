/* cmpl.c - rohan drape, 8/97 */

#include <stdio.h>
#include <stdlib.h>

#include "libpct.h"

static void usage(char *pname)
{
	fprintf(stderr, "usage: %s [ pcset ... ]\n", pname);
	exit(EXIT_FAILURE);
}

int main(int argc, char **argv)
{
	Pco_t *p, *q;
	int i;

	p = pco_create("pcset");
	q = pco_create("pcset");

	if (argc > 1) {
		for (i = 1; i < argc; i++) {
			if (pco_read(p, argv[i])) {
				pco_complement(q, p);
				pco_put(q, stdout);
			} else {
				usage(argv[0]);
			}
		}
	} else {
		while (pco_get(p, stdin)) {
			pco_complement(q, p);
			pco_put(q, stdout);
		}
	}

	exit(EXIT_SUCCESS);
	return EXIT_SUCCESS;
}
