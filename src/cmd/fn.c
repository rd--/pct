/* fn.c - rohan drape, 8/97 */

#include <stdio.h>
#include <stdlib.h>

#include "libpct.h"

static void usage(char *pname)
{
	fprintf(stderr, "usage: %s [ -t ] [ -v ] [ pcset ... ]\n", pname);
	exit(EXIT_FAILURE);
}

int main(int argc, char **argv)
{
	char str[MAX_STR];
	int p[MAX_PCS], plen;
	int i, format = 3, read = 1;

	for (i = 1; i < argc; i++) {
		if (argv[i][0] == '-') {
			switch (argv[i][1]) {
			case 'v':
				format = 0;
				break;
			case 't':
				format = 4;
				break;
			default:
				usage(argv[0]);
				break;
			}
		} else if (str2pcs(argv[i], p, &plen)) {
			pcs2scstr(p, plen, str, format);
			puts(str);
			read = 0;
		} else {
			usage(argv[0]);
		}
	}

	if (read) {
		while (fgets(str, MAX_STR, stdin)) {
			if (str2pcs(str, p, &plen)) {
				pcs2scstr(p, plen, str, format);
				puts(str);
			}
		}
	}

	exit(EXIT_SUCCESS);
	return EXIT_SUCCESS;
}
