/* adms.c - rohan drape,  5/98 */

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>

#include "libpct.h"

static void usage(char *pname)
{
	fprintf(stderr, "usage: %s [ -r | -c | -p ]\n", pname);
	exit(EXIT_FAILURE);
}

int main(int argc, char **argv)
{
	char option = 'a';
	int i, p;
	Ppca_t *e;

	for (i = 1; i < argc; i++) {
		if (argv[i][0] == '-' && argv[i][1] != 'h') {
			option = argv[i][1];
		} else {
			usage(argv[0]);
		}
	}

	e = paopen("adms");
	assert(e);
	while (paread(e, stdin)) {
		switch (option) {
		case 'r':
			printf("%d\n", e->r);
			break;
		case 'c':
			printf("%d\n", e->c);
			break;
		case 'p':
			for (p = 0, i = 0; i < e->r; i++) {
				p += e->size[i];
			}
			printf("%d\n", p);
			break;
		case 'a':
			for (p = 0, i = 0; i < e->r; i++) {
				p += e->size[i];
			}
			printf("%d %d %d\n", e->r, e->c, p);
			break;
		default:
			usage(argv[0]);
			break;
		}
	}
	paclose(e);

	exit(EXIT_SUCCESS);
	return EXIT_SUCCESS;
}
