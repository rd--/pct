/***** ess.c - (c) rohan drape, 1997 *****/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "libpct.h"

static void usage(char *pname)
{
	fprintf(stderr, "usage: %s [ -m ] pcseg\n", pname);
	exit(EXIT_FAILURE);
}

static void embedded_segments(Pco_t *p, Pco_t *s, int m)
{
	Pco_t *q;
	tr_t *t;
	int n;

	q = pco_create("pcseg");
	t = tr_init(pco_data(p), pco_sizeof(p), 1, m, 0);

	while (tr_next(t, pco_data(q), &n)) {
		pco_set_sizeof(q, n);
		if (segemb(pco_data(s), pco_sizeof(s), pco_data(q), pco_sizeof(q))) {
			pco_put(q, stdout);
		}
	}

	tr_free(t);
	pco_free(q);
}

int main(int argc, char **argv)
{
	Pco_t p, s;
	int m = 1; /* M5 inclusion flag */
	int i;

	pco_set_typeof(&p, pcseg);
	pco_set_typeof(&s, pcseg);

	for (i = 1; i < argc; i++) {
		if (strcmp(argv[i], "-h") == 0)
			usage(argv[0]);
		else if (strcmp(argv[i], "-m") == 0)
			m = 0;
		else
			pco_read(&p, argv[i]);
	}

	while (pco_get(&s, stdin)) {
		embedded_segments(&p, &s, m);
	}

	return EXIT_SUCCESS;
}
