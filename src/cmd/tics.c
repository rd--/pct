/*
 * tics.c
 * tics vector
 *
 * rohan drape, 8/97
 */

#include <stdio.h>
#include <stdlib.h>

#include "libpct.h"

void usage(char *pname);

int main(int argc, char **argv)
{
	char str[MAX_STR];
	int p[MAX_PCS], plen, v[12];
	int i;

	if (argc > 1) {
		for (i = 1; i < argc; i++) {
			if (str2pcs(argv[i], p, &plen)) {
				prime(p, &plen, NULL);
				pcs2tics(p, plen, v);
				pcs2str(v, 12, str, FALSE);
				puts(str);
			} else {
				usage(argv[0]);
			}
		}
	} else {
		while (fgets(str, MAX_STR, stdin)) {
			if (str2pcs(str, p, &plen)) {
				prime(p, &plen, NULL);
				pcs2tics(p, plen, v);
				pcs2str(v, 12, str, FALSE);
				puts(str);
			}
		}
	}

	exit(EXIT_SUCCESS);
	return EXIT_SUCCESS;
}

void usage(char *pname)
{
	fprintf(stderr, "usage: %s [ pcset ... ]\n", pname);
	exit(EXIT_FAILURE);
}
