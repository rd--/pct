/* afmt.c - rohan drape,  9/97 */

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "libpct.h"

static void usage(char *pname)
{
	fprintf(stderr, "%s [ File ... ]\n", pname);
	exit(EXIT_FAILURE);
}

/* returns control when a close brace is found */
static int proc_array_desc(FILE *ifp, FILE *ofp)
{
	char s1[MAX_STR], s2[MAX_STR], s3[MAX_STR];
	int p[MAX_PCS], plen;
	Ppca_t *e;
	rrtnmi_t o;

	if (!(e = paopen("proccm"))) {
		return FALSE;
	}
	if (!fgets(s1, MAX_STR, ifp) || !str2pcs(s1, p, &plen)) {
		return FALSE;
	}

	e->r = 0;
	e->c = 0;
	while (fgets(s3, MAX_STR, ifp)) {
		if (sscanf(s3, "%s %s", s1, s2) == 2) {
			if (!sro_read(&o, s1)) {
				paclose(e);
				return FALSE;
			}
			cpypcs(e->data[e->r], &(e->size[e->r]), p, plen);
			do_rrtnmi(o, e->data[e->r], e->size[e->r]);
			str2pcs(s2, e->partition[e->r], &(e->c));
			e->r++;
		} else {
			if (e->r != 0) {
				pawrite(e, ofp);
				e->r = 0;
				e->c = 0;
				fprintf(ofp, "\n\n");
			}
		}
		if (s3[0] == '}') {
			break;
		}
	}
	return TRUE;
}

static int proc_obj_desc(FILE *ifp, FILE *ofp)
{
	char s1[MAX_STR], s2[MAX_STR];

	while (fgets(s1, MAX_STR, ifp) != NULL) {
		if (sscanf(s1, "{ %s", s2) == 1) {
			if (strcmp(s2, "array") == 0) {
				proc_array_desc(ifp, ofp);
			} else {
				fprintf(stderr, "error: unknown identifier (%s)\n", s2);
				return FALSE;
			}
		}
	}
	return TRUE;
}

int main(int argc, char **argv)
{
	int i, rv;

	if (argc == 1) {
		rv = proc_obj_desc(stdin, stdout);
		assert(rv);
	}

	for (i = 1; i < argc; i++) {
		if (freopen(argv[i], "r", stdin) == NULL) {
			usage(argv[0]);
		} else {
			rv = proc_obj_desc(stdin, stdout);
			assert(rv);
		}
	}

	exit(EXIT_SUCCESS);
	return EXIT_SUCCESS;
}
