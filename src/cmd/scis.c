/*
 * scis.c
 * SC in PCSEG
 *
 *
 * rohan drape,  6/96
 */

#include <stdio.h>
#include <stdlib.h>

#include "libpct.h"

int bscip(const int *p, int plen, const int *s, int slen);
void usage(char *pname);

int main(int argc, char **argv)
{
	int p[256], plen = 0;
	int s[12], slen = 0;

	if (argc != 3 || !str2pcs(argv[1], p, &plen)
		|| !str2pcs(argv[2], s, &slen)) {
		usage(argv[0]);
	}

	printf("P:  ");
	bscip(p, plen, s, slen);
	invpcs(p[0], p, plen);
	printf("\nI:  ");
	bscip(p, plen, s, slen);
	invpcs(p[0], p, plen);
	multpcs(5, p, plen);
	printf("\nM:  ");
	bscip(p, plen, s, slen);
	multpcs(5, p, plen);
	multpcs(7, p, plen);
	printf("\nMI: ");
	bscip(p, plen, s, slen);
	printf("\n");

	exit(EXIT_SUCCESS);
	return EXIT_SUCCESS;
}

/*
 * bscip()
 * bracket SC in PCSEG
 *
 * bracket the SC s where it
 * occurs in the PCSEG p
 */
int bscip(const int *p, int plen, const int *s, int slen)
{
	char str[256];
	int i;

	for (i = 0; i <= plen - slen; i++) {
		if (isequiv(&p[i], slen, s, slen)) {
			pcs2str(p, plen, str, 0);
			insrtchar(str, ']', i + slen);
			insrtchar(str, '[', i);
			printf("%s ", str);
		}
	}
	return TRUE;
}

void usage(char *pname)
{
	fprintf(stderr, "usage: %s PCSEG SC\n", pname);
	exit(EXIT_FAILURE);
}
