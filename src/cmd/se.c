/*
 * se.c
 *
 * set expansion
 * rohan drape, 5/96
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "libpct.h"

/*
 * se()
 *
 * perform set expansion to cardinalities
 * in c, if icv is TRUE prefix epansion with
 * a period (for generating Morris type ICVs)
 * if nn is TRUE not all elements of p must exist
 * in the output
 */
static int
se(const int *p, int plen, const int *c, int clen, int icv, int nn)
{
	char str[256];
	int q[24], qlen = 0;
	int tl[12] = { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 };
	int i, j, k;

	if (nn) {
		memset(tl, 0, 12 * sizeof(int));
	}
	while (tl[0] < 12) {
		if (isel(sumpcs(tl, plen), c, clen)) {
			qlen = 0;
			for (j = 0; j < plen; j++) {
				for (k = 0; k < tl[j]; k++) {
					q[qlen] = p[j];
					qlen++;
				}
			}
			pcs2str(q, qlen, str, 0);
			if (icv) {
				printf(".");
			}
			printf("%s\n", str);
		}
		tl[plen - 1]++;
		for (i = plen - 1; i > 0; i--) {
			if (tl[i] == 12) {
				if (nn) {
					tl[i] = 0;
				} else {
					tl[i] = 1;
				}
				tl[i - 1]++;
			}
		}
	}
	return TRUE;
}

static void usage(char *pname)
{
	fprintf(stderr, "usage: %s [ -cCSet ] [ -i ] [ -n ] [ SC ... ]\n",
		pname);
	exit(EXIT_FAILURE);
}

int main(int argc, char **argv)
{
	char str[256];
	int p[12], plen = 0;
	int c[12] = { 1, 2, 3, 4, 5, 6 }, clen = 6;
	int i, t = 0, icv = 0, nn = 0;

	for (i = 1; i < argc; i++) {
		if (argv[i][0] == '-') {
			switch (argv[i][1]) {
			case 'c':
				if (!str2pcs(&argv[i][2], c, &clen)) {
					usage(argv[0]);
				}
				break;
			case 'i':
				icv = 1;
				break;
			case 'n':
				nn = 1;
				break;
			default:
				usage(argv[0]);
				break;
			}
		} else {
			t = 1;
			if (str2pcs(argv[i], p, &plen)) {
				se(p, plen, c, clen, icv, nn);
			}
		}
	}

	if (!t) {
		while (fgets(str, 256, stdin) != NULL) {
			if (str2pcs(str, p, &plen)) {
				se(p, plen, c, clen, icv, nn);
			}
		}
	}
	exit(EXIT_SUCCESS);
	return EXIT_SUCCESS;
}
