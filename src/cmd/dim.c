/*
 * dim.c
 * diatonic interval class two implications
 *
 * rohan drape, 1/96.
 */

#include <stdio.h>
#include <stdlib.h>

#include "libpct.h"

int b_dim(int argc, char **argv);
int dim(int *p, int plen);
void usage(char *pname);

int main(int argc, char **argv)
{
	return b_dim(argc, argv);
}

int b_dim(int argc, char **argv)
{
	int p[MAX_PCS], plen;
	char str[MAX_STR];

	if (argc == 2) {
		if (str2pcs(argv[1], p, &plen)) {
			dim(p, plen);
		} else {
			usage(argv[0]);
		}
	}

	if (argc == 1) {
		while (fgets(str, MAX_STR, stdin) != NULL) {
			if (str2pcs(str, p, &plen)) {
				dim(p, plen);
			}
		}
	}

	exit(EXIT_SUCCESS);
	return EXIT_SUCCESS;
}

int dim(int *p, int plen)
{
	int d[7] = { 0, 2, 4, 5, 7, 9, 11 }; /* diatonic collection */
	int m[7] = { 0, 2, 3, 5, 7, 9, 11 }; /* ascending melodic minor */
	int o[8] = { 0, 1, 3, 4, 6, 7, 9, 10 }; /* octotonic collection */
	int i;

	for (i = 0; i < 12; i++) {
		if (islitsub(p, plen, d, 7) || isequal(p, plen, d, 7)) {
			printf("T%cd  ", i2c(i));
		}
		trspcs(1, d, 7);
	}
	printf("\n");
	for (i = 0; i < 12; i++) {
		if (islitsub(p, plen, m, 7) || isequal(p, plen, m, 7)) {
			printf("T%cm  ", i2c(i));
		}
		trspcs(1, m, 7);
	}
	printf("\n");
	for (i = 0; i < 3; i++) {
		if (islitsub(p, plen, o, 8) || isequal(p, plen, o, 8)) {
			printf("T%co  ", i2c(i));
		}
		trspcs(1, o, 8);
	}
	trspcs(-o[0], o, 8);
	printf("\n");
	return TRUE;
}

void usage(char *pname)
{
	fprintf(stderr, "usage: %s [ PCS ]\n", pname);
	exit(EXIT_FAILURE);
}
