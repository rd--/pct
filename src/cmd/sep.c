#include <stdio.h>
#include <stdlib.h>

int main(int argc, char **argv)
{
	int i, c, x = '\n', r = 0, prev = -1;

	for (i = 1; i < argc; i++) {
		if (argv[i][0] == '-') {
			switch (argv[i][1]) {
			case 'c':
				x = ':';
				break;
			case 'l':
				x = '\n';
				break;
			case 't':
				x = '\t';
				break;
			case 's':
				x = ' ';
				break;
			case 'r':
				r = 1;
				if (!x)
					x = '\t';
				break;
			default:
				fprintf(stderr,
					"usage: %s [ -c ] [ -l ] [ -r ] [ -s ] [ -t ] File\n",
					argv[0]);
				exit(EXIT_FAILURE);
				break;
			}
		} else {
			freopen(argv[i], "r", stdin);
		}
	}

	if (r) {
		while ((c = getc(stdin)) != EOF) {
			if (c == '\n') {
				if (prev == '\n')
					putc('\n', stdout);
				else if (x != '\n')
					putc(x, stdout);
			} else {
				putc(c, stdout);
			}
			prev = c;
		}
		putc('\n', stdout);
	} else {
		while ((c = getc(stdin)) != EOF) {
			if (c != x)
				putc(c, stdout);
			else
				putc('\n', stdout);
			if (c == '\n' && x == '\n')
				putc('\n', stdout);
		}
	}

	exit(EXIT_SUCCESS);
	return EXIT_SUCCESS;
}
