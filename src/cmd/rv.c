/* src/cmd/cgm.c - (c) rohan drape, 2000-2001 */

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include "libpct.h"

int main(int argc, char **argv)
{
	Pco_t *p_1, *p_2;
	int N, i, j;

	if (argc != 4) {
		fprintf(stderr, "usage: %s pcseg iset N\n", argv[0]);
		exit(2);
	}

	srand(clock());

	p_1 = pco_create("pcseg");
	pco_read(p_1, argv[1]);

	p_2 = pco_create("iset");
	pco_read(p_2, argv[2]);

	N = atoi(argv[3]);

	for (j = 0; j < N; j++) {
		Pco_t *p_3 = pco_dup(p_1);
		for (i = 0; i < pco_sizeof(p_3); i++) {
			int p2_index = rand() % pco_sizeof(p_2);
			pco_set_element(p_3, i,
				mod12(pco_element(p_3, i) + pco_element(p_2, p2_index)));
		}
		pco_put(p_3, stdout);
		pco_free(p_3);
	}

	return EXIT_SUCCESS;
}
