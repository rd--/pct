/* apg.c - rohan drape, 10/97 */

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>

#include "libpct.h"

/* state of array is NOT preserved?  */
int apg(Ppca_t *a);
int apg(Ppca_t *a)
{
	int i, *rv;
	Pg_t **e;

	/* check input */
	if (a->r < 2) {
		return FALSE;
	}

	/* make pg_t for each row */
	e = malloc(a->r * sizeof(Pg_t *));
	assert(e);
	for (i = 0; i < a->r; i++) {
		e[i] = pgopen(a->data[i], a->size[i]);
		assert(e[i]);
	}

	/* make return values */
	rv = malloc(a->r * sizeof(int));
	assert(rv);
	for (i = 0; i < a->r; i++) {
		rv[i] = TRUE;
	}

	/* step inner rows to start */
	for (i = 0; i < a->r - 1; i++) {
		a->size[i] = pgnext(e[i], a->data[i]);
	}

	while (rv[0]) {
		/* step outside */
		rv[a->r - 1] = a->size[a->r - 1] = pgnext(e[a->r - 1], a->data[a->r - 1]);
		/* if the outside is done, descend stepping the insides */
		if (!rv[a->r - 1]) {
			for (i = a->r - 1; i > 0; i--) {
				if (rv[i] == FALSE) {
					rv[i] = a->size[i] = pgnext(e[i], a->data[i]);
					rv[i - 1] = a->size[i - 1] = pgnext(e[i - 1], a->data[i - 1]);
				}
			}
		}
		/* write array */
		if (rv[0]) {
			pawrite(a, stdout);
		}
	}
	return TRUE;
}

int main(int argc, char **argv)
{
	int rv;
	Ppca_t *a;

	a = paopen("apg");
	assert(a);
	while (paread(a, stdin)) {
		rv = apg(a);
		assert(rv);
	}
	paclose(a);

	exit(EXIT_SUCCESS);
	return EXIT_SUCCESS;
}
