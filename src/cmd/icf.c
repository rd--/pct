/* icf.c - (c) rohan drape, 1996 */

#include <stdio.h>
#include <stdlib.h>

#include "libpct.h"

static void usage(char *pname)
{
	fprintf(stderr, "usage: %s \n", pname);
	exit(EXIT_FAILURE);
}

int main(int argc, char **argv)
{
	char str[MAX_STR];
	int p[MAX_PCS], plen = 0;

	if (argc != 1)
		usage(argv[0]);

	while (fgets(str, MAX_STR, stdin)) {
		if (str2pcs(str, p, &plen)) {
			if (mod12(sumpcs(p, plen)) == 0) {
				puts(str);
			}
		}
	}

	return EXIT_SUCCESS;
}
