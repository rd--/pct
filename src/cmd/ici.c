/* ici.c - (c) rohan drape, 1996 */

#include <stdio.h>
#include <stdlib.h>

#include "libpct.h"

static void usage(char *pname)
{
	fprintf(stderr, "usage: %s -c [ icset  ... ]\n", pname);
	exit(EXIT_FAILURE);
}

static int intervals_to_interval_classes(int *p, int plen, int verbose)
{
	char str[MAX_STR];
	int i;
	int n = plen;
	int *tl = calloc(n, sizeof(int));
	int q[MAX_PCS], qlen = plen;

	if (tl == NULL) {
		return FALSE;
	}

	while (tl[0] < 2) {
		for (i = 0; i < n; i++) {
			if (tl[i] == 0) {
				q[i] = p[i];
			} else {
				q[i] = 12 - p[i];
				MOD(q[i], 12);
			}
		}
		pcs2str(q, qlen, str, 0);
		printf("%s\n", str);
		tl[n - 1]++;
		for (i = n - 1; i > 0; i--) {
			if (tl[i] == 2) {
				tl[i] = 0;
				tl[i - 1]++;
			}
		}
		if (tl[0] == 1 && !verbose)
			return TRUE;
	}
	return TRUE;
}

int main(int argc, char **argv)
{
	char str[MAX_STR];
	int i, p[MAX_PCS], plen;
	int verbose = TRUE;

	for (i = 1; i < argc; i++) {
		if (argv[i][0] == '-') {
			switch (argv[i][1]) {
			case 'c':
				verbose = FALSE;
				break;
			default:
				usage(argv[0]);
				break;
			}
		} else {
			if (str2pcs(argv[i], p, &plen)) {
				intervals_to_interval_classes(p, plen, verbose);
			}
		}
	}

	if (argc == 1 || (argc == 2 && !verbose)) {
		while (fgets(str, MAX_STR, stdin)) {
			if (str2pcs(str, p, &plen)) {
				intervals_to_interval_classes(p, plen, verbose);
			}
		}
	}

	exit(EXIT_SUCCESS);
	return EXIT_SUCCESS;
}
