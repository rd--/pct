/***** pi.c - (c) rohan drape, 1996 *****/

#include <stdio.h>
#include <stdlib.h>

#include "libpct.h"

static void usage(char *pname)
{
	fprintf(stderr, "usage: %s pcseg index-set\n", pname);
	exit(EXIT_FAILURE);
}

int main(int argc, char **argv)
{
	Pco_t I, p, P, q, Q;
	int i, n;
	int exact = 0;
	tr_t *t;

	pco_set_typeof(&p, pcseg);
	pco_set_typeof(&q, pcseg);
	pco_set_typeof(&I, pcset); /* actually holds the indices (an index-set) */

	if (argc != 3 || !pco_read(&p, argv[1]) || !pco_read(&I, argv[2]))
		usage(argv[0]);

	if (exact) {
		pco_set_typeof(&P, pcseg);
		pco_set_typeof(&Q, pcseg);
	} else {
		pco_set_typeof(&P, pcset);
		pco_set_typeof(&Q, pcset);
	}
	t = tr_init(pco_data(&p), pco_sizeof(&p), 1, 1, 0);

	for (i = 0; i < pco_sizeof(&I); i++) {
		pco_set_element(&P, i, pco_element(&p, pco_element(&I, i)));
	}
	pco_set_sizeof(&Q, pco_sizeof(&I));
	pco_set_sizeof(&P, pco_sizeof(&I));

	while (tr_next(t, pco_data(&q), &n)) {
		pco_set_sizeof(&q, n);
		for (i = 0; i < pco_sizeof(&I); i++)
			pco_set_element(&Q, i, pco_element(&q, pco_element(&I, i)));
		if (pco_cmp(&P, &Q) == PCO_EQUAL) {
			pco_put(&q, stdout);
		}
	}

	tr_free(t);
	return EXIT_SUCCESS;
}
