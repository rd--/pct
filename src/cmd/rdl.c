/* rdl.c - (c) rohan drape, 1996 */

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "libpct.h"

#define RDL_MAX_LINES 8192
#define RDL_MAX_STRING 128

static void usage(char *pname)
{
	fprintf(stderr, "usage: %s [ -v ] [ File ]\n", pname);
	exit(EXIT_FAILURE);
}

static int string_in_list(char *string, char **list, int sz)
{
	int i;
	for (i = 0; i < sz; i++) {
		if (strcmp(string, list[i]) == 0)
			return i;
	}
	return -1;
}

int main(int argc, char **argv)
{
	char *string;
	char **list;
	int count[RDL_MAX_LINES], list_size = 0, i, verbose = 0, err;

	string = malloc(RDL_MAX_STRING);
	assert(string);
	list = malloc(RDL_MAX_LINES * sizeof(char *));
	assert(list);
	for (i = 0; i < RDL_MAX_LINES; i++) {
		list[i] = malloc(RDL_MAX_STRING);
		assert(list[i]);
		count[i] = 0;
	}

	for (i = 1; i < argc; i++) {
		if (argv[i][0] == '-') {
			switch (argv[i][1]) {
			case 'v':
				verbose = TRUE;
				break;
			default:
				usage(argv[0]);
				break;
			}
		} else {
			if (!freopen(argv[1], "r", stdin)) {
				usage(argv[0]);
			}
		}
	}

	while (fgets(string, RDL_MAX_STRING, stdin) != NULL) {
		RMNL(string);
		if (string[0] == '\0') {
			continue;
		}
		err = string_in_list(string, list, list_size);
		if (err == -1) {
			strncpy(list[list_size], string, RDL_MAX_STRING);
			count[list_size]++;
			list_size++;
			if (list_size >= RDL_MAX_LINES)
				die("rdl: too many lines");
		} else {
			count[err]++;
		}
	}

	for (i = 0; i < list_size; i++) {
		if (verbose)
			printf("%s (%d)\n", list[i], count[i]);
		else
			printf("%s\n", list[i]);
	}

	return EXIT_SUCCESS;
}
