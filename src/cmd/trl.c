/* trl.c - rohan drape, 3/96 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "libpct.h"

static void usage(char *pname)
{
	fprintf(stderr, "usage: %s [ +m|R|r ] [ -m|R|r ] pcseg ... \n", pname);
	exit(EXIT_FAILURE);
}

int main(int argc, char **argv)
{
	int i, incl_r = 0, incl_m = 0, incl_R = 1;
	Pco_t *obj;
	tr_t *t;

	obj = pco_create("pcseg");
	for (i = 1; i < argc; i++) {
		if (argv[i][0] == '-') {
			switch (argv[i][1]) {
			case 'r':
				incl_r = 0;
				break;
			case 'm':
				incl_m = 0;
				break;
			case 'R':
				incl_R = 0;
				break;
			default:
				usage(argv[0]);
				break;
			}
		} else if (argv[i][0] == '+') {
			switch (argv[i][1]) {
			case 'r':
				incl_r = 1;
				break;
			case 'm':
				incl_m = 1;
				break;
			case 'R':
				incl_R = 1;
				break;
			default:
				usage(argv[0]);
				break;
			}
		} else if (pco_read(obj, argv[i])) {
			t = tr_init(obj->data, obj->size, incl_R, incl_m, incl_r);
			while (tr_next(t, obj->data, &(obj->size))) {
				pco_put(obj, stdout);
			}
			tr_free(t);
		} else {
			usage(argv[0]);
		}
	}
	pco_free(obj);
	return EXIT_SUCCESS;
}
