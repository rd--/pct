/*
 * imb.c
 * imbrications
 *
 * rohan drape, 11/97
 */

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>

#include "libpct.h"

int imb(int *s, int slen, int *p, int plen);
void usage(char *pname);
void usage(char *pname)
{
	fprintf(stderr, "usage: %s [ -ccset ] [ pcseg ... ]\n", pname);
	exit(EXIT_FAILURE);
}

int main(int argc, char **argv)
{
	char str[MAX_STR];
	int s[12], slen, p[MAX_PCS], plen;
	int i, xx = 1;

	s[0] = 1;
	slen = 1;

	if (argc > 1) {
		for (i = 1; i < argc; i++) {
			if (argv[i][0] == '-') {
				switch (argv[i][1]) {
				case 'c':
					str2pcs(&argv[i][2], s, &slen);
					assert(slen);
					break;
				default:
					usage(argv[0]);
					break;
				}
			} else {
				xx = 0;
				if (str2pcs(argv[i], p, &plen)) {
					imb(s, slen, p, plen);
				} else {
					usage(argv[0]);
				}
			}
		}
	}

	if (xx) {
		while (fgets(str, MAX_STR, stdin)) {
			if (str2pcs(str, p, &plen)) {
				imb(s, slen, p, plen);
			}
		}
	}

	exit(EXIT_SUCCESS);
	return EXIT_SUCCESS;
}

/*
 * imbrications
 */
int imb(int *s, int slen, int *p, int plen)
{
	char str[MAX_STR];
	int i, j;

	assert(slen && plen);

	for (i = 0; i < plen; i++) {
		if (isel(i, s, slen)) {
			for (j = 0; j <= plen - i; j++) {
				pcs2str(&p[j], i, str, 0);
				printf("%s\n", str);
			}
		}
	}
	return TRUE;
}
