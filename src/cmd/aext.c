/* aext.c - rohan drape, 10/97 */

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "libpct.h"

static void usage(char *pname)
{
	fprintf(stderr, "usage: %s\n", pname);
	exit(EXIT_FAILURE);
}

int main(int argc, char **argv)
{
	int i, s[2][MAX_PCS], slen;
	Ppca_t *e;
	Cg_t *c;
	Pg_t *p;

	if (argc == 2 && strcmp(argv[1], "-h"))
		usage(argv[0]);

	e = paopen("aep");
	assert(e);
	while (paread(e, stdin)) {
		for (i = 0; i < e->r; i++) {
			(e->size[i])++;
			e->partition[i][e->c] = 1;
		}
		(e->c)++;
		c = cgopen(12, e->r, NULL);
		assert(c);
		while ((slen = cgnext(c, s[0])) != 0) {
			p = pgopen(s[0], slen);
			assert(p);
			while ((slen = pgnext(p, s[1])) != 0) {
				for (i = 0; i < e->r; i++) {
					e->data[i][e->size[i] - 1] = s[1][i];
				}
				pawrite(e, stdout);
			}
			pgclose(p);
		}
		cgclose(c);
	}
	paclose(e);

	exit(EXIT_SUCCESS);
	return EXIT_SUCCESS;
}
