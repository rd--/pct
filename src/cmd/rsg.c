/***** rsg.c - (c) rohan drape, 1997 *****/

#include <stdio.h>
#include <stdlib.h>

#include "libpct.h"

int main(int argc, char **argv)
{
	Pco_t *from, *to;
	Sro_t s;

	from = pco_create("pcseg");
	to = pco_create("pcseg");

	if (argc >= 2)
		pco_read(from, argv[1]);
	if (argc == 3) {
		pco_read(to, argv[2]);
		if (sro_derive(&s, from, to))
			sro_put(&s, stdout);
	} else {
		while (pco_get(to, stdin)) {
			if (sro_derive(&s, from, to))
				sro_put(&s, stdout);
		}
	}

	pco_free(from);
	pco_free(to);
	return EXIT_SUCCESS;
}
