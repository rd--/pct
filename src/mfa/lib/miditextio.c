/*
 * miditextio.c
 *
 * rohan drape, 7/95
 */

#include <stdio.h>
#include "miditextio.h"


int readEvent(FILE *fp,midiEvent *a,midiHdr *h) {
	while(readMidiHdr(fp,h)) {
		;
	}
	return fscanf(fp,"%ld %x %x %x",
                      &(a->ts),
                      (unsigned int *)&(a->stat),
                      (unsigned int *)&(a->d1),
                      (unsigned int *)&(a->d2));
}

int writeEvent (FILE *fp,midiEvent *a) {
	return fprintf(fp,"%8ld  %2x  %2x  %2x\n",
			a->ts,a->stat,a->d1,a->d2);
}

int readNote (FILE *fp,midiNote *a,midiHdr *h) {
	while(readMidiHdr(fp,h)) {
		;
	}
	return fscanf(fp,"%ld %x %x %x %ld",
                      &(a->ts),
                      (unsigned int *)&(a->stat),
                      (unsigned int *)&(a->d1),
                      (unsigned int *)&(a->d2),
                      &(a->dur));
}


int writeNote (FILE *fp,midiNote *a) {
	return fprintf(fp,"%8ld  %2x  %2x  %2x  %8ld\n",
			a->ts,a->stat,a->d1,a->d2,a->dur);
}

int writeMidiHdr(FILE *fp,midiHdr *hdr) {
	return fprintf(fp,"MidiHdr: %d %d %d %d %d %d\n",
			hdr->tempoBPM,hdr->tempoTSPB,
			hdr->tSigN,hdr->tSigD,
			hdr->kSigT,hdr->kSigA);
}


int readMidiHdr(FILE *fp,midiHdr *hdr) {
	long h=ftell(fp);
	if(fscanf(fp,"MidiHdr: %d %d %d %d %d %d",
			&(hdr->tempoBPM),&(hdr->tempoTSPB),
			&(hdr->tSigN),&(hdr->tSigD),
			&(hdr->kSigT),&(hdr->kSigA))!=6) {
		fseek(fp,h,SEEK_SET);
		return 0;
	}
	return 1;
}
