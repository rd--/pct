/*
 * miditextio.h
 *
 * rohan drape, 7/95
 */

#include <stdio.h>

#define MAJOR_KEY 0
#define MINOR_KEY 0

/*
 * basic header structure for midi data.
 */
typedef struct {
	/*
	 * tempo in beats per minute and also
	 * time-stamps per beat.
	 */
	int tempoBPM;
	int tempoTSPB;
	/*
	 * time signature numerator and
	 * denominator.
	 */
	int tSigN;
	int tSigD;
	/*
	 * key signature type (major or minor)
	 * and the number of accidentals (positive
	 * for sharps, negative for flats).
	 */
	int kSigT;
	int kSigA;
}
midiHdr;


/*
 * basic structure for holding midi data.
 */
typedef struct {
	long ts;
	int stat;
	int d1;
	int d2;
}
midiEvent;


/*
 * a midiEvent with an added field for
 * holding a duration.
 */
typedef struct {
	long ts;
	int stat;
	int d1;
	int d2;
	long dur;
}
midiNote;



/*
 * read a text midi event from fp.
 */
int readEvent(FILE *fp,midiEvent *a,midiHdr *h);


/*
 * write a text midi event to fp.
 */
int writeEvent (FILE *fp,midiEvent *a);


/*
 * read a text midi note from fp.
 */
int readNote (FILE *fp,midiNote *a,midiHdr *h);


/*
 * write a text midi note to fp.
 */
#define WRITENOTE(a) printf("%ld %x %x %x %ld",\
(a.ts),(a.stat),(a.d1),(a.d2),(a.dur))
int writeNote (FILE *fp,midiNote *a);

/*
 * write a text midi header to fp.
 */
int writeMidiHdr(FILE *fp,midiHdr *hdr);


/*
 * read a text midi header from fp.
 */
int readMidiHdr(FILE *fp,midiHdr *hdr);

