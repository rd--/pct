/*
 * srtel.c
 * sort event list: format: �ts stat d1 d2�
 *
 * rohan drape, 7/95
 *
 */

#include <stdio.h>
#include <stdlib.h>

#include "libpct.h"
#include "miditextio.h"

#define MAXNOTES 10000

/*
 * an event is defined as preceding another
 * event if its time-stamp is smaller, or,
 * if timestamps are equal, if its status
 * byte is smaller.
 */
void orderMidiEvent(midiEvent *p, midiEvent *q);
void orderMidiEvent(midiEvent *p, midiEvent *q) {
	midiEvent temp;
	if((p->ts>q->ts)||
	   ((p->ts==q->ts)&&(p->stat>q->stat)))
	{
		temp.ts = p->ts;
		temp.stat = p->stat;
		temp.d1 = p->d1;
		temp.d2 = p->d2;

		p->ts = q->ts;
		p->stat = q->stat;
		p->d1 = q->d1;
		p->d2 = q->d2;

		q->ts = temp.ts;
		q->stat = temp.stat;
		q->d1 = temp.d1;
		q->d2 = temp.d2;
	}
}

/*
 * this is a simple bubble-sort algorithm
 */
void midiEventSort(midiEvent a[],long n);
void midiEventSort(midiEvent a[],long n) {
	long i,j;
	for(i=0;i<n-1;++i) {
		for(j=n-1;i<j;--j) {
			orderMidiEvent(&a[j-1],&a[j]);
		}
	}
}

int main(int argc, char **argv) {
	midiEvent *e=NULL;
	midiHdr hdr;
	long numevents;
	long i;

	if(argc==2) {
		if(freopen(argv[1],"r",stdin)==NULL) {
			die("cannot open '%s'",argv[1]);
		}
	}

	e=(midiEvent *)malloc(MAXNOTES*sizeof(midiEvent));
	if(e==NULL) {
		die("memory allocation failed");
	}
	if(!readMidiHdr(stdin,&hdr)) {
		die("midi header expected");
	}
	writeMidiHdr(stdout,&hdr);
	numevents=0;
	while(numevents<MAXNOTES) {
		if(readEvent(stdin,&e[numevents],&hdr)!=4) {
			break;
		}
		numevents+=1;
	}
	if(numevents>MAXNOTES) {
		die("input file too large");
	}
	/*fprintf(stderr,"events = %ld\n",numevents);*/
	midiEventSort(e,numevents);
	for(i=0;i<=numevents-1;i++) {
		writeEvent(stdout,&e[i]);
	}

	exit(EXIT_SUCCESS);return EXIT_SUCCESS;
}
