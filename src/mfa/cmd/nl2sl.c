/*
 * nl2sl.c
 * note list to set list
 *
 * rohan drape, 7-9/95.
 */

#include <stdio.h>
#include <stdlib.h>

#include "libpct.h"
#include "miditextio.h"

#define MAX_SEG   64
#define MAX_NOTES 5000

void usage(char *);
void pl(long lnum,int *p,int plen);

int  main (int argc, char **argv) {
	int lnum; /* line number */
	int pc[MAX_NOTES],pclen; /* PC list */
	int c[MAX_SEG],clen=0; /* cardinality list */
	int i,j;

	midiNote mn;
	midiHdr  mhdr;

	if(argc<2) {
		usage(argv[0]);
	}
	str2pcs(argv[1],c,&clen);
	if(clen>MAX_SEG) {
		die("limits exceeded");
	}
	if(argc==3) {
		if(freopen(argv[2],"r",stdin)==NULL) {
			die("cannot open '%s'",argv[2]);
		}
	}

	if(!readMidiHdr(stdin,&mhdr)) {
		die("midi header expected");
	}
	pclen=0;
	while(readNote(stdin,&mn,&mhdr)==5) {
		pc[pclen]=midi2pc(mn.d1);
		pclen+=1;
		if(pclen>=MAX_NOTES) {
			fprintf(stderr,"file to large\n");
			exit(EXIT_FAILURE);
		}
	}

	for(i=0;i<clen;i++) {
		lnum=0;
		for(j=0;j<pclen-c[i];j++) {
			pl(lnum,&pc[j],c[i]);
			lnum+=1;
		}
	}
	exit(EXIT_SUCCESS);return EXIT_SUCCESS;
}


/*
 * pl - print line
 */
void pl(long lnum,int *p,int plen) {

	int nc;
	int i;
	char str[256];

	pcs2str(p,plen,str,FALSE);
	nc=printf(" %3ld: ^%d <%s>",lnum,plen,str);
	for(i=nc;i<25;i++) {
		printf(" ");
	}
	remdup(p,&plen);
	srtpcs(p,plen);
	pcs2str(p,plen,str,FALSE);
	nc=printf("#%d {%s}",plen,str);
	for(i=nc;i<25;i++) {
		printf(" ");
	}
	pcs2scstr(p,plen,str,1);
	nc=printf("%s",str);
	printf("\n");
}


void usage(char *pname) {
	fprintf(stderr,"usage: %s Cset [file]\n",pname);
	exit(EXIT_FAILURE);
}



