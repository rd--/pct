/*
 * el2vl.c
 * event list to transferance list
 *
 * rohan drape, 7/95.
 */

#include <stdio.h>
#include <stdlib.h>

#include "libpct.h"
#include "miditextio.h"

#define MAX_VERT 24
#define MAX_TRANS 24

int gnv(int *vl,int *card,int *on);
void pl(int cnt,int *pcs,int len);

midiHdr mhdr;

int  main (int argc, char **argv) {
	int vl[MAX_VERT],vllen;
	int m[MAX_VERT],mlen;
	int p[MAX_TRANS][12],plen[MAX_TRANS];
	int tc[12],tclen; /* transferance cardinality */
	int cv; /* current verticality */
	int on;
	int index;
	int i,j,k;

	if(!str2pcs(argv[1],tc,&tclen)) {
		die("transferance cardinality set invalid");
	}
	if(argc==3) {
		if(freopen(argv[2],"r",stdin)==NULL) {
			die("cannot open '%s'",argv[2]);
		}
	}

	for(j=0;j<tclen;j++) {
		fseek(stdin,0,SEEK_SET);
		cv=0;
		index=0;
		vllen=0;
		if(!readMidiHdr(stdin,&mhdr)) {
			die("expected midi header");
		}
		/*
		 * get initial verticals
		 */
		for(i=0;i<tc[j]-1;i++) {
			gnv(vl,&vllen,&on);
			cv++;
			cpypcs(p[index],&plen[index],vl,vllen);
			remdup(p[index],&plen[index]);
			srtpcs(p[index],plen[index]);
			index+=1;
		}
		/*
		 * then so long as we get a new one
		 * print the next section
		 */
		 while(gnv(vl,&vllen,&on)) {
			cv++;
			cpypcs(p[index],&plen[index],vl,vllen);
			remdup(p[index],&plen[index]);
			srtpcs(p[index],plen[index]);
			index+=1;
			if(index==tc[j]) {
				index=0;
			}
			mlen=0;
			for(i=0;i<tc[j];i++) {
				k=index+i;
				MOD(k,tc[j]);
				mergein(m,&mlen,p[k],plen[k]);
				srtpcs(m,mlen);
				pl((cv-(tc[j]-i-1)),p[k],plen[k]);
			}
			pl(0,m,mlen);
			putc('\n',stdout);
		}
	}

	exit(EXIT_SUCCESS); return EXIT_SUCCESS;
}

/*
 * pl - print line
 */
void pl(int cnt,int *p,int len) {
	char str[256];
	int nc;

	if(cnt==0) {
		printf("ts  :  ");
	}
	else {
		printf("v%-3d:  ",cnt);
	}
	if(len<=0) {
		printf("#0  {}\n");
		return;
	}
	pcs2str(p,len,str,FALSE);
	nc=printf("#%-2d {%s}",len,str);
	while(nc<20) {
		fputc(' ',stdout);
		nc++;
	}
	pcs2scstr(p,len,str,1);
	printf("%s\n",str);
}


/*
 * gnv - get next verticality
 * get next vertical pitch class set.
 * vl should point to the previous
 * verticality which should be card
 * members long.
 */
int gnv(int *vl,int *card,int *on)
{
	midiEvent e;         /* event */
	static midiEvent ne; /* next event */

	*on=0;
	while(TRUE) {
		if((ne.stat==0x90)&&(ne.d2!=0)) {
			vl[*card]=midi2pc(ne.d1);
			(*card)++;
			if(*card>MAX_VERT) {
				die("limits exceeded (card=%d)",*card);
			}
			(*on)++;
		}
		if((ne.stat==0x80) ||
		   ((ne.stat==0x90)&&(ne.d2==0)) ) {
			if(!rem1pc(midi2pc(ne.d1),vl,card)) {
				die("note off retrieved when not logged");
			}
		}
		if(readEvent(stdin,&e,&mhdr)==EOF) {
			ne.stat=0;
			return FALSE;
		}
		if(e.ts!=ne.ts) {
			ne=e;
			return TRUE;
		}
		ne=e;
	}
}
