/*
 * el2vl.c
 * event list to verticlity list
 *
 * rohan drape, 7/95.
 */

#include <stdio.h>
#include <stdlib.h>

#include "libpct.h"
#include "miditextio.h"

#define MAX_VERT 24

void usage(char *pname);
void pl(int *p,int len,int on,int numvert);
int getvert(int *vl,int *card,int *on);


midiEvent nme; /* global: next midi event */
midiHdr mhdr;  /* global: midi header, contains ts per beat */
long upbeat;   /* global: the current bar */
int mark_bars; /* global: flag to print bar marking */

int  main (int argc, char **argv) {
	int vl[24],vllen;
	int p[12],plen;
	int i;
	int on;

	if(argc<1) {
		usage(argv[0]);
		exit(EXIT_FAILURE);
	}
	mark_bars=TRUE;
	for(i=1;i<argc;i++) {
		if(argv[i][0]=='-') {
			switch(argv[i][1]) {
				case 'b':
					mark_bars=FALSE;
					break;
				default:
					usage(argv[0]);
					break;
			}
		}
		else {
			if(freopen(argv[i],"r",stdin)==NULL) {
				die("cannot open '%s'",argv[i]);
			}
		}
	}

	if(!readMidiHdr(stdin,&mhdr)) {
		die("expected midi header");
	}
	readEvent(stdin,&nme,&mhdr);
	if(nme.ts==0) {
		upbeat=0;
	}
	else {
		upbeat=1;
	}
	vllen=0;
	while(getvert(vl,&vllen,&on)) {
		cpypcs(p,&plen,vl,vllen);
		remdup(p,&plen);
		srtpcs(p,plen);
		pl(p,plen,on,vllen);
	}
	exit(EXIT_SUCCESS); return EXIT_SUCCESS;
}

/*
 * getvert
 * get next vertical pitch class set.
 */
int getvert(int *vl,int *card,int *on)
{
	midiEvent e;
	static long curbar;
	*on=0;
	while(TRUE) {
		if((nme.stat==0x90)&&(nme.d2!=0)) {
			vl[card[0]]=midi2pc(nme.d1);
			(*card)++;
			if(*card>MAX_VERT) {
				die("limits exceeded");
			}
			(*on)++;
		}
		if((nme.stat==0x80) ||
		   ((nme.stat==0x90)&&(nme.d2==0)) ) {
			if(!rem1pc(midi2pc(nme.d1),vl,card)) {
				die("file corrupt");
			}
		}
		if(readEvent(stdin,&e,&mhdr)==EOF) {
			return FALSE;
		}
		if(e.ts!=nme.ts) {
			if((nme.ts/mhdr.tempoTSPB/mhdr.tSigN)>(curbar-1)) {
				curbar=(nme.ts/mhdr.tempoTSPB/mhdr.tSigN)+1;
				if(mark_bars) {
					printf("Bar %ld:\n",curbar-upbeat);
				}
			}
			nme=e;
			return TRUE;
		}
		nme=e;
	}
}


/*
 * pl - print line
 */
void pl(int *p,int len,int on,int numvert) {
	char str[256];
	int nc;
	int i;
	static int nv=1; /* number of verticals printed */

	pcs2str(p,len,str,FALSE);
	nc=printf(" %-3d: #%d {%s}",nv,len,str);
	for(i=nc;i<20;i++) {
		printf(" ");
	}
	if(len!=0) {
		pcs2scstr(p,len,str,1);
	}
	else {
		str[0]='\0';
	}
	nc=printf("%s",str);
	for(i=nc;i<20;i++) {
		printf(" ");
	}
	nc=0;
	nc+=printf("%d/%d",on,numvert);
	printf("\n");
	nv+=1;
}


/*
 * usage.
 */
void usage(char *pname) {
	fprintf(stderr,"Usage: %s [ -b ] [file]",pname);
	fprintf(stderr,"Flags: -b   = turn off the bar marking facility.\n");
	exit(EXIT_FAILURE);
}



