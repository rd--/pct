/*
 * nl2ps.c
 * note-list to pitch class succession
 *
 * rohan drape, 12/95
 */

#include <stdio.h>
#include <stdlib.h>

#include "libpct.h"
#include "miditextio.h"

int  main (int argc, char **argv) {

	midiNote cn; /* current note */
	midiNote nn; /* next note */
	midiHdr  mh; /* midi header */

	int vr=FALSE; /* struck verticality flag */
	int cc=0; /* character count */

	if(argc==2) {
		if(freopen(argv[1],"r",stdin)==NULL) {
			die("cannot open '%s'",argv[1]);
		}
	}

	if(!readMidiHdr(stdin,&mh)) {
		die("input midiHdr invalid");
	}
	readNote(stdin,&cn,&mh);
	while(readNote(stdin,&nn,&mh)!=EOF) {
		switch(vr) {
			case FALSE:
				if(cn.ts==nn.ts) {
					vr=TRUE;
					putc('(',stdout);
					putc(i2c(midi2pc(cn.d1)),stdout);
					cc+=2;
				}
				else {
					putc(i2c(midi2pc(cn.d1)),stdout);
					cc++;
				}
				break;
			case TRUE:
				if(cn.ts!=nn.ts) {
					putc(i2c(midi2pc(cn.d1)),stdout);
					putc(')',stdout);
					cc+=2;
					vr=FALSE;
				}
				else {
					putc(i2c(midi2pc(cn.d1)),stdout);
					cc++;
				}
				break;
		}
		cn=nn;
		if(cc>40 && !vr) {
			putc('\n',stdout);
			putc('\n',stdout);
			cc=0;
		}
	}
	putc(i2c(midi2pc(cn.d1)),stdout);
	if(vr) {
		putc(')',stdout);
	}
	exit(EXIT_SUCCESS);return EXIT_SUCCESS;
}
