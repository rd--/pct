/*
 * svl.c
 * get struck verticality list
 *
 * rohan drape, 7/95
 */

#include <stdio.h>
#include <stdlib.h>

#include "libpct.h"
#include "miditextio.h"


int getlinestr(FILE *fp,long pos,char *str);

int main(int argc,char **argv) {
	char c,pc=0;
	char linestr[512];
	long pos=0;
	FILE *temp=tmpfile();

	if(argc==2) {
		freopen(argv[1],"r",stdin);
	}
	copyfile(temp,stdin);
	rewind(temp);
	while((c=getc(stdin))!=EOF) {
		switch(c) {
			case '/':
				c=getc(stdin);
				if(c==pc) {
					getlinestr(stdin,pos,linestr);
					puts(linestr);
				}
				break;
			case '\n':
				pos=ftell(stdin);
				break;
			default:
				break;
		}
		pc=c;
	}
	fclose(temp);
	exit(EXIT_SUCCESS); return EXIT_SUCCESS;
}


/*
 * put from pos to end_of_line in str
 * maintain file marker
 */
int getlinestr(FILE *fp,long pos,char *str)
{
	int i=0;
	char  c;
	long hold=ftell(fp);

	fseek(stdin,pos,SEEK_SET);
	while((c=getc(fp))!='\n') {
		str[i]=c;
		i++;
	}
	str[i]='\0';
	fseek(fp,hold,SEEK_SET);
	return 1;
}
