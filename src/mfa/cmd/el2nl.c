/*
 * el2nl.c
 * event list: �ts [0x]stat [0x]d1 [0x]d2�
 * to note list: �ts [0x]stat [0x]d1 [0x]d2 dur�
 *
 * rohan drape, 19/5/95
 */


#include <stdio.h>
#include <stdlib.h>

#include "libpct.h"
#include "miditextio.h"

int main(int argc, char **argv) {
	midiNote e;
	midiEvent e2;
	midiHdr hdr;

	int foundNoteOff=0;
	long pos=0;

	if(argc==2) {
		if(freopen(argv[1],"r",stdin)==NULL) {
			die("cannot open '%s'",argv[1]);
		}
	}

	if(!readMidiHdr(stdin,&hdr)) {
		die("midi header expected");
	}
	writeMidiHdr(stdout,&hdr);
	while(1) {
		if(readEvent(stdin,(midiEvent *)&e,&hdr)!=4) {
			break;
		}
		if(e.stat>=0x90 && e.stat<=0x9f) {
			pos=ftell(stdin);
			foundNoteOff=0;
			while(!foundNoteOff) {
				if(readEvent(stdin,(midiEvent *)&e2,&hdr)!=4) {
					die("no note-off to correspond to note on");
				}
				if(	(e2.stat==e.stat && e2.d1==e.d1 && e2.d2==0) ||
					(e2.stat==(e.stat-16) && e2.d1==e.d1) ) {
					e.dur = e2.ts-e.ts;
					writeNote(stdout,&e);
					fseek(stdin,pos,SEEK_SET);
					foundNoteOff=1;
				}
			}
		}
	}
	exit(EXIT_SUCCESS);return EXIT_SUCCESS;
}

