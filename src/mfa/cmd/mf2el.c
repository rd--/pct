/*
 * mf2el.c
 * midi file to event list
 *
 * based on mftext.c
 *
 * rohan drape, 12/3/95
 */

#include <stdio.h>
#include <stdlib.h>
#include "midifile.h"
#include "miditextio.h"

static FILE *F;
midiHdr hdr;
int frst=1;

void initfuncs(void);

int error(char *s)
{
  fprintf(stderr,"Error: %s\n",s);
  return 1;
}

int main(int argc,char **argv) {
  F=stdin;
  if(argc==2) {
    if(freopen(argv[1],"r",stdin)==NULL) {
      error("cannot open input file");
      exit(EXIT_FAILURE);
    }
  }
  initfuncs();
  mfread();
  fclose(F);
  exit(EXIT_SUCCESS);return EXIT_SUCCESS;
}

int filegetc(void)
{
  return(getc(F));
}

int txt_header(int format, int ntrks, int ldivision)
{
  hdr.tempoTSPB=ldivision;
  /*fprintf(stderr,"header: %d,%d,%d\n",format,ntrks,ldivision);*/
  return 1;
}

int txt_trackstart()
{
  /*printf("\n");*/
  return 1;
}

int txt_trackend()
{
  /*printf("\n");*/
  return 1;
}

int prtime()
{
  printf("%8ld   ",Mf_currtime);
  return 1;
}

int txt_noteon(int chan,int pitch,int vol)
{
  if(frst) {
    writeMidiHdr(stdout,&hdr);
    frst=0;
  }
  prtime();
  printf("90   %2x   %2x\n",pitch,vol);
  return 1;
}

int txt_noteoff(int chan,int pitch,int vol)
{
  if(frst) {
    writeMidiHdr(stdout,&hdr);
    frst=0;
  }
  prtime();
  printf("80   %2x   %2x\n",pitch,vol);
  return 1;
}

int txt_keysig(int sf,int mi)
{
  hdr.kSigT=mi;
  hdr.kSigA=sf;
  if(!frst) {
    writeMidiHdr(stdout,&hdr);
  }
  return 1;
}

int txt_tempo(long ltempo)
{
  hdr.tempoBPM=120; /* have to fix this */
  return 1;
}

int txt_timesig(int nn,int dd,int cc,int bb)
{
  int denom = 1;
  while ( dd-- > 0 ) {
    denom *= 2;
  }
  hdr.tSigN=nn;
  hdr.tSigD=denom;
  if(!frst) {
    writeMidiHdr(stdout,&hdr);
  }
  return 1;
}

void initfuncs(void)
{
  Mf_getc = filegetc;
  Mf_error = error;
  Mf_header =  txt_header;
  Mf_trackstart =  txt_trackstart;
  Mf_trackend =  txt_trackend;
  Mf_noteon =  txt_noteon;
  Mf_noteoff =  txt_noteoff;
  Mf_timesig =  txt_timesig;
  Mf_tempo =  txt_tempo;
  Mf_keysig =  txt_keysig;
}
