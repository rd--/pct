/*
 * csrt.c
 * cardinality sort
 *
 * rohan drape, 2/96
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "libpct.h"
#include "miditextio.h"

#define MAX_LINES 3000
#define MAX_CHARS 256

typedef struct {
  long value;
  long line;
} sortdata;

int csrt(FILE *fp,int n);
int gline(char *line,long num,long maxc,FILE *fp);
void usage(char *pname);


int main(int argc,char **argv) {
  int  n=1,i;
  FILE *temp=tmpfile();

  if(argc>3) {
    usage(argv[0]);
  }
  for(i=1;i<argc;i++) {
    if(strlen(argv[i])<3) {
      if(sscanf(argv[i],"%d",&n)!=1) {
	die("position indicator (%s) invalid",argv[i]);
      }
    }
    else {
      if(freopen(argv[i],"r",stdin)==NULL) {
	die("input file %s unavailable",argv[i]);

      }
    }
  }
  copyfile(temp,stdin);
  rewind(temp);
  csrt(temp,n);
  fclose(temp);
  exit(EXIT_SUCCESS);return EXIT_SUCCESS;
}


/*
 * long comparison routine for qsort
 */
int lcompare(const void *n1,const void *n2);
int lcompare(const void *n1,const void *n2) {
  return (*((long *)n1)-*((long *)n2));
}

/*
 * sort the file at fp by the nth cardinality
 * of each line (n>1)
 * if no cardinality can be found for a
 * line it is ignored
 */
int csrt(FILE *fp,int n) {

  char line[MAX_CHARS];
  int i,j;
  int gc; /* got cardinality for line ? */
  int nline=0; /* number of lines with match */
  int cline=0; /* current line of input file */
  int card=0;

  sortdata sortarray[MAX_LINES];

  rewind(fp);
  while(fgets(line,MAX_CHARS,fp)!=NULL) {
    gc=FALSE;
    i=1;
    j=0;
    while(line[j]!='\0') {
      if(line[j]=='#') {
	if(i==n) {
	  card=c2i(line[j+1]);
	  gc=TRUE;
	}
	else {
	  i++;
	}
      }
      j++;
    }
    if(gc) {
      sortarray[nline].value=card;
      sortarray[nline].line=cline;
      nline++;
    }
    if(nline>=MAX_LINES) {
      die("input file too large");
    }
    cline++;
  }
  qsort(&sortarray[0],nline,sizeof(sortdata),lcompare);
  for(i=0;i<nline;i++) {
    gline(line,sortarray[i].line,MAX_CHARS,fp);
    fputs(line,stdout);
  }
  /*fprintf(stderr,"File contained %d cardinalities at position %d.\n",cline,n);*/
  return TRUE;
}

/*
 * put line 'num' of the file
 * at fp into 'line', where zero
 * is the first line.
 */
int gline(char *line,long num,long maxc,FILE *fp) {
  fseek(fp,0,SEEK_SET);
  fgets(line,maxc,fp);
  while(num--) {
    if(fgets(line,maxc,fp)==NULL) {
      return FALSE;
    }
  }
  return TRUE;
}



/*
 * usage.
 */
void usage(char *pname) {
  fprintf(stderr,"Usage: %s [ Number ] [ File ]\n",pname);
  exit(EXIT_FAILURE);
}

