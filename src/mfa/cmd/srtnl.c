/*
 * srtnl.c
 * sort note list: format: �ts stat d1 d2 dur�
 * into ascending time order.
 *
 * rohan drape, 23/5/95
 *
 */

#include <stdio.h>
#include <stdlib.h>

#include "libpct.h"
#include "miditextio.h"

#define MAXNOTES 10000

/*
 * a note is defined as preceding another
 * note is it time-stamp is smaller, or
 * if time-stamps are equal, if its
 * status byte is smaller.
 */
void order(midiNote *p, midiNote *q);
void order(midiNote *p, midiNote *q)
{
	midiNote temp;
	if((p->ts>q->ts)||
	   ((p->ts==q->ts)&&(p->stat>q->stat)) )
	{
		temp.ts = p->ts;
		temp.stat = p->stat;
		temp.d1 = p->d1;
		temp.d2 = p->d2;
		temp.dur = p->dur;

		p->ts = q->ts;
		p->stat = q->stat;
		p->d1 = q->d1;
		p->d2 = q->d2;
		p->dur = q->dur;

		q->ts = temp.ts;
		q->stat = temp.stat;
		q->d1 = temp.d1;
		q->d2 = temp.d2;
		q->dur = temp.dur;
	}
}

/*
 * a simple bubble-sort.
 */
void midiNoteSort(midiNote a[],long n);
void midiNoteSort(midiNote a[],long n)
{
	long i,j;
	for(i=0;i<n-1;++i)
		for(j=n-1;i<j;--j)
			order(&a[j-1],&a[j]);
}

int main(int argc, char **argv) {
	midiNote *e=NULL;
	long numnotes;
	long i;
	midiHdr hdr;

	if(argc==2) {
		if(freopen(argv[1],"r",stdin)==NULL) {
			die("cannot open '%s'",argv[1]);
		}
	}

	e=(midiNote *)malloc(MAXNOTES*sizeof(midiNote));
	if(e==NULL) {
		die("memory allocation failed");
	}

	if(!readMidiHdr(stdin,&hdr)) {
		die("midi header expected");
	}
	writeMidiHdr(stdout,&hdr);
	numnotes=0;
	while(numnotes<MAXNOTES) {
		if(readNote(stdin,&e[numnotes],&hdr)!=5) {
			break;
		}
		numnotes++;
	}
	if(numnotes>MAXNOTES) {
		die("input file too large");
	}
	/*fprintf(stderr,"notes = %ld\n",numnotes);*/
	midiNoteSort(e,numnotes);
	for(i=0;i<=numnotes-1;i++) {
		writeNote(stdout,&e[i]);
	}
	exit(EXIT_SUCCESS);return EXIT_SUCCESS;
}
