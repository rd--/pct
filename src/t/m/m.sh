#!/bin/sh
# pct/src/t/m.sh
#
# rohan drape, 9/97

case $# in
0)
	echo usage: $0 Filename ;;

1)
	if test -r $1.midi
	then
		echo Tests in the file \"$0\" executed by \"`hostname`\" on \"`date`\".
		echo
		set -x
		mkdir o
		time -p mf2el $1.midi > o/tmp
		time -p srtel o/tmp > o/$1.el
		time -p el2nl o/$1.el > o/$1.nl
		time -p el2vl o/$1.el > o/$1.vl
		time -p el2tl 2 o/$1.el > o/$1.tl.2
		time -p el2tl 3 o/$1.el > o/$1.tl.3
		time -p el2tl 4 o/$1.el > o/$1.tl.4
		time -p nl2ps o/$1.nl > o/$1.ps
		time -p nl2sl 2 o/$1.nl > o/$1.sl.2
		time -p nl2sl 3 o/$1.nl > o/$1.sl.3
		time -p nl2sl 4 o/$1.nl > o/$1.sl.4
		time -p nl2sl 5 o/$1.nl > o/$1.sl.5
		time -p nl2sl 6 o/$1.nl > o/$1.sl.6
	else 
		echo $1.midi does not exist
	fi
esac
