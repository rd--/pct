#!/bin/sh

mkdir -p o
echo "script ----- \"$0\""
echo "machine ---- \"`hostname`\""
echo "in time ---- \"`date`\""
set -x
echo "012345" | pct aap > o/aap.t
pct ac < i/arrays > o/ac.t
pct adms < i/array > o/adms.t
pct aepm -fi/aepm < i/arrays | pct ac > o/aepm.t
pct aext < i/array | pct ac > o/aext.t
pct afmt < i/afmt > o/afmt.t
pct aha "pc-distribution = 321232123212" < i/arrays | pct ac > o/aha-1.t
pct aha "harmonic-size = 3" < i/arrays | pct ac > o/aha-h-2.t
pct aha "harmonic-diversity = 6" < i/arrays | pct ac > o/aha-h-3.t
pct airr q -r13 < i/array > o/airr-q.t
pct apg < i/apg | pct ac > o/apg.t
pct arcs < i/array > o/arcs.t
pct asi -c < i/array > o/asi.t
pct asro T6 < i/array > o/asro.t
pct atg -o0 < i/array | pct ac > o/atg.t
pct atl i/array > o/atl.t
pct atto i/array | pct ac > o/atto.t 
pct bip 0t95728e3416 > o/bip.t
pct ccdg i/poem | sort -f > o/ccdg.t
pct cf 4 < ../../lib/scdb | wc -l > o/cf.t
sed s/://g i/array | pct cfa > o/cfa.t
pct cg -r3 0159 | wc -l > o/cg.t
echo 024579 | pct chn T0 3 | sort -u > o/chn.t
pct cisg 014295e38t76 > o/cisg.t
pct cmpl 02468t > o/cmpl.t
pct cyc 056 > o/cyc.t
pct dim 016 > o/dim.t
pct dis 24 > o/dis.t
echo 024579e | pct doi 6 > o/doi.t
pct fl -c 56 | pct epmq "in sc 7-35" "has sc 4-20" > o/epmq.t
echo 23a | pct ess 0164325 > o/ess.t
pct fl -c 6 | wc -l > o/fl.t
pct fn 023 > o/fn.t
pct frg 024579 > o/frg.t
pct fsrt > o/fsrt.t <<EOF
     sc1: 5-1[01234]
     sc2: 4-1[0123]
     sc3: 3-1[012]
EOF
echo 22341 | pct icf > o/icf.t
pct ici -c 123 > o/ici.t
pct icseg 013265e497t8 > o/icseg.t
pct icv 024579 > o/icv.t
pct imb -c34 024579 | pct pfmt > o/imb.t
pct iseg 014295e38t76 > o/iseg.t
pct se -c5 1247 | pct pg | sort -u | pct isegsc | sort -u | \
pct epmq "is sc 6-1 6-8 6-32 6-7 6-20 6-35" | pct fsrt > o/isegsc.t
pct issl -s"6-Z42" 357 > o/issl.t
pct issb 3-7 6-32 > o/is_sb.t
pct lsi 027 2 > o/lsi.t
echo "5 4 3 2 1" | pct lsrt > o/lsrt.t
pct mbs 3b 147 -s"4-4" > o/mbs.t
pct mcs 5 2 c > o/mcs.t
pct mg 01 23 45 > o/mg.t
echo 024579 | pct mpc 02468t13579e > o/mpc.t
pct mw C "C#" D "D#" E F "F#" G "G#" A "A#" B < i/array > o/mw.t
pct mxs 024579 642 | sort -u > o/mxs.t
pct fl -c 2 | pct pcom pcseg pcset | pct name > o/name.t
pct nrm 0123456543210 > o/nrm.t
sh i/pcom > o/pcom.t
pct pcsisl 012 | sort > o/pcsisl.t
pct pg 112 | pct pdl > o/pdl.t
pct sb 4-11 | pct fn | pct pfmt > o/pfmt.t
pct pg 01234 | wc -l > o/pg.t
pct pi 0236 12 > o/pi.t 
echo 123456343 | pct po > o/po.t
pct prt 0235 3 12 > o/prt.t
pct pup 112 > o/pup.t
pct pvl 034 536 -c23 | pct pfmt 4 | wc -l > o/pvl.t
pct pg 112 | pct rdl -v > o/rdl.t
pct ri 0123456789te > o/ri.t
pct rs 0123 e614 > o/rs.t
pct rsg 0123 05t3 > o/rsg.t
pct rss 012345 4 2 > o/rss.t
pct sb 7-35 8-28 | pct fn | pct pfmt > o/sb.t
pct sb -l 024579 79e0246 | pct cf 4 >> o/sb.t
pct scc 6-32 168 > o/scc.t
pct scdb 0146 > o/scdb.t
pct scis 23547091 013 > o/scis.t
pct se -c4 23 > o/se.t
echo "0 01 02" | pct sep -s > o/sep.t
pct sgdb -s t463 > o/sgdb.t
pct sgi 0136 > o/sgi.t
echo 1234 | pct sgr 2 > o/sgr.t
echo 12345 | pct sgr 2 >> o/sgr.t
pct si 0136 > o/si.t
pct sis 024579b 1357 > o/sis.t
pct sp 05 04 23 | pct cf 4 > o/sp.t
pct sp -r2 -m 05 04 23 >> o/sp.t
pct spsc `fl 3` > o/spsc.t
echo 0:1:9:B:A:7 | pct sra > o/sra.t
echo 024579 | pct sro RT4I > o/sro.t
pct tics 024579 > o/tics.t
pct tmatrix -h 0147 > o/tmatrix.t
pct trl +R +m -r 013 | wc -l > o/trl.t
echo 642 | pct trs 024579 > o/trs.t
pct tto 024579 | wc -l > o/tto.t
pct uis 3-11 | pct fsrt 4 | egrep "2-" > o/usi.t


echo "out time --- \"`date`\""
