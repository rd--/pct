#!/bin/sh

echo "script ----- \"$0\""
echo "machine ---- \"`hostname`\""
echo "in time ---- \"`date`\""
set -x

time -p cat i/array | \
    time -p atg | \
    time -p aha "pc-transference = 1,1" \
    "harmonic-size = 34" \
    "harmonic-content = is-sc 4-11 4-12" \
    "harmonic-diversity = 56" | \
    time -p ac > /dev/null

echo "end time --- \"`date`\""
