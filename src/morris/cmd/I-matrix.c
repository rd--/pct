/*
 * I-matrix.c
 * 
 * rohan drape,  10/96
 */


#include <stdio.h>
#include <stdlib.h>

#include "../lib/mlib.h"


int b_Imatrix(int argc,char **argv);
void usage(char *pname);



int main(int argc,char **argv) {
	return b_Imatrix(argc,argv);
}

int b_Imatrix(int argc,char **argv) {
	char str[MAX_STR];
	PCSEG_t A,B;
	PCA_t E;
	
	if(argc!=3 ||
	   !strpco(argv[1],PCSEG_d,&A) ||
	   !strpco(argv[2],PCSEG_d,&B)) {
		usage(argv[0]);
	}

	Imatrix(A,B,&E);
	pcostr(&E,PCA_d,str);
	printf("I-matrix(%s,%s)=\n\n%s\n",argv[1],argv[2],str);
	
	exit(EXIT_SUCCESS);return EXIT_SUCCESS;
}


void usage(char *pname) {
	fprintf(stderr,"usage: %s A B\n",pname);
	exit(EXIT_FAILURE);
}

