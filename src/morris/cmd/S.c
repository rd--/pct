/*
 * S.c
 * 
 * rohan drape,  10/96
 */


#include <stdio.h>
#include <stdlib.h>

#include "../lib/mlib.h"


int b_S(int argc,char **argv);
void usage(char *pname);



int main(int argc,char **argv) {
	return b_S(argc,argv);
}

int b_S(int argc,char **argv) {
	int n,r;
	PCSET_t A,B;
	
	if(argc!=4 ||
	   !strpco(argv[1],PCSET_d,&A) ||
	   !strpco(argv[2],PCSET_d,&B) ||
       sscanf(argv[3],"%d",&n)!=1) {
		usage(argv[0]);
	}
	
	r=S(A,B,n);
	printf("S(%s,%s,%s)=%d\n",argv[1],argv[2],argv[3],r);
	
	exit(EXIT_SUCCESS);return EXIT_SUCCESS;
}

void usage(char *pname) {
	fprintf(stderr,"usage: %s A B n\n",pname);
	exit(EXIT_FAILURE);
}

