/*
 * gT-matrix.c
 * 
 * rohan drape,  8/97
 */


#include <stdio.h>
#include <stdlib.h>

#include "mlib.h"


int b_gTmatrix(int argc,char **argv);
void usage(char *pname);



int main(int argc,char **argv) {
	return b_gTmatrix(argc,argv);
}

int b_gTmatrix(int argc,char **argv) {
	int i,j;
	PCSEG_t A,B;
	
	if(argc!=3 ||
	   !strpco(argv[1],PCSEG_d,&A) ||
	   !strpco(argv[2],PCSEG_d,&B)) {
		usage(argv[0]);
	}

	printf("digraph Tmatrix {\n\n");
	printf("\trankdir = LR;\n");
	printf("\tranksep = 4;\n\n");
	printf("\tsize = \"4,4\";\n");
	printf("\tratio = auto;\n");
	printf("\tcenter = true;\n\n");
	printf("\tnode [shape = circle];\n\n");
	printf("\tlabel = \"%s %s %s\";\n\n",argv[0],argv[1],argv[2]);
	
	/* nodes are the pcs of each pcseg, each arranged into a subgraph */
	printf("\tsubgraph cluster0 {\n\t\t");
	for(i=0;i<A.plen;i++) {
		printf("A%c [label = \"%c\"]; ",i2c(A.p[i]),i2c(A.p[i]));
	}
	printf("\n\t\tlabel = \"pcset A\";\n");
	printf("\t}\n\n");
	printf("\tsubgraph cluster1 {\n\t\t");
	for(i=0;i<B.plen;i++) {
		printf("B%c [label = \"%c\"]; ",i2c(B.p[i]),i2c(B.p[i]));
	}
	printf("\n\t\tlabel = \"pcset B\";\n");
	printf("\t}\n\n");

	/* make connections from A to B */
	for(i=0;i<A.plen;i++) {
		for(j=0;j<B.plen;j++) {
			printf("\tA%c -> B%c [label = \"%d\"];\n",
			       i2c(A.p[i]),i2c(B.p[j]),interval(A.p[i],B.p[j]));
		}
	}
	
	printf("}\n\n");

	return TRUE;

	exit(EXIT_SUCCESS);return EXIT_SUCCESS;
}


void usage(char *pname) {
	fprintf(stderr,"usage: %s A B\n",pname);
	exit(EXIT_FAILURE);
}

