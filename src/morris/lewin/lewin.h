/*
 * this file: pct/morris/src/lewin/lewin.h
 *
 * rohan drape, 8/97
 */




#include "../mtypes.h" 



/*
 * the number of ways to span i from X to Y
 *
 * Definition: Lewin 1987, p.88, DEF 5.1.3
 */
int IFUNC(PCSET_t X, PCSET_t Y,I_t i);




/*
 * the number of forms of X that are in Y
 *
 * Definition: Lewin 1987, p.105, DEF 5.3.1
 *
 * Notes: the discussion of this on p.120 makes
 * it unclear what is meant by 'forms', it seems
 * 'non-identical pcsets related by TnI'
 */
int EMB(PCSET_t X, PCSET_t Y);




/*
 * the number of forms of Y that include X
 *
 * Definition: Lewin 1987, p.120
 *
 * Notes: the discussion of this on p.120 makes
 * it unclear what is meant by 'forms', it seems
 * 'non-identical pcsets related by TnI'
 */
int COV(PCSET_t X, PCSET_t Y);




/*
 * the number of elements x in X such that f(x) is in Y
 *
 * Definition: Lewin 1987, p.121
 */
int INJ(PCSET_t X, PCSET_t Y, MAPPING_t f);



/*
 * the number of forms of Y that both include X and are in Z
 *
 * Definition: Lewin 1987, p.121
 */
int SANDW(PCSET_t X, SC_t Y, SC_t Z);



/*
 * the number of forms of Y where X and Y do not intersect
 * and the union of X and Y is in Z
 *
 * Definition: Lewin 1987, p.121
 */
int ADJOIN(PCSET_t X, PCSET_t Y, SC_t Z);




/*
 * the RI form of X where whose first two elements
 * are xn-1 and xn in that order
 *
 * Definition: Lewin 1987, p.180
 */
int RICH(PCSEG_t X);




/*
 * RICH(RICH(X)), that is some transposition of X
 *
 * Definition: Lewin 1987, p.181
 */
int TCH(PCSEG_t X);





/*
 * the RI form of X which overlaps X to the maximum possible extent
 *
 * Definition: Lewin 1987, p.183
 */
int MUCH(PCSEG_t X);




/*
 * transpose the segment X by its last interval
 *
 * Definition: Lewin 1987, p.188
 */
int TLAST(PCSEG_t X);




/*
 * transpose the segment X by the complement of its last interval
 *
 * Definition: Lewin 1987, p.188
 */
int TLAST_1(PCSEG_t X);




/*
 * transpose the segment X by its first interval
 *
 * Definition: Lewin 1987, p.188
 */
int TFIRST(PCSEG_t X);





/*
 * transpose the segment X by the complement of its first interval
 *
 * Definition: Lewin 1987, p.188
 */
int TFIRST_1(PCSEG_t X);




