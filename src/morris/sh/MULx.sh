#!/bin/sh

case $# in
2)
	for i in 0 1 2 3 4 5 6 7 8 9 10 11
	do
		pct MUL $1 $2 $i
	done
	;;
*)
	echo "usage: sh "$0" pcset pcset" ;;
esac
