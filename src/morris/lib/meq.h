/*
 * meq.h - equality and equivalence determinants
 *
 * rohan 10/96
 */

/*
 * are the pc objects A and B of type 'type' equal.
 * the determination of equality is dependant upon the type of object 
 */ 
int isequal(void *A,void *B,int type);

/*
 * are the pc objects A and B of type 'type' equivalent.
 * the determination of equivalence is dependant upon the type of object 
 */ 
int isequiv(void *A,void *B,int type);

