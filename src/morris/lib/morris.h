/*
 * morris.h - functions as defined in Morris 1987 and 1991
 *
 * rohan drape, 6/96
 */

/*
 * the sum of a and b modulo 12
 *
 * Definition: Morris 1987, p.70, DEF 3.4.4
 */
PC_t sum(PC_t a,PC_t b);

/*
 * the interval from a to b modulo 12
 *
 * Definition: Morris 1987, p.62, DEF 3.1.1
 */
I_t interval(PC_t a,PC_t b);

/*
 * the interval class from a to b 
 *
 * Definition: Morris 1987, p.62, DEF 3.1.2
 */
IC_t ic(PC_t a,PC_t b);

/*
 * the interval class vector for the pcset A
 *
 * Definition: Morris 1987, p.68
 */
ICSEG_t ICV(SC_t A);

/*
 * the interval vector for the pcsets A and B
 *
 * Definition: Morris 1987, p.69 (see also TH 3.4.3.1 and TH 3.4.3.2)
 */
ISEG_t IV(PCSET_t A,PCSET_t B);

/*
 * the multiplicity of the interval n between the sets A and B
 *
 * Definition: Morris 1987, p.70, DEF 3.4.5.1
 */
int MUL(PCSET_t A,PCSET_t B,int n);

/*
 * the multiplicity of the sum(a,b)=n between the sets A and B
 *
 * Definition: Morris 1987, p.70, DEF 3.4.5.2
 */
int S(PCSET_t A,PCSET_t B,int n);

/*
 * the interval succesion at distance m of the pcseg A
 * 
 * Definition: Morris 1987, p.107, DEF 3.10.1
 */
ISEG_t INTm(int m,PCSEG_t A);

/*
 * the interval succesion at distance m of the pccyc A
 * 
 * Definition: Morris 1987, p.107, DEF 3.10.2
 */
ISEG_t CINTm(int m,PCCYC_t A);

/*
 * the number of pairs of non-equal positions in the pcseg A 
 * 
 * Definition: Morris 1987, p.117, DEF 3.13.1
 */
int PR(PCSEG_t A);

/*
 * the segment for the pcseg Q from P (OMpP is the 'reference segment')
 *
 * Definition: Morris 1987, p.115, DEF 3.13
 */
SEG_t OMpQ(PCSEG_t P,PCSEG_t Q);

/*
 * the ORMAPtable for the pcseg p
 *
 * Definition: Morris 1991, p.70 (generalized here to any segment length)
 *
 * Notes: The ORMAPtable is nor a pitch class array.
 */
PCA_t ORMAPtable(PCSEG_t P);


/*
 * Definition: Morris 1987, p.117, DEF 3.13.2
 */
int OI(PCSEG_t P,PCSEG_t Q);

/*
 * the displacement of the pcseg Q from P
 *
 * Definition: Morris 1987, p.119, DEF 3.14.1
 */
int DIS(PCSEG_t P,PCSEG_t Q);

/*
 * the scattering of the pcseg Q from P
 *
 * Definition: Morris 1987, p.120, DEF 3.15.1
 *
 * Bugs: The explanation in Morris is unclear, with numerous
 * apparent errors. The coding here follows the actual
 * definition, ignoring the examples.
 */
int SCAT(PCSEG_t P,PCSEG_t Q);

/*
 * the correlation coefficient of the pcseg Q from P
 *
 * Definition: Morris 1987, p.120, DEF 3.16.1
 */
double CC(PCSEG_t P,PCSEG_t Q);

/*
 * the T-matrix E, for A and B, is constructed so that E(x,y)=i<Ax,By>
 *
 * Definition: Morris 1987, p.110, DEF 3.12.1
 */
int Tmatrix(PCSEG_t A,PCSEG_t B,PCA_t *E);

/*
 * the sub T-matrix(n) is the T-matrix showing only occurences of pc n
 *
 * Definition: Morris 1987, p.111
 */
int subTmatrix(PCSEG_t A,PCSEG_t B,PCA_t *E,PC_t n);

/*
 * the I-matrix E, for A and B, is constructed so that E(x,y)=sum<Ax,By>
 *
 * Definition: Morris 1987, p.110, DEF 3.12.2
 */
int Imatrix(PCSEG_t A,PCSEG_t B,PCA_t *E);

/*
 * the sub I-matrix(n) is the I-matrix showing only occurences of pc n
 *
 * Definition: Morris 1987, p.111
 */
int subImatrix(PCSEG_t A,PCSEG_t B,PCA_t *E,PC_t n);
