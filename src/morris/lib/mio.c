/*
 * mio.c
 * morris input output
 *
 * rohan drape, 10/96
 */


#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "mtypes.h"
#include "mio.h"
#include "mutil.h"


int pcstr(PC_t A,char *str);
int pcsetstr(PCSET_t A,char *str);
int pccycstr(PCCYC_t A,char *str);
int scstr(SC_t A,char *str);
int pcastr(PCA_t A,char *str);
int ttostr(TTO_t A,char *str);
int sostr(SO_t A,char *str);

int strpc(char *str,PC_t *A);
int strpcseg(char *str,PCSEG_t *A);
int strsc(char *str,SC_t A);
int strpca(char *str,PCA_t *A);
int strtto(char *str,TTO_t *A);
int strso(char *str,SO_t *A);



int pcostr(void *pco,int type,char *str) {
	switch(type) {
		case PC_d:
		case IC_d:
			return pcstr(*(PC_t *)pco,str);
			break;
		case PCSET_d:
		case PCSEG_d:
		case ICSEG_d:
		case ISEG_d:
		case SEG_d:
			return pcsetstr(*(PCSET_t *)pco,str);
			break;
		case PCCYC_d:
			return pccycstr(*(PCCYC_t *)pco,str);
			break;
		case SC_d:
			return pcsetstr(*(PCSET_t *)pco,str);
			/*return scstr(*(SC_t *)pco,str);*/
			break;
		case PCA_d:
			return pcastr(*(PCA_t *)pco,str);
			break;
		case TTO_d:
			return ttostr(*(TTO_t *)pco,str);
			break;
		case SO_d:
			return sostr(*(SO_t *)pco,str);
			break;
		default:
			fprintf(stderr,"perror: illegal call to pcostr (type=%d)\n",type);
			str[0]='\0';
			return FALSE;
			break;
	}
}

int pcstr(PC_t A,char *str) {
	char c=i2c((int)A);
	sprintf(str,"%c",c);
	return TRUE;
}

int pcsetstr(PCSET_t A,char *str) {
	int i;
	for(i=0;i<A.plen;i++) {
		str[i]=i2c(A.p[i]);
	}
	str[i]='\0';
	return TRUE;
}

int pccycstr(PCCYC_t A,char *str) {
	int i;
	for(i=0;i<A.plen;i++) {
		str[i]=i2c(A.p[i]);
	}
	str[i]='-';
	str[i+1]='\0';
	return TRUE;
	
}

/* should be able to write forte names */
int scstr(SC_t A,char *str) {
	fprintf(stderr,"perror: scstr() unimplemented\n"); abort(); 
	return FALSE;
}

void nextcol(int x) {
	while(x--) {
		putc(' ',stdout);
	}
}

int pcastr(PCA_t A,char *str) {
	int maxprt;
	int i,j,k;

	str[0]='\0';
	for(i=0;i<A.r;i++) { /* i is the current row */
		for(j=0;j<A.c;j++) { /* j is the current collumn */
			for(k=0,maxprt=0;k<A.r;k++) {
				if(A.W[i][j].plen>maxprt) {
					maxprt=A.W[i][j].plen;
				}
			}
			pcostr(&(A.W[i][j]),PCSEG_d,&str[strlen(str)]);
		}
		sprintf(&str[strlen(str)],"\n");
	}
	return TRUE;
}


int ttostr(TTO_t A,char *str) {
	int i=0;
	str[i]='T';
	i++;
	str[i]=i2c(A.t);
	i++;
	if(A.m) {
		str[i]='M';
		i++;
	}
	if(A.i) {
		str[i]='I';
		i++;
	}
	str[i]='\0';
	return TRUE;
}

int sostr(SO_t A,char *str) {
	int i=0;
	if(A.rot) {
		str[i]='r';
		i++;
		str[i]=i2c(A.rot);
		i++;
	}
	if(A.r) {
		str[i]='R';
		i++;
	}
	str[i]='T';
	i++;
	str[i]=i2c(A.t);
	i++;
	if(A.m) {
		str[i]='M';
		i++;
	}
	if(A.i) {
		str[i]='I';
		i++;
	}
	str[i]='\0';
	return TRUE;
}






int strpco(char *str,int type,void *pco) {
	switch(type) {
		case PC_d:
		case IC_d:
			return strpc(str,(PC_t *)pco);
			break;
		case PCSET_d:
		case PCSEG_d:
		case PCCYC_d:
		case ICSEG_d:
		case ISEG_d:
		case SEG_d:
			return strpcseg(str,(PCSEG_t *)pco);
			break;
		case SC_d:
			/*return strsc(str,(SC_t *)pco);*/
			return strpcseg(str,(PCSEG_t *)pco);
			break;
		case PCA_d:
			return strpca(str,(PCA_t *)pco);
			break;
		case TTO_d:
			return strtto(str,(TTO_t *)pco);
			break;
		case SO_d:
			return strso(str,(SO_t *)pco);
			break;
		default:
			fprintf(stderr,"perror: illegal call to strpco (type=%d)\n",type);
			str[0]='\0';
			return FALSE;
			break;
	}
}

int strpc(char *str,PC_t *A) {
	*A=c2i(str[0]);
	return TRUE;
}

int strpcseg(char *str,PCSEG_t *A) {
	int i;
	
	A->plen=0;
	for(i=0;(size_t)i<strlen(str);i++) {
		if((A->p[A->plen]=c2i(str[i]))!=-1) {
			(A->plen)++;
		}
		else {
			if(   str[i]!='{'
		       && str[i]!='['
		       && str[i]!='<'
			   && str[i]!=',') {
				break;
			}
		}
		if(A->plen>=MAX_PCSET) {
			fprintf(stderr,"perror: pco to large (MAX_PCSET=%d)\n",MAX_PCSET);
			abort();
		}
	}
	return A->plen;
}

/* should be able to read forte names */
int strsc(char *str,SC_t A) {
	fprintf(stderr,"perror: strsc() unimplemented\n"); abort(); 
	return FALSE;
}

int strpca(char *str,PCA_t *A) {
	fprintf(stderr,"perror: strpca() unimplemented\n"); abort(); 
	return FALSE;
}

int strtto(char *str,TTO_t *A) {
	int i=0;
	
	A->i=FALSE;
	A->m=FALSE;

	if(str[i]!='T') {	
		return FALSE;
	}
	i++;
	if((A->t=c2i(str[i]))==-1) {
		return FALSE;
	}
	i++;
	if(str[i]=='M') {
		A->m=TRUE;
		i++;
	}
	if(str[i]=='I') {
		A->i=TRUE;
	}
	return TRUE;
}

int strso(char *str,SO_t *A) {
	int i=0;
	if(str[i]=='r') {
		i++;
		A->rot=-c2i(str[i]);
		i++;
	}
	else {
		A->rot=FALSE;
	}

	if(str[i]=='R') {
		A->r=TRUE;
		i++;
	}
	else {
		A->r=FALSE;
	}

	if(str[i]!='T') {
		return FALSE;
	}
	else {
		i++;
		A->t=c2i(str[i]);
		i++;
	}
	
	if(str[i]=='M') {
		A->m=TRUE;
		i++;
	}
	else {
		A->m=FALSE;
	}
	
	if(str[i]=='I') {
		A->i=TRUE;
		i++;
	}
	else {
		A->i=FALSE;
	}
	
	if(A->rot==-1 || A->t==-1) { 
		return FALSE;
	}
	
	return TRUE;
}



