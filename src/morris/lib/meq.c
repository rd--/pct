/*
 * meq.c
 * morris equality/equivalunce tests
 *
 * rohan drape, 11/96
 */


#include <stdio.h>

#include "mtypes.h"
#include "meq.h"
#include "mrel.h"




int pcequal(PC_t A,PC_t B);
int pcsetequal(PCSET_t A,PCSET_t B);
int pcsegequal(PCSEG_t A,PCSEG_t B);
int pcaequal(PCA_t A,PCA_t B);
int ttoequal(TTO_t A,TTO_t B);
int soequal(SO_t A,SO_t B);

int isequal(void *A,void *B,int type) {
	switch(type) {
		case PC_d:
		case IC_d:
			 return pcequal(*(PC_t *)A,*(PC_t *)B);
			 break;
		case PCSET_d:
			 return pcsetequal(*(PCSET_t *)A,*(PCSET_t *)B);
			 break;
		case PCSEG_d:
			 return pcsegequal(*(PCSEG_t *)A,*(PCSEG_t *)B);
			 break;
		case PCA_d:
			 return pcaequal(*(PCA_t *)A,*(PCA_t *)B);
			 break;
		case TTO_d:
			 return ttoequal(*(TTO_t *)A,*(TTO_t *)B);
			 break;
		case SO_d:
			 return soequal(*(SO_t *)A,*(SO_t *)B);
			 break;
		default:
			fprintf(stderr,"perror: illegal call to isequal (type=%d)\n",type);
			return FALSE;
			break;
	}
}

int pcequal(PC_t A,PC_t B) {
	if(A==B) {
		return TRUE;
	}
	return FALSE;
}

/*
 * is the PC A an element of the PCSET B
 * 
 */
int isel(PC_t A,PCSET_t B);
int isel(PC_t A,PCSET_t B) {
	int i;
	for(i=0;i<B.plen;i++) {
		if(B.p[i]==A) {
			return TRUE;
		}
	}
	return FALSE;
}

int pcsetequal(PCSET_t A,PCSET_t B) {
	int i;
	
	if(A.plen!=B.plen) {
		return FALSE;
	}
	for(i=0;i<A.plen;i++) {
		if(!isel(A.p[i],B)) {
			return FALSE;
		}
	}
	return TRUE;
}

int pcsegequal(PCSEG_t A,PCSEG_t B) {
	int i;
	
	if(A.plen!=B.plen) {
		return FALSE;
	}
	for(i=0;i<A.plen;i++) {
		if(A.p[i]!=B.p[i]) {
			return FALSE;
		}
	}
	return TRUE;
}

int pcaequal(PCA_t A,PCA_t B) {
	return FALSE;
}

int ttoequal(TTO_t A,TTO_t B) {
	if(A.t==B.t && A.i==B.i && A.m==B.m) {
		return TRUE;
	}
	else {
		return FALSE;
	}
}

int soequal(SO_t A,SO_t B) {
	if(A.t==B.t && A.i==B.i && A.m==B.m && A.r==B.r && A.rot==B.rot) {
		return TRUE;
	}
	else {
		return FALSE;
	}
}







int pcequiv(PC_t A,PC_t B);
int pcsetequiv(PCSET_t A,PCSET_t B);
int pcsegequiv(PCSEG_t A,PCSEG_t B);
int pcaequiv(PCA_t A,PCA_t B);
int ttoequiv(TTO_t A,TTO_t B);
int soequiv(SO_t A,SO_t B);

int isequiv(void *A,void *B,int type) {
	switch(type) {
		case PCSET_d:
			 return pcsetequiv(*(PCSET_t *)A,*(PCSET_t *)B);
			 break;
		case PCSEG_d:
			 return pcsegequiv(*(PCSEG_t *)A,*(PCSEG_t *)B);
			 break;
		case PCA_d:
			 return pcaequiv(*(PCA_t *)A,*(PCA_t *)B);
			 break;
		case TTO_d:
			 return ttoequiv(*(TTO_t *)A,*(TTO_t *)B);
			 break;
		case SO_d:
			 return soequiv(*(SO_t *)A,*(SO_t *)B);
			 break;
		default:
			fprintf(stderr,"perror: illegal call to isequiv (type=%d)\n",type);
			return FALSE;
			break;
	}
}

int pcsetequiv(PCSET_t A,PCSET_t B) {
	TTO_t C;
	
	if(A.plen!=B.plen) {
		return FALSE;
	}
	if(ttorel(A,B,&C)) {
		return TRUE;
	}
	else {
		return FALSE;
	}
}

int pcsegequiv(PCSEG_t A,PCSEG_t B) {
	SO_t C;
	
	if(A.plen!=B.plen) {
		return FALSE;
	}
	if(sorel(A,B,&C)) {
		return TRUE;
	}
	else {
		return FALSE;
	}
}

int pcaequiv(PCA_t A,PCA_t B) {
	return FALSE;
}

int ttoequiv(TTO_t A,TTO_t B) {
	return FALSE;
}

int soequiv(SO_t A,SO_t B) {
	return FALSE;
}
