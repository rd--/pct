/*
 * mio.h - pc object input and ouptut 
 *
 * rohan 10/96
 */

/*
 * place a string representation of the pc object 'pco'
 * into the string 'str'. 'type' defines the type of the 
 * pc object at 'pco'.
 */
int pcostr(void *pco,int type,char *str);

/*
 * interpret the string at 'str' as a pc object of type
 * 'type' and place it at 'pco'.
 */
int strpco(char *str,int type,void *pco);
