#!/bin/sh
# generates level-3 documentation for the morris functions


c2man -F" int f(a,b)"  -o../../../doc/3 -xPARAMETERS -g mtypes.h
c2man -F" int f(a,b)" -i'"mtypes.h"' -o../../../doc/3 -xPARAMETERS -g mio.h
c2man -F" int f(a,b)" -i'"mtypes.h"' -o../../../doc/3 -xPARAMETERS -g mutil.h
c2man -F" int f(a,b)" -i'"mtypes.h"' -o../../../doc/3 -xPARAMETERS -g morris.h
c2man -F" int f(a,b)" -i'"mtypes.h"' -o../../../doc/3 -xPARAMETERS -g mrel.h
