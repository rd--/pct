/*
 * mrel.c
 * morris relationship tests
 *
 * rohan drape, 11/96
 */


#include <stdio.h>

#include "mtypes.h"
#include "mutil.h"
#include "meq.h"
#include "mrel.h"



int trelpcset(PCSET_t *A,PCSET_t *B,int *n);			
int trelpcseg(PCSEG_t *A,PCSEG_t *B,int *n);			

int trel(void *A,void *B,int type,int *n) {
	switch(type) {
		case PCSET_d:
			return trelpcset((PCSET_t *)A,(PCSET_t *)B,n);
			break;
		case PCSEG_d:
			return trelpcseg((PCSEG_t *)A,(PCSEG_t *)B,n);
			break;
		default:
			fprintf(stderr,"perror: illegal call to trel (type=%d)\n",type);
			return FALSE;
			break;
	}
}

int trelpcset(PCSET_t *A,PCSET_t *B,int *n) {			
	int i;
	PCSET_t P=*A;
	
	*n=0;
	for(i=0;i<12;i++) {
		if(isequal(&P,B,PCSET_d)) {
			*n=i;
			return TRUE;
		}
		trspcset(1,&P);
	}
	return FALSE;
}

int trelpcseg(PCSEG_t *A,PCSEG_t *B,int *n) {			
	int i;
	PCSEG_t P;
	
	*n=0;
	cpypcseg(&P,*A);
	for(i=0;i<12;i++) {
		if(isequal(&P,B,PCSEG_d)) {
			*n=i;
			return TRUE;
		}
		trspcseg(1,&P);
	}
	return FALSE;
}




int irelpcset(PCSET_t *A,PCSET_t *B);			
int irelpcseg(PCSEG_t *A,PCSEG_t *B);			

int irel(void *A,void *B,int type) {
	switch(type) {
		case PCSET_d:
			return irelpcset((PCSET_t *)A,(PCSET_t *)B);
			break;
		case PCSEG_d:
			return irelpcseg((PCSEG_t *)A,(PCSEG_t *)B);
			break;
		default:
			fprintf(stderr,"perror: illegal call to irel (type=%d)\n",type);
			return FALSE;
			break;
	}
}

int irelpcset(PCSET_t *A,PCSET_t *B) {			
	PCSET_t P=*A;
	
	multpcset(11,&P);
	if(isequal(&P,B,PCSET_d)) {
		return TRUE;
	}
	else {
		return FALSE;
	}
}

int irelpcseg(PCSEG_t *A,PCSEG_t *B) {			
	PCSEG_t P;
	
	cpypcseg(&P,*A);
	multpcseg(11,&P);
	if(isequal(&P,B,PCSEG_d)) {
		return TRUE;
	}
	else {
		return FALSE;
	}
}



int mrelpcset(PCSET_t *A,PCSET_t *B);			
int mrelpcseg(PCSEG_t *A,PCSEG_t *B);			

int mrel(void *A,void *B,int type) {
	switch(type) {
		case PCSET_d:
			return mrelpcset((PCSET_t *)A,(PCSET_t *)B);
			break;
		case PCSEG_d:
			return mrelpcseg((PCSEG_t *)A,(PCSEG_t *)B);
			break;
		default:
			fprintf(stderr,"perror: illegal call to mrel (type=%d)\n",type);
			return FALSE;
			break;
	}
}

int mrelpcset(PCSET_t *A,PCSET_t *B) {			
	PCSET_t P=*A;
	
	multpcset(5,&P);
	if(isequal(&P,B,PCSET_d)) {
		return TRUE;
	}
	else {
		return FALSE;
	}
}

int mrelpcseg(PCSEG_t *A,PCSEG_t *B) {			
	PCSEG_t P=*A;
	
	multpcseg(5,&P);
	if(isequal(&P,B,PCSEG_d)) {
		return TRUE;
	}
	else {
		return FALSE;
	}
}

int rrel(PCSEG_t *A,PCSEG_t *B) {
	int i;
	if(A->plen!=B->plen) {
		return FALSE;
	}
	for(i=0;i<A->plen;i++) {
		if(A->p[i]!=B->p[B->plen-1-i]) {
			return FALSE;
		}
	}
	return TRUE;
}

int rotrel(PCSEG_t *A,PCSEG_t *B,int *n) {
	int i;
	PCSEG_t P=*A;
	
	*n=0;
	if(A->plen!=B->plen) {
		return FALSE;
	}
	for(i=0;i<A->plen;i++) {
		rotpcseg(1,&P);
		if(isequal(&P,B,PCSEG_d)) {
			*n=i;
			return TRUE;
		}
	}
	return FALSE;
}





int ttorel(PCSET_t A,PCSET_t B,TTO_t *C) {
	PCSET_t P=A;
	
	C->i=FALSE;
	C->t=0;
	C->m=FALSE;

	if(A.plen!=B.plen) {
		return FALSE;
	}

	/* Tn */
	if(trel(&P,&B,PCSET_d,&(C->t))) {
		return TRUE;
	}

	/* TnM */
	multpcset(5,&P);
	C->m=TRUE;
	if(trel(&P,&B,PCSET_d,&(C->t))) {
		return TRUE;
	}
	multpcset(5,&P);
	C->m=FALSE;
	 
	/* TnI */
	multpcset(11,&P);
	C->i=TRUE;
	if(trel(&P,&B,PCSET_d,&(C->t))) {
		return TRUE;
	}

	/* TnMI */
	multpcset(5,&P);
	C->m=TRUE;
	if(trel(&P,&B,PCSET_d,&(C->t))) {
		return TRUE;
	}
	
	C->i=FALSE;
	C->t=0;
	C->m=FALSE;
	return FALSE;
}




int ordered_ttorel(PCSEG_t A,PCSEG_t B,SO_t *C);
int ordered_ttorel(PCSEG_t A,PCSEG_t B,SO_t *C) {
	PCSEG_t P;
	
	C->i=FALSE;
	C->t=0;
	C->m=FALSE;

	if(A.plen!=B.plen) {
		return FALSE;
	}
	cpypcseg(&P,A);

	/* Tn */
	if(trel(&P,&B,PCSEG_d,&(C->t))) {
		return TRUE;
	}

	/* TnM */
	multpcseg(5,&P);
	if(trel(&P,&B,PCSEG_d,&(C->t))) {
		return TRUE;
	}
	multpcseg(5,&P);
	C->m=FALSE;
	 
	/* TnI */
	multpcseg(11,&P);
	C->i=TRUE;
	if(trel(&P,&B,PCSEG_d,&(C->t))) {
		return TRUE;
	}

	/* TnMI */
	multpcseg(5,&P);
	C->m=TRUE;
	if(trel(&P,&B,PCSEG_d,&(C->t))) {
		return TRUE;
	}
	
	C->i=FALSE;
	C->t=0;
	C->m=FALSE;
	return FALSE;
}

int sorel(PCSEG_t A,PCSEG_t B,SO_t *C) {
	PCSEG_t P;
	int i;
	
	C->i=FALSE;
	C->t=0;
	C->m=FALSE;
	C->r=FALSE;
	C->rot=FALSE;

	if(A.plen!=B.plen) {
		return FALSE;
	}
	cpypcseg(&P,A);
	
	if(ordered_ttorel(P,B,C)) {
		return TRUE;
	}
	
	for(i=0;i<A.plen;i++) {
		if(ordered_ttorel(P,B,C)) {
			return TRUE;
		}
		retpcseg(&P);
		if(ordered_ttorel(P,B,C)) {
			C->r=TRUE;
			return TRUE;
		}
		retpcseg(&P);
		C->r=FALSE;
		rotpcseg(1,&P);
		C->rot++;
	}	
	
	return FALSE;
}

