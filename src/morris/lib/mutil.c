/*
 * mutil.c
 *
 * rohan drape, 10/96
 */


#include "mtypes.h"
#include "mutil.h"

int mod(int a,int b) {
	while(a>=b){
		a-=b;
	}
	while(a<0){
		a+=b;
	}
	return a;
}

int cpypcset(PCSET_t *A,PCSET_t B) {
	int i;
	for(i=0;i<B.plen;i++) {
		A->p[i]=B.p[i];
	}
	A->plen=B.plen;
	return TRUE;
}

int cpypcseg(PCSEG_t *A,PCSEG_t B) {
	int i;
	for(i=0;i<B.plen;i++) {
		A->p[i]=B.p[i];
	}
	A->plen=B.plen;
	return TRUE;
}

int trspcset(int n,PCSET_t *A) {
	int i;
	for(i=0;i<A->plen;i++) {
		A->p[i]+=n;
		A->p[i]=mod(A->p[i],12);
	}
	return TRUE;
}

int multpcseg(int n,PCSEG_t *A) {
	return multpcset(n,(PCSET_t *)A);
}

int multpcset(int n,PCSET_t *A) {
	int i;
	for(i=0;i<A->plen;i++) {
		A->p[i]*=n;
		A->p[i]=mod(A->p[i],12);
	}
	return TRUE;
}

int trspcseg(int n,PCSEG_t *A) {
	return trspcset(n,(PCSET_t *)A);
}

int retpcseg(PCSEG_t *A) {
	int i,temp,index=A->plen/2;
	
	for(i=0;i<index;i++) {
		temp=A->p[i];
		A->p[i]=A->p[A->plen-1-i];
		A->p[A->plen-1-i]=temp;
	}
	return TRUE;
}

int rotpcseg(int n,PCSEG_t *A) {
	int i,temp;
	
	n=mod(n,A->plen);
	while(n--) {
		temp=A->p[A->plen-1];
		for(i=A->plen-1;i>0;i--) {
			A->p[i]=A->p[i-1];
		}
		A->p[0]=temp;
	}
	return TRUE;
}

char i2c(int i) {
	switch(i) {
		case 0: return '0';
		case 1: return '1';
		case 2: return '2';
		case 3: return '3';
		case 4: return '4';
		case 5: return '5';
		case 6: return '6';
		case 7: return '7';
		case 8: return '8';
		case 9: return '9';
		case 10: return 'A';
		case 11: return 'B';
		case 12: return 'C';
		default : return ' '; /* this is required */
	}
}

int c2i(char c) {
	switch(c) {
		case '0': return 0;
		case '1': return 1;
		case '2': return 2;
		case '3': return 3;
		case '4': return 4;
		case '5': return 5;
		case '6': return 6;
		case '7': return 7;
		case '8': return 8;
		case '9': return 9;
		case 'a': return 10;
		case 'A': return 10;
		case 't': return 10;
		case 'b': return 11;
		case 'B': return 11;
		case 'e': return 11;
		default : return -1;
	}
}

