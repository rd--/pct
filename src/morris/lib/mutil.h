/*
 * mutil.h - standard utilities
 *
 * rohan drape, 6/96
 */

/*
 * the value of a modulo b
 */
int mod(int a,int b);

/*
 * copy the PCSET B into the PCSET A
 */
int cpypcset(PCSET_t *A,PCSET_t B);

/*
 * copy the PCSEG B into the PCSEG A
 */
int cpypcseg(PCSEG_t *A,PCSEG_t B);

/*
 * apply the operation Tn to the PCSET A
 *
 * Definition: Morris 1987, p.65, DEF 3.2.1
 */
int trspcset(int n,PCSET_t *A);

/*
 * apply the operation Mn to the PCSET A
 *
 * Definition: Morris 1987, p.65, DEF 3.2.2
 */
int multpcset(int n,PCSET_t *A);

/*
 * apply the operation Tn to the PCSEG A
 *
 * Definition: Morris 1987, p.65, DEF 3.2.1
 */
int trspcseg(int n,PCSEG_t *A);

/*
 * apply the operation Mn to the PCSEG A
 *
 * Definition: Morris 1987, p.65, DEF 3.2.2
 */
int multpcseg(int n,PCSEG_t *A);

/*
 * apply the operation R to the PCSEG A
 *
 * Definition: Morris 1987, p.108, DEF 3.11.1
 */
int retpcseg(PCSEG_t *A);

/*
 * apply the operation rn to the PCSEG A
 *
 * Definition: Morris 1987, p.108, DEF 3.11.2
 */
int rotpcseg(int n,PCSEG_t *A);

/*
 * convert an integer into its character representation
 *
 * Definition: Morris 1987, p.???
 */
char i2c(int i);

/*
 * convert an character into the integer it representats
 *
 * Definition: Morris 1987, p.???
 */
int c2i(char c);
