/*
 * morris.c
 *
 * rohan drape, 10/96
 */

#include <stdio.h>
#include <stdlib.h>

#include "mtypes.h"
#include "morris.h"
#include "mutil.h"

PC_t sum(PC_t a,PC_t b) {
	PC_t r=a+b;
	return mod(r,12);
}

I_t interval(PC_t a,PC_t b) {
	I_t r=b-a;
	return mod(r,12);
}

IC_t ic(PC_t a,PC_t b) {
	IC_t r1=b-a,r2=a-b;
	if((r1=mod(r1,12))>(r2=mod(r2,12))) {
		return r2;
	}
	else {
		return r1;
	}
}

ISEG_t IV(PCSET_t A,PCSET_t B) {
	int x,y;
	ISEG_t C;

	C.plen=12;
	for(x=0;x<12;x++) {
		C.p[x]=0;
	}
	
	for(x=0;x<A.plen;x++) {
		for(y=0;y<B.plen;y++) {
			C.p[interval(A.p[x],B.p[y])]++;
		}
	}
	return C;
}

ICSEG_t ICV(SC_t A) {
	int x,y;
	SC_t B=A;
	ICSEG_t C;

	C.plen=7;
	for(x=0;x<7;x++) {
		C.p[x]=0;
	}
	
	for(x=0;x<A.plen;x++) {
		for(y=0;y<B.plen;y++) {
			if(y<=x) {
				C.p[ic(A.p[x],B.p[y])]++;
			}
		}
	}
	return C;
	
}

int MUL(PCSET_t A,PCSET_t B,int n) {
	int i,j;
	int r=0;
	
	for(i=0;i<A.plen;i++) {
		for(j=0;j<B.plen;j++) {
			/*printf("i<%X,%X>=%d\n",A.p[i],B.p[j],interval(A.p[i],B.p[j]));*/
			if(interval(A.p[i],B.p[j])==n) {
				r++;
			}
		}
	}
	return r;
}

int S(PCSET_t A,PCSET_t B,int n) {
	int i,j;
	int r=0;
	
	for(i=0;i<A.plen;i++) {
		for(j=0;j<B.plen;j++) {
			/*printf("sum<%X,%X>=%d\n",A.p[i],B.p[j],sum(A.p[i],B.p[j]));*/
			if(sum(A.p[i],B.p[j])==n) {
				r++;
			}
		}
	}
	return r;
}

ISEG_t INTm(int m,PCSEG_t A) {
	int i;
	ISEG_t B;
	
	m=mod(m,A.plen);
	B.plen=A.plen-m;
	for(i=0;i<A.plen-m;i++) {
		B.p[i]=mod(A.p[i+m]-A.p[i],12);
	}
	return B;
}

ISEG_t CINTm(int m,PCCYC_t A) {
	int i;
	ISEG_t B;
	
	m=mod(m,A.plen);
	B.plen=A.plen;
	for(i=0;i<A.plen;i++) {
		B.p[i]=mod(A.p[mod(i+m,A.plen)]-A.p[i],12);
	}
	return B;
}

int PR(PCSEG_t A) {
	int r=0,n;
	for(n=1;n<A.plen;n++) {
		r+=n;
	}
	return r;
}


SEG_t OMpQ(PCSEG_t P,PCSEG_t Q) {
	int i,j;
	SEG_t A;
	
	A.plen=Q.plen;
	for(i=0;i<Q.plen;i++) {
		for(j=0;j<P.plen;j++) {
			if(Q.p[i]==P.p[j]) {
				A.p[i]=j;
			}
		}
	}
	return A;
}

PCA_t ORMAPtable(PCSEG_t P) {
	int i;
	PCA_t E;
	PCSEG_t Q=P;
	SEG_t S;
	
	E.r=P.plen;
	E.c=1;
	for(i=0;i<P.plen;i++) {
		E.W[i][0].plen=P.plen;
	}
	
	for(i=0;i<P.plen;i++) {
		S=OMpQ(P,Q);
		E.W[i][0]=S;
		trspcseg(P.p[i]-P.p[i+1],&Q);
	}
	return E;
}

int OI(PCSEG_t P,PCSEG_t Q) {
	int r=0,i,j;
	SEG_t S=OMpQ(P,Q);
	
	for(i=0;i<S.plen;i++) {
		for(j=i+1;j<S.plen;j++) {
			if((S.p[i]-S.p[j])>0) {
				r++;
			}
		}
	}
	return r;
}

int DIS(PCSEG_t P,PCSEG_t Q) {
	int r=0,n;
	SEG_t S=OMpQ(P,Q);
	
	for(n=0;n<S.plen;n++) {
		r+=abs(n-S.p[n]);
	}
	return r;
}

int SCAT(PCSEG_t P,PCSEG_t Q) {
	int r=0,k;
	SEG_t V=OMpQ(P,Q);
	
	for(k=0;k<P.plen-1;k++) {
		r+=abs(V.p[k]-V.p[k+1]);
	}
	r-=P.plen-1;
	return r;
}

double FSUM(SEG_t X,SEG_t Y);
double FSUM(SEG_t X,SEG_t Y) {
	int n;
	double a=0.0,b=0.0;
	
	for(n=0;n<X.plen;n++) {
		a+=X.p[n]*Y.p[n];
	}
	for(n=0;n<X.plen;n++) {
		b+=n;
	}
	b*=b;
	b/=X.plen;
	return (a-b);
}

double CC(PCSEG_t P,PCSEG_t Q) {
	SEG_t S=OMpQ(P,P),V=OMpQ(P,Q);
	
	return FSUM(S,V)/FSUM(S,S);
}

int Tmatrix(PCSEG_t A,PCSEG_t B,PCA_t *E) {
	int i,x,y;
	
	E->r=A.plen;
	E->c=1;
	for(i=0;i<A.plen;i++) {
		E->W[i][0].plen=B.plen;
	}
	
	for(x=0;x<A.plen;x++) {
		for(y=0;y<B.plen;y++) {
			E->W[x][0].p[y]=interval(A.p[x],B.p[y]);
		}
	}
	return TRUE;
}

int subTmatrix(PCSEG_t A,PCSEG_t B,PCA_t *E,PC_t n) {
	int i,x,y;
	
	E->r=A.plen;
	E->c=1;
	for(i=0;i<A.plen;i++) {
		E->W[i][0].plen=B.plen;
	}
	
	for(x=0;x<A.plen;x++) {
		for(y=0;y<B.plen;y++) {
			E->W[x][0].p[y]=interval(A.p[x],B.p[y]);
			if(E->W[x][0].p[y]!=n) {
				E->W[x][0].p[y]=-1;
			}
		}
	}
	return TRUE;
}

/* the following functions are simply the above
two with the call to interval() replaced by a call
to sum(). any fixes should be applied above and
then reproduced here. */
int Imatrix(PCSEG_t A,PCSEG_t B,PCA_t *E) {
	int i,x,y;
	
	E->r=A.plen;
	E->c=1;
	for(i=0;i<A.plen;i++) {
		E->W[i][0].plen=B.plen;
	}
	
	for(x=0;x<A.plen;x++) {
		for(y=0;y<B.plen;y++) {
			E->W[x][0].p[y]=sum(A.p[x],B.p[y]);
		}
	}
	return TRUE;
}

int subImatrix(PCSEG_t A,PCSEG_t B,PCA_t *E,PC_t n) {
	int i,x,y;
	
	E->r=A.plen;
	E->c=1;
	for(i=0;i<A.plen;i++) {
		E->W[i][0].plen=B.plen;
	}
	
	for(x=0;x<A.plen;x++) {
		for(y=0;y<B.plen;y++) {
			E->W[x][0].p[y]=sum(A.p[x],B.p[y]);
			if(E->W[x][0].p[y]!=n) {
				E->W[x][0].p[y]=-1;
			}
		}
	}
	return TRUE;
}
