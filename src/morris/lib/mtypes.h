/*
 * mtypes.h - typedefs for pc objects and general #defines
 *
 * rohan drape, 6/96
 */


#define TRUE  1
#define FALSE 0

#define MAX_PCSET 24
#define MAX_STR 512

#define PC_d     1
#define IC_d     2
#define ICSEG_d  3
#define ISEG_d   4
#define SEG_d    5
#define PCSET_d  6
#define PCSEG_d  7
#define PCCYC_d  8
#define SC_d     9
#define PCA_d    10
#define TTO_d    11
#define SO_d     12



/*
 * Definition: Morris 1987, p.60, DEF 3.1
 */
typedef int PC_t;

/*
 * Definition: Morris 1987, p.62, DEF 3.1.1
 */
typedef int I_t;

/*
 * Definition: Morris 1987, p.63, DEF 3.1.2.1
 */
typedef int IC_t;

/*
 * Definition: Morris 1987, p.64
 */
typedef struct {
	PC_t p[MAX_PCSET];
	int plen;
} PCSET_t;

/*
 * Definition: Morris 1987, p.64
 */
typedef PCSET_t PCSEG_t;

typedef PCSET_t ICSEG_t;

typedef PCSET_t ISEG_t;

typedef PCSET_t SEG_t; /* generic segment type */

/*
 * Definition: Morris 1987, p.65
 */
typedef PCSET_t PCCYC_t;

typedef PCSET_t SC_t;

/*
 * Definition: Morris 1987, p.66, DEF 3.3
 */
typedef struct {
	int t,i,m;
} TTO_t;

typedef struct {
	int t,i,m,r,rot;
} SO_t;

/*
 * Definition: Morris 1987, p.184, DEF 5.1
 */
typedef struct {
	PCSEG_t W[MAX_PCSET][MAX_PCSET];
	int r,c;
} PCA_t;





