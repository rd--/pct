/*
 * mrel.h - TTO and order operator relationship determinants
 *
 * rohan drape, 11/96
 */


#include <stdio.h>

/*
 * are the PCSETs A and B related by a TTO
 * if true the TTO to map A onto B is placed into C
 *
 * Definition: Morris 1987, p.66, DEF 3.3 (see also THs 3.3.1 to 3.3.4)
 */
int ttorel(PCSET_t A,PCSET_t B,TTO_t *C);

/*
 * are the PCSEGs A and B related by a SO
 * if true the SO to map A onto B is placed into C
 *
 * Definition: Morris 1991, p.16, (extended to include the operator M)
 */
int sorel(PCSEG_t A,PCSEG_t B,SO_t *C);

/*
 * are the PCSETs or PCSEGs A and B related by Tn
 * if true n holds the transposition subscript
 *
 * Definition: Morris 1987, p.65, DEF 3.2.1
 */
int trel(void *A,void *B,int type,int *n);

/*
 * are the PCSETs or PCSEGs A and B related by M11 (inversion)
 *
 * Definition: Morris 1987, p.65, DEF 3.2.2
 */
int irel(void *A,void *B,int type);

/*
 * are the PCSETs or PCSEGs A and B related by M5
 *
 * Definition: Morris 1987, p.65, DEF 3.2.2
 */
int mrel(void *A,void *B,int type);

/*
 * are the PCSEGs A and B related by R
 *
 * Definition: Morris 1987, p.108, DEF 3.11.1
 */
int rrel(PCSEG_t *A,PCSEG_t *B);

/*
 * are the PCSEGs A and B related by rotation
 * if true n holds the rotation subscript
 *
 * Definition: Morris 1987, p.108, DEF 3.11.2
 */
int rotrel(PCSEG_t *A,PCSEG_t *B,int *n);

