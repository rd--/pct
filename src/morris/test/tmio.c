/*
 * tmio.c
 * 
 * rohan drape,  11/96
 */


#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

#include "../lib/mlib.h"


void usage(char *pname);


int main(int argc,char **argv) {
	char str[MAX_STR];
	int i;
	PC_t A;
	I_t B;
	IC_t C;
	ICSEG_t D;
	ISEG_t E;
	PCSET_t F;
	PCSEG_t G;
	PCCYC_t H;
	SC_t I;
	PCA_t J;
	SO_t K;
	TTO_t L;
	
	strpco("5",PC_d,&A);
	pcostr(&A,PC_d,str);
	printf("\t%d = %s\n",A,str);
	
	strpco("125",PCSET_d,&F);
	pcostr(&F,PCSET_d,str);
	printf("\t{125} = {%s}\n",str);
	
	strpco("125",PCCYC_d,&H);
	pcostr(&H, PCCYC_d,str);
	printf("\t{125-} = {%s}\n",str);
	
	printf("\nall tests passed\n");
	exit(EXIT_SUCCESS);return EXIT_SUCCESS;
}

void usage(char *pname) {
	fprintf(stderr,"usage: %s \n",pname);
	exit(EXIT_FAILURE);
}