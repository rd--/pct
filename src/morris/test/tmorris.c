/*
 * testmorris.c
 * 
 * 
 * rohan drape,  11/96
 */


#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

#include "../lib/mlib.h"


void usage(char *pname);


int main(int argc,char **argv) {
	char strA[MAX_STR],strB[MAX_STR];
	int i;
	PC_t pcA,pcB;
	I_t iA,iB;
	IC_t icA,icB;
	ICSEG_t icsegA,icsegB;
	ISEG_t isegA,isegB;
	PCSET_t pcsetA,pcsetB;
	PCSEG_t pcsegA,pcsegB;
	PCCYC_t pccycA,pccycB;
	SC_t scA,scB;
	PCA_t pcaA,pcaB;
	SO_t soA,soB;
	TTO_t ttoA,ttoB;
	
	printf("\ntest sum()\n");
	for(pcA=0,pcB=4;pcB<12;pcA++,pcB++) {
		i=sum(pcA,pcB);
		printf("\tsum(%X,%X)=%X\n",pcA,pcB,i);
	}
	
	printf("\ntest interval()\n");
	for(pcA=0,pcB=1;pcB<12;pcB++) {
		iA=interval(pcA,pcB);
		printf("\tinterval(%X,%X)=%X\n",pcA,pcB,iA);
	}
	
	printf("\ntest ic()\n");
	for(pcA=0,pcB=1;pcB<12;pcB++) {
		icA=ic(pcA,pcB);
		printf("\tic(%X,%X)=%X\n",pcA,pcB,icA);
	}
	
	printf("\ntest ICV()\n");
	strpco("0279B",SC_d,&scA);
	icsegA=ICV(scA);pcostr(&icsegA,ICSEG_d,strA);
	printf("\tICV({0279B})=[%s]\n",strA);

	printf("\ntest IV()\n");
	strpco("069",PCSET_d,&pcsetA);strpco("5276",PCSET_d,&pcsetB);
	isegA=IV(pcsetA,pcsetB);pcostr(&isegA,ISEG_d,strA);
	printf("\tIV({069},{5276})=[%s]\n",strA);

	printf("\ntest MUL()\n");
	strpco("01",PCSET_d,&pcsetA);strpco("05",PCSET_d,&pcsetB);
	pcostr(&pcsetA,PCSET_d,strA);pcostr(&pcsetB,PCSET_d,strB);
	for(i=0;i<12;i++) {
		printf("\tMUL({%s},{%s},%d)=%d\n",strA,strB,i,MUL(pcsetA,pcsetB,i));
	}

	printf("\ntest S()\n");
	strpco("069",PCSET_d,&pcsetA);strpco("15276",PCSET_d,&pcsetB);
	pcostr(&pcsetA,PCSET_d,strA);pcostr(&pcsetB,PCSET_d,strB);
	for(i=0;i<12;i++) {
		printf("\tS({%s},{%s},%d)=%d\n",strA,strB,i,S(pcsetA,pcsetB,i));
	}

	printf("\ntest INTm()\n");
	strpco("0612",PCSEG_d,&pcsegA);
	for(i=0;i<=pcsegA.plen;i++) {
		isegA=INTm(i,pcsegA);
		pcostr(&pcsegA,PCSEG_d,strA);pcostr(&isegA,ISEG_d,strB);
		printf("\tINT%X(<%s>)=<%s>\n",i,strA,strB);
	}
	
	printf("\ntest CINTm()\n");
	strpco("0612",PCCYC_d,&pccycA);
	for(i=0;i<=pccycA.plen;i++) {
		isegA=CINTm(i,pccycA);
		pcostr(&pccycA,PCCYC_d,strA);pcostr(&isegA,ISEG_d,strB);
		printf("\tCINT%X(<%s>)=<%s>\n",i,strA,strB);
	}
	
	printf("\ntest PR()\n");
	strpco("01234",PCSEG_d,&pcsegA);
	printf("\tPR({01234})=%d\n",PR(pcsegA));
	
	printf("\ntest ORMAPtable()\n");
	strpco("04523A19B768",PCSEG_d,&pcsegA);
	pcaA=ORMAPtable(pcsegA);
	pcostr(&pcaA,PCA_d,strA);printf("\tORMAP table({04523A19B768})=\n%s\n",strA);
	
	printf("\ntest OI()\n");
	strpco("0527",PCSEG_d,&pcsegA);strpco("5027",PCSEG_d,&pcsegB);
	printf("\tOI({0527},{5027})=%d\n",OI(pcsegA,pcsegB));
	strpco("0527",PCSEG_d,&pcsegA);strpco("7025",PCSEG_d,&pcsegB);
	printf("\tOI({0527},{7025})=%d\n",OI(pcsegA,pcsegB));
	
	printf("\ntest DIS()\n");
	strpco("03B72",PCSEG_d,&pcsegA);strpco("37B20",PCSEG_d,&pcsegB);
	printf("\tDIS({03B72},{37B20})=%d\n",DIS(pcsegA,pcsegB));
	
	printf("\ntest SCAT()\n");
	strpco("4671",PCSEG_d,&pcsegA);strpco("4671",PCSEG_d,&pcsegB);
	printf("\tSCAT({4671},{4671})=%d\n",SCAT(pcsegA,pcsegB));
	strpco("4671",PCSEG_d,&pcsegA);strpco("1647",PCSEG_d,&pcsegB);
	printf("\tSCAT({4671},{1647})=%d\n",SCAT(pcsegA,pcsegB));
	
	printf("\ntest CC()\n");
	strpco("0562",PCSEG_d,&pcsegA);strpco("0562",PCSEG_d,&pcsegB);
	printf("\tCC({0562},{0562})=%f\n",CC(pcsegA,pcsegB));
	strpco("0562",PCSEG_d,&pcsegA);strpco("2560",PCSEG_d,&pcsegB);
	printf("\tCC({0562},{2560})=%f\n",CC(pcsegA,pcsegB));
	
	printf("\ntest Tmatrix()\n");
	strpco("025",PCSEG_d,&pcsegA);strpco("1B2407",PCSEG_d,&pcsegB);
	assert(Tmatrix(pcsegA,pcsegB,&pcaA));
	pcostr(&pcaA,PCA_d,strA);printf("\tT-matrix({025},{1B2407})=\n%s\n",strA);
	
	printf("\ntest Imatrix()\n");
	strpco("025",PCSEG_d,&pcsegA);strpco("1B2407",PCSEG_d,&pcsegB);
	assert(Imatrix(pcsegA,pcsegB,&pcaA));
	pcostr(&pcaA,PCA_d,strA);printf("\tI-matrix({025},{1B2407})=\n%s\n",strA);
	
	printf("\nall tests passed\n");
	exit(EXIT_SUCCESS);return EXIT_SUCCESS;
}

void usage(char *pname) {
	fprintf(stderr,"usage: %s \n",pname);
	exit(EXIT_FAILURE);
}