/*
 * testmrel.c
 * 
 * 
 * rohan drape,  11/96
 */


#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

#include "../lib/mlib.h"


void usage(char *pname);


int main(int argc,char **argv) {
	char str[MAX_STR];
	PC_t A,B;
	PCSET_t C,D;
	PCSEG_t E,F;
	SO_t G,H;
	TTO_t I,J;
	
	printf("testing ttorel(A,B,PCSET_d)\n");
	strpco("0124",PCSET_d,&C);strpco("1235",PCSET_d,&D); /* T1 */
	assert(ttorel(C,D,&I));
	pcostr(&I,TTO_d,str);printf("\t%s\n",str);
	strpco("0124",PCSET_d,&C);strpco("10B9",PCSET_d,&D); /* T1I */
	assert(ttorel(C,D,&I));
	pcostr(&I,TTO_d,str);printf("\t%s\n",str);
	strpco("0124",PCSET_d,&C);strpco("1835",PCSET_d,&D); /* T1MI */
	assert(ttorel(C,D,&I));
	pcostr(&I,TTO_d,str);printf("\t%s\n",str);
	
	printf("testing sorel(A,B,PCSEG_d)\n");
	strpco("0124",PCSEG_d,&E);strpco("5321",PCSEG_d,&F); /* RT1 */
	assert(sorel(E,F,&G));
	pcostr(&G,SO_d,str);printf("\t%s\n",str);
	strpco("0124",PCSEG_d,&E);strpco("9B01",PCSEG_d,&F); /* RT1I */
	assert(sorel(E,F,&G));
	pcostr(&G,SO_d,str);printf("\t%s\n",str);
	strpco("0124",PCSEG_d,&E);strpco("5381",PCSEG_d,&F); /* RT1MI */
	assert(sorel(E,F,&G));
	strpco("0124",PCSEG_d,&E);strpco("3815",PCSEG_d,&F); /* r1RT1MI */
	assert(sorel(E,F,&G));
	pcostr(&G,SO_d,str);printf("\t%s\n",str);
		
	printf("\nall tests passed\n");
	exit(EXIT_SUCCESS);return EXIT_SUCCESS;
}

void usage(char *pname) {
	fprintf(stderr,"usage: %s \n",pname);
	exit(EXIT_FAILURE);
}