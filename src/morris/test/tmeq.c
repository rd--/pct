/*
 * testmeq.c
 * 
 * 
 * rohan drape,  11/96
 */


#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

#include "../lib/mlib.h"


void usage(char *pname);


int main(int argc,char **argv) {
	char str[MAX_STR];
	PC_t A,B;
	PCSET_t C,D;
	PCSEG_t E,F;
	SO_t G,H;
	TTO_t I,J;
	
	printf("testing isequal(A,B,PC_d)\n");
	A=1;B=2;
	assert(!isequal(&A,&B,PC_d));
	A=1;B=1;
	assert(isequal(&A,&B,PC_d));
	
	printf("testing isequal(A,B,PCSET_d)\n");
	strpco("0135",PCSET_d,&C);strpco("0235",PCSET_d,&D); /* false */
	assert(!isequal(&C,&D,PCSET_d));
	strpco("0135",PCSET_d,&C);strpco("0135",PCSET_d,&D); /* true */
	assert(isequal(&C,&D,PCSET_d));
	
	printf("testing isequal(A,B,PCSEG_d)\n");
	strpco("0123",PCSEG_d,&E);strpco("0132",PCSEG_d,&F); /* false */
	assert(!isequal(&E,&F,PCSEG_d));
	strpco("0123",PCSEG_d,&E);strpco("0123",PCSEG_d,&F); /* true */
	assert(isequal(&E,&F,PCSEG_d));
	
	printf("testing isequiv(A,B,PCSET_d)\n");
	strpco("0135",PCSET_d,&C);strpco("1345",PCSET_d,&D); /* false */
	assert(!isequiv(&C,&D,PCSET_d));
	strpco("0123",PCSET_d,&C);strpco("1234",PCSET_d,&D);/* T1 */
	assert(isequiv(&C,&D,PCSET_d));
	strpco("0123",PCSET_d,&C);strpco("10BA",PCSET_d,&D);/* T1I */
	assert(isequiv(&C,&D,PCSET_d));
	strpco("0123",PCSET_d,&C);strpco("183A",PCSET_d,&D);/* T1MI */
	assert(isequiv(&C,&D,PCSET_d));
	
	printf("testing isequiv(A,B,PCSEG_d)\n");
	strpco("0135",PCSEG_d,&E);strpco("0153",PCSEG_d,&F); /* false */
	assert(!isequiv(&E,&F,PCSEG_d));
	strpco("0123",PCSEG_d,&E);strpco("4321",PCSEG_d,&F); /* RT1 */
	assert(isequiv(&E,&F,PCSEG_d));
	strpco("0123",PCSEG_d,&E);strpco("1432",PCSEG_d,&F); /* r1RT1 */
	assert(isequiv(&E,&F,PCSEG_d));
		
	printf("\nall tests passed\n");
	exit(EXIT_SUCCESS);return EXIT_SUCCESS;
}

void usage(char *pname) {
	fprintf(stderr,"usage: %s \n",pname);
	exit(EXIT_FAILURE);
}