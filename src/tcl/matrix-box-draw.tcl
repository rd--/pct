# matrix-box-draw.tcl - (c) rohan drape, 2000-2002

# A matrix is stored as a list of lists.

# Matrix meta-data.

set g_matrix(canvas_width)       "30c"
set g_matrix(canvas_height)      "6c"
set g_matrix(seperator_position) "^"
set g_matrix(seperator_row)      "\n"
set g_matrix(font)               "Helvetica 6"
set g_matrix(draw_box_width)     1
set g_matrix(draw_location)      1

option add *Button.Pad           0
option add *Font                 "Helvetica 10"
option add *Menu.TearOff         0

# Return the sum of the list of numbers 'l'.

proc list_sum {l} {
    set sum 0
    foreach n $l {
	set sum [expr $sum + $n]
    }
    return $sum
}

# Return the largest number in the list of numbers 'l'.

proc list_maxima {l} {
    set max 0
    foreach n $l {
	if {$n > $max} {
	    set max $n
	}
    }
    return $max
}

# Return the number of rows in 'matrix'.

proc matrix_rows {matrix} {
    return [llength $matrix]
}

# Return the position at 'row' and 'column' in 'matrix'.

proc matrix_position {matrix row column} {
    set row_l [lindex $matrix row]
    return [lindex $row_l $column]
}

# Return the matrix described at 'text'.

proc matrix_text_parse {text} {
    global g_matrix
    set matrix [list]
    set row_l [split $text $g_matrix(seperator_row)]
    foreach row $row_l {
	lappend matrix [split $row $g_matrix(seperator_position)]
    }
    return $matrix
}

# Return a text description of 'matrix'.

proc matrix_text_gen {matrix} {
    global g_matrix
    set row_l [list]
    foreach row $matrix {
	lappend row_l [join $row $g_matrix(seperator_position)]
    }
    return [join $row_l $g_matrix(seperator_row)]
}

# Multiply all elements in 'matrix' by 'n'.

proc matrix_scale {matrix n} {
    set matrix_n [list]
    foreach row $matrix {
	set row_n [list]
	foreach element $row {
	    lappend row_n [expr $element * $n]
	}
	lappend matrix_n $row_n
    }
    return $matrix_n
}

# Returns a list of the sums of each row in 'matrix'.

proc matrix_row_lengths {matrix} {
    set row_lengths_l [list]
    foreach row $matrix {
	lappend row_lengths_l [list_sum $row]
    }
    return $row_lengths_l
}

# Return a cumulative matrix of 'matrix'.

proc matrix_cumulative {matrix} {
    set matrix_c [list]
    foreach row $matrix {
	set row_c [list]
	set l 0
	foreach element $row {
	    lappend row_c $l
	    set l [expr $l + $element]
	}
	lappend matrix_c $row_c
    }
    return $matrix_c
}

# Return an integer matrix of 'matrix'.

proc matrix_integer {matrix} {
    set matrix_c [list]
    foreach row $matrix {
	set row_c [list]
	foreach element $row {
	    lappend row_c [expr int($element)]
	}
	lappend matrix_c $row_c
    }
    return $matrix_c
}

# Subdivide positions in 'matrix' according to 'divisions_l'.

proc matrix_subdivide {matrix divisions_l} {
    proc _get_new_position_l {position divisions_l} {
	foreach division $divisions_l {
	    set numerator [lindex $division 0]
	    set denominator [lindex $division 1]
	    if {$position == $numerator} {
		set r [expr double($numerator) / $denominator]
		set r_l [list]
		for {set i 0} {$i < $denominator} {incr i} {
		    lappend r_l $r
		}
		return $r_l
	    }
	}
	return $position
    }
    set matrix_s [list]
    foreach row $matrix {
	set row_s [list]
	foreach element $row {
	    set row_s [concat $row_s [_get_new_position_l $element $divisions_l]]
	}
	lappend matrix_s $row_s
    }
    return $matrix_s
}

# Draw a 'box' representation of text descvription at 'matrix' into
# the canvas 'c'.  Use the text description `commentary' matrix to
# label each box.  Commentary can be empty or partial.

proc matrix_box_draw {matrix commentary c} {
    global g_matrix

    # The drawing should not run to the very edges of the canvas.
    set margin 20
    set half_margin [expr $margin / 2]

    set matrix_c [matrix_cumulative $matrix]
    set number_of_rows [llength $matrix]
    set row_length_l [matrix_row_lengths $matrix]
    set maximum_row_length [list_maxima $row_length_l]

    set drawing_width  [$c cget -width]
    set drawing_height [$c cget -height]

    # Allow space for `margins'.
    set drawing_width [expr $drawing_width - $margin]
    set drawing_height [expr $drawing_height - $margin]

    set unit_length [expr double($drawing_width) / $maximum_row_length]
    set row_height [expr double($drawing_height) / $number_of_rows]

    # The starting point.
    set x1 $half_margin
    set y1 $half_margin

    $c delete all

    # Due to the nature of the Tcl foreach command there is no
    # need to handle commentary especially.  If it is empty or
    # partial then empty labels will be drawn.
    foreach row $matrix row_c $matrix_c row_t $commentary {
	foreach position $row position_c $row_c position_t $row_t {
	    # x1,y1 is the upper left point.
	    # x2,y2 is the lower right point.
	    # x3,y3 is the center of the box.
	    set x2 [expr $x1 + ($unit_length * $position)]
	    set y2 [expr $y1 + $row_height]
	    set x3 [expr $x1 + (($x2 - $x1) / 2)]
	    set y3 [expr $y1 + (($y2 - $y1) / 2)]
	    # Create box.
	    $c create rectangle $x1 $y1 $x2 $y2
	    # Write box width text if requested.
	    if {$g_matrix(draw_box_width)} {
		$c create text $x3 $y1 \
			-text [format "%.2G" $position] \
			-anchor n
	    }
	    # Write cummulative `location' text if requested.
	    if {$g_matrix(draw_location)} {
		$c create text $x1 $y3 \
			-text [format "%.2G" $position_c] \
			-anchor w \
			-fill red \
			-font $g_matrix(font)
	    }
	    # Write commentary text, the text is forced to not exceed
	    # the box dimensions by using the '-width' option..
	    $c create text $x3 $y3 \
		    -text $position_t \
		    -anchor center \
		    -fill blue \
		    -font $g_matrix(font) \
		    -width [expr $x2 - $x1]
	    set x1 $x2
	}
	set x1 $half_margin
	set y1 $y2
    }
}

# Perform an action as requested at `op'.  `c' is the drawing canvas
# widget. `n' is the numerical matrix text widget.  `t' is the textual
# matrix text widget.

proc interface_operation {w c n t op} {
    switch $op {
	"clear" {
	    $c delete all
	    $n delete 0.0 end
	    $t delete 0.0 end
	}
	"read-numbers" {
	    set stream [open [tk_getOpenFile] "r"]
	    $n insert end [read $stream]
	    close $stream
	}
	"read-commentary" {
	    set stream [open [tk_getOpenFile] "r"]
	    $t insert end [read $stream]
	    close $stream
	}
	"draw" {
	    matrix_box_draw \
		    [matrix_text_parse [$n get 0.0 end]] \
		    [matrix_text_parse [$t get 0.0 end]] \
		    $c
	}
	"cumulative" {
	    set stream [open [tk_getSaveFile] "w"]
	    puts $stream \
		    [matrix_text_gen \
		    [matrix_cumulative \
		    [matrix_text_parse [$n get 0.0 end]]]]
	    close $stream
	}
	"eps" {
	    update
	    $c postscript -file [tk_getSaveFile]
	}
	"dismiss" {
	    exit
	}
	default {
	    error "interface_operation: illegal operation"
	}
    }
    return
}


proc make_interface {} {
    global g_matrix

    # Make window
    set w .a
    toplevel $w
    wm title $w "Matrix Box Draw"

    # Make drawing canvas.
    set c .a.c
    canvas $c \
	    -width $g_matrix(canvas_width) \
	    -height $g_matrix(canvas_height) \
	    -background white
    pack $c -expand true -fill both

    # Make numerical text editor.
    set n .a.n
    text $n \
	    -height 6
    pack $n -expand t -fill x

    # Make textual text editor.
    set t .a.t
    text $t \
	    -height 6
    pack $t -expand t -fill x

    # Make control frame.
    frame .a.f
    pack .a.f
    checkbutton .a.f.a \
	    -indicatoron 1 \
	    -variable g_matrix(draw_location) \
	    -text "Draw Location"
    checkbutton .a.f.b \
	    -indicatoron 1 \
	    -variable g_matrix(draw_box_width) \
	    -text "Draw Box Width"
    entry .a.f.c \
	    -textvariable g_matrix(seperator_position)
    entry .a.f.d \
	    -textvariable g_matrix(font)
    pack .a.f.a .a.f.b .a.f.c .a.f.d -side left -expand t -fill x

    # Make menubar.
    set m .a.m
    menu $m -type menubar
    $w configure -menu $m

    # Matrix menu.
    $m add cascade -label "Matrix" -menu $m.matrix
    menu $m.matrix
    $m.matrix add command -label "Read Numbers..." \
	    -command "interface_operation $w $c $n $t read-numbers"
    $m.matrix add command -label "Read Commentary..." \
	    -command "interface_operation $w $c $n $t read-commentary"
    $m.matrix add command -label "Draw" \
	    -command "interface_operation $w $c $n $t draw"
    $m.matrix add command -label "Clear" \
	    -command "interface_operation $w $c $n $t clear"
    $m.matrix add command -label "Cumulative..." \
	    -command "interface_operation $w $c $n $t cumulative"
    $m.matrix add command -label "Eps..." \
	    -command "interface_operation $w $c $n $t eps"
    $m.matrix add command -label "Dismiss" \
	-command "interface_operation $w $c $n $t dismiss"

    # Help menu.
    $m add cascade -label "Help" -menu $m.help
    menu $m.help

    return
}

make_interface
wm withdraw .
