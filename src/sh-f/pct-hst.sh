# compatibility functions (historical)

function bip
{
    pcom pcseg iseg $@ | pcom iseg icseg | nrm -r ;
}

function cf
{
    if test $# = 1 ;
    then epmq "in cset $1" ;
    else echo "usage: cf cset" ;
    fi ;
}

function cisg
{
    cyc $@ | pcom pcseg iseg ;
}

function icseg
{
    pcom pcseg iseg $@ | pcom iseg icseg ;
}

function issb
{
    if test $# = 2 ;
    then scc $2 $1 | fn | sort -u ;
    else echo "usage: is-sb sc pcset" ;
    fi ;
}

function iseg
{
    pcom pcseg iseg $@ ;
}

function mxs
{
    if test $# = 2 ;
    then echo $2 | trs -m $1 ;
    else echo "usage: mxs pcseg pcseg" ;
    fi ;
}

function name
{
    mw C C# D D# E F F# G G# A A# B ;
}

function pcsisl
{
    if test $# = 1 ;
    then pg $1 | pcom pcseg iseg | pcom iseg iset | sort -u ;
    else echo "usage: pcsisl pcset" ;
    fi ;
}

function prt
{
    if test $# = 3 ;
    then partition $1 $2 $3 | lsrt | sort -u ;
    else echo "usage: prt pcset n cset" ;
    fi ;
}

function pup
{
    pg $@ | sort -u ;
}
