#include <stdio.h>

#include "pco.h"

#define EPM_STR_MAX 256
#define EPM_RST_SET_MAX 32
#define EPM_RST_MBR_MAX 16
#define EPM_PRPR_INCL 'p'

#define IS 1 /* values for rs_t.rel */
#define IN 2
#define HAS 3
#define ISNT 4
#define NOTIN 5
#define HASNT 6

typedef struct {
	char name[EPM_STR_MAX];
	int incl_type, rst_set_num, rst_mbr_num[EPM_RST_SET_MAX];
	Pco_t *rst_obj[EPM_RST_SET_MAX][EPM_RST_MBR_MAX];
	int rst_rel[EPM_RST_SET_MAX];
} Epm_t;

Epm_t *epmopen(char *name, char *inclusion);
void epmclose(Epm_t *e);
int epmeval(Epm_t *e, const Pco_t *obj);
int epmparse(Epm_t *e, char *str);
int epmstr(Epm_t *e, int n, char *str);
int epmread(Epm_t *e, FILE *fp);
int epmwrite(Epm_t *e, FILE *fp);
int epmrmv(Epm_t *e, char *str);

#define epmsetincltype(e, t) ((e)->incl_type = t[0])
