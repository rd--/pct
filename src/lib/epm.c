#include "epm.h"
#include "emem.h"
#include "pco.h"
#include "pct.h"
#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

Epm_t *epmopen(char *name, char *inclusion)
{
	Epm_t *e;
	e = emalloc(sizeof(Epm_t));
	strcpy(e->name, name);
	e->rst_set_num = 0;
	e->incl_type = inclusion[0];
	return e;
}

void epmclose(Epm_t *e)
{
	free(e);
}

static int evaluate_epm(const Pco_t *x, int rel, const Pco_t *y)
{
	int err = pco_cmp(y, x);

	switch (rel) {
	case IS:
		return (err == PCO_EQUAL ? 1 : 0);
		break;
	case IN:
		return (err == PCO_SUPERSET ? 1 : 0);
		break;
	case HAS:
		return (err == PCO_SUBSET ? 1 : 0);
		break;
	case ISNT:
		return (err != PCO_EQUAL ? 1 : 0);
		break;
	case NOTIN:
		return ((err != PCO_SUPERSET && err != PCO_EQUAL) ? 1 : 0);
		break;
	case HASNT:
		return ((err != PCO_SUBSET && err != PCO_EQUAL) ? 1 : 0);
		break;
	}
	return FALSE;
}

/* returns -1 for success */
int epmeval(Epm_t *e, const Pco_t *obj)
{
	int i, j;
	for (i = 0; i < e->rst_set_num; i++) {
		int set_valid = 0;
		for (j = 0; j < e->rst_mbr_num[i]; j++) {
			if (e->incl_type == EPM_PRPR_INCL || e->rst_rel[i] == ISNT || e->rst_rel[i] == HASNT) { /* proper inclusion */
				if (evaluate_epm(obj, e->rst_rel[i], e->rst_obj[i][j])) {
					set_valid = 1;
					break;
				}
			} else { /* allow equality */
				if (evaluate_epm(obj, e->rst_rel[i], e->rst_obj[i][j]) || evaluate_epm(obj, IS, e->rst_obj[i][j])) {
					set_valid = 1;
					break;
				}
			}
		}
		if (!set_valid) {
			return i;
		}
	}
	return -1;
}

static int interpret_relation(char *word)
{
	if (strcmp(word, "is") == 0) {
		return IS;
	} else if (strcmp(word, "in") == 0) {
		return IN;
	} else if (strcmp(word, "has") == 0) {
		return HAS;
	}
	if (strcmp(word, "isnt") == 0) {
		return ISNT;
	} else if (strcmp(word, "notin") == 0) {
		return NOTIN;
	} else if (strcmp(word, "hasnt") == 0) {
		return HASNT;
	} else {
		return FALSE;
	}
}

int epmparse(Epm_t *e, char *str)
{
	char word[EPM_STR_MAX], type[PCO_TYPMAX];
	int m, n, i = e->rst_set_num, j = 0;
	int err;

	assert(e->rst_set_num < EPM_RST_SET_MAX);

	/* get relation */
	err = sscanf(str, "%s%n", word, &n);
	assert(err == 1);
	e->rst_rel[i] = interpret_relation(word);
	assert(e->rst_rel[i]);

	/* get kind */
	err = sscanf(&str[n], "%s%n", type, &m);
	assert(err == 1);
	n += m;

	/* for each argument make an rs_t */
	while (sscanf(&str[n], "%s%n", word, &m) == 1) {
		n += m;
		e->rst_obj[i][j] = pco_create(type);
		assert(e->rst_obj[i][j]);
		pco_read(e->rst_obj[i][j], word);
		j++;
		if (j >= EPM_RST_MBR_MAX) {
			fprintf(stderr, "EPM_RST_MBR_MAX reached in epmparse().\n");
			break;
		}
	}

	(e->rst_set_num)++;
	e->rst_mbr_num[i] = j;
	return j;
}

static char *describe_relation(int r, char *s)
{
	switch (r) {
	case IS:
		strcpy(s, "is");
		return s;
	case IN:
		strcpy(s, "in");
		return s;
	case HAS:
		strcpy(s, "has");
		return s;
	case ISNT:
		strcpy(s, "isnt");
		return s;
	case NOTIN:
		strcpy(s, "notin");
		return s;
	case HASNT:
		strcpy(s, "hasnt");
		return s;
	}
	return NULL;
}

int epmstr(Epm_t *e, int n, char *str)
{
	char lstr[512];
	int i;
	if (n >= e->rst_set_num) {
		return FALSE;
	}
	str[0] = '\0';
	for (i = 0; i < e->rst_mbr_num[n]; i++) {
		strcat(str, describe_relation(e->rst_rel[n], lstr));
		strcat(str, " ");
		pco_set_opt(e->rst_obj[n][i], PCO_PRINT_TYPE);
		pco_write(e->rst_obj[n][i], lstr);
		strcat(str, lstr);
		strcat(str, "\n");
	}
	return TRUE;
}

int epmwrite(Epm_t *e, FILE *fp)
{
	char str[EPM_STR_MAX];
	int i, err;

	for (i = 0; i < e->rst_set_num; i++) {
		if (!(e->rst_rel[i] == IN && pco_typeof(e->rst_obj[i][0]) == sc
				&& pco_sizeof(e->rst_obj[i][0]) == 12)) {
			err = epmstr(e, i, str);
			assert(err);
			fprintf(fp, "%s\n", str);
		}
	}
	return TRUE;
}

int epmread(Epm_t *e, FILE *fp)
{
	char str[EPM_STR_MAX];
	int r, c = 0;

	while (fgets(str, EPM_STR_MAX, fp) != NULL) {
		r = epmparse(e, str);
		if (!r) {
			fprintf(stderr, "epmparse() failed in epmread()\n");
			return r;
		}
		c += r;
	}
	return c;
}

int epmrmv(Epm_t *e, char *str)
{
	fprintf(stderr, "error: unimplemented function\n");
	assert(0);
	return 1;
}
