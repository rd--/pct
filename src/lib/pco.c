#include "pco.h"
#include "emem.h"
#include "pct.h"
#include "sro.h"
#include <assert.h>
#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

static char *pco_type_list[] = { "empty", "cset", "pcset", "pcseg", "sc", "pcv",
	"iset", "iseg", "icset", "icseg", "iv", "icv" };
static int n_pco_types = 12;

#define print_error(err) fprintf(stderr, "%s: %d: %s\n", __FILE__, __LINE__, err)

int pco_allow_ambigous_mappings = 0;

/*+ Evaluates to the number of elements at a. +*/
int pco_sizeof(const Pco_t *a)
{
	return a->size;
}

/*+ Evaluates to the data adresss for elements at a. +*/
int *pco_data(Pco_t *a)
{
	return a->data;
}

/*+ Evaluates to the type-number of the object at a. +*/
int pco_typeof(const Pco_t *a)
{
	return a->type;
}

/*+ Evaluates to nth element at a. +*/
int pco_element(const Pco_t *a, int n)
{
	return a->data[n];
}

/*+ Sets the type field of a to the value given by b. +*/
void pco_set_typeof(Pco_t *a, int b)
{
	a->type = b;
}

/*+ Sets the nth element to e. +*/
void pco_set_element(Pco_t *a, int n, int e)
{
	a->data[n] = e;
}

/*+ Sets the size to n. +*/
void pco_set_sizeof(Pco_t *a, int n)
{
	a->size = n;
}

/*+ Increment the size by n. +*/
void pco_incr_sizeof(Pco_t *a, int n)
{
	a->size += n;
}

/*+ Sets the option field of a to the value given by b. +*/
void pco_set_opt(Pco_t *a, int b)
{
	a->opt |= b;
}

/*+ Returns 1 iff a is an ordered object type, else 0. +*/
static int pco_is_ordered(const Pco_t *a)
{
	if (a->type == pcseg || a->type == icseg || a->type == iseg || a->type == iv || a->type == icv || a->type == pcv)
		return 1;
	else
		return 0;
}

/*+ Returns the type-number named by word, else -1. +*/
static int word_to_pcotype(const char *word)
{
	int i;
	for (i = 0; i < n_pco_types; i++) {
		if (strcmp(word, pco_type_list[i]) == 0)
			return i;
	}
	return -1;
}

/*+ Writes to word the string representing type. +*/
void pcotype_to_word(int type, char *word)
{
	if (type < 0 || type >= n_pco_types)
		die("illegal type value");
	strcpy(word, pco_type_list[type]);
}

/*+ Returns 1 iff all a_sz elements at a are between low and high, else 0. +*/
static int array_bounds_check(int *a, int a_sz, int low, int high)
{
	while (a_sz--) {
		if (*a < low || *a > high)
			return 0;
		a++;
	}
	return 1;
}

Pco_t *pco_create(const char *type)
{
	Pco_t *p;

	p = emalloc(sizeof(Pco_t));
	p->type = word_to_pcotype(type);
	p->size = 0;
	p->opt = 0;
	return p;
}

void pco_free(Pco_t *p)
{
	free(p);
}

int pco_validate(Pco_t *obj)
{
	int err = 0;

	switch (obj->type) {
	case pcv:
		if (obj->size != 12) {
			print_error("pco_validate(): pcv has in-correct size");
			return 0;
		}
		break;
	case pcset:
	case iset:
		remdup(obj->data, &(obj->size));
		srtpcs(obj->data, obj->size);
	/* fall through */
	case pcseg:
	case iseg:
		err = array_bounds_check(obj->data, obj->size, 0, 11);
		if (err == 0) {
			print_error("pco_validate(): bounds check failed");
			return 0;
		}
		break;
	case icv:
		if (obj->size != 7) {
			print_error("pco_validate(): icv has in-correct size");
			return 0;
		}
		break;
	case icset:
		remdup(obj->data, &(obj->size));
		srtpcs(obj->data, obj->size);
	/* fall through */
	case icseg:
		err = array_bounds_check(obj->data, obj->size, 0, 6);
		if (err == 0) {
			print_error("pco_validate(): bounds check failed");
			return 0;
		}
		break;
	case sc:
		prime(obj->data, &(obj->size), NULL);
		break;
	case cset:
		/* nought to be done */
		break;
	default:
		print_error("pco_validate(): invalid object type");
		return 0;
		break;
	}
	return 1;
}

void pco_copy(Pco_t *dst, const Pco_t *src)
{
	dst->type = src->type;
	dst->size = src->size;
	dst->opt = src->opt;
	memcpy(dst->data, src->data, src->size * sizeof(int));
}

Pco_t *pco_dup(const Pco_t *src)
{
	Pco_t *cpy;
	cpy = pco_create("empty");
	pco_copy(cpy, src);
	return cpy;
}

void pco_swap(Pco_t *a, Pco_t *b)
{
	Pco_t *tmp = pco_dup(a);
	pco_copy(a, b);
	pco_copy(b, tmp);
	pco_free(tmp);
}

int pco_read(Pco_t *p, const char *str)
{
	int i = 0, n = 0, err;
	char type[PCO_TYPMAX];

	while (isspace(str[n]))
		n++;
	if (isalpha(str[n]) && (str[n] != 'A' && str[n] != 'B' && str[n] != 't' && str[n] != 'e')) {
		while (isalpha(str[n])) {
			type[i] = str[n];
			i++;
			n++;
		}
		type[i] = '\0';
		p->type = word_to_pcotype(type);
		p->opt |= PCO_PRINT_TYPE;
	} else {
		p->opt = 0;
		/* WRONG: should just remove mask, but at the moment it is the only one? */
	}
	while (isspace(str[n]))
		n++;
	err = str2pcs(&str[n], p->data, &(p->size));
	if (err != 0)
		pco_validate(p);
	return err;
}

int pco_write(Pco_t *p, char *str)
{
	int n = 0;
	char type[PCO_TYPMAX];

	/* THIS WAS ADDED RECENTLY, IT MIGHT HAVE BROKEN SOME PROGRAMS !!!!! */
	pco_validate(p);
	if (p->opt & PCO_PRINT_TYPE) {
		pcotype_to_word(p->type, type);
		sprintf(str, "%s %n", type, &n);
	}
	return pcs2str(p->data, p->size, &str[n], FALSE);
}

int pco_write_pretty(Pco_t *p, char *str)
{
	if (p->type == sc)
		return pcs2scstr(p->data, p->size, str, 3);
	else if (p->type == pcset || p->type == iset || p->type == icset
		|| p->type == cset) {
		*str = '{';
		pcs2str(p->data, p->size, str + 1, 0);
		*(str + p->size + 1) = '}';
		*(str + p->size + 2) = '\0';
	} else if (p->type == pcseg || p->type == iseg || p->type == icseg) {
		*str = '<';
		pcs2str(p->data, p->size, str + 1, 0);
		*(str + p->size + 1) = '>';
		*(str + p->size + 2) = '\0';
	} else if (p->type == icv || p->type == iv || p->type == pcv) {
		*str = '[';
		pcs2str(p->data, p->size, str + 1, 0);
		*(str + p->size + 1) = ']';
		*(str + p->size + 2) = '\0';
	}
	return p->size;
}

int pco_get(Pco_t *obj, FILE *stream)
{
	char str[MAX_STR];
	if (fgets(str, MAX_STR, stream))
		return pco_read(obj, str);
	else
		return 0;
}

int pco_put(Pco_t *obj, FILE *stream)
{
	char str[MAX_STR];
	if (pco_write(obj, str)) {
		fprintf(stream, "%s\n", str);
		return 1;
	} else
		return 0;
}

int pco_put_pretty(Pco_t *obj, FILE *stream)
{
	char str[MAX_STR];
	if (pco_write_pretty(obj, str)) {
		fprintf(stream, "%s\n", str);
		return 1;
	} else
		return 0;
}

void pco_tf(Pco_t *obj, int *tf)
{
	int i;
	for (i = 0; i < obj->size; i++)
		obj->data[i] = tf[obj->data[i]];
}

int pco_sro_apply(Pco_t *obj, const Sro_t *op)
{
	do_rrtnmi(*(op), obj->data, obj->size);
	pco_validate(obj);
	return 1;
}

int pco_sro_derive(const Pco_t *from, const Pco_t *to, Sro_t *op)
{
	if (pco_is_ordered(to)) {
		return get_sro(from->data, from->size, to->data, to->size, op);
	} else {
		op->r = 0;
		op->rt = 0;
		return get_tto(from->data, from->size, to->data, to->size,
			(Tto_t *)op);
	}
}

int pco_has_element(const Pco_t *obj, int n)
{
	int i;
	for (i = 0; i < pco_sizeof(obj); i++) {
		if (pco_element(obj, i) == n)
			return 1;
	}
	return 0;
}

void pco_cat(Pco_t *dst, const Pco_t *src)
{
	memmove(&(dst->data[dst->size]), src->data, (src->size) * sizeof(int));
	dst->size += src->size;
}

void pco_union(Pco_t *r, const Pco_t *a, const Pco_t *b)
{
	pco_copy(r, a);
	pco_cat(r, b);
	pco_validate(r);
}

void pco_intersection(Pco_t *r, const Pco_t *a, const Pco_t *b)
{
	int i;

	if (pco_is_ordered(a) || pco_is_ordered(b)) {
		print_error("pco_intersection(): ordered set");
	}
	pco_set_sizeof(r, 0);
	for (i = 0; i < pco_sizeof(a); i++) {
		if (pco_has_element(b, pco_element(a, i))) {
			pco_set_element(r, pco_sizeof(r), pco_element(a, i));
			pco_incr_sizeof(r, 1);
		}
	}
	r->type = a->type;
	pco_validate(r);
}

int pco_degree_of_intersection(const Pco_t *a, const Pco_t *b)
{
	Pco_t r;
	pco_intersection(&r, a, b);
	return pco_sizeof(&r);
}

void pco_complement(Pco_t *r, const Pco_t *a)
{
	int i;

	if (pco_is_ordered(a)) {
		print_error("pco_complement(): ordered set");
	}
	pco_set_sizeof(r, 0);
	for (i = 0; i < 12; i++) {
		if (!pco_has_element(a, i)) {
			pco_set_element(r, pco_sizeof(r), i);
			pco_incr_sizeof(r, 1);
		}
	}
	r->type = a->type;
	pco_validate(r);
}

int pco_map(Pco_t *dst, int type, const Pco_t *src)
{
	int i, err = 0;
	Pco_t *indirect = 0;

	dst->type = type;
	/* csets are easy, do it now */
	if (dst->type == cset) {
		dst->data[0] = pco_sizeof(src);
		return dst->size = 1;
	}
	dst->size = src->size;
	memmove(dst->data, src->data, src->size * sizeof(int));
	/* if this is just a fancy copy, we are done */
	if (dst->type == src->type)
		return TRUE;

	indirect = pco_create("empty");

	switch (src->type) {
	case pcseg:
		if (dst->type == pcset) {
			remdup(dst->data, &(dst->size));
			srtpcs(dst->data, dst->size);
		} else if (dst->type == iseg) {
			dst->size = pcs2int(src->data, src->size, dst->data);
		} else if (dst->type == sc) {
			pco_map(indirect, pcset, src);
			pco_map(dst, sc, indirect);
		} else if (dst->type == icv) {
			/* implicit cast to pcset */
			pco_map(indirect, sc, src);
			pco_map(dst, icv, indirect);
		} else if (dst->type == iset) {
			pco_map(indirect, iseg, src);
			pco_map(dst, iset, indirect);
		} else if (dst->type == icseg) {
			pco_map(indirect, iseg, src);
			pco_map(dst, icseg, indirect);
		} else if (dst->type == icset) {
			if (pco_allow_ambigous_mappings) {
				pco_map(indirect, icseg, src);
				pco_map(dst, icset, indirect);
			} else {
				print_error("pco_map(): ambiguous mapping\n");
				err = 1;
			}
		} else {
			err = 1;
		}
		break;
	case pcset:
		if (dst->type == sc) {
			prime(dst->data, &(dst->size), NULL);
		} else if (dst->type == icv) {
			pco_map(indirect, sc, src);
			pco_map(dst, icv, indirect);
		} else if (dst->type == icset) {
			/* implicit cast to sc */
			pco_map(indirect, icv, src);
			pco_map(dst, icset, indirect);
		} else {
			err = 1;
		}
		break;
	case iseg:
		if (dst->type == iset) {
			remdup(dst->data, &(dst->size));
			srtpcs(dst->data, dst->size);
		} else if (dst->type == icseg) {
			isegicseg(src->data, src->size, dst->data, &(dst->size));
		} else if (dst->type == sc) {
			isegsc(src->data, src->size, dst->data, &(dst->size));
		} else if (dst->type == icv) {
			pco_map(indirect, sc, src);
			pco_map(dst, icv, indirect);
		} else if (dst->type == icset) {
			if (pco_allow_ambigous_mappings) {
				pco_map(indirect, icseg, src);
				pco_map(dst, icset, indirect);
			} else {
				print_error("pco_map(): ambiguous mapping\n");
				err = 1;
			}
		} else {
			err = 1;
		}
		break;
	case iset:
		if (dst->type == icset) {
			isegicseg(src->data, src->size, dst->data, &(dst->size));
			remdup(dst->data, &(dst->size));
			srtpcs(dst->data, dst->size);
		} else {
			err = 1;
		}
		break;
	case icseg:
		if (dst->type == icset) {
			remdup(dst->data, &(dst->size));
			srtpcs(dst->data, dst->size);
		} else {
			err = 1;
		}
		break;
	case icv:
		if (dst->type == icset) {
			for (i = 1, dst->size = 0; i < 7; i++) {
				if (src->data[i] > 0) {
					dst->data[dst->size] = i;
					(dst->size)++;
				}
			}
		} else {
			err = 1;
		}
		break;
	case sc:
		if (dst->type == icv) {
			dst->size = pcs2icv(src->data, src->size, dst->data);
		} else {
			err = 1;
		}
		break;
	default:
		err = 1;
		break;
	}
	pco_free(indirect);
	if (err)
		print_error("pco_map(): invalid mapping requested");
	return !err;
}

int pco_convert(Pco_t *p, int type)
{
	Pco_t *q = pco_dup(p);
	int err = pco_map(p, type, q);
	pco_free(q);
	return err;
}

int pco_n_cmp(const Pco_t *a, const Pco_t *b, int n)
{
	Pco_t *A = pco_dup(a);
	Pco_t *B = pco_dup(b);
	int err;
	pco_set_sizeof(A, n);
	pco_set_sizeof(B, n);
	err = pco_cmp(A, B);
	pco_free(A);
	pco_free(B);
	return err;
}

int pco_cmp(const Pco_t *a, const Pco_t *b)
{
	char atype[PCO_TYPMAX], btype[PCO_TYPMAX];
	int err, result;
	Pco_t *c;

	c = pco_create("epmty");
	err = pco_map(c, a->type, b);
	if (err <= 0) {
		pcotype_to_word(a->type, atype);
		pcotype_to_word(b->type, btype);
		fprintf(stderr, "%s: %d: pco_cmp(): cannot promote %s to %s.\n",
			__FILE__, __LINE__, btype, atype);
	}

	if (a->size == c->size) {
		if (pco_is_ordered(a)) {
			if (isident(a->data, c->data, c->size))
				result = PCO_EQUAL;
			else
				result = PCO_NOT_EQUAL;
		} else {
			if (a->type == sc) {
				if (isequiv(a->data, a->size, c->data, c->size))
					result = PCO_EQUAL;
				else
					result = PCO_NOT_EQUAL;
			} else {
				if (isequal(a->data, a->size, c->data, c->size))
					result = PCO_EQUAL;
				else
					result = PCO_NOT_EQUAL;
			}
		}
	} else if (a->size < c->size) {
		if (pco_is_ordered(a)) {
			if (seginc(a->data, a->size, c->data, c->size))
				result = PCO_SUBSET;
			else
				result = PCO_NOT_EQUAL;
		} else {
			if (a->type == sc) {
				if (isabssub(a->data, a->size, c->data, c->size))
					result = PCO_SUBSET;
				else
					result = PCO_NOT_EQUAL;
			} else {
				if (islitsub(a->data, a->size, c->data, c->size))
					result = PCO_SUBSET;
				else
					result = PCO_NOT_EQUAL;
			}
		}
	} else {
		if (pco_is_ordered(a)) {
			if (seginc(c->data, c->size, a->data, a->size))
				result = PCO_SUPERSET;
			else
				result = PCO_NOT_EQUAL;
		} else {
			if (a->type == sc) {
				if (isabssub(c->data, c->size, a->data, a->size))
					result = PCO_SUPERSET;
				else
					result = PCO_NOT_EQUAL;
			} else {
				if (islitsub(c->data, c->size, a->data, a->size))
					result = PCO_SUPERSET;
				else
					result = PCO_NOT_EQUAL;
			}
		}
	}
	pco_free(c);
	return result;
}

#if _POSIX_C_SOURCE == 2
#include <regex.h>
int pco_match(const Pco_t *obj, const char *pattern)
{
	char obj_str[MAX_STR], error_str[MAX_STR];
	int err;
	regex_t preg;

	pcs2str(obj->data, obj->size, obj_str, FALSE);
	/*fprintf(stderr,"obj_str=%s pat_str=%s\n",obj_str,pattern); */
	err = regcomp(&preg, pattern, REG_EXTENDED | REG_NOSUB | REG_NEWLINE);
	if (err != 0) {
		regerror(err, &preg, error_str, MAX_STR);
		print_error(error_str);
		return 0;
	}
	err = regexec(&preg, obj_str, 0, 0, 0 /*|REG_NOTBOL|REG_NOTEOL */);
	if (err != 0 && err != REG_NOMATCH) {
		regerror(err, &preg, error_str, MAX_STR);
		print_error(error_str);
		return 0;
	}
	regfree(&preg);
	if (err == 0)
		return 1;
	else
		return 0;
}
#else
int pco_match(const Pco_t *obj, const char *pattern)
{
	print_error("pco_match(): posix regex disabled?\n");
	exit(EXIT_FAILURE);
}
#endif
