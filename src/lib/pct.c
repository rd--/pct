#include <assert.h>
#include <ctype.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include "emem.h"
#include "ftbl.h"
#include "pct.h"

char pct_data_dir[] = PCT_DIR;
char sc_lookup[] = PCT_DIR "/scs";
char univ_lookup[] = PCT_DIR "/univ";

/* this would be the proper way to do this */
/*
struct
{
  int transposition ;
  int inversion ;
  int multiplication ;
  int retrograde ;
  int rotation ;
}
CANON = { 1 , 1 , 1 , 1 , 1 } ;
*/

int M_in_tto = TRUE;
int M_in_sro = TRUE;

/*
 * integer comparison routine for qsort
 */
static int icompare(const void *n1, const void *n2)
{
	return (*((int *)n1) - *((int *)n2));
}

/*
 * mod() - take the modulo n of an integer
 *
 * returns the mod n value of i.
 */
int mod(int i, int n)
{
	MOD(i, n);
	return i;
}

/*
 * mod12() - take the mod12 of an integer
 *
 * returns the mod12 value of i.
 */
int mod12(int i)
{
	MOD(i, 12);
	return i;
}

/*
 * interval()
 *
 * returns the interval from PC a to PC b
 */
int interval(int a, int b)
{
	int r = b - a;
	return mod12(r);
}

/*
 * p2pc()
 *
 * pitch to pitch-class.
 */
int p2pc(int p)
{
	MOD(p, 12);
	return p;
}

/*
 * ps2pcs()
 *
 * pitch segment to PCSEG
 */
int ps2pcs(const int *p, int plen, int *q)
{
	int i;
	for (i = 0; i < plen; i++) {
		q[i] = mod12(p[i]);
	}
	return TRUE;
}

/*
 * srtpcs()
 *
 * sort the PCS p into ascending order.
 */
int srtpcs(int *p, int plen)
{
	qsort(p, (size_t)plen, sizeof(int), icompare);
	return TRUE;
}

/*
 * i2ic()
 *
 * ordered pitch(class) interval to interval class
 */
int i2ic(int i)
{
	int a = i;
	MOD(a, 12);
	switch (a) {
	case 7:
		a = 5;
		break;
	case 8:
		a = 4;
		break;
	case 9:
		a = 3;
		break;
	case 10:
		a = 2;
		break;
	case 11:
		a = 1;
		break;
	}
	return a;
}

/*
 * pcs2icv()
 *
 * get the interval-class vector of p.
 * interval-class vectors have 7 positions (see Morris)
 */
int pcs2icv(const int *p, int plen, int *icv)
{
	int i, j;
	int a;

	if (isdup(p, plen)) {
		return FALSE;
	}
	for (i = 0; i < 7; i++) {
		icv[i] = 0;
	}
	for (i = 0; i < plen; i++) {
		for (j = 0; j < plen - i; j++) {
			a = p[i + j] - p[i];
			a = i2ic(a);
			icv[a]++;
		}
	}
	return 7;
}

/*
 * pcsiv()
 *
 * get the interval vector of p.
 * interval vectors have 12 positions
 *
 */
int pcsiv(const int *p, int plen, int *iv)
{
	int i, j;
	int a;

	for (i = 0; i < 12; i++) {
		iv[i] = 0;
	}
	for (i = 0; i < plen; i++) {
		for (j = 0; j < plen; j++) {
			a = p[j] - p[i];
			MOD(a, 12);
			iv[a]++;
		}
	}
	return 12;
}

/*
 * pc2compl()
 *
 * pitch class to complement
 */
int pc2compl(int pc)
{
	MOD(pc, 12);
	return 12 - pc;
}

/*
 * pcs2cmpl()
 *
 * get the literal complement of p
 * returns cardinality of q
 */
int pcs2cmpl(const int *p, int plen, int *q)
{
	int i;
	int j = 0;

	if (isdup(p, plen)) {
		return FALSE;
	}
	for (i = 0; i < 12; i++) {
		if (!isel(i, p, plen)) {
			q[j] = i;
			j++;
		}
	}
	return j;
}

/*
 * pcs2cmpa()
 *
 * get the abstract complement of p
 * returns cardinality of q
 */
int pcs2cmpa(const int *p, int plen, int *q)
{
	int i;
	int j = 0;

	if (isdup(p, plen)) {
		return FALSE;
	}
	for (i = 0; i < 12; i++) {
		if (!isel(i, p, plen)) {
			q[j] = i;
			j++;
		}
	}
	prime(q, &j, NULL);
	return j;
}

/*
 * pcs2cmpn()
 *
 * put those elements of q that are not
 * elements of p into comp
 * returns the cardinality of comp
 */
int pcs2cmpn(const int *p, int plen, const int *q, int qlen, int *comp)
{
	int i = 0, j = 0;
	for (i = 0; i < qlen; i++) {
		if (!isel(q[i], p, plen)) {
			comp[j] = q[i];
			j += 1;
		}
	}
	return j;
}

/*
 * isel()
 *
 * is a an element of the PCS p
 */
int isel(int a, const int *p, int plen)
{
	int i;
	for (i = 0; i < plen; i++) {
		if (p[i] == a) {
			return TRUE;
		}
	}
	return FALSE;
}

/*
 * stackleft()
 *
 * get left stacked rotation of p.
 */
int stackleft(int *p, int plen)
{
	int i, j, k;
	int g = 0;
	int norm[256];
	int intv;
	int out = 11;
	int in[12] = { 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11 };

	cpypcs(norm, &plen, p, plen);
	srtpcs(norm, plen);
	for (i = 0; i <= plen; i++) {
		rotpcs(1, norm, plen);
		intv = norm[plen - 1] - norm[0];
		MOD(intv, 12);
		if (intv < out) {
			out = intv;
			g = TRUE;
			for (k = 1; k < plen; k++) {
				intv = norm[k] - norm[k - 1];
				MOD(intv, 12);
				in[k - 1] = intv;
			}
		}
		if (intv == out) {
			for (j = 1; j < plen; j++) {
				intv = norm[j] - norm[j - 1];
				MOD(intv, 12);
				if (intv > in[j - 1]) {
					g = FALSE;
					break;
				}
				if (intv == in[j - 1]) {
					g = TRUE;
					break;
				}
				if (intv < in[j - 1]) {
					g = TRUE;
					for (k = j; k < plen; k++) {
						intv = norm[k] - norm[k - 1];
						MOD(intv, 12);
						in[k - 1] = intv;
					}
					break;
				}
			}
		}
		if (intv > out) {
			g = FALSE;
		}
		if (g) {
			cpypcs(p, &plen, norm, plen);
		}
	}
	trspcs(-p[0], p, plen);
	return TRUE;
}

/*
 * prime()
 *
 * convert p to its prime form
 * t may be NULL
 */
int prime(int *p, int *plen, tni_t *t)
{
	int i, j, k;
	int pr[12]; /* prime */
	int c[12]; /* copy */
	int cl;
	int out = 11;
	int in[12] = { 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11 };
	int g = 0;
	int intv;

	remdup(p, plen);
	cl = (*plen);
	if (cl < 1 || cl > 12) {
		return FALSE;
	}
	if (t != NULL) {
		t->i = FALSE;
		t->t = 0;
	}

	/* check inversion */
	for (i = 0; i < cl; i++) {
		c[i] = 12 - p[i];
		MOD(c[i], 12);
	}
	srtpcs(c, cl);
	for (i = 0; i <= cl; i++) {
		rotpcs(1, c, cl);
		intv = c[cl - 1] - c[0];
		MOD(intv, 12);
		if (intv < out) {
			out = intv;
			g = TRUE;
			for (k = 1; k < cl; k++) {
				intv = c[k] - c[k - 1];
				MOD(intv, 12);
				in[k - 1] = intv;
			}
		}
		if (intv == out) {
			for (j = 1; j < cl; j++) {
				intv = c[j] - c[j - 1];
				MOD(intv, 12);
				if (intv > in[j - 1]) {
					g = FALSE;
					break;
				}
				if (intv == in[j - 1]) {
					g = TRUE;
				}
				if (intv < in[j - 1]) {
					g = TRUE;
					for (k = j; k < cl; k++) {
						intv = c[k] - c[k - 1];
						MOD(intv, 12);
						in[k - 1] = intv;
					}
					break;
				}
			}
		}
		if (intv > out) {
			g = FALSE;
		}
		if (g) {
			for (k = 0; k < cl; k++) {
				pr[k] = c[k];
			}
			if (t != NULL) {
				t->i = TRUE;
				t->t = 12 - pr[0];
				MOD(t->t, 12);
			}
		}
	}
	/* check original */
	for (i = 0; i < cl; i++) {
		c[i] = p[i];
		MOD(c[i], 12);
	}
	srtpcs(c, cl);
	for (i = 0; i <= cl; i++) {
		rotpcs(1, c, cl);
		intv = c[cl - 1] - c[0];
		MOD(intv, 12);
		if (intv < out) {
			out = intv;
			g = TRUE;
			for (k = 1; k < cl; k++) {
				intv = c[k] - c[k - 1];
				MOD(intv, 12);
				in[k - 1] = intv;
			}
		}
		if (intv == out) {
			for (j = 1; j < cl; j++) {
				intv = c[j] - c[j - 1];
				MOD(intv, 12);
				if (intv > in[j - 1]) {
					g = FALSE;
					break;
				}
				if (intv == in[j - 1]) {
					g = TRUE;
				}
				if (intv < in[j - 1]) {
					g = TRUE;
					for (k = j; k < cl; k++) {
						intv = c[k] - c[k - 1];
						MOD(intv, 12);
						in[k - 1] = intv;
					}
					break;
				}
			}
		}
		if (intv > out) {
			g = FALSE;
		}
		if (g) {
			for (k = 0; k < cl; k++) {
				pr[k] = c[k];
			}
			if (t != NULL) {
				t->i = FALSE;
				t->t = pr[0];
			}
		}
	}

	/* bring it down to zero and return */
	trspcs(-pr[0], pr, *plen);
	cpypcs(p, &cl, pr, cl);
	return TRUE;
}

/*
 * pcs2prime()
 *
 * put prime form of p into pr
 * t may be NULL
 * returns cardinality of pr
 */
int pcs2prime(const int *p, int plen, tni_t *t, int *pr)
{
	int prlen = plen;
	if (isdup(p, plen)) {
		return FALSE;
	}
	cpypcs(pr, &prlen, p, plen);
	prime(pr, &prlen, t);
	return prlen;
}

/*
 * invpcs()
 *
 * invert pitch class segment about n.
 */
int invpcs(int n, int *p, int plen)
{
	int i;
	for (i = 0; i < plen; i++) {
		p[i] = n + (n - p[i]);
		MOD(p[i], 12);
	}
	return plen;
}

/*
 * trspcs()
 *
 * transpose the PCSEG p by t steps
 * t may be negative
 */
int trspcs(int t, int *p, int plen)
{
	int i;
	for (i = 0; i < plen; i++) {
		p[i] += t;
		MOD(p[i], 12);
	}
	return t;
}

/*
 * multpcs()
 *
 * perform an Mn operation on the PCSEG p
 */
int multpcs(int n, int *p, int plen)
{
	int i;
	for (i = 0; i < plen; i++) {
		p[i] *= n;
		MOD(p[i], 12);
	}
	return n;
}

/*
 * rtrpcs()
 *
 * retrograde the PCSEG p
 */
int rtrpcs(int *p, int plen)
{
	int i, temp = 0;
	long index = plen / 2;

	plen--;
	for (i = 0; i < index; i++) {
		temp = p[i];
		p[i] = p[plen];
		p[plen] = temp;
		plen--;
	}
	return TRUE;
}

/*
 * rotpcs()
 *
 * rotates a pitch class segment to the
 * left by 'r' steps. negative values
 * for 'r' will therefore shift right.
 * for Morris conformance use negative
 * values (ie for r5TP, use r-5TP)
 */
int rotpcs(int r, int *p, int plen)
{
	int i;
	int v;

	MOD(r, plen);
	while (r--) {
		v = p[0];
		for (i = 1; i < plen; i++) {
			p[i - 1] = p[i];
		}
		p[plen - 1] = v;
	}
	return plen;
}

/*
 * isdup()
 *
 * is there duplication of pitch classes
 * within the PCS p.
 */
int isdup(const int *p, int plen)
{
	int i, j;
	for (i = 0; i < plen - 1; i++) {
		for (j = i + 1; j < plen; j++) {
			if (p[j] == p[i]) {
				return TRUE;
			}
		}
	}
	return FALSE;
}

/*
 * getdup()
 *
 * put the duplicated PCs from
 * within the PCS p into d.
 */
int getdup(const int *p, int plen, int *d, int *dlen)
{
	int i;
	int duplist[12];
	int n;

	for (i = 0; i < 12; i++) {
		duplist[i] = 0;
	}
	for (i = 0; i < plen; i++) {
		duplist[p[i]] += 1;
	}
	n = 0;
	for (i = 0; i < 12; i++) {
		if (duplist[i] > 1) {
			d[n] = i;
			n += 1;
		}
	}
	*dlen = n;
	return TRUE;
}

/*
 * remdup()
 *
 * remove duplicates. ie if there are
 * four pc5's when called, when returned
 * only one will remain.
 */
int remdup(int *p, int *plen)
{
	int duplist[12];
	int *newp = emalloc((*plen) * sizeof(int));
	int i;
	int n;

	for (i = 0; i < 12; i++) {
		duplist[i] = 0;
	}
	n = 0;
	for (i = 0; i < plen[0]; i++) {
		if (duplist[p[i]] == 0) {
			duplist[p[i]] += 1;
			newp[n] = p[i];
			n += 1;
		}
	}
	plen[0] = n;
	for (i = 0; i < n; i++) {
		p[i] = newp[i];
	}
	free(newp);
	return TRUE;
}

/*
 * rempc()
 *
 * remove all instances of the pitch class
 * 'pc' from the pitch class segment 'p' and
 * update plen.
 */
int rempc(int pc, int *p, int *plen)
{
	int i, j;
	for (i = 0; i < *plen; i++) {
		if (p[i] == pc) {
			for (j = i; j < *plen - 1; j++) {
				p[j] = p[j + 1];
			}
			*plen -= 1;
		}
	}
	return TRUE;
}

/*
 * rem1pc()
 *
 * removes a single instance of the pitch class
 * 'pc' from the pitch class segment 'p' and
 * updates plen.
 */
int rem1pc(int pc, int *p, int *plen)
{
	int i, j;
	for (i = 0; i < *plen; i++) {
		if (p[i] == pc) {
			for (j = i; j < *plen - 1; j++) {
				p[j] = p[j + 1];
			}
			(*plen)--;
			return TRUE;
		}
	}
	return FALSE;
}

/*
 * islitsub()
 *
 * is p a literal subset of q.
 * NOTE: '012' is not in '012'
 */
int islitsub(const int *p, int plen, const int *q, int qlen)
{
	int i;
	if (plen >= qlen || plen < 1) {
		return FALSE;
	}
	for (i = 0; i < plen; i++) {
		if (!isel(p[i], q, qlen)) {
			return FALSE;
		}
	}
	return TRUE;
}

/*
 * isabssub()
 *
 * is p an abstract subset of q.
 */
int isabssub(const int *p, int plen, const int *q, int qlen)
{
	int cp[12]; /* copy of p */
	int i, j;

	if (plen >= qlen || plen < 1) {
		return FALSE;
	}
	cpypcs(cp, &plen, p, plen);
	for (i = 0; i <= 12; i++) {
		trspcs(1, cp, plen);
		for (j = 0; j < 2; j++) {
			invpcs(cp[0], cp, plen);
			if (islitsub(cp, plen, q, qlen)) {
				return TRUE;
			}
		}
	}
	return FALSE;
}

/*
 * isequiv()
 *
 * is p equivalent to q within the standard
 * set operations of transposition, inversion,
 * and combinations thereof.
 * ie does it resolve to the same prime.
 */
int isequiv(const int *p, int plen, const int *q, int qlen)
{
	int prime1[12];
	int prime2[12];
	int i;

	/* what would break if this were fixed ? */
	if (isdup(p, plen) || isdup(q, qlen) || plen != qlen) {
		return FALSE;
	}
	pcs2prime(p, plen, NULL, prime1);
	pcs2prime(q, qlen, NULL, prime2);
	for (i = 0; i < plen; i++) {
		if (prime1[i] != prime2[i]) {
			return FALSE;
		}
	}
	return TRUE;
}

/*
 * isequal()
 *
 * is p equal to q.
 * ie. does it contain exactly the same PCs.
 * NOTE: '012' is equal to '201'
 */
int isequal(const int *p, int plen, const int *q, int qlen)
{
	int i;

	for (i = 0; i < plen; i++) {
		if (!isel(p[i], q, qlen)) {
			return FALSE;
		}
	}
	for (i = 0; i < qlen; i++) {
		if (!isel(q[i], p, plen)) {
			return FALSE;
		}
	}
	return TRUE;
}

/*
 * isident()
 *
 * is identity (are p and q identical)
 * NOTE: '012' is not equal to '210'
 */
int isident(const int *p, const int *q, int plen)
{
	while (plen--) {
		if (*p++ != *q++) {
			return FALSE;
		}
	}
	return TRUE;
}

/*
 * pcs2tni()
 *
 * get the TnI to map the prime form of p onto p.
 */
int pcs2tni(const int *p, int plen, tni_t *t)
{
	int pr[12];
	return pcs2prime(p, plen, t, pr);
}

/*
 * issym()
 *
 * is p symmetrical (ie is there a
 * value n such that T0=TnI).
 */
int issym(const int *p, int plen)
{
	int c1[12];
	int c2[12];
	int i;
	cpypcs(c1, &plen, p, plen);
	srtpcs(c1, plen);
	cpypcs(c2, &plen, c1, plen);
	invpcs(c2[0], c2, plen);
	for (i = 0; i <= 12; i++) {
		trspcs(1, c2, plen);
		if (isequal(c1, plen, c2, plen)) {
			return TRUE;
		}
	}
	return FALSE;
}

/*
 * pcs2int()
 *
 * get the INT() of p.
 */
int pcs2int(const int *p, int plen, int *intv)
{
	int i;
	if (plen <= 1) {
		return FALSE;
	}
	for (i = 0; i < plen - 1; i++) {
		intv[i] = p[i + 1] - p[i];
		MOD(intv[i], 12);
	}
	return plen - 1;
}

/*
 * pcs2intn()
 *
 * get the INTn() of p. 1<n<plen
 */
int pcs2intn(const int *p, int plen, int *intv, int n)
{
	int i;

	if (plen <= 1 || n < 1 || n >= plen) {
		return FALSE;
	}

	for (i = 0; i < plen - n; i++) {
		intv[i] = p[i + n] - p[i];
		MOD(intv[i], 12);
	}

	return n;
}

/*
 * getinvar()
 *
 * get the degree of invariance
 * between two PCSs. (ie the
 * number of common pc's)
 */
int getinvar(const int *p, int plen, const int *q, int qlen)
{
	int i;
	int count = 0;
	if (isdup(p, plen) || isdup(q, qlen)) {
		return -1;
	}
	for (i = 0; i < plen; i++) {
		if (isel(p[i], q, qlen)) {
			count += 1;
		}
	}
	return count;
}

/*
 * pcs2tics()
 *
 * get the tics vector for p.
 * a tics vector contains 12 places.
 * see ???
 */
int pcs2tics(const int *p, int plen, int *tics)
{
	int i;
	int q[12], qlen;
	cpypcs(q, &qlen, p, plen);
	invpcs(q[0], q, qlen);
	for (i = 0; i < 12; i++) {
		tics[i] = getinvar(q, qlen, p, plen);
		trspcs(1, q, qlen);
	}
	return TRUE;
}

/*
 * getmerge()
 *
 * merge p and q into
 * pcs m. does not introduce
 * duplications.
 */
int getmerge(int *m, int *mlen, const int *p, int plen, const int *q,
	int qlen)
{
	int i;
	cpypcs(m, mlen, p, plen);
	for (i = 0; i < qlen; i++) {
		if (!isel(q[i], m, *mlen)) {
			m[*mlen] = q[i];
			*mlen += 1;
		}
	}
	return TRUE;
}

/*
 * mergein()
 *
 * merge p into m. does not introduce duplications
 * merged PCs are placed at the end of m
 */
int mergein(int *m, int *mlen, const int *p, int plen)
{
	int i;
	for (i = 0; i < plen; i++) {
		if (!isel(p[i], m, *mlen)) {
			m[*mlen] = p[i];
			*mlen += 1;
		}
	}
	return TRUE;
}

/*
 * cpypcs()
 *
 * copy q into p
 */
int cpypcs(int *p, int *plen, const int *q, int qlen)
{
	int i;
	*plen = qlen;
	for (i = 0; i < qlen; i++) {
		p[i] = q[i];
	}
	return TRUE;
}

/*
 * midi2pc()
 *
 * midi note number to pitch class.
 */
int midi2pc(int d1)
{
	d1 -= 60;
	MOD(d1, 12);
	return d1;
}

/*
 * getrtni()
 *
 * finds operation o that will map the
 * p onto q.
 * looks only for RTnI operations
 * returns FALSE if no operation is found
 */
static int getrtni(const int *p, const int *q, int plen, rtnmi_t *o)
{
	int pint[256];
	int qint[256];

	/*
	 * T
	 * RI
	 * R
	 * I
	 */
	pcs2int(p, plen, pint);
	pcs2int(q, plen, qint);
	if (isident(pint, qint, plen - 1)) {
		o->i = FALSE;
		o->r = FALSE;
		o->t = q[0] - p[0];
		return TRUE;
	}
	rtrpcs(qint, plen - 1);
	if (isident(pint, qint, plen - 1)) {
		o->i = TRUE;
		o->r = TRUE;
		o->t = q[plen - 1] - (12 - p[0]);
		return TRUE;
	}
	invpcs(0, qint, plen - 1);
	if (isident(pint, qint, plen - 1)) {
		o->i = FALSE;
		o->r = TRUE;
		o->t = q[0] - p[plen - 1];
		return TRUE;
	}
	rtrpcs(qint, plen - 1);
	if (isident(pint, qint, plen - 1)) {
		o->i = TRUE;
		o->r = FALSE;
		o->t = q[0] - (12 - p[0]);
		return TRUE;
	}
	return FALSE;
}

/*
 * getrtnmi()
 *
 * finds the operation o of the form RTnMI that
 * will map the PCSEG p onto q.
 *
 * returns FALSE if no operation is found
 */
int getrtnmi(const int *p, const int *q, int plen, rtnmi_t *o)
{
	int pc[256], qc[256];
	cpypcs(pc, &plen, p, plen);
	cpypcs(qc, &plen, q, plen);
	o->m = FALSE;
	if (getrtni(pc, qc, plen, o)) {
		return TRUE;
	}
	if (M_in_sro) {
		multpcs(5, pc, plen);
		o->m = TRUE;
		if (getrtni(pc, qc, plen, o)) {
			return TRUE;
		}
	}
	return FALSE;
}

/*
 * get_tto()
 *
 * finds twelve-tone operator o (TnMI) that will
 * map the PCS p onto q.
 * returns FALSE if no operation is found
 */
int get_tto(const int *p, int plen, const int *q, int qlen, tnmi_t *o)
{
	int pp[MAX_PCS], pplen;
	int i;

	cpypcs(pp, &pplen, p, plen);
	o->i = FALSE;
	o->t = 0;
	o->m = FALSE;

	for (i = 0; i < 12; i++) {
		if (isequal(pp, pplen, q, qlen)) {
			o->t = i;
			return TRUE;
		}
		trspcs(1, pp, pplen);
	}

	if (M_in_tto) {
		multpcs(5, pp, pplen);
		o->m = TRUE;
		for (i = 0; i < 12; i++) {
			if (isequal(pp, pplen, q, qlen)) {
				o->t = i;
				return TRUE;
			}
			trspcs(1, pp, pplen);
		}
		multpcs(5, pp, pplen);
		o->m = FALSE;
	}

	invpcs(0, pp, pplen);
	o->i = TRUE;
	for (i = 0; i < 12; i++) {
		if (isequal(pp, pplen, q, qlen)) {
			o->t = i;
			return TRUE;
		}
		trspcs(1, pp, pplen);
	}

	if (M_in_tto) {
		multpcs(5, pp, pplen);
		o->m = TRUE;
		for (i = 0; i < 12; i++) {
			if (isequal(pp, pplen, q, qlen)) {
				o->t = i;
				return TRUE;
			}
			trspcs(1, pp, pplen);
		}
	}

	return FALSE;
}

/*
 * get_sro()
 *
 * finds serial operator o (rRTnMI) that will
 * map the PCSEG p onto q.
 * returns FALSE if no operation is found
 */
int get_sro(const int *p, int plen, const int *q, int qlen, rrtnmi_t *o)
{
	int pc[256], qc[256];
	int i;

	if (plen != qlen) {
		return 0;
	}
	cpypcs(pc, &plen, p, plen);
	cpypcs(qc, &qlen, q, qlen);
	if (getrtnmi(pc, qc, plen, (rtnmi_t *)o)) {
		o->rt = 0;
		return TRUE;
	}
	for (i = 1; i < plen; i++) {
		/*
		 * -1 seems more intuitive
		 * but Morris opts for positive 1
		 */
		rotpcs(1, qc, plen);
		if (getrtnmi(pc, qc, plen, (rtnmi_t *)o)) {
			o->rt = i;
			return TRUE;
		}
	}
	return FALSE;
}

/*
 * pcs2bip()
 *
 * get the bip (Hall) of p into b
 * it will be plen-1 places long
 */
int pcs2bip(const int *p, int plen, int *b)
{
	int i;
	for (i = 0; i < plen - 1; i++) {
		b[i] = p[i + 1] - p[i];
		b[i] = i2ic(b[i]);
	}
	return TRUE;
}

/*
 * printpcs()
 *
 * print optionally formatted pcs with
 * optional context text
 */
void printpcs(FILE *fp, char *s1, const int *buf, int plen, char *s2,
	int frmt)
{
	char str[256];
	if (plen <= 0 || buf == NULL) {
		fprintf(fp, "%s{}%s.\n", s1, s2);
		return;
	}
	if (frmt) {
		pcs2str(buf, plen, str, TRUE);
		fprintf(fp, "%s{%s}%s", s1, str, s2);
	} else {
		pcs2str(buf, plen, str, FALSE);
		fprintf(fp, "%s%s%s", s1, str, s2);
	}
}

/*
 * tni2str()
 *
 * make string of format Tn[I]
 * always occupies 3 chars
 */
int tni2str(int inv, int trs, char *str)
{
	if (inv) {
		sprintf(str, "T%XI", trs);
	} else {
		sprintf(str, "T%X ", trs);
	}
	return TRUE;
}

/*
 * tnmi2str()
 *
 * puts str of format Tn[M][I]
 */
int tnmi2str(tnmi_t o, char *str)
{
	int i = 0;
	str[i] = 'T';
	i++;
	MOD(o.t, 12);
	str[i] = i2c(o.t);
	i++;
	if (o.m) {
		str[i] = 'M';
		i++;
	}
	if (o.i) {
		str[i] = 'I';
		i++;
	}
	str[i] = '\0';
	return TRUE;
}

/*
 * rtnmi2str()
 *
 * puts str of format [R]Tn[MI]
 */
int rtnmi2str(rtnmi_t o, char *str)
{
	int i = 0;
	if (o.r) {
		str[i] = 'R';
		i++;
	}
	str[i] = 'T';
	i++;
	MOD(o.t, 12);
	str[i] = i2c(o.t);
	i++;
	if (o.m) {
		str[i] = 'M';
		i++;
	}
	if (o.i) {
		str[i] = 'I';
		i++;
	}
	str[i] = '\0';
	return TRUE;
}

/*
 * rrtnmi2str()
 *
 * makes a string of format [rR]Tn[MI]
 * from the SO at o
 */
int rrtnmi2str(rrtnmi_t o, char *str)
{
	int i = 0;
	if (o.rt) {
		str[i] = 'r';
		i++;
		str[i] = i2c(o.rt);
		i++;
	}
	if (o.r) {
		str[i] = 'R';
		i++;
	}
	str[i] = 'T';
	i++;
	MOD(o.t, 12);
	str[i] = i2c(o.t);
	i++;
	if (o.m) {
		str[i] = 'M';
		i++;
	}
	if (o.i) {
		str[i] = 'I';
		i++;
	}
	str[i] = '\0';
	return TRUE;
}

/*
 * pcs2str()
 *
 * puts the PCO p into str.
 * if sep is TRUE output is in decimal and commas are used for seperation
 */
int pcs2str(const int *p, int plen, char *str, int sep)
{
	int nc, cc = 0; /* cc = cumulative character count */

	if (p == NULL || plen <= 0) {
		str[0] = '\0';
		return TRUE;
	}
	plen -= 1;
	while (plen--) {
		if (sep)
			sprintf(&str[cc], "%d,%n", *p++, &nc);
		else
			sprintf(&str[cc], "%X%n", *p++, &nc);
		cc += nc;
	}
	if (sep)
		sprintf(&str[cc], "%d", *p++);
	else
		sprintf(&str[cc], "%X", *p++);
	return TRUE;
}

/*
 * pcs2scstr()
 *
 * gets a str representing the SC membership of p in a
 * format determined by the value of pt.
 * as an example if p were {567} and:
 * pt is 0: 3-1[012]
 * pt is 1: T5 3-1[012]
 * pt is 2: T5 3-1
 * pt is 3: 3-1
 * pt is 4: 3-1 T5
 */
int pcs2scstr(const int *p, int plen, char *str, int pt)
{
	char name_str[256], prime_str[256], tni_str[256];
	int q[256], qlen = plen;
	tni_t t;

	if (plen == 0) {
		str[0] = '\0';
		return TRUE;
	}
	memcpy(q, p, plen * sizeof(int));
	prime(q, &qlen, &t);
	assert(pcs_to_name(name_str, q, qlen));
	switch (pt) {
	case 1:
		tni2str(t.i, t.t, tni_str);
		pcs2str(q, qlen, prime_str, FALSE);
		sprintf(str, "%s %s[%s]", tni_str, name_str, prime_str);
		break;
	case 2:
		tni2str(t.i, t.t, tni_str);
		sprintf(str, "%s %s", tni_str, name_str);
		break;
	case 3:
		sprintf(str, "%s", name_str);
		break;
	case 4:
		tni2str(t.i, t.t, tni_str);
		sprintf(str, "%s\t(%s)", name_str, tni_str);
		break;
	case 0:
	default:
		pcs2str(q, qlen, prime_str, FALSE);
		sprintf(str, "%s[%s]", name_str, prime_str);
		break;
	}
	return TRUE;
}

/*
 * getnextpcs()
 *
 * get the next PCS in the stream at fp.
 * open_marker defines the type of bracketing
 * to be used. if sep is TRUE then commas,
 * full stops, spaces etc must be used to
 * seperate numbers within the string.
 * ie. to get [01AB]    open_marker='[' and sep=FALSE
 *     to get {0,1,A,11} open_marker='{' and sep=TRUE
 *     to get (0 1 10 B) open_marker='(' and sep=TRUE
 *
 * returns: location of start of line
 * in which pcs was found, or EOF if an
 * error occured or the pcs was not found.
 */
long getnextpcs(FILE *fp, int *p, int *plen, char open_marker, int sep)
{
	char numstr[10];
	char close_marker = getclose(open_marker);
	int count;
	int c;
	long pos = 0;

	while (TRUE) {
		c = getc(fp);
		if (c == EOF) {
			return EOF;
		}
		if (c == open_marker) {
			break;
		}
		if (c == '\n') {
			pos = ftell(fp);
		}
	}
	*plen = 0;
	if (!close_marker) {
		return EOF;
	}
	while (TRUE) {
		count = 0;
		c = getc(fp);
		if (c == close_marker) {
			return pos;
		}
		if (isdigit(c) != 0) {
			if (sep) {
				numstr[count] = c;
				count += 1;
				while (isdigit(c = getc(fp))) {
					numstr[count] = c;
					count += 1;
				}
				ungetc(c, fp);
				numstr[count] = '\0';
			} else {
				numstr[0] = c;
				numstr[1] = '\0';
			}
			p[*plen] = (int)atoi(numstr);
			*plen += 1;
		}
		if (c == 'a' || c == 'A' || c == 't') {
			p[*plen] = 10;
			*plen += 1;
		}
		if (c == 'b' || c == 'B' || c == 'e') {
			p[*plen] = 11;
			*plen += 1;
		}
		if (c == EOF) {
			return EOF;
		}
	}
}

/*
 * isclose()
 *
 * is c a closing brace, bracket etc.
 */
int isclose(char c)
{
	if (c == '}' || c == '>' || c == ')' || c == ']') {
		return TRUE;
	} else {
		return FALSE;
	}
}

/*
 * isopen()
 *
 * is c an opening brace, bracket etc.
 */
int isopen(char c)
{
	if (c == '{' || c == '<' || c == '(' || c == '[') {
		return TRUE;
	} else {
		return FALSE;
	}
}

/*
 * getclose()
 *
 * get the char that closes 'c'
 */
char getclose(char c)
{
	switch (c) {
	case '{':
		return '}';
	case '[':
		return ']';
	case '<':
		return '>';
	case '(':
		return ')';
	default:
		return FALSE;
	}
}

/*
 * pcseginfile()
 *
 * is the PCSEG p in the file at fp
 */
int pcseginfile(FILE *fp, int *p, int plen)
{
	int q[12];
	int l;
	long h = ftell(fp);
	fseek(fp, 0, SEEK_SET);
	while (getnextpcs(fp, q, &l, '<', 0) != EOF) {
		if (plen == l) {
			if (isident(q, p, l)) {
				fseek(fp, h, SEEK_SET);
				return TRUE;
			}
		}
	}
	fseek(fp, h, SEEK_SET);
	return FALSE;
}

/*
 * pcsinfile()
 *
 * is the PCS p in the file at fp
 */
int pcsinfile(FILE *fp, int *p, int plen)
{
	int q[12];
	int l;
	long h = ftell(fp);
	fseek(fp, 0, SEEK_SET);
	while (getnextpcs(fp, q, &l, '{', 0) != EOF) {
		if (plen == l) {
			if (isequal(q, l, p, plen)) {
				fseek(fp, h, SEEK_SET);
				return TRUE;
			}
		}
	}
	fseek(fp, h, SEEK_SET);
	return FALSE;
}

/*
 * scinfile()
 *
 * is the SC p represented in the file at fp
 */
int scinfile(FILE *fp, int *p, int plen)
{
	int c[12], clen;
	int q[12], qlen;
	long h = ftell(fp);

	cpypcs(c, &clen, p, plen);
	remdup(c, &clen);
	rewind(fp);
	while (getnextpcs(fp, q, &qlen, '[', 0) != EOF) {
		if (isequiv(q, qlen, c, clen)) {
			fseek(fp, h, SEEK_SET);
			return TRUE;
		}
	}
	fseek(fp, h, SEEK_SET);
	return FALSE;
}

/*
 * strinfile()
 *
 * is the string str a line in the file at fp
 */
int strinfile(FILE *fp, const char *str)
{
	char line[2056];
	long h = ftell(fp);

	rewind(fp);
	while (fgets(line, 2056, fp) != NULL) {
		if (strcmp(line, str) == 0) {
			fseek(fp, h, SEEK_SET);
			return TRUE;
		}
	}
	fseek(fp, h, SEEK_SET);
	return FALSE;
}

/*
 * lineinfile()
 *
 * is the line ln in the file at fp
 */
int lineinfile(FILE *fp, const char *ln)
{
	char line[2056];
	long h = ftell(fp);

	rewind(fp);
	while (fgets(line, 2056, fp) != NULL) {
		if (strcmp(line, ln) == 0) {
			fseek(fp, h, SEEK_SET);
			return TRUE;
		}
	}
	fseek(fp, h, SEEK_SET);
	return FALSE;
}

/*
 * previnfile()
 *
 * returns TRUE if str has already occured in fp
 */
int previnfile(const char *str, FILE *fp)
{
	long h = ftell(fp);
	char ln[2056];

	rewind(fp);
	while (fgets(ln, 2056, fp) != NULL) {
		if (ftell(fp) < h) {
			if (strstr(ln, str)) {
				fseek(fp, h, SEEK_SET);
				return TRUE;
			}
		}
	}
	fseek(fp, h, SEEK_SET);
	return FALSE;
}

/*
 * c2i()
 *
 * convert a character to an int
 * returns -1 if char is out of range
 */
int c2i(char c)
{
	switch (c) {
	case '0':
		return 0;
	case '1':
		return 1;
	case '2':
		return 2;
	case '3':
		return 3;
	case '4':
		return 4;
	case '5':
		return 5;
	case '6':
		return 6;
	case '7':
		return 7;
	case '8':
		return 8;
	case '9':
		return 9;
	case 'a':
		return 10;
	case 'A':
		return 10;
	case 't':
		return 10;
	case 'b':
		return 11;
	case 'B':
		return 11;
	case 'e':
		return 11;
	default:
		return -1;
	}
}

/*
 * i2c()
 *
 * convert an int to a char
 * returns '*' if int is out of range
 */
char i2c(int i)
{
	switch (i) {
	case 0:
		return '0';
	case 1:
		return '1';
	case 2:
		return '2';
	case 3:
		return '3';
	case 4:
		return '4';
	case 5:
		return '5';
	case 6:
		return '6';
	case 7:
		return '7';
	case 8:
		return '8';
	case 9:
		return '9';
	case 10:
		return 'A';
	case 11:
		return 'B';
	case 12:
		return 'C';
	default:
		return '*';
	}
}

/*
 * str2forte()
 *
 * get the first forte name from
 * str and put it into forte. a
 * forte name is x-[Z]y, where x
 * and y are decimal numbers and Z
 * is optional.
 * returns the offset to the end of
 * the forte substring if succesful,
 * FALSE if not
 */
int str2forte(const char *str, char *forte)
{
	int i = 0, j = 0;

	for (i = 1; i < ((int)strlen(str) - 1); i++) {
		if (str[i] == '-') {
			if (isdigit(str[i - 1])
				&& (isdigit(str[i + 1]) || str[i + 1] == 'Z')) {
				if (isdigit(str[i - 2])) {
					i--;
				}
				i--;
				while (j < 7) {
					if (isdigit(str[i + j]) || str[i + j] == '-'
						|| str[i + j] == 'Z') {
						forte[j] = str[i + j];
						j++;
					} else {
						forte[j] = '\0';
						break;
					}
				}
				if (strlen(forte) > 2 && strlen(forte) < 7) {
					return i + j;
				}
			}
		}
	}
	return FALSE;
}

/*
 * str2rrtnmi()
 *
 * attempt to extract an rRTnMI from str
 * returns TRUE if o (operator) is
 * succesfully filled, else FALSE.
 *
 * WARNING: should bounds check str.
 */
int str2rrtnmi(char *str, rrtnmi_t *o)
{
	int i = 0;
	if (str[i] == 'r') {
		i++;
		/*
		 * positive but for Morris?
		 */
		o->rt = -c2i(str[i]);
		i++;
	} else {
		o->rt = FALSE;
	}

	if (str[i] == 'R') {
		o->r = TRUE;
		i++;
	} else {
		o->r = FALSE;
	}

	if (str[i] != 'T') {
		return FALSE;
	} else {
		i++;
		o->t = c2i(str[i]);
		i++;
	}

	if (str[i] == 'M') {
		o->m = TRUE;
		i++;
	} else {
		o->m = FALSE;
	}

	if (str[i] == 'I') {
		o->i = TRUE;
		i++;
	} else {
		o->i = FALSE;
	}

	if (o->rt == -1 || o->t == -1) {
		return FALSE;
	}

	return TRUE;
}

/*
 * do_rrtnmi()
 *
 * apply the rRTnMI o to the PCSEG p
 */
int do_rrtnmi(rrtnmi_t o, int *p, int plen)
{
	if (o.i) {
		invpcs(0, p, plen);
	}
	if (o.m && M_in_sro) {
		multpcs(5, p, plen);
	}
	if (o.t) {
		trspcs(o.t, p, plen);
	}
	if (o.r) {
		rtrpcs(p, plen);
	}
	if (o.rt) {
		rotpcs(o.rt, p, plen);
	}
	return TRUE;
}

/*
 * str2pcs()
 *
 * attempt to get a PCSEG from str
 * is very lenient with regards format
 * returns FALSE if parse fails
 */
int str2pcs(const char *str, int *p, int *plen)
{
	int i;

	if (fstr2pcs(str, p, plen)) {
		return TRUE;
	}
	while (*str == ' ' || *str == '\t')
		str++;
	*plen = 0;
	for (i = 0; (size_t)i < strlen(str); i++) {
		if ((p[*plen] = c2i(str[i])) != -1) {
			(*plen)++;
		} else {
			if (str[i] != '{'
				&& str[i] != '['
				&& str[i] != '<'
				&& str[i] != 'i'
				&& str[i] != 'c' && str[i] != 'p' && str[i] != ',') {
				break;
			}
		}
	}
	if (*plen < 1) {
		*plen = 0;
		return 0;
	} else {
		return i;
	}
}

/*
 * str2pcsn()
 *
 * attempt to get the nth PCSEG from str
 * is very lenient with regards format
 * returns FALSE if parse fails
 */
int str2pcsn(char *str, int *p, int *plen, int n)
{
	char *s2 = str;
	int i, j, k = 0;
	for (i = 0; i < n; i++) {
		if (!(j = str2pcs(s2, p, plen))) {
			return FALSE;
		}
		k += j;
		s2 = &str[k];
	}
	return k;
}

/*
 * copyfile()
 *
 * copy the entirety of file fp2
 * into file fp1 at the current marker
 * used primarily to avert problems with
 * programs in pipelines
 */
int copyfile(FILE *fp1, FILE *fp2)
{
	int c;
	long h = ftell(fp2);

	rewind(fp2);
	while ((c = getc(fp2)) != EOF) {
		putc(c, fp1);
	}
	fseek(fp2, h, SEEK_SET);
	return TRUE;
}

/*
 * die()
 *
 * prints the time a process died, an error
 *  message, and calls exit(EXIT_FAILURE)
 */
void die(char *str, ...)
{
	va_list ap;
	time_t now;
	struct tm *date;
	char *t;
	time(&now);
	date = localtime(&now);
	t = asctime(date);
	fprintf(stderr, "died: %serror: ", t);
	va_start(ap, str);
	vfprintf(stderr, str, ap);
	va_end(ap);
	fprintf(stderr, ".\n");
	exit(EXIT_FAILURE);
}

/*
 * getintsctn()
 *
 * put the intersection of the PCSs p and q into s
 */
int getintsctn(int *p, int plen, int *q, int qlen, int *s, int *slen)
{
	int i;

	assert(!isdup(p, plen) && !isdup(q, qlen));
	*slen = 0;

	if (plen > qlen) {
		for (i = 0; i < plen; i++) {
			if (isel(p[i], q, qlen)) {
				s[*slen] = p[i];
				(*slen)++;
			}
		}
	} else {
		for (i = 0; i < qlen; i++) {
			if (isel(q[i], p, plen)) {
				s[*slen] = q[i];
				(*slen)++;
			}
		}
	}
	return *slen;
}

/*
 * seginc()
 *
 * is segment p included in segment q
 * if TRUE returns position at which segment
 * starts, where 1 is the first position
 * NOTE: '012' is in '012'
 */
int seginc(const int *p, int plen, const int *q, int qlen)
{
	int i;
	for (i = 0; i <= qlen - plen; i++) {
		if (isident(p, &q[i], plen)) {
			return i + 1;
		}
	}
	return FALSE;
}

/*
 * segemb()
 *
 * is segment p embedded in segment q
 */
int segemb(const int *p, int plen, const int *q, int qlen)
{
	int i, j = 0;
	for (i = 0; i < qlen; i++) {
		if (q[i] == p[j]) {
			j++;
		}
		if (j == plen) {
			return TRUE;
		}
	}
	return FALSE;
}

/*
 * issegsym()
 *
 * is the PCSEG p symetrical
 * ie is there a value n such
 * that RTnIP=P
 */
int issegsym(int *p, int plen)
{
	int q[12], qlen;
	int i;

	qlen = pcs2int(p, plen, q);
	for (i = 0; i <= qlen / 2; i++) {
		if (q[i] != q[qlen - 1 - i]) {
			return FALSE;
		}
	}
	return TRUE;
}

/*
 * isass()
 *
 * is p an abstract subsegment of q
 * recognizes only RTnI relations
 */
int isass(int *p, int plen, int *q, int qlen)
{
	int s[MAX_PCS], slen; /* comparison segment */
	tr_t *t;

	if ((t = tr_init(q, qlen, 1, 0, 0)) == NULL) {
		return FALSE;
	}

	while (tr_next(t, s, &slen)) {
		if (seginc(p, plen, s, slen)) {
			return TRUE;
		}
	}
	tr_free(t);
	return FALSE;
}

/*
 * tr_init()
 */
tr_t *tr_init(const int *p, int plen, int R, int m, int r)
{
	tr_t *t = emalloc(sizeof(tr_t));
	cpypcs(t->p, &(t->plen), p, plen);
	cpypcs(t->X, &(t->Xlen), p, plen);
	t->o.t = 0;
	t->o.i = 0;
	t->o.m = 0;
	t->o.r = 0;
	t->o.rt = 0;
	t->incl_R = R;
	t->incl_M = m;
	t->incl_r = r;
	assert(r == 0); /* UNIMPLEMENTED */
	return t;
}

/*
 * tr_next()
 */
int tr_next(tr_t *t, int *p, int *plen)
{
	int return_value = 1;

	if (t->o.t == 12) {
		t->o.t = 0;
		/* retrograde */
		if (t->incl_R && t->o.r == 0) {
			t->o.r = 1;
		} else {
			t->o.r = 0;
			/* inversion [ multiplication ] */
			if (t->o.i == 0) {
				t->o.i = 1;
			} else if (t->o.i == 1) {
				if (t->incl_M == 1) {
					if (t->o.m == 0) {
						t->o.i = 0;
						t->o.m = 1;
					} else {
						t->o.i = 0;
						t->o.m = 0;
						return_value = 0;
					}
				} else {
					return_value = 0;
				}
			} else {
				return_value = 0;
			}
		}
	}
	cpypcs(p, plen, t->p, t->plen);
	do_rrtnmi(t->o, p, *plen);
	/*printf("%d %d %d %d\n",t->o.t,t->o.r,t->o.i,t->o.m); */
	t->o.t++;
	/* this is not a good way to do this? */
	if (return_value == 0) {
		t->o.t = 0;
		t->o.i = 0;
		t->o.r = 0;
		t->o.m = 0;
		t->o.rt = 0;
		cpypcs(t->p, &(t->plen), t->X, t->Xlen);
	}
	return return_value;
}

/*
 * tr_free()
 */
void tr_free(tr_t *t)
{
	free(t);
}

/*
 * insrtchar()
 *
 * insert the char c into the string str
 * at position pos
 */
int insrtchar(char *str, char c, long pos)
{
	long i;
	long e = strlen(str);
	for (i = e + 1; i > pos; i--) {
		str[i] = str[i - 1];
	}
	str[pos] = c;
	return TRUE;
}

/*
 * fstr2pcs()
 *
 * puts the SC named in str into p
 */
int fstr2pcs(const char *str, int *p, int *plen)
{
	char pstr[256], fstr[256];

	if (!str2forte(str, fstr) || !resolve_name(pstr, fstr)) {
		return FALSE;
	}
	str2pcs(pstr, p, plen);
	return TRUE;
}

/*
 * pcs2idn()
 *
 * converts a PCSEG to a unique
 * identification number
 *
 * double dd=pcs2idn(p,plen);
 * printf("%0.*f\n",plen,dd);
 */
double pcs2idn(const int *p, int plen)
{
	int i, j;
	double id = 0, tmp;

	if (plen < 1) {
		return 0.0;
	}
	for (i = 0; i < plen; i++) {
		tmp = (double)p[i];
		for (j = 0; j <= i; j++) {
			tmp /= 10;
		}
		id -= tmp;
	}
	id *= -1;
	return id;
}

/*
 * sc2idn()
 *
 * converts a SC to a unique
 * identification number
 */
double sc2idn(const int *p, int plen)
{
	int pr[12];

	if (plen < 1) {
		return 0.0;
	}
	pcs2prime(p, plen, (tni_t *)NULL, pr);
	return pcs2idn(pr, plen);
}

/*
 * fn2idn()
 *
 * convert a forte name to an identification number
 * 5-23 become 523
 * 8-Z29 become 829 etc.
 * returns FALSE if p cannot be parsed
 */
int fn2idn(const char *p)
{
	int c, n;

	if (strstr(p, "Z") != NULL) {
		if (sscanf(p, "%d-Z%d", &c, &n) != 2) {
			return FALSE;
		}
	} else {
		if (sscanf(p, "%d-%d", &c, &n) != 2) {
			return FALSE;
		}
	}
	return (c * 100) + n;
}

/*
 * forder()
 *
 * is p a lower forte name than q
 * where 4-1 is lower than 4-3 etc.
 * and 4-10 is lower than 5-1
 * returns: 0 if equal
 *          1 if p>q
 *         -1 if p<q
 */
int forder(const char *p, const char *q)
{
	int pn = fn2idn(p);
	int qn = fn2idn(q);
	if (pn == qn) {
		return 0;
	}
	if (pn > qn) {
		return 1;
	}
	return -1;
}

/*
 * fnsrtcomp()
 *
 * comaparison for qsort in fnsrt()
 */
int fnsrtcomp(const void *n1, const void *n2);
int fnsrtcomp(const void *n1, const void *n2)
{
	return forder(*(const char **)n1, *(const char **)n2);
}

/*
 * fnsrt()
 *
 * sort an array of pointers to forte strings
 */
int fnsrt(int ac, char **av)
{
	qsort(av, (long)ac, sizeof(char **), fnsrtcomp);
	return TRUE;
}

/*
 * allocarray()
 *
 * allocate an array of r rows
 * and c collums.
 * returns valid pointer, or NULL.
 * usage:
 *     int **p,pr=12,pc=12;
 *     if((p=allocarray(pr,pc))==NULL) {
 *         error("allocation failure");
 *         ....
 *      }
 * WARNING: leaks at failure
 */
int **allocarray(int r, int c)
{
	int i;
	int **p;

	p = emalloc(r * sizeof(int *));
	for (i = 0; i < r; i++) {
		p[i] = emalloc(c * sizeof(int));
	}
	return p;
}

/*
 * freearray()
 *
 * free the array p, which has
 * r number of rows. should only
 * be used to free arrays succesfully
 * allocated by allocarray()
 */
void freearray(int **p, int r)
{
	int i;

	for (i = 0; i < r; i++) {
		free(p[i]);
	}
	free(p);
}

/*
 * readarray()
 *
 * read a text representation of
 * a PC array from the file at fp.
 * will move through the file until
 * it finds an array, and reposition
 * to the first line after the array.
 *
 * r = number of rows
 * c = number of collums
 *
 * returns TRUE or FALSE
 */
int readarray(FILE *fp, int **p, int *r, int *c)
{
	char str[256];
	int len;
	long h;

	*r = 0;
	*c = 0;
	while (1) {
		h = ftell(fp);
		if (fgets(str, 256, fp) == NULL) {
			return *r;
		}
		if (!str2pcs(str, p[*r], &len)) {
			if ((*r) != 0) {
				fseek(fp, h, SEEK_SET);
				return TRUE;
			} else {
				continue;
			}
		}
		(*r)++;
		if (*c == 0) {
			*c = len;
		} else {
			if (*c != len) {
				return FALSE;
			}
		}
	}
}

/*
 * writearray()
 *
 * writes a text representation of
 * a PC array to the file at fp.
 *
 * r = number of rows
 * c = number of collums
 */
int writearray(FILE *fp, const int **p, int r, int c)
{
	char str[256];
	int i;

	for (i = 0; i < r; i++) {
		pcs2str(p[i], c, str, 0);
		fprintf(fp, "%s\n", str);
	}
	fprintf(fp, "\n");
	return 1;
}

/*
 * arrayvert()
 *
 * get the nth verticality of
 * the array of r rows at p.
 * ordered from bottom to top.
 */
int arrayvert(int **p, int r, int n, int *v)
{
	int i;

	for (i = 0; i < r; i++) {
		v[r - i - 1] = p[i][n];
	}
	return r;
}

/*
 * diset2iset()
 *
 * convert the diatonic ISET at p into a standard ISET.
 */
int diset2iset(int *s, int *slen)
{
	int q[16], qlen = 0; /* put the ISET into here while working */
	int i;

	if (!bcdiset(s, *slen)) {
		return FALSE;
	}
	for (i = 0; i < *slen; i++) {
		switch (s[i]) {
		case 0:
			q[qlen] = 0;
			qlen++;
			break;
		case 2:
			q[qlen] = 1;
			q[qlen + 1] = 2;
			qlen += 2;
			break;
		case 3:
			q[qlen] = 3;
			q[qlen + 1] = 4;
			qlen += 2;
			break;
		case 4:
			q[qlen] = 5;
			q[qlen + 1] = 6;
			qlen += 2;
			break;
		case 5:
			q[qlen] = 6;
			q[qlen + 1] = 7;
			qlen += 2;
			break;
		case 6:
			q[qlen] = 8;
			q[qlen + 1] = 9;
			qlen += 2;
			break;
		case 7:
			q[qlen] = 10;
			q[qlen + 1] = 11;
			qlen += 2;
			break;
		default:
			return FALSE;
			break;
		}
		remdup(q, &qlen);
	}
	srtpcs(q, qlen);
	cpypcs(s, slen, q, qlen);
	return TRUE;
}

/*
 * iset2diset()
 *
 * convert the standard ISET at s into a diatonic ISET.
 */
int iset2diset(int *s, int *slen)
{
	int q[16], qlen = 0; /* put the dISET into here while working */
	int i;

	for (i = 0; i < *slen; i++) {
		switch (s[i]) {
		case 0:
			q[qlen] = 0;
			qlen++;
			break;
		case 1:
		case 2:
			q[qlen] = 2;
			qlen++;
			break;
		case 3:
		case 4:
			q[qlen] = 3;
			qlen++;
			break;
		case 5:
			q[qlen] = 4;
			qlen++;
			break;
		case 6:
			/* ambiguous, but should never appear by itself */
			break;
		case 7:
			q[qlen] = 5;
			qlen++;
			break;
		case 8:
		case 9:
			q[qlen] = 6;
			qlen++;
			break;
		case 10:
		case 11:
			q[qlen] = 7;
			qlen++;
			break;
		default:
			return FALSE;
			break;
		}
		remdup(q, &qlen);
	}
	srtpcs(q, qlen);
	cpypcs(s, slen, q, qlen);
	return TRUE;
}

/*
 * bcdiset()
 *
 * bounds check a dISET.
 * must be a member of the set {0,2,3,4,5,6,7}.
 */
int bcdiset(int *s, int slen)
{
	int q[7] = { 0, 2, 3, 4, 5, 6, 7 }, qlen = 7;
	int i;

	for (i = 0; i < slen; i++) {
		if (!isel(s[i], q, qlen)) {
			return FALSE;
		}
	}
	return TRUE;
}

/*
 * sumpcs()
 *
 * returns the sum of all members of p.
 */
int sumpcs(const int *p, int plen)
{
	int i, t = 0;
	for (i = 0; i < plen; i++) {
		t += p[i];
	}
	return t;
}

/*
 * nextcol()
 *
 * move to the next collumn which is x spaces away.
 */
void nextcol(int x)
{
	while (x--) {
		putc(' ', stdout);
	}
}

/*
 * str2tnmi()
 *
 * convert the string str to a TnMI.
 */
int str2tnmi(char *str, tnmi_t *t)
{
	int i = 0;

	t->i = FALSE;
	t->m = FALSE;

	if (str[i] != 'T') {
		return FALSE;
	}
	i++;
	if ((t->t = c2i(str[i])) == -1) {
		return FALSE;
	}
	i++;
	if (str[i] == 'M') {
		t->m = TRUE;
		i++;
	}
	if (str[i] == 'I') {
		t->i = TRUE;
	}
	return TRUE;
}

/*
 * do_tnmi()
 *
 * apply the tnmi t to the PCS p.
 */
int do_tnmi(tnmi_t t, int *p, int plen)
{
	int i;

	for (i = 0; i < plen; i++) {
		if (t.i) {
			p[i] = 12 - p[i];
		}
		if (t.m) {
			p[i] *= 5;
		}
		p[i] += t.t;
		p[i] = mod12(p[i]);
	}
	return TRUE;
}

int isegsc(const int *s, int slen, int *p, int *plen)
{
	int i;
	*plen = slen + 1;
	p[0] = 0;
	for (i = 1; i < *plen; i++) {
		p[i] = mod12(p[i - 1] + s[i - 1]);
	}
	prime(p, plen, NULL);
	return TRUE;
}

int isegicseg(const int *s, int slen, int *t, int *tlen)
{
	int i;
	*tlen = slen;
	for (i = 0; i < slen; i++) {
		t[i] = i2ic(s[i]);
	}
	return TRUE;
}

int pcsegicseg(const int *p, int plen, int *s, int *slen)
{
	int i;
	for (i = 0; i < plen - 1; i++) {
		s[i] = p[i + 1] - p[i];
		s[i] = i2ic(s[i]);
	}
	*slen = plen - 1;
	return TRUE;
}

int pcsegiseg(const int *p, int plen, int *s, int *slen)
{
	int i;
	for (i = 0; i < plen - 1; i++) {
		s[i] = p[i + 1] - p[i];
		MOD(s[i], 12);
	}
	*slen = plen - 1;
	return TRUE;
}

int pcsegciseg(const int *p, int plen, int *s, int *slen)
{
	int i;
	for (i = 0; i < plen - 1; i++) {
		s[i] = p[i + 1] - p[i];
		MOD(s[i], 12);
	}
	s[i] = p[0] - p[i];
	MOD(s[i], 12);
	*slen = plen;
	return TRUE;
}

int pcsegicset(const int *p, int plen, int *s, int *slen)
{
	int i;
	*slen = 0;
	for (i = 0; i < plen - 1; i++) {
		if (!isel(p[i + 1] - p[i], s, *slen)) {
			s[*slen] = p[i + 1] - p[i];
			s[*slen] = i2ic(s[*slen]);
			(*slen)++;
		}
	}
	return TRUE;
}

int pcsegiset(const int *p, int plen, int *s, int *slen)
{
	int i;
	*slen = 0;
	for (i = 0; i < plen - 1; i++) {
		if (!isel(p[i + 1] - p[i], s, *slen)) {
			s[*slen] = p[i + 1] - p[i];
			MOD(s[*slen], 12);
			(*slen)++;
		}
	}
	return TRUE;
}

/*
 * returns the intersection of p and q
 */
int intersection(int *p, int plen, int *q, int qlen)
{
	int i, rv = 0;

	assert(!isdup(p, plen) && !isdup(q, qlen));
	if (plen > qlen) {
		for (i = 0; i < plen; i++) {
			if (isel(p[i], q, qlen)) {
				rv++;
			}
		}
	} else {
		for (i = 0; i < qlen; i++) {
			if (isel(q[i], p, plen)) {
				rv++;
			}
		}
	}
	return rv;
}
