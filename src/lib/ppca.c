#include "ppca.h"
#include "emem.h"
#include "pco.h"
#include "pct.h"
#include "sro.h"
#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

Ppca_t *paopen(const char *name)
{
	int i, j;
	Ppca_t *e;

	e = emalloc(sizeof(Ppca_t));

	/* alloc */
	e->data = emalloc(MAX_PCS * sizeof(int *));
	e->partition = emalloc(MAX_PCS * sizeof(int *));
	e->size = emalloc(MAX_PCS * sizeof(int));

	for (i = 0; i < MAX_PCS; i++) {
		e->data[i] = emalloc(MAX_PCS * sizeof(int));
		e->partition[i] = emalloc(MAX_PCS * sizeof(int));
	}

	/* init */
	strcpy(e->name, name);
	strcpy(e->row_sep, PPCA_ROW_SEP);
	strcpy(e->column_sep, PPCA_COLUMN_SEP);
	e->r = 0;
	e->c = 0;
	for (i = 0; i < MAX_PCS; i++) {
		for (j = 0; j < MAX_PCS; j++) {
			e->partition[i][j] = 0;
		}
	}
	for (i = 0; i < MAX_PCS; i++) {
		e->size[i] = 0;
	}

	return e;
}

void paclose(Ppca_t *e)
{
	int i;

	for (i = 0; i < MAX_PCS; i++) {
		free(e->partition[i]);
		free(e->data[i]);
	}
	free(e->data);
	free(e->partition);
	free(e->size);
}

/* remove all characters in s2 from s1 */
static char *clean_string(char *s1, const char *s2)
{
	char s[MAX_STR];
	int i, j;

	for (i = 0, j = 0; i < strlen(s1) && i < MAX_STR; i++) {
		if (!strchr(s2, s1[i])) {
			s[j] = s1[i];
			j++;
		}
	}
	s[j] = '\0';
	strcpy(s1, s);
	return s1;
}

int paread(Ppca_t *e, FILE *fp)
{
	char s[MAX_STR];
	int i = 0, j;

	e->c = 0;
	e->r = 0;

	while (fgets(s, MAX_STR, fp)) {
		clean_string(s, " \t");
		if (s[0] == '\n') {
			if (e->r > 0) {
				return TRUE;
			}
		} else if (s[0] != '#') {
			/* get partition */
			i = j = e->partition[e->r][0] = 0;
			while (1) {
				if (s[j] == ':') {
					i++;
					e->partition[e->r][i] = 0;
				} else if (s[j] == '\n') {
					i++;
					break;
				} else {
					e->partition[e->r][i]++;
				}
				j++;
			}
			if (!e->c) {
				e->c = i;
			} else {
				if (i != e->c) {
					die("paread(): incorrect array : "
						"%s : ( %d , %d )",
						s, e->c, i);
				}
			}
			/* get pcseg */
			clean_string(s, e->column_sep);
			str2pcs(s, e->data[e->r], &(e->size[e->r]));
			assert(e->size[e->r] < MAX_PCS);
			e->r++;
			assert(e->r < MAX_PCS);
		}
	}
	return (e->r ? TRUE : FALSE);
}

int pawrite(Ppca_t *e, FILE *fp)
{
	char str[MAX_STR];
	int maxprt;
	int index;
	int i, j, k;

	for (i = 0; i < e->r; i++) { /* i is the current row */
		for (j = 0; j < e->c; j++) { /* j is the current partition */
			/* get position */
			for (index = 0, k = 0; k < j; k++) {
				index += e->partition[i][k];
			}
			pcs2str(&(e->data[i][index]), e->partition[i][j], str, FALSE);
			/* do neat printout */
			for (maxprt = 0, k = 0; k < e->r; k++) {
				if (e->partition[k][j] > maxprt) {
					maxprt = e->partition[k][j];
				}
			}
			if (maxprt == 0) {
				maxprt++;
			}
			fprintf(fp, "%s", str);
			nextcol(maxprt - strlen(str));
			j + 1 < e->c ? fprintf(fp, "%s", e->column_sep) : fprintf(fp, "\n");
		}
	}
	fprintf(fp, "\n");
	return TRUE;
}

int pasro(Ppca_t *e, Sro_t *o)
{
	int i;

	for (i = 0; i < e->r; i++) {
		do_rrtnmi(*o, e->data[i], e->size[i]);
		if (o->r) {
			rtrpcs(e->partition[i], e->c);
		}
	}
	/*if(o->r) rtrpcs(e->partition[i],e->c); why was this here as well? */
	return TRUE;
}

int pacolumn(Ppca_t *e, int n, Pco_t *q, int action)
{
	int r, cnt;
	Pco_t *p = pco_create("pcseg");

	assert(n <= e->c);
	if (action == PPCA_READ) {
		for (r = e->r - 1, q->size = 0; r >= 0; r--) {
			paposition(e, r, n, p, PPCA_READ);
			pco_cat(q, p);
		}
	} else if (action == PPCA_INSERT) {
		for (r = e->r - 1, cnt = 0; r >= 0; r--) {
			p->size = e->partition[r][n];
			memcpy(p->data, &(q->data[cnt]), p->size * sizeof(int));
			cnt += paposition(e, r, n, p, PPCA_INSERT);
		}
	}
	pco_free(p);
	return q->size;
}

int parow(Ppca_t *e, int n, Pco_t *q, int action)
{
	assert(n <= e->r);
	if (action == PPCA_READ) {
		q->size = e->size[n];
		memmove(q->data, e->data[n], q->size * sizeof(int));
	} else if (action == PPCA_INSERT) {
		e->size[n] = q->size;
		memmove(e->data[n], q->data, q->size * sizeof(int));
	}
	return q->size;
}

int paposition(Ppca_t *e, int r, int c, Pco_t *q, int action)
{
	int i, index = 0, size = 0;

	assert(r <= e->r);
	assert(c <= e->c);
	for (i = 0; i < c; i++)
		index += e->partition[r][i];
	for (i = 0; i < e->c; i++)
		size += e->partition[r][i];
	if (action == PPCA_READ) {
		q->size = e->partition[r][c];
		memmove(q->data, &(e->data[r][index]), q->size * sizeof(int));
	} else if (action == PPCA_INSERT) {
		memmove(&(e->data[r][index + (q->size - e->partition[r][c])]),
			&(e->data[r][index]), (size - index) * sizeof(int));
		e->partition[r][c] = q->size;
		memmove(&(e->data[r][index]), q->data, q->size * sizeof(int));
	}
	return q->size;
}

int pacopy(Ppca_t *e, const Ppca_t *f)
{
	int i;

	e->r = f->r;
	e->c = f->c;
	for (i = 0; i < f->r; i++) {
		memcpy(e->data[i], f->data[i], MAX_PCS * sizeof(int));
	}
	for (i = 0; i < f->r; i++) {
		e->size[i] = f->size[i];
	}
	for (i = 0; i < f->r; i++) {
		memcpy(e->partition[i], f->partition[i], MAX_PCS * sizeof(int));
	}
	return TRUE;
}

/* writes the pitch-class distribution of e into the pcv p */
int padistr(Ppca_t *e, Pco_t *p)
{
	int i, j;

	for (i = 0; i < 12; i++)
		p->data[i] = 0;
	pco_set_typeof(p, pcv);

	for (i = 0; i < e->r; i++) {
		for (j = 0; j < e->size[i]; j++) {
			p->data[e->data[i][j]]++;
		}
	}
	return TRUE;
}

/* arrays are equal if the data arrays and partition maps are equal */
int paequal(Ppca_t *e_1, Ppca_t *e_2)
{
	int i;
	/* rows and columns */
	if ((e_1->r != e_2->r) || (e_1->c != e_2->c)) {
		return 0;
	}
	for (i = 0; i < e_1->r; i++) {
		/* size of row */
		if (e_1->size[i] != e_2->size[i]) {
			return 0;
		}
		/* row data */
		if (memcmp(e_1->data[i], e_2->data[i], e_1->size[i] * sizeof(int))
			!= 0) {
			return 0;
		}
		/* row partition map */
		if (memcmp(e_1->partition[i], e_2->partition[i],
				e_1->c * sizeof(int))
			!= 0) {
			return 0;
		}
	}
	return 1;
}

/* normalize the partition map (each position contains n elements) */
int paprtnrm(Ppca_t *e, int n)
{
	int i, j;
	/* check there is at least one row */
	if (e->r < 1) {
		return FALSE;
	}
	/* check all rows have equal numbers of elements */
	for (i = 1; i < e->r; i++) {
		if (e->size[i] != e->size[i - 1]) {
			return FALSE;
		}
	}
	/* check the number of elements is divisible by n */
	if (e->size[0] % n != 0) {
		return FALSE;
	}
	/* write new partition map */
	for (i = 0; i < e->r; i++) {
		for (j = 0; j < e->size[0] / n; j++) {
			e->partition[i][j] = n;
		}
	}
	/* write new number of columns */
	e->c = e->size[0] / n;
	return TRUE;
}
