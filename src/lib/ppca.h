#ifndef _PPCA_H
#define _PPCA_H

#include "sro.h"

#define PPCA_COLUMN_SEP ":"
#define PPCA_ROW_SEP "\n"
#define PPCA_READ 0
#define PPCA_INSERT 1

/*
 * p    = pointer to the array
 * plen = number of elements in rows
 * r    = number of rows
 * prt  = pointer to the partition map
 * c    = number of columns
 */
typedef struct
{
	char name[FILENAME_MAX];
	char column_sep[2];
	char row_sep[2];
	int **data;
	int *size;
	int r;
	int **partition;
	int c;
} Ppca_t;

Ppca_t *paopen(const char *name);
void paclose(Ppca_t *e);
int paread(Ppca_t *e, FILE *fp);
int pawrite(Ppca_t *e, FILE *fp);
int pacopy(Ppca_t *p, const Ppca_t *q);
int pacolumn(Ppca_t *e, int n, Pco_t *q, int action);
int parow(Ppca_t *e, int n, Pco_t *q, int action);
int paposition(Ppca_t *e, int r, int c, Pco_t *q, int action);
int pasro(Ppca_t *e, Sro_t *o);
int padistr(Ppca_t *p, Pco_t *d);
int paequal(Ppca_t *e_1, Ppca_t *e_2);
int paprtnrm(Ppca_t *e, int n);

#endif
