#include <assert.h>
#include <stdio.h>
#include <stdlib.h>

#include "epm.h"
#include "pct.h"

int main()
{
	Epm_t *e;
	char str[256];
	int p[12] = { 0, 1, 4, 5 }, plen = 4;

	/*
	   printf("Testing epmopen().\n");
	   e=epmopen("test");
	   if(e==NULL) {
	   printf("epmopen() failed");
	   exit(0);
	   }
	   printf("Testing epmclose().\n");
	   if(!epmclose(e)) {
	   printf("epmclose() failed");
	   exit(0);
	   }
	 */

	/*
	   printf("Testing epmparse().\n");
	   e=epmopen("test");
	   assert(e!=NULL);
	   if(!epmparse(e,"is pcset 0145") ||
	   !epmparse(e,"in sc 037") ||
	   !epmparse(e,"has iset 0147") ||
	   !epmparse(e,"is icset 12 165 197") ||
	   !epmparse(e,"in pset 249 162 30648") ||
	   !epmparse(e,"has pcseg 037 125976 03125")) {
	   printf("epmparse() failed");
	   exit(0);
	   }
	   printf("Testing epmwrite().\n");
	   if(!epmwrite(e,stdout)) {
	   printf("epmwrite() failed");
	   exit(0);
	   }
	   epmclose(e);
	 */

	/*
	   printf("Testing epmread().\n");
	   e=epmopen("test");
	   assert(e!=NULL);
	   if(!epmread(e,stdin)) {
	   printf("epmread() failed");
	   exit(0);
	   }
	   if(!epmwrite(e,stdout)) {
	   printf("epmwrite() failed");
	   exit(0);
	   }
	   epmclose(e);
	 */

	printf("Testing epmeval().\n");
	e = epmopen("test");
	assert(e != NULL);
	if (!epmparse(e, "in sc 7-35") || !epmparse(e, "has sc 3-11") || !epmparse(e, "in pcset 0123456789") || !epmparse(e, "has icset 23") || !epmparse(e, "notin sc 0235689B") || !epmparse(e, "hasnt icset 1")) {
		printf("epmparse() failed");
		exit(0);
	}
	if (!epmwrite(e, stdout)) {
		printf("epmwrite() failed");
		exit(0);
	}
	while (fgets(str, 256, stdin) != NULL) {
		if (str2pcs(str, p, &plen) && epmeval(e, p, plen) == -1) {
			printf("that passes\n");
		}
	}
	epmclose(e);
	epmclose(e);

	printf("Testing complete.\n");
	exit(1);
	return 1;
}
