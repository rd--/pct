#ifndef _CG_H
#define _CG_H

typedef struct {
	int n, r;
	int *l, *s;
} Cg_t;

Cg_t *cgopen(int n, int r, int *s);
int cgnext(Cg_t *c, int *s);
void cgclose(Cg_t *c);

#endif
