#ifndef _SRO_H
#define _SRO_H

#include "pco.h"
#include "pct.h"

/*typedef rrtnmi_t Sro_t ;*/
#define Sro_t rrtnmi_t

int sro_write(Sro_t *sro, char *str);
int sro_read(Sro_t *sro, char *str);
int sro_derive(Sro_t *sro, Pco_t *from, Pco_t *to);
void sro_apply(Sro_t *sro, Pco_t *pco);
void sro_put(Sro_t *sro, FILE *stream);
void sro_get(Sro_t *sro, FILE *stream);

#endif
