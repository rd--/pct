#include <stddef.h>

void *emalloc(size_t size);
void *ecalloc(size_t nmemb, size_t size);
void *erealloc(void *p, size_t size);
void efree(void *p);
