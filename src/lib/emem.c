#include "emem.h"
#include "pct.h"
#include <stdio.h>
#include <stdlib.h>

void *emalloc(size_t size)
{
	void *p = malloc(size);
	if (p == 0)
		die("lib/emem.c : emalloc() : malloc failed");
	return p;
}

void *ecalloc(size_t nmemb, size_t size)
{
	void *p = calloc(nmemb, size);
	if (p == 0)
		die("lib/emem.c : ecalloc() : calloc failed");
	return p;
}

void *erealloc(void *p, size_t size)
{
	p = realloc(p, size);
	if (p == 0)
		die("lib/emem.c : erealloc() : realloc failed");
	return p;
}

void efree(void *p)
{
	if (p)
		free(p);
	else
		fprintf(stderr,
			"lib/emem.c : efree() : attempt to free null pointer\n");
}
