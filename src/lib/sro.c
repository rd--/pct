#include "libpct.h"
#include <stdio.h>

int sro_write(Sro_t *sro, char *str)
{
	return rrtnmi2str(*sro, str);
}

int sro_read(Sro_t *sro, char *str)
{
	return str2rrtnmi(str, sro);
}

int sro_derive(Sro_t *sro, Pco_t *from, Pco_t *to)
{
	return get_sro(from->data, from->size, to->data, to->size, sro);
}

void sro_apply(Sro_t *sro, Pco_t *pco)
{
	do_rrtnmi(*sro, pco->data, pco->size);
}

void sro_put(Sro_t *sro, FILE *stream)
{
	char str[MAX_STR];
	sro_write(sro, str);
	fprintf(stream, "%s\n", str);
}

void sro_get(Sro_t *sro, FILE *stream)
{
	char str[MAX_STR];
	fgets(str, MAX_STR, stream);
	sro_read(sro, str);
}
