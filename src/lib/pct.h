#ifndef _PCT_H
#define _PCT_H

#include <stdio.h>

/********************* definitions **************************/

#ifndef TRUE
#define TRUE 1
#define FALSE 0
#endif
#define MAX_RSD_STR 256
#define MAX_PCS MAX_RSD_STR
#define MAX_STR MAX_RSD_STR
#define RMNL(s) \
	if (s[strlen(s) - 1] == '\n') { \
		s[strlen(s) - 1] = '\0'; \
	}
#define MOD(a, b) \
	{ \
		while (a >= b) { \
			a -= b; \
		} \
		while (a < 0) { \
			a += b; \
		} \
	}
#define streq(a, b) (strcmp(a, b) == 0)
#define Tto_t tnmi_t
#define ttostr(a, b) tnmi2str(a, b)
#define strtto(a, b) str2tnmi(a, b)

/********************* types **************************/
typedef struct
{
	int t; /* transposition (0-11) */
	int i; /* inversion (0 or 1) */
} tni_t;

typedef struct
{
	int t;
	int i;
	int m; /* M5, multiplication (0 or 1) */
} tnmi_t;

typedef struct
{
	int t;
	int i;
	int m;
	int r;
} rtnmi_t;

typedef struct
{
	int t;
	int i;
	int m;
	int r;
	int rt; /* rotation (-/+Number) */
} rrtnmi_t;

typedef struct
{
	int p[MAX_PCS], plen;
	int X[MAX_PCS], Xlen;
	rrtnmi_t o;
	int incl_R, incl_M, incl_r;
} tr_t;

/********************* variables **************************/
extern int M_in_tto;
extern int M_in_sro;
extern char pct_data_dir[];
extern char sc_lookup[];
extern char univ_lookup[];

/********************* array-proc **************************/
int **allocarray(int r, int c);
void freearray(int **p, int r);
int readarray(FILE *fp, int **p, int *r, int *c);
int writearray(FILE *fp, const int **p, int r, int c);
int arrayvert(int **p, int r, int n, int *v);

/********************* io **************************/
int pcs2scstr(const int *p, int plen, char *str, int pt);
int pcs2str(const int *p, int len, char *str, int sep);
int rrtnmi2str(rrtnmi_t o, char *str);
int rtnmi2str(rtnmi_t o, char *str);
int tnmi2str(tnmi_t o, char *str);
int tni2str(int inv, int trs, char *str);
int pcseginfile(FILE *fp, int *p, int plen);
int pcsinfile(FILE *fp, int *p, int plen);
int previnfile(const char *str, FILE *fp);
int scinfile(FILE *fp, int *p, int plen);
int strinfile(FILE *fp, const char *str);
int lineinfile(FILE *fp, const char *ln);
int copyfile(FILE *fp1, FILE *fp2);
long getnextpcs(FILE *fp, int *p, int *len, char open_marker, int sep);
int str2forte(const char *str, char *forte);
int str2pcs(const char *str, int *p, int *plen);
int str2pcsn(char *str, int *p, int *plen, int n);
int str2rrtnmi(char *str, rrtnmi_t *o);
int str2tnmi(char *str, tnmi_t *t);
int fstr2pcs(const char *str, int *p, int *plen);
int c2i(char c);
char i2c(int i);
int midi2pc(int d1);
void printpcs(FILE *fp, char *s1, const int *buf, int len, char *s2, int frmt);
void nextcol(int a);
char getclose(char c);
int isclose(char c);
int isopen(char c);

/********************* pc - trs **************************/
int interval(int a, int b);
int i2ic(int i);
int p2pc(int p);
int ps2pcs(const int *p, int len, int *q);
int pc2compl(int p);
int pcs2icv(const int *p, int len, int *icv);
int pcsiv(const int *p, int len, int *iv);
int pcs2tics(const int *p, int len, int *tics);
int pcs2cmpa(const int *p, int plen, int *q);
int pcs2cmpl(const int *p, int plen, int *q);
int pcs2cmpn(const int *p, int plen, const int *q, int qlen, int *cmp);
int pcs2prime(const int *p, int len, tni_t *t, int *pr);
int pcs2int(const int *p, int len, int *intv);
int pcs2intn(const int *p, int len, int *intv, int n);
int pcs2tni(const int *p, int len, tni_t *t);
int pcs2bip(const int *p, int len, int *b);
int invpcs(int n, int *p, int plen);
int multpcs(int n, int *p, int plen);
int rotpcs(int r, int *p, int len);
int rtrpcs(int *p, int len);
int srtpcs(int *p, int len);
int trspcs(int t, int *p, int len);
int prime(int *p, int *plen, tni_t *t);
int stackleft(int *p, int len);
int getdup(const int *p, int len, int *d, int *dlen);
int isdup(const int *p, int plen);
int rem1pc(int pc, int *p, int *plen);
int remdup(int *p, int *plen);
int rempc(int pc, int *p, int *plen);
int isabssub(const int *p, int plen, const int *q, int qlen);
int isel(int a, const int *p, int len);
int isequal(const int *p, int plen, const int *q, int qlen);
int isequiv(const int *p, int plen, const int *q, int qlen);
int isident(const int *p, const int *q, int len);
int islitsub(const int *p, int plen, const int *q, int qlen);
int issegsym(int *p, int plen);
int issym(const int *p, int len);
int isass(int *p, int plen, int *q, int qlen);
int seginc(const int *p, int plen, const int *q, int qlen);
int segemb(const int *p, int plen, const int *q, int qlen);
int intersection(int *p, int plen, int *q, int qlen);
int getintsctn(int *p, int plen, int *q, int qlen, int *s, int *slen);
int getinvar(const int *p, int plen, const int *q, int qlen);
int getmerge(int *m, int *mlen, const int *p, int plen, const int *q, int qlen);
int mergein(int *m, int *mlen, const int *p, int plen);
int do_tnmi(tnmi_t t, int *p, int plen);
int do_rrtnmi(rrtnmi_t o, int *p, int plen);
int getrtnmi(const int *p, const int *q, int len, rtnmi_t *o);
int get_tto(const int *p, int plen, const int *q, int qlen, tnmi_t *o);
int get_sro(const int *p, int plen, const int *q, int qlen, rrtnmi_t *o);
int rrtnmi2str(rrtnmi_t o, char *str);
int rtnmi2str(rtnmi_t o, char *str);
int tnmi2str(tnmi_t o, char *str);
int tni2str(int inv, int trs, char *str);
int isegsc(const int *s, int slen, int *p, int *plen);
int isegicseg(const int *s, int slen, int *t, int *tlen);
int pcsegicseg(const int *p, int plen, int *s, int *slen);
int pcsegiseg(const int *p, int plen, int *s, int *slen);
int pcsegicset(const int *p, int plen, int *s, int *slen);
int pcsegiset(const int *p, int plen, int *s, int *slen);
int pcsegciseg(const int *p, int plen, int *s, int *slen);
int bcdiset(int *s, int slen);
int diset2iset(int *s, int *slen);
int iset2diset(int *s, int *slen);
tr_t *tr_init(const int *p, int plen, int R, int m, int r);
int tr_next(tr_t *t, int *p, int *plen);
void tr_free(tr_t *t);

/********************* utilities **************************/
int mod(int i, int n);
int mod12(int i);
int cpypcs(int *p, int *plen, const int *q, int qlen);
void die(char *str, ...);
int insrtchar(char *str, char c, long pos);
double sc2idn(const int *p, int plen);
double pcs2idn(const int *p, int plen);
int fn2idn(const char *p);
int forder(const char *p, const char *q);
int fnsrt(int ac, char **av);
int sumpcs(const int *p, int plen);

#endif
