#include "libpct.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

static int estreq(const char *a, const char *b)
{
	while (1) {
		if (*a != *b)
			return 0;
		if (*a == '\0')
			return 1;
		a++;
		b++;
	}
}

int forte_name(char *name_str, const char *residue_str)
{
	char *forte_table[] = FORTE_TABLE;
	int i;

	for (i = 0; i < 223; i++) {
		if (estreq(residue_str, forte_table[(i * 2) + 1])) {
			strcpy(name_str, forte_table[(i * 2)]);
			break;
		}
	}
	if (i == 223)
		return 0;
	else
		return 1;
}

int resolve_name(char *residue_str, const char *name_str)
{
	char *forte_table[] = FORTE_TABLE;
	int i;

	for (i = 0; i < 223; i++) {
		if (estreq(name_str, forte_table[(i * 2)])) {
			strcpy(residue_str, forte_table[(i * 2) + 1]);
			break;
		}
	}
	if (i == 223)
		return 0;
	else
		return 1;
}

int pcs_to_name(char *name_str, const int *p, int plen)
{
	char residue_str[MAX_PCS];
	int q[MAX_PCS], qlen = plen, i;

	memcpy(q, p, plen * sizeof(int));
	prime(q, &qlen, NULL);
	for (i = 0; i < qlen; i++) {
		residue_str[i] = i2c(q[i]);
	}
	residue_str[i] = '\0';
	return forte_name(name_str, residue_str);
}

int name_to_pcs(int *p, int *plen, const char *name_str)
{
	char residue_str[13];

	resolve_name(residue_str, name_str);
	printf("residue_str = %s \n", residue_str);
	*plen = 0;
	while (residue_str[*plen] != '\0') {
		p[*plen] = c2i(residue_str[*plen]);
		printf("next char in residue_str = %c \n", residue_str[*plen]);
		(*plen)++;
	}
	return *plen;
}
