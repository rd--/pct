typedef struct {
	int *s, slen; /* initial set */
	int *p, plen; /* permutations */
	int *index; /* indexes */
	int last;
} Pg_t;

Pg_t *pgopen(int *s, int slen);
int pgnext(Pg_t *e, int *s);
void pginit(Pg_t *e, int *s, int slen);
void pgclose(Pg_t *e);
