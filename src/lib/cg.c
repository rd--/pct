#include "cg.h"
#include "emem.h"
#include <assert.h>
#include <stdlib.h>
#include <string.h>

Cg_t *cgopen(int n, int r, int *s)
{
	Cg_t *c;
	int i;

	assert(n >= r);
	c = emalloc(sizeof(Cg_t));
	c->n = n;
	c->r = r;
	c->l = calloc(c->n, sizeof(int));
	assert(c->l);
	for (i = 0; i < r; i++)
		c->l[i] = i; /* init c->l[] counters for all r */
	if (s != NULL) {
		c->s = emalloc(c->n * sizeof(int));
		memcpy(c->s, s, n * sizeof(int));
	} else {
		c->s = NULL;
	}
	return c;
}

int cgnext(Cg_t *c, int *s)
{
	int i;

	if (c->l[0] >= (c->n - (c->r - 1))) {
		return 0;
	} else {
		/* put combination */
		if (c->s != NULL) {
			for (i = 0; i < c->r; i++) {
				s[i] = c->s[c->l[i]];
			}
		} else {
			for (i = 0; i < c->r; i++) {
				s[i] = c->l[i];
			}
		}
		/* increment indexes as required from right to left */
		c->l[c->r - 1]++;
		for (i = c->r - 1; i > 0; i--) {
			if (c->l[i] > c->n - (c->r - i)) {
				c->l[i - 1]++;
				c->l[i] = 0;
			}
		}
		/* reset indexes as required from left to right */
		for (i = 1; i < c->r; i++) {
			if (c->l[i] <= c->l[i - 1]) {
				c->l[i] = c->l[i - 1] + 1;
			}
		}
	}
	return c->r;
}

void cgclose(Cg_t *c)
{
	if (c->s != NULL) {
		free(c->s);
	}
	free(c->l);
	free(c);
}
