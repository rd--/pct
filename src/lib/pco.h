#ifndef _PCO_H
#define _PCO_H

#include "pct.h"

/* values for obj->type */
enum { empty,
	cset,
	pcset,
	pcseg,
	sc,
	pcv,
	iset,
	iseg,
	icset,
	icseg,
	iv,
	icv };

/* comparison values returned by pco_cmp() */
#define PCO_NOT_EQUAL 0
#define PCO_EQUAL 1
#define PCO_SUBSET 2
#define PCO_SUPERSET 3

/* values for opt */
#define PCO_PRINT_TYPE 1

/* general limits */
#define PCO_BUFMAX 64 /*+ limit of object data +*/
#define PCO_TYPMAX 8 /*+ limit of object string descriptor +*/

typedef struct
{
	int type; /*+ type of pc object +*/
	int data[PCO_BUFMAX]; /*+ pco data array +*/
	int size; /*+ number of elements at data +*/
	int opt; /*+ option mask +*/
} Pco_t;

extern int pco_allow_ambigous_mappings;

int pco_sizeof(const Pco_t *a);
int *pco_data(Pco_t *a);
int pco_typeof(const Pco_t *a);
int pco_element(const Pco_t *a, int n);
void pco_set_typeof(Pco_t *a, int b);
void pco_set_sizeof(Pco_t *a, int n);
void pco_set_element(Pco_t *a, int n, int e);
void pco_set_opt(Pco_t *a, int b);
Pco_t *pco_create(const char *type);
void pco_free(Pco_t *p);
int pco_validate(Pco_t *obj);
void pco_copy(Pco_t *dst, const Pco_t *src);
Pco_t *pco_dup(const Pco_t *src);
void pco_swap(Pco_t *a, Pco_t *b);
int pco_read(Pco_t *p, const char *str);
int pco_write(Pco_t *p, char *str);
int pco_write_pretty(Pco_t *p, char *str);
int pco_get(Pco_t *obj, FILE *stream);
int pco_put(Pco_t *obj, FILE *stream);
int pco_put_pretty(Pco_t *obj, FILE *stream);
void pco_tf(Pco_t *obj, int *tf);
int pco_sro_apply(Pco_t *obj, const rrtnmi_t *op);
int pco_sro_derive(const Pco_t *from, const Pco_t *to, rrtnmi_t *op);
int pco_has_element(const Pco_t *obj, int n);
void pco_union(Pco_t *r, const Pco_t *a, const Pco_t *b);
void pco_intersection(Pco_t *r, const Pco_t *a, const Pco_t *b);
int pco_degree_of_intersection(const Pco_t *a, const Pco_t *b);
void pco_complement(Pco_t *r, const Pco_t *a);
void pco_cat(Pco_t *dst, const Pco_t *src);
int pco_map(Pco_t *dst, int type, const Pco_t *src);
int pco_convert(Pco_t *p, int type);
int pco_cmp(const Pco_t *a, const Pco_t *b);
int pco_n_cmp(const Pco_t *a, const Pco_t *b, int n);
int pco_match(const Pco_t *obj, const char *pattern);

#endif
