#include "pg.h"
#include "emem.h"
#include "pct.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

static int are_duplications(const int *p, int plen)
{
	int i, j;
	for (i = 0; i < plen - 1; i++) {
		for (j = i + 1; j < plen; j++) {
			if (p[j] == p[i]) {
				return 1;
			}
		}
	}
	return 0;
}

Pg_t *pgopen(int *s, int slen)
{
	Pg_t *e;

	if (slen < 3)
		die("lib/pg.c : pgopen() : duple sets unsupported");

	e = emalloc(sizeof(Pg_t));

	e->s = emalloc(slen * sizeof(int));
	e->slen = slen;

	e->p = emalloc(slen * sizeof(int));
	e->plen = 0;

	e->index = emalloc(slen * sizeof(int));

	pginit(e, s, slen);

	return e;
}

void pginit(Pg_t *e, int *s, int slen)
{
	int i;

	for (i = 0; i < e->slen; i++) {
		e->index[i] = i;
	}

	memcpy(e->s, s, slen * sizeof(int));
	e->slen = slen;

	memcpy(e->p, s, slen * sizeof(int));
	e->plen = slen;

	e->last = 0;
}

void pgclose(Pg_t *e)
{
	free(e->s);
	free(e->p);
	free(e->index);
	free(e);
}

int pgnext(Pg_t *e, int *s)
{
	int i;

	if (e->last) {
		pginit(e, e->s, e->slen);
		return 0;
	}

	memcpy(s, e->p, e->plen * sizeof(int));

	while (e->index[0] < e->slen) {
		e->index[e->slen - 1]++;
		for (i = e->slen - 1; i > 0; i--) {
			if (e->index[i] > e->slen - 1) {
				e->index[i] = 0;
				e->index[i - 1]++;
			}
		}
		if (!are_duplications(e->index, e->slen)) {
			for (i = 0; i < e->slen; i++) {
				e->p[i] = e->s[e->index[i]];
			}
			e->plen = e->slen;
			return e->slen;
		}
	}

	e->last = 1;
	return e->slen;
}
