pct - software for studying abstract pitch-class structure - 0.2\
Rohan Drape\
1999-2001

__pct__ is a software package for studying abstract and interpreted
pre-compositional pitch-class structures that runs on most unix-like
systems.  The design promotes simple open interfaces and an extensible
architecture, resulting in a set of co-operative and reusable software
tools.  pct is [free software](http://www.gnu.org).

# Introduction

pct is a software package for theorists, analysts, and composers
studying traditional modulo-twelve pitch-class structures, and draws
on theoretical work by Forte (Forte1973a), Babbitt (Babbitt1960a,
Babbitt1961a), Lewin (Lewin1987a, Lewin1993a), Morris
(Morris1987a, Morris1991a), and others.  The host environment must
provide an ISO compliant C compiler (Kernighan1988a, ANSI1989b,
ISO1990) and a Bourne-compatible shell (Bourne1978a); typically the
host is a unix-like (Kernighan1978a, Pike1984a, Ritchie1984a)
operating system.  The installation and configuration scripts provided
require the program `make` (Feldman1979a) or a compatible derivative
(Stallman1997a).

The open and user-extensible design of pct allows complex compositional
and theoretical questions to be studied in a practical way.  Arbitrary
sets of musical objects are processed by arbitrary arrangements of
musical rules.  This section describes appropriate forms for stating
musical questions within this context.

The timing information presented gives values for a pre-release version
of pct compiled with `gcc` (v.2.7.2.3) under GNU/Linux (v.2.0.34)
running on a lightly loaded Intel-Pentium-II-266 computer.

# Directory structure

Throughout this manual files are named relative to the base `pct`
directory.

- `lib` holds user-level run-time libraries.  `lib/scs` is a list of
the 223 set-classes.  `lib/scdb` is the database for the `scdb`
program.  `lib/sgdb` is the database for the `sgdb` program.
`lib/univ` is a list of all possible pitch-class sets.

- `src` holds the program source-code.  `src/cmd` has the C sources
for the base pct commands.  `src/lib` has the C library source.
`src/sh` has executable bourne-shell scripts, `sh-f` has bourne-shell
function definitions that are loaded using the `source` shell command.

- `doc` holds the documentation.
- `bin` holds executable files accessed through the `pct` command.

# Design Principles

pct is not a single large application but a collection of software tools
(Kernighan1976a) designed to work together cooperatively.  pct
programs are unix filters (Kernighan1984a); they read and write
ASCII text streams, and communicate through half-duplex pipes and named
files.  pct is provided as a source-code archive.  This section notes
some implications of these design principles.

pct presents three types of services to users, each offering a different
balance between flexibility and generality on the one hand, and
simplicity and end-user orientation on the other.  The first services
are called _end-user applications_, and have simple and inflexible
interfaces.  An example of this type of service is the
_set-information_ command (`si`), which writes a formatted list
of information about a pitch-class set.

The second services are open-ended software tools, designed to be used
cooperatively, and to be accessed through a high-level
_action-language_, typically provided by a shell.  These tools make
up the largest part of pct, and are the focus of this presentation.

The third service is a collection of re-usable ISO-C source-code
libraries.  These libraries provide functions to store and manipulate
pitch-class objects and transformations (including partitioned
pitch-class arrays and serial-operators), and to translate between
machine and ASCII-text representations of these.

# Documentation

This manual includes a reference section that contains detailed
information about individual commands and is usually read _on-line_.
The manual is written using the `texinfo` documentation system.  For
users with a html viewer a version can be read by visiting the file
`doc/pct.html`.

The layout of the reference section of the manual follows the standard
unix online documentation system; most fields are self explanatory.
The __SYNTAX__ section lists the command-line arguments where optional
arguments are shown between brackets `[]` and an ellipsis `...`
indicates that an arbitrary number of arguments of the previously
specified type can be given.  The __EXAMPLE__ section typically gives
a short shell transcript.  The __NOTES__ provide additional
information, including current limitations, known bugs, and unusual
behaviors.

# Definitions

pct processes act on three types of objects: pitch-class-objects,
pitch-class-object-relations, and pitch-class-object-transformations.

A pitch-class object is a string of modulo-twelve residues.  To allow
residues to be catenated without using a delimiter each residue is
represented by a single character; ten by `t` or `A`, and eleven
by `e` or `B`.  An object is terminated by a non-residue
character.  Rules operate on arbitrary strings of residues, or require
objects to be named.

The names of pitch-class objects have been taken where possible from the
standard non-tonal theoretical literature, and are generally
self-explanatory; a set is typically a proper set, but in some contexts
is a multi-set, a segment is an ordered set.  The object-type names are
listed in the table below.

~~~~
----- ----------------------
pc    pitch-class
pcset pitch-class set
pcseg pitch-class segment
sc    set-class
iset  interval set
iseg  interval segment
icset interval-class set
icseg interval-class segment
icv   interval-class vector
pcv   pitch-class vector
cset  cardinality set
----- ----------------------
~~~~

At any point where a residue string would be read a _Forte-name_ may
be used as a shorthand for the prime-form of a set-class.  The symbol
recognized has the form `x-Zy`; where `x` and `y` are integers, and
the character `Z` is _required_ for the `Z`-related set-classes.

The following characters are discarded silently and may be used as a
form of self-documentation: `ic,{[<`.  The suggested convention is to
write unordered sets between braces, ordered set between
angled-brackets, and vectors between brackets; thus `{013}` is a
pitch-class set and `i<47>` an interval segment.

Since most pct programs are line-oriented, and expect only a single
pitch-class object on each line, comments can be included with
pitch-class objects.  By convention these comments follow a semi-colon
character (`;`) for future portability.

The pitch-class transformations are restricted to twelve-tone and serial
operators, and the pitch-class relations to set-theoretical inclusion
relations.  A serial operator is a concatenation of the rotation,
retrograde, transposition, multiplication, and inversion operations, and
acts on ordered sets.  The text token representing a serial operation
has the form `rxRTnMI`, where `x` is any integer and `n`
any modulo-twelve residue.  `T` is the only required operator.  A
twelve-tone relation acts on unordered sets, and has the form
`TnMI`.

The supported pitch-class-object-relations are given in the table
below, note that the some tokens are used to represent two relations
since the intention can be derived from context; for example `{013} in
3-11` indicates that the pitch-class set `013` is an element of the
set-class `3-11`, whereas `{013} in {0134}` indicates that the same
pitch-class set is a subset of the pitch-class set `0134`.

~~~~
----- --------------------------------------------------
is    is equal to
isnt  is not equal to
in    is an element of, is a subset of
notin is not an element of, is not a subset of
has   includes the element, is a superset of
hasnt does not include the element, is not a superset of
----- --------------------------------------------------
~~~~

# Rules

There is a general model for interaction with pct.  An initial set of
pitch-class objects `S` is transformed by a set of rules `R` into a
terminal set of pitch-class objects `T`; this is written `R(S)-->T`.
A pct command typically implements a rule-class `R` and provides a
simple interface for selecting a rule `r` from `R`, or a set of rules
`r0,r1,...,rN` from `R`.  Objects `s` in `S` are read from the
standard input stream `stdin`, and objects `t` in `T` are written to
the standard output stream `stdout`.  Since `S` and `T` are both sets
of pitch-class objects the set of rules `R` is a simple chain of the
rules `r` in `R`, written `r0(r1(rN(S)))-->T`.  This model requires
the user to adopt a _transformational_ approach to object relations
(Lewin1987a).

# Serial Operators

The _serial-operations_ command (`sro`) applies an arbitrary
serial-operation to a pitch-class object or set of pitch-class
objects, and provides a succinct example of a typical pct program.
The notation of an individual serial-operation is as described
earlier, following the usual left orthography whereby the object is
placed at the extreme right and operations are performed from right to
left.  However, when two instances of the `sro` command are catenated,
with the operations `X` and `Y`, the result is better expressed by the
right orthography `((obj)Y)X`.  As an example consider the expression
`T4(I(156))=((156)I)T4=3BA`, and the shell dialogue:

~~~~
$ echo 156 | pct sro T4I
3BA
$ echo 156 | pct sro T0I | pct sro T4
3BA
$ echo 156 | pct sro T4  | pct sro T0I
732
$ pct rsg 156 3BA
T4I
$
~~~~

The first example shows the correct left orthography, the second the
correct right orthography, and the third an unfortunate confusion of
the two.  The fourth example shows the _relate-segments_ command used
to derives the simplest serial operation that holds between two
pitch-class segments; `sro` and `rsg` are associated as complementary
or _reverse of_ programs.

# Simple Translations

Rules that transform pitch-class objects of one type into objects of
another type are called _translations_.  A translation is called
_simple_ if each input object has a unique output object into which it
can be transformed.  Translation of a set-class to an interval-class
vector is simple, the reverse is not.  A sequence of simple
translations is called an _indirect_ translation; indirect
translations are simple in all but one case.  A graph shows both the
simple and, through recursion, the indirect translations provided by
the _pitch-class-object-translation_ command `pcom`.

![](sw/pct/svg/simple-translations.svg)

An example demonstrates the only non-simple indirect translation, a
pitch-class segment to an interval-class set.

~~~~
$ pct pcom pcseg iseg 01549 | pct pcom iseg icseg | pct pcom icseg icset
145
$ pct pcom pcseg pcset 01549 | pct pcom pcset sc | pct pcom sc icv | pct pcom icv icset
1345
$ pct pcom pcseg icset 01549
pco.c: 430: pco_map(): ambiguous mapping
pco.c: 529: pco_map(): invalid mapping requested
$
~~~~

The first sequence follows the right hand side of the graph and writes
the interval-classes that are given by the direct interval-succession
of the pitch-class segment; the second sequence follows the left hand
side and writes all of the interval-classes spanned, including those
between non-adjacent elements of the segment. The third shows that the
program will refuse to make ambiguous mappings; there is a variable
that can alter this behaviour.

# Basic Interval Patterns

This section outlines a model for general query construction in pct.
The examples relate to a widely available article published by Allen
Forte (Forte1973a), and indicate the role of the shell as an action
language for structuring pct processes, and the close interaction
between pct commands and the standard utilities of the host
environment.  Page numbers in this section refer to the Forte article.

The _basic interval pattern_ (pp.235-236) of a pitch-class segment
lists the interval-classes spanned by the successive elements of the
segment in ascending order, and can be calculated by a simple sequence
of three pct commands: `pcom pcseg iseg | pcom iseg icseg | nrm -r`.
This sequence is encapsulated in a shell function to simplify later
examples.

~~~~
$ function bip { pct pcom pcseg iseg $@ | pct pcom iseg icseg | pct nrm -r }
$ pct bip 0t95728e3416
11223344556
$
~~~~

The _normalise_ command (`nrm`) is used to sort the interval-class
segment into ascending order without discarding any duplicated
interval-classes (which is _not_ what `pcom icseg icset` would do).

Typing `pg 5-Z37 | bip | rdl -v | sort` writes the information
given by Forte in the second column of Example 3 (p.240).  In this
sequence the input to `bip` is a list of the permutations of the
prime form of the set-class 5-Z37; the output is first processed to
indicate the number of permutations that resolved to each unique
interval pattern, and then sorted into ascending order.  The dialogue
below finds and counts the patterns common to the set-classes 5-Z17 and
5-Z37, which are shown by `*`'s in Forte's table.

~~~~
$ pct pg 5-Z17 | pct bip | sort -u > 5-Z17.bip ; \
  pct pg 5-Z37 | pct bip | sort -u > 5-Z37.bip ; \
  comm 5-Z17.bip 5-Z37.bip -1 -2 | wc -l
16
$
~~~~

To write the basic interval patterns for a set of set-classes, each
set-class must be presented to the command sequence separately.
Consider writing the list of patterns for all set-classes of a
particular size.  The script below shows a solution.

~~~~
$ cat ../db.sh
for sc in $(pct fl -c $1)
do
  pct pg $sc | pct bip | sort -u > $sc
done
$ sh ../db.sh 4
$ ls
4-1   4-12  4-16  4-19  4-21  4-24  4-27  4-4   4-7   4-Z15
4-10  4-13  4-17  4-2   4-22  4-25  4-28  4-5   4-8   4-Z29
4-11  4-14  4-18  4-20  4-23  4-26  4-3   4-6   4-9
$
~~~~

The requested size is read from the command line; the appropriate set of
set-classes is written by the _forte-list_ command (`fl`); a
loop passes the set-class names (using the variable `sc` to the
command sequence one at a time; and the sequence writes the patterns to
a named file in the current directory.

The output of this script, when run from an empty directory, can be
interpreted as a type of database, on which simple queries can be
expressed using the standard text processing utilities.  A _view_ of
this database is given by Forte in his Example 10 (pp.248-249).  A
script to construct a related view, sorted instead by interval
pattern, is given below.  The elegance of this example results
primarily from a sensitive use of the file-system name space.

~~~~
$ cat view.sh
for i in $(pct fl -c $1 | pct pg | pct bip | sort -u)
do
  echo $i":" $(grep -l $i * | sort -t '-' +1  -n | tr "\n" " ")
done
$ sh view.sh 4
111: 4-1
112: 4-1 4-2 4-3
113: 4-1 4-3 4-4 4-7
...
$
~~~~

The open and cooperative design demonstrated by these examples does
not lead to prohibitive execution times.  The database construction
(db.sh) took 1.24 seconds, and the view construction (view.sh) 1.25
seconds.

# Evaluating Pitch-Class Object Relations

The _evaluate-relations_ command (`epmq`) asserts inclusion relations.
Pitch-class segments at `stdin` are evaluated against a set of
conditions.  A condition is a triple (_relation_, _type_, _object-set_)
abbreviated `(r,t,O)`; the elements `o` in `O` are of type `t`.
Values for `r` and `t` were given above.

Given a pitch-class segment `x`, a condition is met if any element `o`
in `O` stands in the relation `r` to `x` as translated into an object
of type `t`.  Translations are as discussed earlier.  A set of
conditions `S` is met if `x` meets all of the conditions `s` in `S`.
This distinction (of _any_ `o` in `O` and _all_ `s` in `S`) allows
simple and-or relations to be asserted.

The file `lib/scs` lists the two-hundred and twenty-three set-classes;
the file `lib/univ` lists the four-thousand and ninety-five
pitch-class sets.  The first example below writes the seven and eight
note scales that do not have a repeated semi-tone; the results are
well known.  The _cycle_ command (`cyc`) extends a pitch-class object
by its first element; the _set-class-database_ command (`scdb`) prints
information from a user-level database.  The second example writes the
hexachordal subsets of the F-major collection that include an instance
of the complement of the diatonic collection, cannot span a tritone,
and are not in the C-major collection.

~~~~
$ pct cyc <  ~/sw/pct/share/scs | \
  pct epmq "in cset 89" "is icset 12" "hasnt icseg 11" | \
  pct scdb
7-34    ascending melodic minor collection
7-35    diatonic collection (d)
8-28    octotonic collection (Messiaen Mode II)
$ pct epmq < ~/sw/pct/share/univ "in cset 6" "in pcset 579t024" \
  "has sc 5-35" "hasnt sc 2-6" "notin pcset 024579e"
02579A
$
~~~~

Approached thoughtfully `epmq` can be used to elucidate
constraints operating in a wide variety of situations.  Elapsed times
for the two examples were 0.05 and 0.04 seconds.

# Object Relations

The _object relations_ command (`orl`) enumerates inclusion relations
between members of a set of pitch-class objects at `stdin`.  Objects
of types _pcset_, _pcseg_, and _sc_ are considered as _begin_ objects;
all objects types are considered as _end_ objects.  The relations
`is`, `in` and `has` are written from begin objects to end objects.
Serial and twelve-tone operators are written between objects of types
`pcseg` and ` pcset`.  The relations `isnt`, `notin`, and `hasnt` are
supressed.  The number of relations written is equal to the product of
the begin objects and the end objects minus the supressed relations.

The network of relations made by this process is best studied by
drawing a graph.  The _graph-object-relations_ command (`gorl`) will
write graph description files for the graph-drawing program `dot`
(Koutsofios1991b).

# Pitch-Class Arrays

pct commands that begin with the letter `a` read and write
sets of pitch-class arrays on text streams.  Columns are delimited by
a colon character (`:`), and rows by a new line character; an
array is terminated by an empty line.  The array processing facilities
of pct are discussed in detail in ??.

# Predicate Programs

A predicate program is used within a shell script to determine the
truth of a statement, the system program `test` is a familiar example.
The command `pvl -e` is a predicate program that determines if a
constrained voice leading exists between two set-classes.  The
dialogue below shows a script that will write the trichordal
set-classes that can reach one another where voice leading is
constrained to interval-class one, and the union of the two trichords
is constrained to be a hexachord.

~~~~
$ cat > ex.sh
(for a in $(pct fl 3)
do
  for b in $(pct fl 3)
  do
      if test $a = $b
      then
          break
      fi
      if pct pvl -e -c1 -s6 $a $b
      then
          echo $b $a
      fi
  done
done) | pct fsrt
Ctl-D
$ sh ex.sh | wc -l
  12
$
~~~~

# Complex Translations

Simple translations were discussed earlier.  The complex translations
are one-to-many and are implemented by a collection of programs.  A
graph shows the set of all complex translations and the names of the
pct commands that implement each complex translation.

![](sw/pct/svg/complex-translations.svg)

An unordered set with duplicate elements is called a multi-set.
Multi-sets are not addressed by this graph.  Simple translations of
ordered sets to unordered sets remove duplicate elements before
sorting.  The set of multi-sets that can be derived from a set is
written by the _set-expansion_ command (`se`).  Combining this
program with the translations as shown above allows for any complex
translation to be determined.

The example below counts the hexachordal set-classes that can be
formed by permutations of the five element multi-sets of the
interval-class set `1245`.  The set of permutations (the output
of the `ici` program) has fifteen-thousand three-hundred and sixty
members; the total elapsed time for the query is 1.14 seconds.

~~~~
$ pct se -c5 1245 | pct pg | pct ici | pct pcom iseg sc | \
  sort -u | pct epmq "in cset 6" | wc -l
42
$
~~~~

A detailed discussion of the musical aspects of the complex translation
of interval-objects to pitch-class objects is given in ??.

# Pattern Matching

A regular expression is a text string that defines a pattern which other
strings either match or do not match.  The system programs `grep`
and `sed` (McMahon1978a), among many others, provide mechanisms
for filtering text streams by associating sets of regular expressions
with a set of editing operations.  The example below shows the four and
five element set-classes that allow the possibility of spanning exactly
one instance each of the interval classes one, four and six, and either
two or three instances of the interval-class five.

~~~~
$ pct fl -v 45 | grep "<.1..1[2-3]1>"
4-16[0157]        <4110121>         <322002222010>
5-25[02358]       <5123121>         <122224124032>
5-29[01368]       <5122131>         <223230322402>
$
~~~~

pct offers some direct support for pattern matching when compiled for
POSIX systems, the above example could be made by typing `icv -r
.1..1[2-3]1}.

# Shell Functions

There are two sets of shell functions, the first are defined in the file
`src/sh/use-hst.sh` and provide backwards compatibility for old
scripts.  The second set are defined in the file
`src/sh/use-pct.sh` and provide long descriptive names for the
basic set of pct commands as well as for various small command
groupings.  These names are designed so that the program name, with
arguments, can be read as a proper, ie. grammatical, description of what
will be written.  Typing the long names is not generally a problem as
most modern shells provide a name-completion facility.  This file is a
good place to start learning the system as it shows functional grouping
of commands and includes useful well-named program variants.
Contributions are welcome.  The long function names are not POSIX
compliant, but should be acceptable to a large number of shells.  To
make these commands accessible type `. use-hst` or
`. use-pct`.

# Source Libraries

pct is distributed as a source-code archive.  This means not only
that existing commands can be re-written to meet particular
requirements, but also that a source-library `libpct` is available
to simplify writing new commands.  The public interface is divided
into conceptually independent parts, each individually documented (see
`doc/3/`).  This section gives three simple examples to
illustrate the design method followed throughout.

`libpco` provides services for reading, writing and transforming
pitch-class objects.  A pitch-class object is stored as a modulo-twelve
residue-string (an integer array) and a type name (a character array).
An annotated version of the source-code of the `sro` command is
given below.

~~~~
int main (int argc, char **argv)
{
  Pco_t *p;                    /* pitch-class object */
  Sro_t  o;                    /* serial operation */
  sro_read(argv[1],&o);        /* read serial-operator */
  p=pco_open("pcseg");         /* allocate memory */
  while(pco_get(p,stdin)) {    /* read a pco from stdin */
    pco_sro(p,o);              /* transform object */
    pco_put(p,stdout);         /* write to stdout */
  }
  pco_close(p);                /* free memory */
  return EXIT_SUCCESS;
}
~~~~

`libcg` and `libpg` provide support for deriving combinations and
permutations respectively of integer arrays (typically modulo-twelve
residue strings).  The example below, taken from the command `pg`,
prints the permutations of a pitch-class object to `stdout`.

~~~~
void print_object_permutations(Pco_t *obj)
{
  Pg_t *a=pgopen(obj->data,obj->size);    /* initialize */
  while(obj->size=pgnext(a,obj->data)) {  /* fetch next permutation */
    pco_put(obj,stdout);                  /* write to stdout */
  }
  pgclose(a);                             /* free memory */
}
~~~~

`libppca` provides services for reading, writing and transforming
arbitrarily partitioned pitch-class arrays.  An array is stored as an
ordered set of residue strings (a three dimensional integer array), a
partition map (also a three dimensional integer array), and a name (a
character array).  The partition map stores the number of elements in
each position.  An annotated version of the source-code of the
`asro` command is given.

~~~~
int main(int argc,char **argv)
{
  Ppca_t *e;                 /* pitch-class array */
  Sro_t   o;                 /* serial operator */
  strsro(argv[1],&o);        /* read a serial operation */
  e=paopen("asro");          /* allocate memory */
  while(paread(e,stdin)) {   /* read input arrays */
      pasro(e,o);            /* transform the array */
      pawrite(e,stdout);     /* write the transformed array */
  }
  paclose(e);                /* free memory */
  return EXIT_SUCCESS;
}
~~~~

Other sub-libraries are:

- libepm: evaluates inclusion relations for pitch-class segments, see `src/cmd/epmq.c`

- libftbl: determines and resolves the standard set-class names, see `src/lib/pct.c`

- libsro: provides services to store and manipulate serial operators, see `src/cmd/rsg.c`

- libtr: fetches the serial transformations of a residue-string in sequence, see `pct/src/cmd/trl.c`

# Status and Future Work

The base command set of the current release is stable and well tested.
There are test scripts included with the system, go to `src/t` and
type `make`, this will run the scripts `t.sh` and `p.sh` and write the
files `T.OUTPUT`, `T.RESULTS`, and `P.OUTPUT`.

The shell functions should be cleaned up and properly worked into the
documentation.  The most useful work would be to clean up all of the
attribute-value specifications and the implementation of proper
boolean logic into all programs implementing attribute-value selection
rules.  For pedagogical purposes it may be useful to provide simple
graphical interfaces to some aspects of pct.  Such interfaces should
be not less portable than the base programs, and should not require
any alteration to the existing source.  Writing simple Tcl-Tk
(Ousterhout1994a) scripts that assemble command sequences for
execution by a shell and arrange to display the results is a possible
approach.

# Related Work

The `mod12` program (Demske1996a) is useful for work involving
unordered pitch-class sets, but does not support processing of ordered
input.  The internal macro language provides fast execution and
formulation times for a wide variety of problems.  pct does not
duplicate this effort to any great degree, the two are in a sense
complementary.  The design of `mod12` is not open, but is
plain-text based, and is fully functional under popular commercial
operating systems.  The program provides explicit support for
structured use in analytical situations and a lengthy and well written
manual is part of the distribution.  Source code is not available.

# Installation guide

This installation guide is intended for musicians who want to install
pct on a unix-like system, and who are not sure how to go about it.  It
is assumed that the reader has an account on a unix-like machine, has a
basic grasp of a shell (command interpreter), and is willing to read the
online documentation as required (type `man man` to start).  The
various user-level programs mentioned below should exist on most
unix-like systems, and all are available as part of a standard GNU
system (see <http://www.gnu.org>).  If any of these programs are
missing there may be a local alternative (ask your
system-administrator), or you can get a copy and install it.

## Unpacking the archive

The file `pct-V.R.tar.gz` is an archive of the source code of pct,
version `V`, revision `R`.  You should check that you have the most
recent release by visiting the pct distribution site (see the `README`
for the address).  Once you have the proper archive you need to unpack
it.  First determine where you want the base pct directory to go and
move the archive there.  Often `~/opt` is a good place.  To extract
the archive type `tar zxf pct-V.R.tar.gz` if you have the GNU tar
program, or `gzip -dc pct-V.R.tar.gz | tar xf -`.  You can then remove
the archive with `rm pct-V.R.tar.gz`.

You will now have created a directory called `pct-V.R`, which in
turn has a number of subdirectories.  `pct-V.R` is called the
base-directory of the system, pct associated files are usually named in
relation to this directory (ie.  the file `README` is found at
`pct-V.R/README`).

## Compilation

pct is distributed as source code only, and users run the local compiler
to make executable files.  The compilation/installation process is
managed by the program `make`.  Before compiling the system you may
need to edit the configuration file `src/Config.mk`.  This is a
plain text file that has some simple information about the local
environment needed during the installation process.  The information
falls into three catgeories.

## Location variables

The `PREFIX` variable must point to the base pct directory.  If you
unpacked the archive in `~/src` (as recommended above) then you
won't have to change it.  The `INSTALL_DIR` variable tells
`make` where to install the executable files.  The simplest thing
to do is make sure you have directories called `~/bin` and
`~/lib` (do this by typing `mkdir ~/bin ~/lib`) and leave the
`src/Config.mk` script as it is.  You will want the `BIN_DIR`
directory to be in your `PATH` environment variable, which allows
the shell to locate programs in order to execute them.  The `PATH`
variable is set differently for different shells so check the relevant
documentation; if you don't know what shell you are running type
`finger my-user-name` and that should tell you.

## Library variable

If you get an error during compilation, try changing the value of the
`LIBRARY` variable in `src/Config.mk` to `libpct.a`.  The will result
in a larger installation, but is the only option that will work for
some systems.

## Compiler variables

I use the GNU compiler and associated tools.  If your system doesn't
have these you will have to adjust the compiler variables.  Doing this
is probably straight-forwards but requires knowledge of your local
system.  You can try fixing them by reading `man cc`, if that fails
ask your system-administrator.

After saving any changes to `src/Config.mk` you can type `make -s
install` from the directory `src` to build the base system.  When you
wish to clean the system, that is to remove all binaries and
object-libraries, you can type `make uninstall` from the `src`
directory.

To build an extended system type `make install-further`, to
clean this type `make uninstall-further`.  Only do this if you
actually need the extensions, they are not well-supported.

While there are no name conflicts between pct and my local system, it is
possible this may occur on your system.  A name conflict occurs where
two or more commands share the same relative name.  Name conflicts are
resolved by the order of the directories given in the PATH variable, the
first directory is searched first, and so on.

## If things don't work

- Reread this file.

- Ensure that the `PREFIX` variable in `src/Config.mk` is correct.  It
must start with the character `/`.  If you are unsure of the correct
value type `/bin/pwd` from the base pct directory.  Also make sure
that the `BIN_DIR` and `LIB_DIR` variables point to a directory that
exists.

- Ask someone local if they can help.

- Send me a note.  Include your system information and a transcript
showing what you did and the error messages you got, and I'll do what
I can.

## And if they do

pct commands read and write human readable plain ascii text, so it helps
to become familiar with a text editor.  In general working with pct is
much easier and more productive with a windowing system and a good text
editor (ie.  X, twm and emacs).

## Other platforms

The ISO-C commands will compile and operate under many other operating
systems, however if there is no `make` for your system you will
have to write your own configuration and installation scripts.
Contributions are welcome.

# Reference Manuals

Program names are mnemonics (acronyms and/or abbreviations).  All
array-processing commands begin with the letter `a`, and all graph
generating commands begin with the letter `g`.  Commands can be
renamed using shell aliases; this is generally better than renaming
binaries or editing makefiles.

Options are not parsed using `getopt`. Options must be
specified separately (ie. `-a -b` not `-ab`) and arguments to options
must be catenated (ie. `-a1` not `-a 1`).

# References

For a more complete bibliography see ??.
