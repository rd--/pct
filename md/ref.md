pct - reference manual - 0.2\
Rohan Drape\
1999-2001

# aap - array, add partitioning

## SYNTAX

aap [ -sN ]

## PURPOSE

Write partition markers into pcsets.

## DESCRIPTION

The `aap` command reads unpartitioned pcsegs and writes partition
markers every _n_ positions, where _n_ is set by `-s` (default n=1).
`aap` makes even partition arrays from sets of pcsegs.

## EXAMPLE

~~~~
$ pct aap 2
0123
4567
Ctl-d
01:23
45:67
Ctl-d
$
~~~~

## NOTES

The reverse of this operation is given by `sed s/://g`.

# ac - array list count

## SYNTAX

ac

## PURPOSE

Count arrays.

## DESCRIPTION

`ac` counts the number of arrays at stdin.

## EXAMPLE

~~~~
$ pct ac < array_set
16
$
~~~~

# adms - array dimensions

## SYNTAX

adms [ -r | -c | -p ]

## PURPOSE

Write the dimensions of an array.

## DESCRIPTION

`adms` writes the dimensions of arrays at stdin to stdout.  The format
is three decimal integers giving the number of rows, columns, and pcs
respectively.  Options will write only one value, `-r` for rows, `-c`
for columns, and `-p` for pcs.

## EXAMPLE

~~~~
$ pct adms
0:1:34
2:56:7
8: :9
Ctl-d
3 3 10
Ctl-d
$
~~~~

# aepm - array evaluate pitch materials

## SYNTAX

aepm [ -a ] [ -f file ] [ Condition ] ...

## PURPOSE

Extract arrays with row and column inclusion relation attributes.

## DESCRIPTION

`aepm` acts as a file filter, writing to stdout only those arrays at
stdin that reflect conditions specified.  Conditions may be given as
quoted strings on the command line, or in files specified by the `-f`
option.  The `-a` option allows ambigous mappings.  This commmand is
related to the command ??.  Conditions are specified in an extension
of the format given at ??.  The extension is a single token to specify
what part of the array is to be evaluated, and must come at the start
of each line.  The token has two elements, a character followed by a
decimal number.  The character is either _r_ for row, or _c_ for
column.  The number gives the row or column referenced (numbered from
zero).  If this number is negative then the condition is applied to
__all__ rows or columns.  Any whitespace preceding this token is
ignored.  Inclusion is __not__ proper.  Rows and columns for which no
conditions are set are automatically accepted.  This is implemented
through a default condition of `in sc 12-1`.  Lines begining with a
hash character are ignored.  The example below fetches arrays where
the first column is in the diatonic collection, and where all rows are
in the pcset _0357t_.

## EXAMPLE

~~~~
$ cat aepm_condition_set
c0 in sc 7-35
r-1 in pcset 0357t
$
~~~~

## NOTES

Simple extension to allow positions to be examined.

# aext - array extend

## PURPOSE

Extend an array by one column.

## SYNTAX

aext

## DESCRIPTION

`aext` reads arrays at stdin, and extends each by one column.  Each
array is extended by all possible permutations of all possible
combinations of pcs, and therefore gives _12Cr * rPr_ arrays, where
_r_ is the number of rows in the array.

## NOTES

Errors occur if a set has less than three elements.  This problem lies
in the `pct/lib/pg.c` library.

# afmt - format partitioned array

## PURPOSE

Read an array description file and write formatted text output.

## SYNTAX

afmt [ file ...  ]

## DESCRIPTION

`afmt` processes an input file, reading array descriptions and writing
formatted text arrays to stdout.  If no file names are given standard
input is read.  An array description begins with a line containing
only the string `{ array` and ends with a line containing only the
string `}`.  The first line of the description contains T0P.  The
remainder of the description breaks into blocks of lines that describe
the partitioning of each block of the array.  Blocks are separated by
empty lines.  Each line within a block is made up of two words.  The
first gives the transformation (sro) of _P_, the second the number of
elements within each partition.  The string `RT3IP 430401` indicates
that this row is part of a six partition array, and that it
contributes four pcs to the first partition, three to the second, and
so on.

## EXAMPLE

~~~~
$ pct afmt
{ array
024579

T0P  231
T5IP 132

RT3P  141
RT9IP 303
}
Ctl-d
02:457:9
5 :310:A8

0  :A875:3
024:    :579
$
~~~~

# aha - array harmonic attributes

## SYNTAX

aha "attr = value" ...

## PURPOSE

Extract arrays with an arbitrary set of attributes.

## DESCRIPTION

aha filters arrays at stdin according to a set of attribute-value
pairs.  `pc-transference: n` is the number of pitch-classes shared by
successive harmonies, `e` is the number of exceptions allowed.
`pc-distribution:` the vector gives the number of occurences of each
pitch-class, a `.` can represent any number.  `harmonic-diversity:`
the set gives allowable numbers of unique harmonies in the array.
`harmonic-size;` the set gives the allowable sizes for harmonies,
`{34}` gives trichords and tetrachords.  This is not the same as `aepm
"c-1 in cset 34"` since that counts the number of pitch-classes, not
the cardinality of the set-class formed.  `harmonic-content:` the
value of `relation` may be `in-sc`, `is-sc`, `is-pcset`, `is-pcseg`.
To evaluate as true at at least one harmony in the array must stand in
relation `relation` to at least one `pc-object.`

## IMPLEMENTATION NOTES

Each attribute type is implemented through an `init` function, an
`evaluate` function, and a stored-data structure; there is no clean-up
function, so dynamically allocated memory will not be freed.  A symbol
table associates each attribute name with the functions that support
it, to add an attribute write the required functions and extend the
symbol table.  The `init` function is passed the `value` string and
returns a pointer to a data structure (of type `void*`).  The `evaluate`
function is passed an array `e` and a pointer to a data structure
allocated by a call to the corresponding `init` function (again of
type void* so it will have to be explicitly typed for access) and
returns 1 if the array evaluates as true and 0 if not.  Each command
line argument generates an entry in a `attribute` `table` by calling
the appropriate `init` function and associating the pointer returned
with the appropriate `evaluate` function.  The order attributes are
registered determines the order of evaluation, evaluation halts for an
array as soon as an attribute evaluates as untrue.  The `init` -
`evaluate` division avoids the `value` string being parsed for each
array evaluated, which is otherwise a significant execution time
problem.

## NOTES

The splitting of the attribute and value string is very fragile.

# airr - array inter-row relations

## PURPOSE

Examine intervalic relations between rows of an array.

## SYNTAX

airr { q [ -rN ] } | { a [ -cN ] -f file }

- `q` query mode
- `-r` set the row pair to be examined (def.  all combinations)
- `a` assert mode
- `-c` set columns to be tested (def.  all columns)
- `-f` set assertion file

## DESCRIPTION

The `airr` command examines intervalic relations between rows of an
array.  There are two modes of operation.

The first `q`, query mode, writes information about the intervallic
relations of each array at stdin to stdout.

The second `a`, assertion mode, filters the arrays at stdin according
to a set of conditions regarding intervals occurring between rows.

In query mode each array at stdin is examined and the interval
relations between positions in voices are written.  An ordered duple
`<xy>` can be given `-r` by default all valid duples are examined.
For example, to see the intervals from the positions of the first row
to the positions of the third row write `-r02`.  Since `x` and `y` are
modulo-twelve residues arrays of more than twelve rows are not
supported.  The number of intervals written for each column is the
product of the number of residues in each position.  Columns are
separated by whitespace.  The first example below is of airr in
query-mode.

In assertion mode `-a` a set of rules read from a file `-f` specify
allowable intervals for row inter-relations.  Lines starting with a
hash symbol are ignored.  A rule has the form `rxy` `I` where `r` is a
character constant, `x` and `y` are modulo-twelve residues, and `I` is
an interval-set.  The command line option `-c` gives a set of integers
`N`, the default set has all `n` where `0 <= n c(e)`.  A rule is met
if for all `n` in `N` the following is true: for all residues `s` in
the position `(x,n)` and all residues `t` in the position `(y,n)` the
interval `t-s` is in the interval-set `I`.  Note that `I` is not an
icset, so to specify that only interval classes three and four can
occur between the second and third voices write `r12 3948`; also `r12
34` is equivalent to `r21 89`.  Any number of rules may be given, each
is evaluated in turn.  There is no requirement to set rules against
every row.  The second example below shows `airr` in assertion-mode.

## EXAMPLE

~~~~
$ pct airr q -r13
0:B:8:4:3:7
0:1:4:8:9:5
0:8:9:1:4:5
0:4:3:e:8:7
Ctl-d
0 3 B 3 B 2
Ctl-d
$ airr a -frule_file < array_set
...
$
~~~~

## NOTES

Should read assertions from command line if no `-f` is set.

# apg - array permutations generator

## PURPOSE

Generate permutations of an array.

## SYNTAX

apg

## DESCRIPTION

`apg` constructs permutations of arrays at stdin.

The rows of each array are written in all permutations of all
permutations.

The number of arrays produced by this command will depend on the number
of rows in the input array, and on the number of pitch-classes in each row
(which need not be equal).

In the simple case of an array with six rows and three columns the number
of output arrays will be given by _3P3 ^ 6 = 46656_.

## EXAMPLE

~~~~
$ pct apg | pct ac
1:2:3:4
1:2:3:4
1:2:3:4
Ctl-d
Ctl-d
13824
$
~~~~

## NOTES

1.  Errors occur if a row has less than three elements.
This problem lies in the `pct/lib/pg.c` library.

# arcs - array row column swap

## SYNTAX

arcs [ -r ] [ file ]

- -r reverse operation.

## PURPOSE

Swap the rows and columns of arrays.

## DESCRIPTION

arcs writes to stdout the arrays at stdin, but with
the columns and rows in swapped positions.
Columns are ordered from bottom to top (low to high).
The -r option does the reverse procedure (ie.  an array
transformed by arcs will be returned to its original
state by arcs -r).
The ordering of elements within positions is maintained
in the -r operation.

## EXAMPLE

~~~~
$ pct arcs
1:23:456
78:9:te0
Ctl-d
78 :1
9  :23
AB0:456
$
~~~~

# ard - array remove duplicates

## SYNTAX

ard [-d]

## PURPOSE

Remove duplicate arrays from a set of arrays.

## DESCRIPTION

If -d is given only arrays for which there is a duplicate are written.
Arrays that occure three times will be written twice ans so on.

# asi - array sc information

## SYNTAX

asi [ -a ] [ -b ] [ -c ] [ -p ] [ -r ] [ -v ] [ file ]

- -a All, write columns and rows and positions.
- -b Browse arrays, print the input arrays also.
- -c Write columns.
- -p Write positions.
- -r Write rows.
- -v Verbose mode, long sc names and TnI values.

## PURPOSE

Display sc membership for rows columns and positions of arrays.

## DESCRIPTION

The `asi` command prints the sc membership of each row, column, and
position of the arrays at stdin.
It is typically used to view or print sets of arrays.

## EXAMPLE

~~~~
$ pct asi -a < i/arrays
...
$
~~~~

# asro - array serial operation

## PURPOSE

Apply a serial operation to an array.

## SYNTAX

asro sro

## DESCRIPTION

`asro` transforms all arrays at stdin by an sro.  The partitioning is
retrograded if sro has _R_, but is unmodified under rotation (_r_).

## EXAMPLE

~~~~
$ pct asro T0 < babbitt/myends/array.b1
BA3 :762  :458   :190   :7        :      :A     :6B23510498
8940:15   :32B6A7:      :859410A23:B     :6     :7
2561:9A   :0     :87B436:         :5A219 :B03847:
7   :4830B:91    :2A5   :6B       :073486:5291  :A
$
~~~~

# atg - array transformations generator

## PURPOSE

Independently apply a set of operations to the rows of an array.

## SYNTAX

atg [ -oN ] [ file ]

- -o0 = Tn (def.)
- -o1 = TnI
- -o2 = RTnI
- -o3 = RTn

## DESCRIPTION

`atg` reads arrays at stdin, for each array _e_ an array class _E_ is
written.  _E_ is formed by applying each transformation _t_ in a set
of transformations _T_ to _e_.  The set _T_ is given by the option
`-o`.  The first row of _e_ is not subject to transposition.
Therefore _#E_ is given by _(#T ^ r) / 12_ where _r_ is the number of
rows in _e_.

# atl - array to latex

## PURPOSE

Convert pct arrays into LaTeX tables.

## SYNTAX

atl [ file ]

## DESCRIPTION

A simple command to format arrays to well-formatted tables for printing
with LaTeX.
Modifications to output are only available through source.

## EXAMPLE

~~~~
$ pct atl < babbitt/myends/array.b1
\begin{tabular}{|l|l|l|l|l|l|l|l|}                                    \hline
BA3  &762   &458    &190    &7         &       &A      &6B23510498 \\ \hline
8940 &15    &32B6A7 &       &859410A23 &B      &6      &7          \\ \hline
2561 &9A    &0      &87B436 &          &5A219  &B03847 &           \\ \hline
7    &4830B &91     &2A5    &6B        &073486 &5291   &A          \\ \hline
\end{tabular}
$
~~~~

# atto - apply all ttos to an array

## PURPOSE

Make all tto transformations of an array.

## SYNTAX

atto file

## DESCRIPTION

This command reads an input file and writes all transformations
of the form Tn[M][I] (ie.  48 in total).

## NOTES

1.  Shell script.

# bip - basic interval pattern

## SYNTAX

bip [ pcseg ...  ]

## PURPOSE

Print the basic interval pattern of a pcseg.

## DESCRIPTION

The `bip` command prints the basic interval pattern of pcsegs.
If no arguments are given stdin is read.

## EXAMPLE

~~~~
$ pct bip 0t95728e3416
11223344556
$
~~~~

## NOTES

1. Shell function.
2. For the definition of the basic interval pattern see Allen Forte
"The Basic Interval Patterns" JMT 17/2 (1973):234-272 [@Forte1973b].

# ccdg - concordance generator

## PURPOSE

Generate a simple textual concordance.

## SYNTAX

ccdg [ file ] | sort -f

## DESCRIPTION

The command `ccdg` is a file filter.  It prints a basic concordance for the
file at stdin to stdout.  It is generally used with the standard sorting
utility `sort`.   The output lists each word of the input file on a
separate line, with two values separated by commas.  These values
represent the line and word numbers.  Lines that are empty (the first
character must be a new-line char) are not counted.  If multiple
filenames are specified on the command line, the name of the file from
which the word was drawn will be printed directly before the line
number.

## EXAMPLE

~~~~
$ pct ccdg | sort -f
This is a
short bad
poem
Ctl-d
a 1,3
bad 2,2
is 1,2
poem 3,1
short 2,1
This 1,1
$
~~~~

# cf - cardinality filter

## SYNTAX

cf cset

## PURPOSE

Filter pcsegs by cardinality.

## DESCRIPTION

The `cf` command acts as a file filter, writing to stdout only those pc objects
at stdin that have a cardinality that is a member of the specified cset.

## EXAMPLE

~~~~
$ pct cf 4 < ~/sw/pct/share/scdb
...
4-Z15   All-Interval Tetrachord (see also 4-Z29)
4-Z29   All-Interval Tetrachord (see also 4-Z15)
4-19    minor major-seventh chord
4-20    major-seventh chord
4-25    french augmented sixth chord
4-28    dimished-seventh chord
4-26    minor-seventh chord
4-27    half-dimished seventh(P)/dominant-seventh(I) chord
...
$
~~~~

## NOTES

1.  Shell function.

# cfa - cardinality frequency analysis

## PURPOSE

Tabulate the number of pcsets of each cardinality in the input.

## SYNTAX

cfa [ file ...  ]

## DESCRIPTION

The `cfa` command reads input for pcsets and counts the number of
occurrences of pcsets of each cardinality.

The output is displayed as a vector, the first value is the number
of pcsets of cardinality one, and the last is the number of pcsets
of cardinality twelve.

If no files are given as arguments stdin is read.

If filenames are given, each line of output is preceded by the name
of the file to which the line refers.

## EXAMPLE

~~~~
$ pct cfa
01
01234
Ctl-d
010010000000                        (2)
$
~~~~

## NOTES

1.  Counts only one pcset per line.

# cg - combinations generator

## PURPOSE

Write combinations drawn from a set.

## SYNTAX

cg [ -rN ] pcset ...

## DESCRIPTION

The `cg` command writes the combinations of input sets to stdout.
If `-r` is given prints _nCr_, else all values of _r_ in _nCr_ are given.

## EXAMPLE

~~~~
$ pct cg -r3 0159
015
019
059
159
$
~~~~

# cgm - combinations generator meta

## PURPOSE

Write combination of pcsets.

## SYNTAX

cgm R pcset pcset...

## DESCRIPTION

The `cgm` command writes the union of all _R_ element combinations of
input sets to stdout.

## EXAMPLE

~~~~
$ pct cgm 2 012 345 678
012345
012678
345678
$
~~~~

# chn - chain pcsegs

## SYNTAX

chn sro n

## PURPOSE

Make pcseg chains.

## DESCRIPTION

The `chn` command prints the transformations of pcsegs for which the first
_n_ elements are related by the serial operator sro to the last _n_ elements
of the original pcseg.  The transformation to map the input onto the
output pcseg is also given (in parentheses).  The transformations
searched include all forms of RTnMI.

## EXAMPLE

~~~~
$ echo 024579 | pct chn T0 3 | sort -u
579468 (RT8M)
579A02 (T5)
$ echo 02457t | pct chn T0 2
7A0135 (RT5I)
7A81B9 (RT9MI)
$
~~~~

## NOTES

Needs interface to verbose flag.  Should allow a search for the
maximal chaining possible.

# cisg - cyclic interval succession

## SYNTAX

cisg [ pcseg ...  ]

## PURPOSE

Print the cyclic interval succession of a pcseg.

## DESCRIPTION

The `cisg` command prints the cyclic interval succession of pcsegs.

## EXAMPLE

~~~~
$ echo 014295e38t76 | pct cisg
13A7864529B6
$
~~~~

## NOTES

1.  Shell function.

# cmpl - complement

## SYNTAX

cmpl [ pcset ...  ]

## PURPOSE

Print the complement of a pcset.

## DESCRIPTION

The `cmpl` command prints the complement of pcsets.
If no arguments are given `cmpl` reads stdin.

## EXAMPLE

~~~~
$ pct cmpl 02468t
13579B
$
~~~~

# cyc - cycles

## SYNTAX

cyc [ pco ...  ]

## PURPOSE

Writes cyclic pc objects.

## DESCRIPTION

The `cyc` command is a file filter, writing to stdout input pc
objects with the first element repeated last.

## EXAMPLE

~~~~
$ pct cyc 056
0560
$
~~~~

# dim - diatonic interval class two implications

## PURPOSE

Print the diatonic implications of a pcset.

## SYNTAX

dim [ pcset ]

## DESCRIPTION

The `dim` command accepts as a single argument a pcset.  It prints the
T-levels of the three d-ic2 cycle scs of which the entered set
is a member.  These scs are:

- d: the major/natural-minor collection: 7-35[013568A]
- m: the ascending melodic-minor collection: 7-34[013468A]
- o: the octotonic collection: 8-28[0134679A]

These are the only three collections that can be generated by a
diatonic-ic two cycle in which there is no instance of a consecutive
ic1 (see [@Hall1997a]).  d and m are rotated and transposed so that
T0d and T0m are the C major and C melodic-minor (ascending)
collections respectively.

## EXAMPLE

~~~~
$ pct dim 016
T1d
T1m
T0o
$
~~~~

## NOTES

1.  Thanks to Tom Hall.

# dis - diatonic interval sets

## PURPOSE

Translate diatonic isets to isets.

## SYNTAX

dis [ d-iset ]

## DESCRIPTION

The `dis` command accepts a single d-iset as an argument.  It
translates the d-iset to an iset and writes the result to stdout.

The translation table is: (diset->iset) = ({2}->{12}) ({3}->{34})
({4}->{56}) ({5}->{67}) ({6}->{89}) ({7}->{AB}).

## EXAMPLE

~~~~
$ pct dis 24
1256
$
~~~~

# doi - degree of intersection

## SYNTAX

doi cset [ sc ]

## PURPOSE

Generate pcsets with a given degree of intersection.

## DESCRIPTION

The `doi` command reads pcsets and writes the members of sc (if not
specfified sc is taken to be the input pcset) which have a degree of
intersection with the input pcset that is a member of cset.

## EXAMPLE

~~~~
$ echo 024579e | pct doi 6 | sort -u
024579A
024679B
$ echo 01234 | pct doi 2 7-35 | sort -u
13568AB
$
~~~~

# epmq - epm queries

## SYNTAX

empq [ -f file ] [ Condition ...  ]

- -f read conditions from a named file.

## PURPOSE

Evaluate pitch materials against a set of conditions.

## DESCRIPTION

The `empq` command acts as a file filter, writing to stdout only those
pcsegs at stdin that reflect all conditions specified as arguments or
in _file_.  Conditions are specified as described in and inclusion
relations are __not__ proper.

Several subcommands are recognized, and may be invoked in interactive
use.  These are given in the table below.  The example prints the
pentachordal and hexachordal subsets of the diatonic collection that
include the major-seventh collection.

~~~~
- ----------------------------------------------
+ add a condition
- remove a condition
< run the current epm over a file
@ print all active conditions to stderr
^ print all pc objects referenced
& toggle printing of the condition a test fails
- ----------------------------------------------
~~~~

## EXAMPLE

~~~~
$ pct fl -c 56 | pct epmq "in sc 7-35" "has sc 4-20"
5-20
5-27
6-Z25
6-Z26
6-32
$
~~~~

## NOTES

1.  The argument to the `<` subcommand __must__ be catenated directly
to the command.

# ess - embedded segment search

## PURPOSE

Search for embedded pcsegs.

## SYNTAX

ess [ -m ] pcseg

## DESCRIPTION

The `ess` command searches all transformations of a pcseg for embedded
segments.  A pcseg embeds another when the second occurs _in order_
within the first, though not necessarily as a sub-string.  If `-m` is
given, M related segments are __not__ searched.

## EXAMPLE

~~~~
$ echo 23a | pct ess 0164325
2B013A9
923507A
$
~~~~

# fc - file comparison

## PURPOSE

Makes a list comparing the contents of a collection of files.

## SYNTAX

fc [ -v ] file1 file2 ...

## DESCRIPTION

The `fc` command allows the user to make comparisons between text
files.

In concise (default) mode, the output of the program is a list of
lines contained in all files entered on the command line.

In verbose mode (`-v`) the output of the program is in multiple
sections.

The first section contains lines found in all of the input files, the
following sections list the lines found in each file that are not in
the first section (ie.  are not in all the files but may be in one or
more of the other files).

## NOTES

Conflicts with a shell built-in under `ksh`.  `csh` is ok, but this
should be fixed.

# fl - forte list

## PURPOSE

View the forte list.

## SYNTAX

fl [ -c ] [ -l ] [ -v ] [ cset ]

- -c Concise.  4-1 instead of 4-1[0123].
- -v Verbose.  prints also the icv and tics vector.
- -l Literal.  Writes the prime-form.

## DESCRIPTION

The `fl` command generates a forte list for scs of specified
cardinalities.  The default cset will print the entire list.

## EXAMPLE

~~~~
$ pct fl -v 3
SC Name           Interval Vector   TICS Vector
--------------------------------------------------
3-1[012]          <3210000>         <123210000000>
3-2[013]          <3111000>         <121220100000>
3-3[014]          <3101100>         <121022001000>
3-4[015]          <3100110>         <121002200010>
3-5[016]          <3100011>         <221000220000>
3-6[024]          <3020100>         <102030201000>
3-7[025]          <3011010>         <102012020010>
3-8[026]          <3010101>         <202010202000>
3-9[027]          <3010020>         <103010020200>
3-10[036]         <3002001>         <200200300200>
3-11[037]         <3001110>         <101200120020>
3-12[048]         <3000300>         <300030003000>
$
~~~~

# fmerge - file merge

## PURPOSE

Merge various input files.

## SYNTAX

fmerge file ...

## DESCRIPTION

The `fmerge` command reads words from input files and writes a tab
delimited output file.

A word is read from each input file, placed into the output file
followed by a tab, and a newline character if the file was the last in
the list.

Therefore, if the words from each file are of a similar length, the
output will be a _table_ in which the columns hold the successive
words of each file (columns run left to right in the order of files in
the argument list).

Words will continue to be written until the end of all files are
reached, empty strings will be placed in columns of files after the
eof is reached on that file.

Words are white-space delimited.

## EXAMPLE

~~~~
$ pct mcs 5 2 s | pct fsplit 2
$ pct fmerge *.spl
<01>    <10>    <20>    <30>    <40>
<02>    <12>    <21>    <31>    <41>
<03>    <13>    <23>    <32>    <42>
<04>    <14>    <24>    <34>    <43>
$ rm *.spl
~~~~

## NOTES

# fn - forte name

## SYNTAX

fn [ -t ] [ -v ] [ pcset ...  ]

- -t write TnI values.
- -v verbose, write long forte names.

## PURPOSE

Print the forte name of a pcset.

## DESCRIPTION

The `fn` command prints the Forte names [@Forte1973a] of pcsets.

## EXAMPLE

~~~~
$ pct fn 023
3-2
$
~~~~

## NOTES

1.  The options are mutually exclusive.

# frg - fragmentation of cycles

## PURPOSE

Gives the fragmentation of cycles for pcsets.

## SYNTAX

frg [ pcset ...  ]

## DESCRIPTION

The `frg` command accepts any number of pcsets as arguments.  It
prints the fragmentation of interval-class cycles for each pcset.

The fragmentation lists show adjacencies and non-adjacencies of
pcs within a pcset at each interval-class.  Each interval class
divides the aggregate into a number of cycles, this command shows
where each pc of the pcset falls with regard these cycles.

## EXAMPLE

~~~~
$ pct frg 024579
Fragmentation of 1-cycle(s):  [0-2-45-7-9--]
Fragmentation of 2-cycle(s):  [024---] [--579-]
Fragmentation of 3-cycle(s):  [0--9] [-47-] [25--]
Fragmentation of 4-cycle(s):  [04-] [-59] [2--] [-7-]
Fragmentation of 5-cycle(s):  [05------4927]
Fragmentation of 6-cycle(s):  [0-] [-7] [2-] [-9] [4-] [5-]
IC cycle vector: <1> <22> <111> <1100> <5> <000000>
$
~~~~

## NOTES

1.  The design of this command is derived from the set-information
sub-command of Michael Buchler's "Set Maker" program.

SetMaker(v4.6a, Michael Buchler)

~~~~
Input set: [024579] = T0 of [024579] Forte label: 6-32
SET PROPERTIES:
IC vector: <143250> Invariance vector: <11001100>
VTICS: <325052341614>
Fragmentation of 1-cycle: [0-2-45-7-9--]
Fragmentation of 2-cycles: [024---] [--579-]
Fragmentation of 3-cycles: [0--9] [-47-] [25--]
Fragmentation of 4-cycles: [04-] [-59] [2--] [-7-]
Fragmentation of 5-cycle: [05------4927]
Fragmentation of 6-cycles: [0-] [-7] [2-] [-9] [4-] [5-]
IC Cycle Vector: <1000> <2200> <111> <1100> <5000> <000000>
RELATED SETS / SET-CLASSES:
Literal comp: [68ab13] = T6 of [024579] (Abstract comp.)
M/MI = [012345] (6-1)
~~~~

# fsplit - file split

## PURPOSE

Split white-space delimited files.

## SYNTAX

fsplit n

## DESCRIPTION

The `fsplit` command splits its input into a number of files, writing
each word to a file containing only words identical to n places.

Words are delimited by whitespace.

The main purpose of the command is to place pc objects identical to n
places into a single file, possibly to be reformatted with the
command.

Files are named by the n character decision string with a `.spl`
suffix.

## EXAMPLE

~~~~
$ pct mcs 5 2 c | pct fsplit 2
$ ls *.spl
{0.spl  {1.spl  {2.spl  {3.spl
$ rm *.spl
~~~~

## NOTES

# fsrt - forte sort

## PURPOSE

Sort lines of a text file by forte name.

## SYNTAX

fsrt [ Number ] [ file ]

## DESCRIPTION

`fsrt` is a file filter.  Lines at stdin are sorted by the first forte
name of each line.  If a line does not contain a forte name it is
discarded.  The output is printed to stdout.  An integer argument is
optional.  The command `fsrt 3 file` would sort the file `file` by the
third forte name of each line.  In this case if a line does not
contain three forte names it is discarded.

## EXAMPLE

~~~~
$ pct fsrt
     sc1: 5-1[01234]
     sc2: 4-1[0123]
     sc3: 3-1[012]
Ctl-d
     sc3: 3-1[012]
     sc2: 4-1[0123]
     sc1: 5-1[01234]
$
~~~~

## NOTES

1.  The command determines whether an argument is a position indicator
or a filename by its length.  Arguments of 1 or 2 characters are
considered position indicators, longer arguments are considered filenames.

# gdg - generate directed graph

## PURPOSE

Make a graph file for the graph layout program dot.

## SYNTAX

gdg

## DESCRIPTION

Lines at stdin consist of two white-space delimited tokens.
Each line is an edge in the graph.

## NOTES

1.  Shell script.

# gics1 - icset to sc mapping graph v.1

## PURPOSE

Write a graph mapping an icset to \#Y scs.

## SYNTAX

gics1 icset X Y | dot -Tps | lpr

## DESCRIPTION

The `gics1` command shows the relationship of an icset to scs of a given
cardinality.  It writes a graph description file for processing by dot.

The graph is divided into two sub-graphs, the first contains only the
isets derivable from the icset, and the second the scs of cardinality Y.

Edges connect each iset to the scs it can form with an expansion to a
multi-set of X elements.

The output file should look ok for most
inputs, but no attempt is made at altering the graph for different
degrees of complexity.  The output may need to be edited by hand to make
the graph layout entirely acceptable.  To aid editing the basic layout
is briefly described here (the output files contain comments showing
most of this anyway).  Firstly there is some general information about
the graph as a whole, next the nodes of the sub-graphs are declared
separately, and then finally the edges are declared.  To remove nodes
from the graph they must be removed not only from the sub-graph
definition but also from the edge definitions (else they will be
instantiated and drawn outside of any subgraph).  For details of the
graph format see the `dot` manual.

## EXAMPLE

~~~~
$ pct gics1 {124} 3 4 | dot -Tps > /tmp/icg.ps ; gv /tmp/icg.ps
~~~~

## NOTES

1.  Shell script.
2.  In normal use Y = X+1.  However if X is large then the command
will, I think, search for instances of pc repetition such that the
isets will form scs of cardinality Y.  If Y is greater than X I don't
know what happens.  Also I think it should be ok if X and Y are csets
and not integers, but this is so far untested.

# gics2 - icset to sc mapping graph v.2

## PURPOSE

Write a graph mapping the \#X icsets to \#Z scs.

## SYNTAX

gics2 X Y Z | dot -Tps | lpr

## DESCRIPTION

The `gics2` command shows the relationship of an icset to scs of a given
cardinality.  It writes a graph description file for processing by dot.

The graph is divided into two sub-graphs, the first contains the icsets
of cardinality _X_, and the second the scs of cardinality _Z_.

Edges connect each iset to the scs it can form with an expansion to a
multi-set of _Y_ elements.

## EXAMPLE

~~~~
$ pct gics2 3 3 4 | dot -Tps > /tmp/g.ps; gv /tmp/g.ps
~~~~

## NOTES

1.  Shell script.
2.  In normal use Z = Y+1.

# gisdb - graph an isdb

## PURPOSE

Make a graph of an iset to sc database.

## SYNTAX

gisdb dir

## DESCRIPTION

gisdb is a shell script to generate a graph description file of an
iset->sc database as written by  It requires a directory name
`dir` as a single argument.  It makes a node for each file in
`dir` and writes edges to each line in that file.  Output can be
sent through either

## NOTES

1. Shell script.
2. The graph files produced will be huge and impractical.  Best to run
them over subsets of isdb directories (ie make a copy of only a small
number of the files).

# gmbs - graph mbs

## PURPOSE

Graph information about a set of isets.

## SYNTAX

gmbs [ -ssc ] iset ...

## DESCRIPTION

The `gmbs` command is equivalent to the command `mbs -c` but
produces output in a graph format.

## EXAMPLE

~~~~
$ pct gmbs 3b 147 | dot -Tps > /tmp/g.ps; gv /tmp/g.ps
~~~~

## NOTES

1.  The inital part of the graph is unneccesary.

# gorl - graph object (pc) relations

## PURPOSE

Graph the relations existing between a set of pc objects.

## SYNTAX

orl -c: | gorl | dot -Tps

## DESCRIPTION

The `gorl` command reads the output of  and generates a graph
file for input to dot.

## EXAMPLE

~~~~
$ pct orl -m -c: < ~/analysis/webern/op10n4/pcos | gorl > ex7.dot
$
~~~~

## NOTES

1. Shell script.
2. Why insist on `:` as delimiter?

# gpvl1 - graph proximate voice leading

## PURPOSE

Write a graph showing an abstract proximate voice leading network.

## SYNTAX

gpvl1 constraint-iset in-sc-cset out-sc-cset

# gpvl2 - graph proximate voice leading

## PURPOSE

Write a graph showing an abstract proximate voice leading network.

## SYNTAX

gpvl2 constraint-iset sc-set

## EXAMPLE

~~~~
$ pct gpvl2 013 "3-3 3-5 3-8 3-11" | neato -Tps > /tmp/ex12.eps; gv /tmp/ex12.eps
~~~~

# gug - generate undirected graph

## PURPOSE

Make a graph file for the graph layout program neato.

## SYNTAX

gug

## DESCRIPTION

Lines at stdin consist of two white-space delimited tokens.
Each line is an edge in the graph.

## NOTES

1.  Shell script.

# icf - interval cycle filter

## PURPOSE

Determine if an interval collection is cyclic.

## SYNTAX

icf

## DESCRIPTION

The `icf` command determines if an interval collection
is cyclic (ie.  sums to zero mod12).

## EXAMPLE

~~~~
$ echo 22341 | pct icf
22341
$
~~~~

# ici - interval-class to inteval

## PURPOSE

Write the isets that can be formed from an icset.

## SYNTAX

ici -c [ icset ...  ]

- -c concise, dont write inverses.

## DESCRIPTION

The `ici` command lists the isets that can be formed from a given
icset.  This involves interpreting each ic as one of its two possible
intervals, and doing so in all possible combinations.  In concise
operation inversionally equivalent isets are not written (ie.  123=I(BA9)).

## EXAMPLE

~~~~
$ pct ici -c 123
123
129
1A3
1A9
$
~~~~

## NOTES

1.  Errors if ic 0 or 6 present, send through `sort -u`.

# icseg - interval class succession

## SYNTAX

icseg [ pcseg ...  ]

## PURPOSE

Print the interval class succession of a pcseg.

## DESCRIPTION

icseg prints the interval class succession of pcsegs.

## EXAMPLE

~~~~
$ pct icseg 013265e497t8
12141655232
$
~~~~

## NOTES

1.  Shell function.

# icv - interval class vector

## SYNTAX

icv [ -r ] [ pcset | icv ] ...

- -r reverse operation.

## PURPOSE

Map to and from interval class vectors.

## DESCRIPTION

The `icv` command prints the interval class vectors of pcsets.
If `-r` is run, `icv` reads icvs and writes scs.

## EXAMPLE

~~~~
$ pct icv 6-1
6543210
$ pct icv -r .1..1.1
4-12
4-Z15
4-16
4-18
4-Z29
5-25
5-29
$
~~~~

## NOTES

`-r` at times failed.  This should be fixed now.

# imb - imbrications

## PURPOSE

Imbricate pcsegs.

## SYNTAX

imb [ -ccset ] [ pcseg ...  ]

- -c set cardinalities for imbrication.

## DESCRIPTION

The `imb` command reads pcsegs and writes their imbrications.  We
specify the cardinalities for imbrication with the `-c` option,
the default is to `-c1`.

## EXAMPLE

~~~~
$ pct imb -c34 024579 | pct pfmt
024 245 457 579
0245 2457 4579
$
~~~~

# isdb - interval-set database

## PURPOSE

To make a iset to sc database.

## SYNTAX

isdb n e

## DESCRIPTION

The `isdb` command allows us to examine the relationship between isets
and scs.  The `isdb` command generates the directory which will act as a
database of iset-sc mappings of cardinality `n` and degree of
expansion `e`.  Parent directories will be created where required.

## EXAMPLE

~~~~
$ pct isdb 4 6
isdb: making the directory "isdb/4/6"
isdb: generating the file "isdb/4/6/1234"
...
isdb: generating the file "isdb/4/6/89AB"
$
~~~~

## NOTES

1.  Shell script.

# iseg - interval succession

## SYNTAX

iseg [ pcseg ...  ]

## PURPOSE

Print the interval succession of a pcseg.

## DESCRIPTION

The `iseg` command prints the interval succession of pcsegs.

## EXAMPLE

~~~~
$ pct iseg 014295e38t76
13A7864529B
$
~~~~

## NOTES

1.  Shell script.

# isegsc - iseg to sc

## PURPOSE

Convert an iseg to a sc.

## SYNTAX

isegsc [ -l ]

- -l print a literal pcseg (not the sc name)

## DESCRIPTION

The `isegsc` command converts isegs to scs.  Default operation is equal to
`pcom iseg sc`, however if `-l` is given a literal pcseg
(without removing pc duplications) is printed to stdout.

## EXAMPLE

~~~~
$ pct se -c5 1247 | pct pg | sort -u | pct isegsc | sort -u | \
  pct epmq "is sc 6-1 6-8 6-32 6-7 6-20 6-35" | pct fsrt
6-1
6-7
6-8
6-32
$
~~~~

# issb - is subset

## PURPOSE

Is one sc in another.

## SYNTAX

issb sc sc

## DESCRIPTION

The issb determines if the sc A is an abstract subset of sc B.
It prints the scs that will combine with A to make B.

## EXAMPLE

~~~~
$ pct issb 3-7 6-32
3-7
3-2
3-11
$
~~~~

## NOTES

1.  Shell function.

# issl - interval set to set list

## PURPOSE

List all pcsets that can be formed from an interval set.

## SYNTAX

issl [ -ssc ] [ iset ...  ]

- -s write only instances of sc

## DESCRIPTION

This command accepts any number of isets as arguments.  It lists
all the pcsets (and the scs of which they are members) that can be
formed from the input iset.  The output is in three columns.  The
first column shows the the T-levels of each of the intervals of
the iset.  The second column shows the pcset formed.  The third
column shows the sc of which the second column pcset is a member
and the TnI level at which it maps onto the pcset.  The number of
pcsets formed is a function of the cardinality of
the iset, and is of the form `#pcsets = 12 ^ (#iset - 1)`.

## EXAMPLE

~~~~
$ pct issl -s6-Z42 357
-012- {012369}            T0  6-Z42[012369]
-096- {012369}            T0  6-Z42[012369]
$
~~~~

## NOTES

1.  The listing is not strictly complete since the left-most T-level
is never incremented.

# ist - interval segment transposition

## PURPOSE

Transform a pcseg by applying an interval segment as a complex
transposition.

## SYNTAX

ist pcseg < ISEG_LIST

## DESCRIPTION

Applies a set of interval segments as order-location dependent
transpositions on a pitch-class segment.  The second example writes the
set of pcsegs that `shadow' the first half of the Op.31 row at a
`diatonic third'.

## EXAMPLE

~~~~
$ echo 012 | pct ist 543
555
$ pct se -n -c5 34 | pct pup | pct ist t4635 | wc -l
   32
$
~~~~

# lsi - list subsets and intersections

## PURPOSE

Write the intersection for partitions of a sc.

## SYNTAX

lsi sc cset

## DESCRIPTION

The `lsi` command requires a single sc and a single cset as arguments.
It determines all the possible 2-part partitions of the sc entered,
where the subsets are of cardinalities specified in the Cset entered,
and prints these partitions along with the intersection set formed
(ie.  the overlap between the two scs).

## EXAMPLE

~~~~
$ pct lsi 027 2
T0  2-2[02]             T2  2-5[05]             T0  3-9     T2  1-1[0]
T0  2-2[02]             T7  2-5[05]             T0  3-9     T0  1-1[0]
T0  2-5[05]             T5  2-2[02]             T5  3-9     T5  1-1[0]
T0  2-5[05]             TA  2-2[02]             TA  3-9     T0  1-1[0]
T0  2-5[05]             T5  2-5[05]             TA  3-9     T5  1-1[0]
T0  2-5[05]             T7  2-5[05]             T5  3-9     T0  1-1[0]
$
~~~~

## NOTES

1.  Should rewrite as a script.
2.  Design by Tom Hall.

# lsrt - line sort

## PURPOSE

Sort strings within lines of a text file.

## SYNTAX

lsrt

## DESCRIPTION

lsrt is a file filter.  Lines at stdin are internally sorted by
strings within each line.  Strings are separated by white space.
The sorted lines are printed to stdout.

## EXAMPLE

~~~~
$ echo "5 4 3 2 1" | pct lsrt
1 2 3 4 5
$
~~~~

# mbs - mbs

## PURPOSE

Print information about a set of isets.

## SYNTAX

mbs [ -ssc ] [ -c ] iset ...

- -c concise; only scs are printed, and only once
- -s filter output for instances of sc

## DESCRIPTION

The `mbs` command prints the list of isets that can be formed from a set
of input isets.  For each iset that is formed (or sub-iset) the
program prints output as for the command The formation of isets is
governed by the rule that all superisets must contribute one interval
to each sub-iset.  The example shows that although there are six
subisets that can be formed from the two superisets `{3b}` and
`{147}`, only two of these subisets can form the SC `4-4[0125]`.

## EXAMPLE

~~~~
$ pct mbs 3b 147 -s"4-4"

iset={31}
-04- {0345}              T5I 4-4[0125]
-0A- {03AB}              TA  4-4[0125]

iset={34}

iset={37}

iset={B1}

iset={B4}

iset={B7}
-01- {018B}              T1I 4-4[0125]
-03- {03AB}              TA  4-4[0125]
$
~~~~

## NOTES

1.  Error/feature when iset = `{33}`.

# mcs - maximally connected sets

## PURPOSE

Implement ideas from paper mcs.tex (1996)

## SYNTAX

mcs x n [ c | s | f ]

## DESCRIPTION

This command produces three tables.  It can be made to produce any
single table by including one of the optional arguments `c`,
`s`, or `f` after the two required arguments.  The required
arguments are the cardinality of the input set (x) and the length of
the subsets and subsegments to be examined (n).  The first table (c)
prints the \#Cn combinations.  The second table (s) replaces each
combination in (c) with a list of the permutations of that combination
(the table therefore lists \#Pn).  The final table (f) reformats the
table above into a columnar format.  This table is generated by a
constructing a command pipe that includes a recursive call to
`mcs` \# n `s` in combination with calls to the pct commands
and and to the unix commands more and rm, all via the ANSI-C function
`system`.

## EXAMPLE

~~~~
$ pct mcs 5 2 c
{0,1} {0,2} {0,3} {0,4}
{1,2} {1,3} {1,4}
{2,3} {2,4}
{3,4}
$ pct mcs 5 2 s
<01> <10> <02> <20> <03> <30> <04> <40>
<12> <21> <13> <31> <14> <41>
<23> <32> <24> <42>
<34> <43>
$ pct mcs 5 2 f
<01>    <10>    <20>    <30>    <40>
<02>    <12>    <21>    <31>    <41>
<03>    <13>    <23>    <32>    <42>
<04>    <14>    <24>    <34>    <43>
$
~~~~

## NOTES

1.  The (f) option removes all files whose names match the regular
expression `"<"*.spl` from the directory before and after running,
so be sure there are no such files you want to keep!

2. The `system` string can easily be modified in the source if similar
functionality can be achieved via similar commands on any OS which
supports `system`.

# mg - merge

## PURPOSE

Print the pcset that results from merging entered pcsets.

## SYNTAX

mg pcset ...

## DESCRIPTION

This command merges the pcsets entered as arguments and prints the
result.
Up to 12 pcsets may be merged.

## EXAMPLE

~~~~
$ pct mg 01 23 45
012345
$
~~~~

# mpc - arbitrary pc map

## PURPOSE

Do arbitrary pc mappings.

## SYNTAX

mpc pcv

## DESCRIPTION

Any pc operation can be specified as a set of duples showing
the mapping of each individual pc under the operation.

This set can be written as a twelve position vector, the position
giving the input pc, and the value the output pc.

The `mpc` command reads such a vector and performs the specified
mapping on all pcs at stdin.

For example the operation T4I is given by the vector [43210BA98765].
(The command `sro T4I 0123456789te` gives this vector, and so on.)

## EXAMPLE

~~~~
$ echo 024579 | pct mpc 02468t13579e
048A37
$
~~~~

# mw - map words

## PURPOSE

Maps pcs to arbitrary words.

## SYNTAX

mw [ word0 word1 ...  word11 ]

## DESCRIPTION

The `mw` command replaces each occurrence of a pc at stdin with an
arbitrary word specified by the user.  Up to twelve words may be
entered as arguments, and will be mapped to the pcs from zero upwards.
At least one word must be specified.  If words are not specified for
all pcs, those pcs without a mapping are printed unaltered.  Words are
proceeded by white space, thus ``012` might become `C C\# D'.

## EXAMPLE

~~~~
$ echo 0146 | pct mw C C# D D# E F F# G G# A A# B
C C# E F#
$
~~~~

# mxs - matrix search

## PURPOSE

Search the matrix of a pcseg (the 48 RTnI transformations).

## SYNTAX

mxs pcseg pcseg

## DESCRIPTION

The `mxs` command searches for pcsegs within the 48 RTnI
transformations of a pcseg.

## EXAMPLE

~~~~
$ pct mxs 024579 642 | sort -u
6421B9
B97642
$
~~~~

## NOTES

1.  Shell function.

# name - name pcs

## PURPOSE

Name pcs.

## SYNTAX

name

## DESCRIPTION

The `name` command reads pcs and writes pc names (ie C\# for 1 etc.).

## EXAMPLE

~~~~
$ pct fl 2 | pct pcom pcseg pcset | pct name
C C#
C D
C D#
C E
C F
C F#
$
~~~~

## NOTES

1.  Shell function.

# nrm - normalize

## SYNTAX

nrm [ -r ] [ pcseg ...  ]

- -r  retain duplicate elements

## PURPOSE

Normalize a multiset.

## DESCRIPTION

The `nrm` command normalises multisets by removing duplicate
elements and sorting the result into ascending order.

Invoking the `-r` option retains duplicate elements.

If no arguments are given nrm reads stdin.

This command in effect will convert pcsegs to pcsets, icsegs
to isets, and isegs to isets.

## EXAMPLE

~~~~
$ pct nrm 0123456543210
0123456
$
~~~~

# orl - object relations

## PURPOSE

Determine the relations existing between a set of pc objects.

## SYNTAX

orl [ -v ] [ -cN ] [ -m ] [ file ]

- -m inhibit M relations in ttos and sros
- -v verbose mode
- -c set the output field delimiter (a space by default)

## DESCRIPTION

The `orl` command reads a set of pc objects, and writes the set of
relations that exist between those objects.

The output gives:

1.  pco `a`
2.  the relation: in, has, is, tto, or sro, and
3.  pco `b`

Inclusion relations __are__ proper.
Left operands are "pcseg", "pcset", and "sc".

## EXAMPLE

~~~~
$ cat ~/analysis/webern/op10n4/pcos
sc 6-Z43
sc 6-Z17
pcset 012568   %    H
pcset 9AB235   % T9(H)
pcset 432BA8   %  L(H)
pcset 876320   %  I(H)
pcset 3479AB   %    h
pcset 014678   % T9(h)
pcset 109765   %  L(h)
sc 5-6
sc 5-7
pcset 01256    %    X
pcset 349te    %    y
$ !! | pct orl -m -c: | pct gorl | dot -Tps > /tmp/ex7.eps ; gv /tmp/ex7.eps
~~~~

# pcom - pitch-class object mappings

## PURPOSE

Calculate the simple pco translations.

## SYNTAX

pcom src_type dst_type [ pco ]

## DESCRIPTION

The `pcom` command provides all direct (and hence through recursion
all indirect) one-to-one mappings between pc objects.

One-to-many mappings (such as iseg -> pcseg, or icv -> sc) are
not supported, seperate commands exist to perform such mappings.

The list of mapping supported, and references to the standard
literature for the mapping, are in a table given below.

The examples give a single instance of each mapping
in the order listed in the above table.

The final example shows the `pcom` command used recursively to
map a pcseg to an interval-class set (a three stage process that
could be arrived at in different ways for different purposes).

~~~~
In        Out      Description
--------- -------- -------------------------------------------------------
pcseg     pcset    discard duplications and sort
pcseg     iseg     get interval succession (Morris INT())
pcset     sc       get prime form (prints Forte name)
sc        icv      enumerate interval classes (see Forte 1973)
iseg      icseg    discard direction (all intervals to interval classes)
iseg      iset     discard duplications and sort
iset      icset    discard direction and sort
icseg     icset    discard duplications and sort
icv       icset    discard interval count
--------- -------- -------------------------------------------------------
~~~~

## EXAMPLE

~~~~
~$ sh -x ~/sw/pct/doc/1/pcom.sh
+ pcom pcseg pcset 115533
135
+ pcom pcseg iseg 0136t39
123456
+ pcom pcset sc 01355310
0135
+ pcom sc icv 024579
6143250
+ pcom iseg icseg 1e2t394857
1122334455
+ pcom iseg iset 115533
135
+ pcom iset icset 1e2t394857
12345
+ pcom icseg icset 115533
135
+ pcom icv icset 3111000
123
+ pcom pcseg iseg 02e5497
+ pcom iseg icseg
+ pcom icseg icset
12356
~$
~~~~

## COMPLEX TRANSLATIONS

# pcsisl - pitch class set to interval set list

## PURPOSE

List the isets that can `make` a pcset.

## SYNTAX

pcsisl pcset

## DESCRIPTION

The `pcsisl` command lists the isets that can `make` a pcset.
Realisations are linear.

## EXAMPLE

~~~~
$ pct pcsisl 012
1A
1
2B
B
$
~~~~

## NOTES

1.  Shell function.

# pdl - print duplicate lines

## PURPOSE

Print lines that appear more than once in a file.

## SYNTAX

pdl [ file ]

## DESCRIPTION

The `pdl` command prints all lines that occur more than once at stdin
to stdout.

A blank line is considered as a special case and is not printed.

Lines that appear more than twice are still only printed once.

## EXAMPLE

~~~~
$ pct pg 113 | pct pdl
113
131
311
$
~~~~

## NOTES

1.  There is an arbitrary limit to the length of lines.

# pfmt - pc object formating

## PURPOSE

Format a stream of pc objects.

## SYNTAX

pfmt [ n ]

## DESCRIPTION

`pfmt` formats streams of pc objects.  If `n` is given each set of `n`
input lines is printed on a single output line, else consecutive input
pc objects are written to the same output line as long as the pc
objects are of the same cardinality.  One important use is over sorted
streams of scs, where the output will be easily readable, however this
command is used very widely.

## EXAMPLE

~~~~
$ pct sb 4-11 | pct fn | pct pfmt
1-1
2-1 2-2 2-3 2-4 2-5
3-2 3-4 3-6 3-7
$ pct pvl 034 536 -c 23 | pct pfmt 3
304 536 232
$
~~~~

# pg - permutations generator

## PURPOSE

Write permutations of a set.

## SYNTAX

pg [ pcset ...  ]

## DESCRIPTION

The `pg` command writes the permutations of input sets to stdout.

## EXAMPLE

~~~~
$ pct pg 013
013
031
103
130
301
310
$
~~~~

## NOTES

1.  To permute multi-sets use
2.  Errors occur if a set has less than three elements.
This problem lies in the `pct/lib/pg.c` library.

# pi - pitch-class invariances

## PURPOSE

Print the transformations of a pcseg which have pc invariances
at specified locations.

## SYNTAX

pi pcseg index-set

## DESCRIPTION

Prints transformations of the pcseg entered as an argument in which a
pcset located arbitrarily across the pcseg remains invariant.  The
indices at which the set is located are given by `index-set`. The
index of the first element is zero.

## EXAMPLE

~~~~
$ pct pi 0236 12
0236
6320
532B
B235
$
~~~~

# po - pitch-class occurrences

## PURPOSE

Determine pc distribution.

## SYNTAX

po

## DESCRIPTION

The `po` command prints the number of occurrences of each pc at stdin,
and the percentage this is of the total number of pcs at stdin.  A
line is printed only for those pcs that occur at least once.

## EXAMPLE

~~~~
$ echo 123456343 | pct po
1     1  11.111111
2     1  11.111111
3     3  33.333333
4     2  22.222222
5     1  11.111111
6     1  11.111111
$
~~~~

## NOTES

1.  The input is examined character at a time.

# prt - partition pcsets

## PURPOSE

Writes n-part partitions of pcsets.

## SYNTAX

prt pcset n cset

## DESCRIPTION

The `prt` command writes the n-part partitions of a pcset.
It runs "partition pcset n cset | lsrt | sort -u".

## EXAMPLE

~~~~
$ pct prt 0235 3 12
0 2 35
0 23 5
0 25 3
02 3 5
03 2 5
05 2 3
$
~~~~

## NOTES

1.  Shell function.

# pup - print unique permutations

## PURPOSE

Write the unique permutations of input sets.

## SYNTAX

pup [ pcset ...  ]

## DESCRIPTION

The `pup` command prints the unique permutations of input sets to
stdout.

## EXAMPLE

~~~~
$ pct pup 112
112
121
211
$
~~~~

## NOTES

1.  Shell function.

# pvl - proximate voice leading

## PURPOSE

Determine possible proximate voice-leadings.

## SYNTAX

pvl [ -e ] [ -cicset ] [ -scset ] sc sc

- -e run exists search
- -c set constraint set
- -s set union size set

## DESCRIPTION

If `-e` is given then nothing is printed, the exit status is 0 if at
least one voice-leading exists, else 1.  `-c` sets the interval
constraint set.  `-s` sets the size of allowable union sets.  The
example script would print the trichordal set classes that can reach
one another by ic1 step motion.  The trivial case

## EXAMPLE

~~~~
$ cat ex.sh
(for a in $(pct fl 3)
do
    for b in $(pct fl 3)
    do
        if test $a = $b
        then
            break
        fi
        if pct pvl -e -c1 -s6 $a $b
        then
            echo $b $a
        fi
    done
done) | pct fsrt
$ sh ex.sh | wc -l
    12
$
~~~~

## SEE ALSO

Morris "Voice-Leading Spaces" MTS 20/2 1998.

# rdl - remove duplicate lines

## PURPOSE

Remove all but the first occurrence of lines from a file.

## SYNTAX

rdl [ -v ] [ file ]

- -v print the number of occurrences of each line.

## DESCRIPTION

The `rdl` command removes all duplicate occurrences of a line from
the file at stdin, writing the output to stdout.

## EXAMPLE

~~~~
$ pct pg 113 | pct rdl -v
113 (2)
131 (2)
311 (2)
$
~~~~

## NOTES

1.  There is an arbitrary limit both to the number of lines and to
their length.

# ri - row information

## PURPOSE

Prints information about the structure of a row.

## SYNTAX

ri row

## DESCRIPTION

The `ri` command requires a TTR as its only argument.
The program writes:

1.  The `INT()` of the row,
2.  A standard 12x12 matrix,
3.  A list of the discrete hexachords,tetrachords and trichords,
4.  A list of the imbricated hexachords,tetrachords and trichords.

~~~~
$ pct ri 1032674598BA
P=1032674598BA
INT(P)=B3B41914B3B

Matrix:
 0  B  2  1  5  6  3  4  8  7  A  9
 1  0  3  2  6  7  4  5  9  8  B  A
 A  9  0  B  3  4  1  2  6  5  8  7
 B  A  1  0  4  5  2  3  7  6  9  8
 7  6  9  8  0  1  A  B  3  2  5  4
 6  5  8  7  B  0  9  A  2  1  4  3
 9  8  B  A  2  3  0  1  5  4  7  6
 8  7  A  9  1  2  B  0  4  3  6  5
 4  3  6  5  9  A  7  8  0  B  2  1
 5  4  7  6  A  B  8  9  1  0  3  2
 2  1  4  3  7  8  5  6  A  9  0  B
 3  2  5  4  8  9  6  7  B  A  1  0


Discrete Subsets:
 Hexachords:
        <103267>           T0  6-5[012367]
        <4598BA>           TBI 6-5[012367]

 Tetrachords:
        <1032>             T0  4-1[0123]
        <6745>             T4  4-1[0123]
        <98BA>             T8  4-1[0123]

 Trichords:
        <103>              T0  3-2[013]
        <267>              T7I 3-4[015]
        <459>              T4  3-4[015]
        <8BA>              TBI 3-2[013]


Imbricated Subsets:
 Hexachords:
        <103267>           T0  6-5[012367]
        <032674>           T7I 6-Z10[013457]
        <326745>           T2  6-1[012345]
        <267459>           T2  6-8[023457]
        <674598>           T4  6-1[012345]
        <74598B>           T4  6-Z10[013457]
        <4598BA>           TBI 6-5[012367]

 Tetrachords:
        <1032>             T0  4-1[0123]
        <0326>             T0  4-12[0236]
        <3267>             T2  4-7[0145]
        <2674>             T7I 4-11[0135]
        <6745>             T4  4-1[0123]
        <7459>             T4  4-11[0135]
        <4598>             T4  4-7[0145]
        <598B>             TBI 4-12[0236]
        <98BA>             T8  4-1[0123]

 Trichords:
        <103>              T0  3-2[013]
        <032>              T3I 3-2[013]
        <326>              T2  3-3[014]
        <267>              T7I 3-4[015]
        <674>              T7I 3-2[013]
        <745>              T4  3-2[013]
        <459>              T4  3-4[015]
        <598>              T9I 3-3[014]
        <98B>              T8  3-2[013]
        <8BA>              TBI 3-2[013]
$
~~~~

# rs - relate sets

## SYNTAX

rs pcset pcset

## PURPOSE

Print the twelve-tone operation that relates two pcsets.

## DESCRIPTION

rs examines a pair of pcsets and prints the
twelve-tone operation that maps the first onto the second
if such an operation exists.
The operation is of the form TnMI.

## EXAMPLE

~~~~
$ pct rs 0123 e614
T1M
$ pct rs 0123 641e
T1M
$ pct rs 0123 641e416
T1M
$
~~~~

# rsg - relate segments

## SYNTAX

rsg pcseg [ pcseg ]

## PURPOSE

Print the serial operation that relates two pcsegs.

## DESCRIPTION

The `rsg` command examines a pair of pcsegs and prints the simplest
serial operation that maps the first onto the second, if such an
operation exists.  The operation is of the form `rmRTnMI`.

## EXAMPLE

~~~~
$ pct rsg 0123 05t3
T0M
$ pct rsg 0123 4e61
RT1M
$ echo e614 | pct rsg 0123
r3RT1M
$
~~~~

# rss - rotational sub-string

## SYNTAX

rss pcseg cset [ Number ]

## PURPOSE

Print a sub-pcseg through all rotations of a pcseg.

## DESCRIPTION

The user enters a pcseg and a cardinality set.  The cardinality set
defines the length of the subpcsegs to be printed.  In default usage
the subpcseg will start at the first element of the pcseg.
An integer argument is optional and specifies the element of
the pcseg at which to start the substring.  1 is the first element,
and default value.

## EXAMPLE

~~~~
$ pct rss 012345 4 2
<1234>
<2345>
<3450>
<4501>
<5012>
<0123>
$
~~~~

# rv - random variations

## SYNTAX

rv pcseg iset N

## PURPOSE

Write random varitions of a pcseg.

## DESCRIPTION

Generates N random variations of pcseg by transposing each element of
pcseg by a randomly selected element of iset.  The random seed is set
from the value given by the clock(3) function.  Due to computational
inefficiencies in permuting multi-sets this can be a practical method of
generating near-complete sets in some situations.

## EXAMPLE

~~~~
$ pct rv 012 01 3
113
123
013
$
~~~~

# sb - subsets

## PURPOSE

Write the subsets of a set of pcsets.

## SYNTAX

sb { sc ...} | { -l pcset ...}

- -l write literal shared subsets.

## DESCRIPTION

The `sb` command writes the sc subsets of a set of scs (ie.  those
scs which are abstract subsets of all scs).

The first example shows the scs that are subsets of both the
B and C type all-combinatorial hexachords.

The second example writes a list showing how many trichordal scs
each hexachordal sc includes (a list that shows, among other things,
that 6-Z17 is the only all-thrichord-hexachord).

## EXAMPLE

~~~~
$ pct sb 6-32 6-8 | pct fn | pct pfmt
1-1
2-1 2-2 2-3 2-4 2-5
3-2 3-4 3-6 3-7 3-9 3-11
4-10 4-11 4-14 4-22 4-23
5-23
$ for i in `cat ~/sw/pct/share/scs | pct cf 6 | pct fn` ; \
  do echo $i ; pct sb $i | pct cf 3 | wc -l ; done
6-1
6
6-2
9
6-Z3
9
...
$
~~~~

## NOTE

The first command above `sb 6-32 6-8` is equivalent to
`epmq -p "in sc 6-32" "in sc 6-8" < ~/sw/pct/share/scs`.

# scc - set class completion

## PURPOSE

Write pcsets that merge with a pcset to form a sc.

## SYNTAX

scc sc pcset

## DESCRIPTION

The `scc` command has a structure similar to ??.  It sets up a
referential sc _R_ entered by the user, if a pcset entered is an
abstract subset of _R_, the program prints the set of pcsets that can
merge with the input pcset to form a member of _R_.

## EXAMPLE

~~~~
$ pct scc 6-32 168
35A
49B
3AB
34B
$
~~~~

# scdb - set class data base

## PURPOSE

Identify significant uses and properties of scs.

## SYNTAX

scdb [ sc ...  ]

## DESCRIPTION

Searches the file `lib/scdb` for instances of the entered sc, and
prints the information found to stdout.  The _database_ is a very
simple file format, each line has a sc followed by whatever comments
are thought pertinent.  Users can modify the database at will, and
submit any extensions for addition to subsequent releases.

## EXAMPLE

~~~~
$ pct scdb 0146
4-Z15   All-Interval Tetrachord (see also 4-Z29)
$
~~~~

## NOTES

1.  See Morris "Recommendations" [-@Morris1994a] and Friedman "Ear
Training" [-@Friedmann1990a].

# scfa - set class frequency analysis

## PURPOSE

Do set class frequency analysis of a file.

## SYNTAX

scfa [ -i ] [ -oCharacter ] [ -r file ] [ -s ] [ -v ] file

- -i search inclusion relations
- -o set the delimiter character for pcsets (def.  "{")
- -r set the standard list file
- -s all comma or whitespace delimiters within pcsets
- -v set verbose mode

## DESCRIPTION

The `scfa` command allows the user to do sc frequency analysis on a text
file.  In default usage input is scanned for pcsets in the format
`{xyz}`.  The output lists the number of occurrences of each sc that
appears in the input, and the percentage that this is of the input
list as a whole.

The example line of output indicates that of the pcsets in the input
file five were members of 4-3[0134] and that this is 10.42 percent of
the total number of pcsets in the input file (there were therefore 48
pcsets in total).

If `-i` is given inclusion relations are searched also (ie.  012 is in
4-1).  When this option is used the output, though the same in
appearance, will obviously have a totally different analytic use.  If
this option has been invoked the first line of the output file will
state that this is the case.

Legal arguments to `-o` are `{`, `<`, `(`, and `[`.  A `-r` file with
a `.srt` extension has the file sorted by the middle column.

To use this command with an input file containing pcsets formatted
`(x,y,z)` the user must specify the options `-o` and `-s`.

The `-v` option lists all scs for which the input file was examined,
even when no instances were found.

## EXAMPLE

~~~~
$ pct el2vl < webern.nl | pct scfa
...
4-3[0134]          5              10.42%
...
$
~~~~

# scis - set class in pitch class segment

## PURPOSE

Find the locations of a sc within a pcseg.

## SYNTAX

scis pcseg sc

## DESCRIPTION

Indicates by bracketing the locations within each transformation-class
of a given pcseg where a given sc is located.

## EXAMPLE

~~~~
$ pct scis 23547091 013
P: [235]47091 23[547]091
I: [21B]09473 21[B09]473
M: A318[B09]5
MI: 29B4[103]7
$
~~~~

## NOTES

1.  I and MI are redundant.

# se - set expansion

## PURPOSE

Write multi-sets derived from an input set.

## SYNTAX

se [ -ccset ] [ -i ] [ -n ] [ pcset ...  ]

- -c specify a cardinality set to filter output.
- -i prepend a .  to each line.
- -n allow sets that do not include all elements of the input set.

## DESCRIPTION

The `se` command expands the pcset entered by repeating elements from
within the set.

In default operation the input set is expanded to
a maximum cardinality of six.

## EXAMPLE

~~~~
$ pct se -c4 23
2333
2233
2223
$
~~~~

## NOTES

1.  Could bail earlier on searches where pcset is large, but `-cX` is small.

# sep - separate

## PURPOSE

Separate pc objects.

## SYNTAX

sep [ -c ] [ -l ] [ -r ] [ -s ] [ -t ] file

- -l line delimited
- -r reverse operation
- -s space delimited
- -t tab delimited

## DESCRIPTION

The `sep` command separates pc objects by replacing
an internal delimiter by an external delimiter.

Its purpose is to allow for wider use of the array
processing commands.

The command behaviour is given by an option, `sep -l` reads a new-line
delimited file and writes an epmty-line delimited file, the `-c` `-s`
and `-t` options replace colons, spaces, and tabs by newline
characters repsectively.

The `-r` option is the reverse, thus `sep -t -r` makes a tab
delimited file from a new-line/empty-line delimted file.

The default option is `-l`.

## EXAMPLE

~~~~
$ echo "0 01 02" | pct sep -s
0
01
02
$ echo "0 01 02" | pct sep -s | pct sep -t -r
0       01      02
$
~~~~

## NOTES

1.  Needs look-ahead to not write delimiter at end of line.

# sgdb - segment data base

## PURPOSE

Identify significant uses/properties of pcsegs.

## SYNTAX

sgdb [ -a ] [ -s ] [ pcseg ...  ]

- -a allow all sros to be recognized (ie rxRTxMI).
- -s search for sub-segments.

## DESCRIPTION

Searches the file `lib/sgdb` for instances of the entered pcseg (or
RTnI transformations thereof), and prints the information found to
stdout.  The database is very simple, each line has a pcseg followed by
whatever comments are thought pertinent.  Users can modify the
database at will, contributions and fixes are welcome.

## EXAMPLE

~~~~
$ pct sgdb -s t463
<023475B6A981> Babbitt, Milton "Two Sonnets" (1955)
<06857B439A12> Schoenberg, Arnold "Variations for Orchestra, Op.  31"
$
~~~~

# sgi - segment info

## PURPOSE

Prints information about the structure of a pcseg.

## SYNTAX

sgi [ pcseg ...  ]

## DESCRIPTION

The `sgi` command accepts any number of pcsegs as arguments.
The program prints:

1.  the interval succession of the pcseg
2.  a T-matrix of the pcseg, see [@Morris1989a]
3.  a list of the imbricated sub-segments

## EXAMPLE

~~~~
$ pct sgi 0136
4-13[0136]      P=<0136>
INT(P)=<123>    T0MP=<0536>

        0136
        B025
        9A03
        6790

#2 : 2-1 2-2 2-3
#3 : 3-2 3-7
$
~~~~

# sgr - segment reorder

## SYNTAX

sgr n

## PURPOSE

Reorder pcsegs by taking every nth element.

## DESCRIPTION

The `sgr` command reorders the pcsegs at stdin by taking every _n_th
element of the input pcseg, with corrected wrap-around.

## EXAMPLE

~~~~
$ echo 1234 | pct sgr 2
1324
$ echo 12345 | pct sgr 2
13524
$
~~~~

# si - set information

## PURPOSE

Print information about a pcset.

## SYNTAX

si [ pcset ...  ]

## DESCRIPTION

The `si` command writes:

1.  The sorted input set
2.  The set-class of which the set is a member, and its TnI level
3.  The interval-class vector of the set
4.  The TICS vector of the set
5.  The literal and abstract complement of the set
6.  The M5 transformation of the set (ie T0M(P))

## EXAMPLE

~~~~
$ pct si 0136
PCS: {0136}
SC: T0  4-13[0136]
ICV: <4112011>
TICS: <221220320200>
Complement: {245789AB} (TBI 8-13)
M5 Transform: {0536} (T6I 4-13)
$
~~~~

# sidb - sc to iset database generator

## PURPOSE

Generate an sidb (sc to iset database).

## SYNTAX

sidb sc ...

## DESCRIPTION

The `sidb` command generates files named `sidb/sc` which contain the isets
that can form each named sc.  If the directory `sidb` does not exist it
is created, if it does exist all files below it are deleted.  To query
the database you can use the pct file comparison command.  The
example dialogue shows one way to determine what, if any, isets can
generate the hexachords 6-1, 6-7, 6-8 and 6-32.

## EXAMPLE

~~~~
$ pct sidb 6-1 6-32 6-7 6-8
making directory sidb
generating sidb/6-1
...
$ pct fc sidb/*
1247
58AB
$
~~~~

## NOTES

1.  Shell script.

# sis - pitch class set in pitch class segment

## PURPOSE

Find the transformations of a pcseg that contain a pcset.

## SYNTAX

sis pcseg pcset

## DESCRIPTION

Indicates by bracketing the locations within each transformation
of a given pcseg that contains a given pcset as a sub-segment where
the sub-segment is located.

## EXAMPLE

~~~~
$ pct sis 024579b 1357
8A0[1357]
[7531]0A8
0A8[7531]
[1357]8A0
642[7531]
[1357]246
246[1357]
[7531]642
$
~~~~

## NOTES

1.  Formatting should include `TnI` information.

# sp - supersets

## PURPOSE

Write the literal supersets of a set of pcsegs.

## SYNTAX

sp [ -rN ] [ -m ] pcseg ...

## DESCRIPTION

The `sp` command writes the supersets formed by all possible
combinations of a set of pcsets.

If `-r` is given prints _nCr_, else all values of _r_ in _nCr_ are given.

If `-m` is given the the pcsegs are __not__ merged into a single
pcset, but given each separately, this option can be used to generate
complete graphs for arbitrary sets of pcsegs.

## EXAMPLE

~~~~
$ pct sp 05 04 23 | pct cf 4
0235
0234
$ pct sp -r2 -m 05 04 23
05 04
05 23
04 23
$
~~~~

# spsc - super-set-class

## PURPOSE

Write the smallest supersets of a set of scs.

## SYNTAX

spsc [ -a ] sc ...

## DESCRIPTION

This command requires at least one sc as an argument.  It prints
the smallest cardinality scs that abstractly include all the
scs entered as arguments.  `-a` disables the "smallest only" feature
and writes all scs that qualify.

## EXAMPLE

~~~~
$ pct spsc 4-11 4-12
5-26[02458]
$ pct spsc 3-11 3-8
4-27[0258]
4-Z29[0137]
$ pct spsc `pct fl 3`
6-Z17[012478]
$ pct spsc `pct fl -c 4`
8-Z15[01234689]
8-Z29[01235679]
$
~~~~

# sra - stravinsky rotational array

## SYNTAX

sra

## PURPOSE

Make Stravinsky rotational arrays.

## DESCRIPTION

The `sra` command reads pcsegs (with optional partitioning information)
at stdin and writes rotational arrays to stdout.

For a description of sras see [@Morris1991a, p.98].

The example is from: Stravinsky "A Sermon, Narrative, and a Prayer".

## EXAMPLE

~~~~
$ echo 0:1:9:B:A:7 | pct sra
0:1:9:B:A:7
0:8:A:9:6:B
0:2:1:A:3:4
0:B:8:1:2:A
0:9:2:3:B:1
0:5:6:2:4:3
$
~~~~

## NOTES

1.  If the initial set is not at zero?

# sro - serial operation

## PURPOSE

Apply a sro to a pcseg.

## SYNTAX

sro sro

## DESCRIPTION

sro uses `sro` to transform pitch-class segments at stdin.

## EXAMPLE

~~~~
$ echo 024579 | pct sro RT4I
79B024
$
~~~~

## NOTES

1.  A serial operator has the form: [rx][R]Tx[M][I].

# tics - tics vector

## SYNTAX

tics [ pcset ...  ]

## PURPOSE

Print the tics vector of a pcset.

## DESCRIPTION

The `tics` command prints the tics vectors of pcsets.

## EXAMPLE

~~~~
$ pct tics 024579
325052341614
$
~~~~

## NOTES

1.  Should have a reverse operation, as in `icv`.

# tmatrix - generate a T-matrix

## PURPOSE

Generate the T-matrix for a pcseg.

## SYNTAX

tmatrix [ -h ] [ pcseg ...  ]

- -h: write a half matrix.

## DESCRIPTION

Writes the transposition matrix of a pcseg and itself.

## EXAMPLE

~~~~
$ pct tmatrix -h 0147
0147
 036
  03
   0
$
~~~~

# trl - transformations list

## PURPOSE

Write transformations of a pcseg.

## SYNTAX

trl [ +m|R|r ] [ -m|R|r ] pcseg ...

## DESCRIPTION

The `trl` command writes transformations of a pcseg.  Duplications
introduced by symetries are not removed.  By default the 48 RTnI
transformations are written.  The R(etrograde), M(ultiplication), and
r(rotation) transformations can be turned on, `+`, or off,
`-`, from the command line.

## EXAMPLE

~~~~
$ pct trl -R -m -r 013 | pct pfmt 12
013 124 235 346 457 568 679 78A 89B 9A0 AB1 B02
0B9 10A 21B 320 431 542 653 764 875 986 A97 BA8
$ pct trl +m +R -r 015 | wc -l
    96
$
~~~~

## NOTES

Options arguments do not apply to pcseg arguments that
precede them.

# trs - transformations search

## PURPOSE

Search for a segment within all transformations of a pcseg.

## SYNTAX

trs [ -m ] pcseg
-m exclude M5 operation

## DESCRIPTION

The `trs` command searches for pcsegs within the 96 RTnMI
transformations of a pcseg.

## EXAMPLE

~~~~
$ for i in $(echo 642 | pct trs 024579 | sort -u) ; \
  do echo $i "--" $(pct rsg 024579 $i)"(P)" ; \
  done
531642 -- T5M(P)
6421B9 -- RT9(P)
642753 -- T6M(P)
B97642 -- RT2(P)
$
~~~~

# tto - twelve tone operations

## PURPOSE

Apply the twenty-four `ttos` to a pcset.

## SYNTAX

tto pcset

## DESCRIPTION

Transforms a pcset by the set of `ttos`.

## EXAMPLE

~~~~
$ pct tto 024579
024579
13568A
...
13568A
24679B
$
~~~~

## NOTES

1.  Shell script.

# uis - union and intersection of sets

## PURPOSE

Write the union and intersection for sc pairs.

## SYNTAX

uis sc [ sc ]

## DESCRIPTION

The `uis` command reads two scs (a and b) as arguments.

It prints the union and intersection of a and b for all TnI.

Columns one and two give a and b, column three shows the union of
a and b, and column four shows the intersection of a and b.

If sc b is not specified, it is set to a.

The internal symmetry of both a and b is considered.

## EXAMPLE

~~~~
$ pct uis 3-11 | pct fsrt 4 | egrep "2-"
T0  3-11       T3I 3-11       T7  4-20[0158]           T0  2-3[03]
T0  3-11       TAI 3-11       T7  4-26[0358]           T3  2-4[04]
T0  3-11       T7I 3-11       T0  4-17[0347]           T7  2-5[05]
$
~~~~

# mfa - midi file analysis

The source code in the `src/mfa` directory builds commands that do
simple pitch-class analysis of musical information stored in midi files.
They share the `src/lib` libraries with the base pct commands,
however the mfa sources are not maintained, and are stored separately.

# morris - Robert Morris

The `src/morris` directory contains code to implement functions
defined by Robert Morris in _Composition with pitch-classes_
[@Morris1987a] and _Class Notes_ [@Morris1991a].  All such functions
are defined in the file `morris.c`.

All library files are named with an m prefix and are stored in
`morris/src/lib`.  The file `mtypes.h` contains type definitions and
defined values.  The file `mlib.h` is a union of all library headers.
The input/ouput system is named mio, and converts between pitch-class
objects and ASCII strings.

Commands are very simple command line interfaces to the functions they
are named for, and are stored in file `function_name.c`.

All library files have a corresponding test file, named with a t prefix.
These test programs can be compiled with the command `make tests`,
and will print output to stdout.

The system can be cleaned with the command `make clean`.

Level three (code level) documentation is produced from the source files
using the `c2man` program.  A shell script exists to autogenerate
all such documentation, and is named l3docs.sh (type `make docs`).

Level one (command level) documentation does not generally exist,
however each command has a usage page.

The term serial-operator (SO) is used to refer to combinations of the
operators Tn, I, M, R, and r. This is not strictly in keeping with
Morris's use since he excludes the operator M (see Morris 1991:
p16n2).

These commands are completely independant of the libraries shared by all
other commands and represent a different approach to materials, the work
is unfinished and likely to remain so, although those commands
implemented work well.

# T-matrix - transposition matrix

## PURPOSE

Make the T-matrix for a pcseg pair.

## SYNTAX

T-matrix A B

## DESCRIPTION

Write the T-matrix for the pcsegs A and B.

## EXAMPLE

~~~~
$ pct T-matrix 03 359
T-matrix(03,359)=
359
026
$
~~~~

## NOTES

1.  See [@Morris1987a].

# csrt - cardinality sort

## PURPOSE

Sort lines of a text file by cardinality.

## SYNTAX

csrt [ Number...  ] [ file...  ]

## DESCRIPTION

The command `csrt` is a file filter.  Lines at stdin are sorted by
cardinality.  A cardinality is a single hexadecimal digit (less than
twelve) preceded by a hash sign.  If a line does not contain a
cardinality marker it is discarded.  An integer argument is optional.

The command `csrt 3 file` would sort _file_ by the third cardinality of
each line.  In this case if a line does not contain three
cardinalities it is discarded.

## NOTES

1.  The command determines whether an argument is a position indicator or
a filename by its length.  Arguments of 1 or 2 characters are considered
position indicators, longer arguments are considered filenames.

# el2nl - event-list to note-list

## PURPOSE

Convert an event-list into a note-list.

## SYNTAX

mf2el [ file ]

## DESCRIPTION

The command `el2nl` converts the event-list at stdin to a note-list
at stdout.

The format of the text files are described in the pages  and

# el2tl - event-list to transference-list

## PURPOSE

Analyse event-lists for sets formed by successive chords.

## SYNTAX

el2tl cset [ file ]

## DESCRIPTION

The command `el2tl` converts an event list into a transference list.

It requires a cardinality set as an argument.

The output lists all verticalities, and the sets formed by successive verticalities.

The number of verticalities grouped to form each transference set
is specified on the command line by the cset (transference cardinality)
argument.

The command `el2tl 23 input.el` would list firstly the
transference sets formed by two successive verticalities, then those
formed by three.

## NOTES

- The extension `.el` conflicts with emacs lisp files.

# el2vl - event-list to verticality-list

## PURPOSE

List each successive verticality of the input event-list.

## SYNTAX

el2vl [ -b ] [ file ]

- -b Turn the bar marking facility off.

## DESCRIPTION

The command `el2vl` lists each successive verticality (chord) of the input
event-list.  The command is designed to extract harmonic information
from the event-list.  A chord list contains 5 columns.

1.  the number of the chord,
2.  the cardinality of the chord (not including duplicated pcs),
3.  the pcset formed by the chord,
4.  the TnI and forte name of the chord, and
5.  the ratio between the number of notes within the current chord
    that were struck to create this chord, and the total number of
    notes in the chord.

## NOTES

1.  Time signature changes are not handled.

# gT-matrix - graph T-matrix

## PURPOSE

Make a graph description file for a T-matrix.

## SYNTAX

gT-matrix A B

## DESCRIPTION

gT-matrix shows the same information as  but produces
output in the graph description language of dot.

## EXAMPLE

~~~~
$ pct gT-matrix 03 359 | dot -Tps > /tmp/g.ps ; gv /tmp/g.ps
~~~~

## NOTES

1.  Should be extended, or another program written, to allow chains
of sets to be graphed (a->b->c etc).   dot

# mf2el - midi-file to event-list

## PURPOSE

Convert a midi file into a plain text file.

## SYNTAX

mf2el [ file ]

## DESCRIPTION

The command `mf2el` converts a midi file to a plain text file.  The format
of the output text file is straightforward, and is labeled an
event-list.  The midi file is expected at stdin and the event-list is
written to stdout.

## NOTES

1.  This command uses a midi file reader written by Tim Thompson and
modified by Michael Czeiszperger and the author.

2.  Most commands in this package require that the event list be
sorted into time-ascending/pitch-ascending order.  Therefore if the
input file contains more than a single track it is recommended the
following command structure be used: `mf2el` input.mf | srtel >
`output.el`.

# nl2ps - note-list to pitch-class succession

## PURPOSE

Print the pitch-class succession of a note list.

## SYNTAX

nl2ps [ file ]

## DESCRIPTION

Prints the successive pcs of the note-list at stdin, pcs that
occur simeltaneously are enclosed in parentheses.

## NOTES

1.  The ps suffix is in conflict with the standard postscript suffix.

# nl2sl - note list to set list

## PURPOSE

Print the imbricated sets from a note list.

## SYNTAX

nl2sl cset [ file ]

## DESCRIPTION

Writes the overlapping sets in the input segment.

# srtel - sort event list

## PURPOSE

Sort an event list into time/pitch ascending order.

## SYNTAX

srtel [ file ]

## DESCRIPTION

The command `srtel` sorts an event list into time ascending order.
Equal time events are sorted into pitch ascending order.
The sorted list is written to stdout.

## NOTES

1.  Most commands in this package require that the event list be
sorted into time-ascending/pitch-ascending order.  Therefore it is
recommended the following command structure be used: `mf2el input.mf |
srtel > output.el`.

# srtnl - sort note list

## PURPOSE

Sort a note list into time/pitch ascending order.

## SYNTAX

srtnl [ file ]

## DESCRIPTION

The `srtnl` command sorts a note list into time ascending order.
Equal time notes are sorted into pitch ascending order.
The sorted list is written to stdout.

## NOTES

1.  If a file contains hdr changes (multiple header lines throughout
the list) these changes will be lost in the sorting process.

# svl - struck verticality-list

## PURPOSE

Get only struck verticalities from a vl file.

## SYNTAX

svl [ file ]

## DESCRIPTION

Extract only those verticalities that are `struck` from a
verticality-list (vl file).  A struck verticality is one in which all
contributing notes are struck to form the verticality.

## NOTES

1.  This command is a simple text filter printing all lines that
contain the pattern x/x where x is any character.

## Libraries reference

# libcg - combinations generator

## PURPOSE

Calculate combinations of a set.

## SYNTAX

\#include "libpct.h"

## TYPES

~~~~
Cg_t;
~~~~

## FUNCTIONS

~~~~
Cg_t *cgopen(int n,int r,int *s);
int cgnext(Cg_t *c,int *s);
void cgclose(Cg_t *c);
~~~~

## DESCRIPTION

The cg library implements integer array combinations.
`cgopen` allocates memory and sets values for `n` (the
number of elements to choose from), and `r` the number
of elements to choose.
`s` is the initial set, if `NULL` an ascending set of `n`
places is used.
`cgnext` calculates the next combination until all
are exhausted, indicated by the return value FALSE.
`cgclose` frees memory.

## EXAMPLE

~~~~
void print_object_combinations(Pco_t *obj) {
  int i;
  Pco_t *s;
  Cg_t *c;

  s=pco_open("pcset");
  for(i=1;i<=obj->plen;i++) {
    c=cgopen(obj->plen,i,obj->p);
    while((s->plen=cgnext(c,s->p))) {
      pco_put(s,stdout);
    }
    cgclose(c);
  }
  pco_close(s);
}
~~~~

# libepm - evaluate pitch-class materials

## PURPOSE

Evaluate a pitch-class segment against a set of conditions.

## SYNTAX

\#include "libpct.h"

## TYPES

~~~~
Epm_t;
~~~~

## FUNCTIONS

~~~~
Epm_t *epmopen(char *name,char *inclusion);
void epmclose(Epm_t *e);
int epmeval(Epm_t *e,int *x,int xlen);
int epmparse(Epm_t *e,char *str);
int epmstr(Epm_t *e,int n,char *str);
int epmread(Epm_t *e,FILE *fp);
int epmwrite(Epm_t *e,FILE *fp);
int epmrmv(Epm_t *e,char *str);
~~~~

## MACRO FUNCTIONS

~~~~
epmsetincltype(e,i);
~~~~

## DESCRIPTION

`epmopen` allocates memory.  _name_ is decorative, _inclusion_ may be
`proper` or `not-proper`.  `epmclose` frees memory.  `epmparse` parses
a set of conditions at `str` and adds it to _e_, returns the number of
objects read.  `epmeval` evaluates _x_ against _e_, returns `-1` on
success, else the number of the condition _x_ failed.  `epmstr` writes
the _n_th condition at _e_ to _str_.  `epmread` reads a condition set
at _fp_ into _e_, returns the number of conditions read.  `epmwrite`
writes the condition set at _e_ to _fp_.  `epmrmv` removes the
condition at _str_ from _e_, returns the number of conditions removed.
`epmsetincltype` sets the inclusion type of the allocated structure
_e_ to _i_.

## NOTES

This is not current.

# ftbl - forte table library

## PURPOSE

Calculate and resolve forte names for set-classes.

## SYNTAX

\#include "libpct.h"

## FUNCTIONS

~~~~
int forte_name(char *name,const char *residue);
int resolve_name(char *residue,const char *name);
int pcs_to_name(char *name,const int *p,int plen);
int name_to_pcs(int *p,int *plen,const char *name);
~~~~

## DESCRIPTION

These functions calculate and resolve names (typically the forte
names) for set-classes.  `forte_name` writes the name of the
prime-form pcset at _residue_ to _name_.  `resolve_name` writes the
prime-form of the sc at _name_ to _residue_. `pcs_to_name` writes the
name of the pcset at _p_ to _name_ by translating _p_ into a
prime-form string and calling `forte_name`.

`name_to_pcs` writes the set-class named at _name_ to the pcset _p_ by
translating _p_ into a prime-form residue string and calling
`resolve_name`.

## NOTES

1.  The prime forms are as in [@Forte1973a], not as in [@Rahn1980a].

# libpco - pitch-class objects

## PURPOSE

Provide services for storing and manipulating pc objects.

## SYNTAX

\#include "libpct.h"

## TYPES

~~~~
Pco_t;
~~~~

## FUNCTIONS

~~~~
Pco_t *pco_create(const char *type);
void pco_free(Pco_t *p);
int pco_validate(Pco_t *obj);
void pco_copy(Pco_t *dst, const Pco_t *src);
Pco_t *pco_dup(const Pco_t *src);
void pco_swap(Pco_t *a, Pco_t *b);
int pco_read(Pco_t *p, const char *str);
int pco_write(const Pco_t *p, char *str);
int pco_write_pretty(const Pco_t *p, char *str);
int pco_get(Pco_t *obj, FILE *stream);
int pco_put(const Pco_t *obj, FILE *stream);
int pco_put_pretty(const Pco_t *obj, FILE *stream);
void pco_tf(Pco_t *obj, int *tf);
int pco_sro_apply(Pco_t *obj, const rrtnmi_t *op);
int pco_sro_derive(const Pco_t *from, const Pco_t *to, rrtnmi_t *op);
int pco_has_element(const Pco_t *obj, int n);
void pco_union(Pco_t *r, const Pco_t *a, const Pco_t *b);
void pco_intersection(Pco_t *r, const Pco_t *a, const Pco_t *b);
void pco_complement(Pco_t *r, const Pco_t *a);
void pco_cat(Pco_t *dst, const Pco_t *src);
int pco_map(Pco_t *dst, int type, const Pco_t *src);
int pco_convert(Pco_t *p, int type);
int pco_n_cmp(const Pco_t *a, const Pco_t *b,int n);
int pco_cmp(const Pco_t *a, const Pco_t *b);
int pco_match(const Pco_t *obj, const char *pattern);
~~~~

## MACRO FUNCTIONS

~~~~
#define pco_sizeof(a) ((a)->size)
#define pco_typeof(a) ((a)->type)
#define pco_element(a,n) ((a)->data[n])
#define pco_settype(a,b) ((a)->type=(b))
#define pco_setopt(a,b) ((a)->opt|=(b))
~~~~

## DESCRIPTION

This library provides support for typed pitch class objects.

An object is a residue string and a type.  Supported types are listed
at `pco_create` allocates memory for an object of type `type.` Use
"empty" where the type is unknown.  `pco_free` frees memory.
`pco_validate` ensures that `obj` is a valid object for the type
argument (ie.  that a set has no duplicate elements, that an
interval-class-vector has seven places, and so on).  The object is
silently fixed where possible, else an error message is written on
stderr.  `pco_copy` copies `src` into `dst.` `pco_dup` allocates
memory and duplicates `src.` `pco_swap` swaps `a` and `b`.  `pco_read`
translates the text `str` into the pco `obj`.  `str` may be either
"type object" or simply "object".  If no `type` is given, the object
inherits the previous type.  If a `type` is given an option is set for
`pco_write` to write the name of the object type.  The value of `type`
may be any alphabetic string that does not begin with a residue
character (A,t,B,e).  The library functions `pco_map` and `pco_cmp`
rely upon the value of `type`, which must be a valid pc object name.
`pco_write` puts `obj` into the text string `str`. By default it will
write "object"; if an internal option is set it will write "type
object".  In the typical case of succesive `pco_read` and `pco_write`
calls, the output format will mirror the input format.
`pco_write_pretty` puts `obj` into the text string `str`, but
prettily.  For sets it writes the residue string between braces, for
segments it writes the residue string between angled brackets, for
set-classes it writes the forte name, and for vectors it writes the
residue string between brackets.  `pco_get` gets the next line from
`stream` and calls `pco_read`.  `pco_put` calls `pco_write` and sends
the string to `stream`.  `pco_put_pretty` calls `pco_write_pretty` and
sends the string to `stream.` `pco_tf` applies the transfer-function
`tf` to `obj.` `tf` is an array of twelve places giving the mapping
for each pitch-class.  Transposition by one is given by
`1,2,3,4,5,6,7,8,9,10,11,0` etc.  `pco_sro_apply` transforms `obj` by
`s`.  `pco_sro_derive` derives the transformation to send `from` to
`to`.  `pco_map` does pc object translations.  The pc object `src` as
promoted to type `type` is written to `dst`. The translation must be
`simple` but can be `indirect`. Mappings to the same type are valid.
If `dst` and `src` are pointers to the same object, I think that
should work.  If this function fails, the contents of `dst` are
undefined.  `pco_cmp` compares `a` with `b` __as mapped into an object
of type__ `a`.  `pco_n_cmp` compares `a` with `b` __as mapped into an
object of type__ `a` to `n` places.  `pco_match` matches a pco with a
regular expression - requires posix.2 features.  `pco_cat` catenates
`src` to `dst`. The type of `dst` is not changed.  `pco_union` copies
`a` into `t`, catenates `b` and `r`, and calls `pco_validate` on `r.`
`pco_intersection` writes the intersection of `a` and `b` to `r`.
Writes a warning if the sets are ordered.  `pco_complement` writes the
complement of `a` to `r.` Writes a warning if the set is ordered.
`pco_typeof` evaluates to the type constant of `a`.  `pco_sizeof`
evaluates to the size of (number of elements in) `a`.  `pco_elementof`
evaluates to the `nth` element of `a`.  `pco_settype` sets the type of
`a` to `t`.  `pco_setopt` `|=s` `b` into the option field of `a.`

## EXAMPLE

~~~~
See `src/cmd/sro.c`.
~~~~

## NOTES

Interval-vector mappings are un-implemented.

# libpg - permutations generator

## PURPOSE

Calculate permutations of a set.

## SYNTAX

\#include "libpct.h"

## TYPES

~~~~
Pg_t;
~~~~

## FUNCTIONS

~~~~
Pg_t *pgopen(int *s,int slen);
int pgnext(Pg_t *e,int *s);
void pginit(Pg_t *e,int *s,int slen);
void pgclose(Pg_t *e);
~~~~

## DESCRIPTION

The pg library implements integer array permutations.  `pgopen`
allocates memory and sets an initial integer-set.  `pgnext`
calculates the next permutation until all are exhausted, indicated by
the return value FALSE.  `pginit` re-initializes the generator
`e`.  `pgclose` frees memory.

## EXAMPLE

~~~~
void print_object_permutations(Pco_t *obj) {
  Pg_t *e;

  e=pgopen(obj->p,obj->plen); assert(e);
  while((obj->plen=pgnext(e,obj->p))!=0) {
    pco_put(obj,stdout);
  }
  pgclose(e);
}
~~~~

## NOTES

1.  Errors occur if a set has less than three elements.

# ppca - partioned pitch class arrays

## PURPOSE

Provide services for storing and manipulating partitioned pc arrays.

## SYNTAX

\#include "libpct.h"

## TYPES

~~~~
Ppca_t;
~~~~

## FUNCTIONS

~~~~
Ppca_t *paopen(char *name);
void paclose(Ppca_t *e);
int paread(Ppca_t *e,FILE *fp);
int pawrite(Ppca_t *e,FILE *fp);
int pacopy(Ppca_t *p,Ppca_t *q);
int pacolumn(Ppca_t *e,int n,int *q,int *qlen,int action);
int parow(Ppca_t *e,int n,int *q,int *qlen,int action);
int paposition(Ppca_t *e,int r,int c,int *q,int *qlen,int action);
int pasro(Ppca_t *e,rrtnmi_t o);
int padistr(Ppca_t *p,int *d);
~~~~

## DESCRIPTION

The ppca library provides an interface to manipulate arbitrarily
partitioned arrays.  `paopen` allocates memory.  `paclose` frees
memory.  `paread` reads an array from the stream _fp_ into _e_.
Consecutive new-line characters terminate an array, lines where the
first character is a hash sign are ignored.  `pawrite` writes the
array `e` to the stream `fp`.  `pacopy` copies the array `q` to `p`.
`pacolumn` reads from (if `action` is `PPCA_READ`) and writes to (if
`action` is `PPCA_INSERT`) the `nth` column of the array `e`.  When
inserting a pc object the partition map is not altered, typically the
new column should be the same size as the old.  `parow` reads from (if
`action` is `PPCA_READ`) and writes to (if `action` is `PPCA_INSERT`)
the `nth` row of the array `e`.  When inserting a pc object the
partition map is not altered, typically the new row should be the same
size as the old.  `paposition` reads from (if `action` is `PPCA_READ`)
and writes to (if `action` is `PPCA_INSERT`) the `nth` position of the
array `e`.  When inserting a pc object the partition map is updated
and subsequent positions shifted appropriately.  `pasro` transforms
the array `e` by the serial operation `o`.  `padistr` puts the
distribution map of `p` into `d`.

## EXAMPLE

~~~~
See `pct/src/cmd/asro.c`.
~~~~

# libsro - serial-operator library

## PURPOSE

Provide services for storing and manipulating serial operators.

## SYNTAX

\#include "libpct.h"

## TYPES

~~~~
Sro_t;
~~~~

## FUNCTIONS

~~~~
int sro_write(Sro_t *sro, char *str);
int sro_read(Sro_t *sro, char *str);
int sro_derive(Sro_t *sro, Pco_t *from, Pco_t *to);
void sro_apply(Sro_t *sro, Pco_t *pco);
void sro_put(Sro_t *sro, FILE *stream);
void sro_get(Sro_t *sro, FILE *stream);
~~~~

## DESCRIPTION

`sro_read` reads an operator at _str_ and writes to _op_.
`sro_get` gets the next line from stdin and calls `sro_read`.
`sro_write` writes the operator at _op_ to _str_.
`sro_put` calls `sro_write} and writes the result to `stream`.
`sro_derive` derives the operator _op_ such that `op(from)=to`.
`sro_apply` applies the operator _op_ to the pc object `obj`.

# libtr - transformations

## PURPOSE

Cycle through the serial transformations of a pcseg.

## SYNTAX

\#include "libtr.h"

## TYPES

~~~~
tr_t;
~~~~

## FUNCTIONS

~~~~
tr_t *tr_init(const int *p,int plen,int R,int m,int r);
int tr_next(tr_t *t,int *p,int *plen);
void tr_free(tr_t *t);
~~~~

## DESCRIPTION

`tr_init} allocates memory.  `p` is the initial segment,
`R} sets if retrogrades are to be given, `m` sets if
multiplication is to be done, `r` sets if rotation is to be done.
`tr_next` gets the next transformation.  Evaluates false after
the last transformation, and cycles back to the initial set.
`tr_free` frees memory.

# el - event list

## PURPOSE

A simple and accessible plain text file format to store midi data.

## DESCRIPTION

The event-listl file format is very simple.  Lines are of 3 types.

1.  header lines.
2.  event lines.
3.  blank lines.

The first line must be a header line.  Header lines have 7 arguments:
<<MidiHdr: t1 t2 ts1 ts2 ks1 ks2>>

- title: "MidiHdr:" *this must be the first 8 characters of the file*
- tempo1: tempo in beats per minute
- tempo2: tempo in times stamps per beat
- timesignature1: numerator
- timesignature1: denominator
- keysignature1: quality (major or minor)
- keysignature2: degree (number of sharps or flats)

Events have four arguments: <<ts stat d1 d2>>

- ts: timestamp: the time at which the event occurs
- status: the type of the event (note-on and note-off events only (ie 90 or 80)) *hexadecimal*
- data1: pitch: the midi note number *hexadecimal*
- data2: dynamic: the midi velocity number *hexadecimal*

## NOTES

The value of the analysis performed by commands in the mfa package is to
a degree dependent upon the ordering of the event-list.  It is very
useful for the file to be ordered in both time and pitch ascending
order.  This means that if the midi file used as input to the mf2el
command contains multiple tracks, or if the program that created the
midi file did not write struck verticalities out in pitch ascending
order the output from the `mf2el` command should be sorted using the `srtel`
command.  References to event-lists in other documentation assume that
the list is so sorted unless otherwise stated.

# epm - epm

## PURPOSE

Encoding of `conditions` for evaluating pitch-class objects.

## SYNTAX

relation type object-list

## DESCRIPTION

A _condition_ has three parts a _relation_ operator, a _type_
descriptor, and an _object-list_.  The _relation_ may be: `is`,
`isnt`, `in`, `notin`, `has`, or `hasnt`.  _object-list_ is a set of
modulo-twelve residue strings.  A condition is satisfied for a pcseg
_A_ if _A_ stands in the relation `relation` to __at least one__ of
the pc objects in _object-list_, which are of type _type_.  A _set_ of
conditions _S_ is satisfied for the `pcseg` _B_ if _B_ satisfies
__all__ of the conditions at _S_.  Note that the condition `hasnt sc
2-3 2-5` means "does not include an ic three __or__ does not include
an ic five" and not "does not include an ic three or five".  That must
be given as two conditions, ie `hasnt sc 2-3` and `hasnt sc 2-5.`

The example below writes the pcsets that are:

1. subsets of the C-major scale __or__ the E flat major scale, __and
2. include an F, __and
3. are an abstract subset of the complement of the C-major collection, __and
4. do not have the possibility of spanning a minor-third, __and
5. do not have the possibility of spanning a perfect-fourth.

## EXAMPLE

~~~~
$ cat file
in pcset 024579e 3578t02
has pcset 5
in sc 5-35
hasnt sc 2-3
hasnt sc 2-5
$ pct epmq -ffile < ~/sw/pct/share/univ
5
35
57
59
357
579
$
~~~~

## NOTES

1. For the kind `cset` the only valid relation is `in`.

2. Individual commands select whether to treat inclusion relations as
_proper_ or not, where `{012}` is not _properly_ in `012`.

# nl - note-list

## PURPOSE

A simple and accesible text based format to store midi data.

## DESCRIPTION

The nl file format is very simple.  Lines are of 3 types.

1.  header lines
2.  note lines
3.  blank lines

The first line must be a header line.  Header lines have 7 arguments:
<<MidiHdr: t1 t2 ts1 ts2 ks1 ks2>>

- title: "MidiHdr:".  This must be the first 8 characters of the file
- tempo1: tempo in beats per minute
- tempo2: tempo in times stamps per beat
- timesignature1: numerator
- timesignature1: denominator
- keysignature1: quality (major or minor)
- keysignature2: degree (number of sharps or flats)

Notes have five arguments: <<ts stat d1 d2 dur>>

- ts: timestamp: the time at which the event occurs
- status: the type of the event (note-on and note-off events only (ie 90 or 80)) *hexadecimal*
- data1: pitch: the midi note number *hexadecimal*
- data2: dynamic: the midi velocity number *hexadecimal*
- dur: the duration of the note in timestampsmidi

# pco - pitch-class objects

## DESCRIPTION

This page lists the names of the pitch-class object types recognized by
pct.  These names are used as strings by end-users, and as enumerated
types in the sources.

~~~~
----- -----------------------
cset  cardinality set
pcset pitch-class set
pcseg pitch-class segment
sc    set-class
iset  interval set
iseg  interval segment
icset interval-class set
icseg interval-class segment
icv   interval-class vector
pcv   pitch-class vector
----- -----------------------
~~~~
