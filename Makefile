prefix=$(HOME)/opt
pkgdatadir=$(prefix)/share/pct

install:
	(cd src; make install)
	mkdir -p $(pkgdatadir)
	cp share/* $(pkgdatadir)

clean:
	(cd src; make clean)

GL_GIT=git@gitlab.com:rd--/pct.git
GL_HTTP=https://gitlab.com/rd--/pct.git

push-gl:
	git push $(GL_GIT)

pull-gl:
	git pull $(GL_HTTP)

push-tags:
	git push $(GL_GIT) --tags

update-rd:
	ssh rd@rohandrape.net "(cd sw/pct ; git pull $(GL_HTTP))"

push-all:
	make push-gl update-rd
