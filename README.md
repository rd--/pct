pct
---

pitch class theory software

pct is a software package for composers and music theorists studying
pitch-class structures.  This is a source-code distribution, there are
no executable files.  To install type:

    $ (cd src; make prefix=~/opt install; make clean)

When installed you can type:

~~~~
$ pct si 024579B
pitch-class-set: {024579B}
set-class: TB  7-35[013568A]
interval-class-vector: [7254361]
tics: [345272543626]
complement: {1368A} (T6  5-35)
multiplication-by-five-transform: {0A81B97} (T7  7-1)
$
~~~~

The manual is
[md/pct.md](?t=pct&e=md/pct.md)
and the reference manual is
[md/ref.md](?t=pct&e=md/ref.md).

tested-with:
[gcc](http://gcc.gnu.org/)-12.2.0
[clang](https://clang.llvm.org/)-13.0.1

© [rohan drape](http://rohandrape.net/),
  1996-2023,
  [gpl](http://gnu.org/copyleft/)

<!--
There are [make](http://www.gnu.org/software/make/) rules to have
[pandoc](http://johnmacfarlane.net/pandoc/) translate the manual into
[texinfo](http://www.gnu.org/software/texinfo/) form, that can then be
translated to a clearer [html](http://www.w3.org/html/) rendering
(with separate pages for each section and a table of contents, etc.).
-->
